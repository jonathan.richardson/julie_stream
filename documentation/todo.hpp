/// @file todo.hpp
/// @date 2012-12-06
/// @author Lee McCuller
/// @copyright @ref LICENSE
///
/// @page arch_todo Architechture improvements
///
/// @todo Configuration
///
/// @todo Logging - 
///     - Use a mask to represent the type of system being logged
///         - DEBUG
///         - MEMORY
///         - CONFIG
///         - DETAIL
///         - ...
///     - Use a level do indicate how relevant the data typically is
///     - Mesh with configuration system to see log levels/masks in subsystems
///
///
/// @todo HWLocalization - 
///     The memory allocator needs to be finished, features that would help are:
///         - warnings when memory is not allocated on a specified NUMA node
///         - Optional alignment to a page size (or cache size?)
///
///
///
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
