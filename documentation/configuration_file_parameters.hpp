//////
/// @file configuration_file_parameters.hpp
/// @date 2012-12-06
/// @author Lee McCuller
/// @copyright @ref LICENSE
///
/// @page config_params Configuration File Parameters
///
/// The configuration file uses the markup of [json](http://www.json.org) to specify its values.
/// This markup is hierarchical: objects may be built out of smaller objects.
///
/// Each configuration value may be: 
///     - a number (integer or float)
///     - a string
///     - a list (comma separated list within "[]")
///     - an object (dictionary within "{}")
///
/// The syntax looks very similar to python objects
///
/// @section config_toplevel Toplevel Configuration Parameters
///
///   - "accumulation_time_s" : 0.020,
///     - Amount of time for each accumulation
///   - "atomic_fetch_E" : 19.0,
///     - not used
///   - "csd_extra_reserve" : 10.0,
///     - number of extra CSDCarriers to fill the queue with
///   - "csd_thread_frac" : 0.75,
///     - Fraction total threads to use within the CSD workers
///   - "csd_thread_min" : 1.0,
///     - minimum number of threads to use for csd workers
///   - "csd_thread_reserve" : 3.0,
///     - minimum number of csd worker threads to leave open for other processes (currently 3 are 
///     used by the fetch manager
///   - "frame_overlap_frac" : 0.50,
///     - window overlap fraction (best at 50% for hanning window)
///   - "frame_width_E" : 16.0,
///     - frame with is 2^frame_width_E
///   - "sample_freq" : 100000000.0,
///     - sample_freq not currently used for much, should affect the 5122's but doesn't
///   - "timeframe_extra_reserve" : 10.0,
///     - number of extra timeframe buffers in the time flow
///   - "use_5122" : true,
///     - choice between the 5122 and a virtual source
///   - "wisdom_fname" : ""
///     - filename to load for fftw wisdom
///
/// @section config_5122 NiScope 5122 configuration options
///
/// @todo Configurations not currently being viewed by the 5122 initialization. Specifically,
///     NiScope5122 should take a shared_ptr<IConfigTree> as a constructor parameter.
///
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
