///  @file documentation_mainpage.hpp
///
///  @date 2012-12-06
///
///  @copyright @ref LICENSE
///
///  @mainpage
///
///  Fast DAQ system for the Fermilab Holometer, designed to read 100MS/s from 4 or more channels and
///  correlate them on a 32-core NUMA architecture.
///
///  [References for CSD accumulation](GH_FFT.pdf)
///
///  @section howto_run How to Run the program
///  
///  First, try running
///      
///      ./julie_stream --help
///
///  to see a list of runtime options. Then, adjust the @ref config_params
///  
///
///  @section howto_build How to Build on Windows
///  
///  Windows needs the following packages:
///
///  - Boost
///  - fftw
///  - hwloc
///  - NiScope (optional, but sort of the point)
///  - MinGW/GCC >=4.7
///  - MSYS for MinGW/GCC
///  - CMake (can be installed within MSYS)
///
///  On Julie, most of these packages are either in typical system locations (Boost, NiScope) 
///  or (fftw, hwloc) are withing the folder  
///
///      C:\mingw_4.7.1\msys\1.0\home\mcculler\
///
///  And will be detected automatically by the CMake build system (using defaults that are
///  specified in the project's cmake files).
///
///  @subsection howto_get How to get a copy
///
///  As the software is constantly improving, the best method for finding the newest
///  version is to clone it from git by running
///
///      git clone USERNAME@holometer.uchicago.edu:/Users/mccullerlp/Public/git/julie_stream
///
///  to do this, you will need an account on holometer.uchicago.edu with SSH access.
///
///  @subsection howto_compile How to compile
///
///  To compile the program, get a copy of it (from GIT, ideally) and run
///
///      ./setup_msys_build.sh
///
///  from within the MSYS shell that is linked to GCC 4.7
///
///  If you are building from Linux or Mac, use
///
///      mkdir build
///      cd build
///      cmake ..
///      make
///
///  It will start to build immediately, but from now on, you can run **make** from within
///  the new "build" directory.
///  @sa @ref about_cmake
///
///  @sa @ref arch_todo
///

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
