/// @file about_cmake.hpp
/// @date 2012-12-06
/// @author Lee McCuller
/// @copyright @ref LICENSE
///
/// @page about_cmake About CMake
///
/// CMake is a modern buildsystem which detects packages and requirements from
/// filesystem and generates for you a set of makefiles to build your project
/// using the autodetected dependencies. If you have ever build software using
/// the command "./configure" then CMake works similarly to that (though is much easier to 
/// create/modify than those configure scripts from GNU autotools).
///
/// The build system is set up in each folder through the CMakeLists.txt files. These
/// indicate things to build ("targets"), which are usually either executables or libraries.
///
/// CMake is designed to allow "out of source" builds, where the folder with all of the
/// compiler intermediate files (object files, libtool archives, etc...) are contained
/// within the build directory. This allows easier separation for version control, and it allows 
/// multiple builds to work at once (one folder for a debug mode, one for release).
///
/// [CMake wiki](http://www.cmake.org/Wiki/CMake)
///
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
