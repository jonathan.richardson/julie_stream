/// @file license.hpp
/// @date 2012-12-06
/// @author Lee McCuller
///
/// @page LICENSE license
///
/// TBD - many of these components will be open-sourced with an OSI-approved license or (since it 
/// may be considered a government creation) the Creative Commons Zero (CC0) Which puts the works
/// in the Public Domain, with a clause to allow an acceptable fallback for jurisdictions where 
/// public domain has restrictions.
///
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
