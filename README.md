# Julie Stream
This is the C++ data acquisition code used by the Fermilab Holometer experiment from 2010 to 2018.

It computes the time-averaged cross-spectral density (CSD) matrix of 2, 4, 6, or 8 time series in real time. An accompanying Python client GUI controls the data acquisition and provides a live display of the accumulating spectral averages.

## Dependencies
TBD

## Usage
TBD
