////
///

#include "jstream_topology.hpp"

namespace JStream {
namespace argparse = boost::program_options;
namespace FS = boost::filesystem;


/////
///  Returns the nearest power of two to the supplied value 
///  (utility function)
template <typename TInt>
unsigned char int_log2(TInt x)
{
    unsigned char ans = 0;
    while(true){
		x = x / 2;
		if(x == 0){
			break;
		}
		ans++;
	};
    return ans;
}

/////
///  Topology manager constructor
JStreamTopologyController::
	JStreamTopologyController(
        int argc, 
        const char* argv[],
        LoggerAccess logger,
        std::ostream &local_stream
    ) :
        m_argc(argc),
        m_argv(argv),
        m_logger(logger),
        m_local_stream(local_stream),
        m_msg_stream(std::cout),
        m_err_stream(std::cerr)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);

    /// Apply the advanced JSON settings for this machine
    auto adv_config = this->advanced_config_parse();    
    if(m_enable_server_rt){
        /// Create the realtime network module
        this->realtime_server_create(); 
    }    

#if JSTREAM_HAS_HDF5    
    /// Create the database manager
    m_db_manager = std::make_shared<DatabaseManager>(
        m_database_path,
        adv_config,
        m_logger.child("database_manager")
    );
    if(m_enable_server_db){
        /// Create the database server
        this->database_server_create();      
    }
#endif //~JSTREAM_HAS_HDF5
}

/////
///  Sets up the CSD server
void JStreamTopologyController::
	realtime_server_create()
{
    m_server_rt = std::make_shared<RealtimeMultiServer>(m_logger.child("server"));
    
    /// Add the addresses/ports on which the servers will listen for clients
    std::string address = "tcp://*:" + std::to_string(m_port_rt);
    m_server_rt->accept_address_add(address);
    
    /// Set the server receive callbacks
    m_server_rt->callback_ifo_lock_state_set(
        [this](bool state){
            return this->ifo_lock_state_set(state);
        }        
    );    
    m_server_rt->callback_run_state_set(
        [this](bool state){
            return this->run_state_set(state, false);
        }        
    );
    m_server_rt->callback_run_state_query_set(
        [this](RealtimeServer::RunStateHandler transmitter){
            return transmitter(m_run_state);
        }
    );
    m_server_rt->callback_run_config_set(
        [this](const std::string &config){
            return this->run_config_set(config);
        }
    );

    /// Set the stream router callbacks for intercepted messages 
    /// (Transmits messages to remote clients)
    m_msg_stream.msg_callback_set(
        [this](const std::string &msg){
            return m_server_rt->msg_broadcaster().broadcast(msg);
        }
    );
    m_err_stream.msg_callback_set(
        [this](const std::string &msg){
            return m_server_rt->msg_broadcaster().broadcast(msg);
        }
    );      
}

#if JSTREAM_HAS_HDF5
/////
///  Creates the database multi-server
void JStreamTopologyController::
	database_server_create()
{
    m_server_db = std::make_shared<DatabaseMultiServer>(m_logger.child("server"));

    /// Add the addresses/ports on which the servers will listen for clients
    std::string address = "tcp://*:" + std::to_string(m_port_db);
    m_server_db->accept_address_add(address);
      
    /// Set the server receive callbacks
    m_server_db->callback_search_set(
        /// Forwards query to database manager with server-specific transmitters
        [this](
            const std::string &query,
            DatabaseManager::SearchHitTransmitter transmitter,
            DatabaseManager::TransmissionTerminator terminator
        ){
            return m_db_manager->search(
                query, 
                transmitter, 
                terminator
            );
        }
    );  
    m_server_db->callback_sequential_transfer_set(
        /// Forwards request to database manager with server-specific transmitters
        [this](
            const std::string &query,
            DatabaseManager::MetadataTransmitter meta_transmitter,
            DatabaseManager::CSDFloatTransmitter csd_transmitter,
            DatabaseManager::TransmissionTerminator terminator
        ){
            return m_db_manager->sequential_transfer(
                query, 
                meta_transmitter,
                csd_transmitter,
                terminator
            );
        }
    ); 
    m_server_db->callback_preaccum_transfer_set(
        /// Forwards request to database manager with server-specific transmitters
        [this](
            const std::string &query,
            DatabaseManager::MetadataTransmitter meta_transmitter,
            DatabaseManager::CSDDoubleTransmitter csd_transmitter,
            DatabaseManager::TransmissionTerminator terminator
        ){
            return m_db_manager->preaccum_transfer(
                query, 
                meta_transmitter,
                csd_transmitter,
                terminator
            );
        }
    );     
}
#endif //~JSTREAM_HAS_HDF5  

/////
///  Receives the JSON run configuration from a remote client
void JStreamTopologyController::
    run_config_set(const std::string& config_str)
{
    Guard guard(m_protect);

    if(not m_run_state){
        m_run_config_str = config_str;
        m_logger.detail("Received run configuration:\n", config_str);
    }
}

/////
///  Parse and set the advanced run configuration settings
ConfigTreeJsonRoot JStreamTopologyController::
    advanced_config_parse()
{
    ConfigTreeJsonRoot  adv_conf_root("jstream_adv", m_logger);
    
    /// Get the number of cores
    m_hw_concurrency = std::thread::hardware_concurrency();
    
    /// Get the command-line options 
    try{
        /// The available options
        argparse::options_description desc("Supported options");
        desc.add_options()
            ("help,h", 
                  "produce help message"
            )
            ("verbose,v", argparse::value< int >(),
                  "enable verbosity"      "\n"
                  " -10: fatal"           "\n"
                  " -5: error"            "\n"
                  " -2: warnings"         "\n"
                  "  0: digest"           "\n"
                  "  2: configuration"    "\n"
                  "  5: runtime"          "\n"
                  "  10: programming"     "\n"
                  "  10: debugging"
            )
            ("adv_conf,a", argparse::value< string >()->default_value("<MISSING>"), 
                "Configuration file: Advanced settings file (overrides system default)"
            )
            ("config,r", argparse::value< string >()->default_value("<MISSING>"), 
                "Configuration file: Run settings file (overrides system default)"
            )
        ;
        /// Parse the command line input
        argparse::variables_map option_variables;
        argparse::store(argparse::command_line_parser(m_argc, m_argv).options(desc).run(), option_variables);
        argparse::notify(option_variables);     
        if(option_variables.count("help")){
            m_local_stream << desc;
            exit(0);
        }

        /// Set the JSON run config filename, if one was provided (overrides the default)
        string run_fname = option_variables["config"].as<std::string>();
        if(run_fname != "<MISSING>"){
            m_run_config_fname = run_fname;
        }
        
        /// Read in the advanced JSON settings file
        std::string conf_basename = option_variables["adv_conf"].as<std::string>();
        if(conf_basename == "<MISSING>"){
            conf_basename.clear();
        }
        conf_basename = advanced_config_find(conf_basename);
        if(not adv_conf_root.parse_file(conf_basename)){
            exit(0);
        }
        
        /// Set the verbosity level for print-outs
        uLogger::TLoggingLevels verbosity_level = 5;
        if(option_variables.count("verbose")){
            /// The command line value overrides the JSON value, if one was supplied
            verbosity_level = option_variables["verbose"].as<TLoggingLevels>();          
        }else{
            /// Set the JSON value
            uLogger::logrouter_root()->settings(-20); //suppresses unneccessary SDT message
            auto config_verbosity = adv_conf_root.access()["verbosity"];
            config_verbosity.get(verbosity_level);
        }
        uLogger::logrouter_root()->settings(verbosity_level);
        
        /// Report the configuration options that were loaded
        m_logger.config("Loading advanced config:\n\t", conf_basename);    
        m_logger.detail("Verbosity level: ", verbosity_level);
    }
    catch(std::exception& e){
        m_logger.fatal(e.what());
        exit(0);
    }
    auto adv_conf = adv_conf_root.access();

    /// Get the hardware localization assignments
    m_loc_manager = localization_manager();
    m_loc_manager->configurations_import(adv_conf["thread_localizations"].ptr(), m_logger);

    {   /// Get the advanced run configuration settings
    
        /// The number of CSD worker threads to use
        adv_conf["csd_worker_threads"].get(m_fft_thread_num, 1, m_hw_concurrency);
        m_logger.digest(m_hw_concurrency, " cores available");
        m_logger.config("Using ", m_fft_thread_num, " CSD compute threads");

        /// The number of extra time frames in the time flow queue (above the number of worker threads)
        adv_conf["timeframe_extra_reserve"].get(m_timeframe_extra, 0, 1024);
        
        /// The number of extra CSD frames in the CSD flow queue (above the number of worker threads)
        adv_conf["csd_extra_reserve"].get(m_fft_extra, 0, 1024);
        
        /// Load the FFTW wisdom
        /// TODO: A nice feature would be to generate the wisdom directly from this script
        adv_conf["wisdom_fname"].get(m_wisdom_fname); 
    
        /// The number of samples the 5122 reads at once from each channel into the fetch buffer
		int fetch_size_E = -1;
		adv_conf["5122_fetch_size_E"].get(fetch_size_E, 10, 21);
		m_5122_fetch_size = (size_t(1) << int(fetch_size_E));

        /// The time delay after which the 5122 start trigger is sent (sec)
        adv_conf["trigger_delay_s"].get(m_trigger_delay_s, 1, 60);      
    }
    
    {   /// Get the realtime server settings
        auto config = adv_conf["servers"]["realtime"];

        /// Enable/disable switch
        config["enabled"].get(m_enable_server_rt);
        if(m_enable_server_rt){
            /// Port on which to listen for realtime clients
            config["port"].get(m_port_rt, 0, 60000);
            m_logger.config("Realtime TCP service:\t", m_port_rt);
        }else{
            m_logger.config("Realtime server manually disabled");
        }
    }
    
    {   /// Get the database server settings
        auto config = adv_conf["servers"]["database"];

        /// Enable/disable switch
        config["enabled"].get(m_enable_server_db);
        if(m_enable_server_db){
            #if JSTREAM_HAS_HDF5            
            /// Port on which to listen for realtime clients
            config["port"].get(m_port_db, 0, 60000);
            m_logger.config("Database TCP service:\t", m_port_db);
            #endif //~JSTREAM_HAS_HDF5  
        }else{
            m_logger.config("Database server manually disabled");
        }
    }
	
    {   /// Get the saturation detector settings (checks for ADC clipping)
        auto config = adv_conf["saturation_detector"];

        /// Enable/disable switch
        config["enabled"].get(m_enable_saturation_detector);
        if(m_enable_saturation_detector){
            /// Fractional bit saturation tolerance level
            config["level"].get(m_saturation_threshold, 0, 1);
            m_logger.config(
                "ADC saturation detector enabled\n\t",
                "Triggering level:\t", m_saturation_threshold * 100, "%"
            );
        }else{
            m_logger.config("ADC saturation detector manually disabled");
        }
    }
    
    {   /// Get the transient detector settings (checks for correlated transients)
        auto config = adv_conf["transient_detector"];

        /// Enable/disable switch
        config["enabled"].get(m_enable_transient_detector);
        if(m_enable_transient_detector){
            /// Relative power excursion trigger threshold
            config["level"].get(m_transient_threshold, 1, 1000);

            /// Number of initial CSD frames to average as the baseline reference set
            config["baseline_size"].get(m_baseline_size, 1, 1000);
            
            /// Auxilliary reference channel(s)
            auto aux_chs = config["auxilliary_chs"];
            if(not aux_chs.is_array()){
                throw(std::logic_error("Auxilliary channel list must be a JSON array"));
            }
            for(auto idx : icount(aux_chs.array_size())){
                unsigned channel;
                aux_chs[idx].get(channel);
                m_aux_chs.push_back(channel);
            }
            
            /// Reporting
            if(m_aux_chs.size() > 0){
                /// Report the detector configuration
                std::stringstream ss;
                for(auto idx : icount(m_aux_chs.size())){
                    ss << m_aux_chs[idx];
                    if(idx != (m_aux_chs.size() - 1)){
                        ss << ", ";
                    }
                }
                m_logger.config(
                    "Real-time transient detector enabled\n\t",
                    "Triggering threshold:\t", m_transient_threshold * 100, "%\n\t",
                    "Baseline size:\t\t", m_baseline_size, "\n\t",
                    "External channels:\t", ss.str()
                );
            }else{
                /// Can't transient detect without at least one channel for external comparison
                m_logger.warning("No auxilliary reference channels supplied. Transient detector will be disabled");
                m_enable_transient_detector = false;
            }
        }else{
            m_logger.config("Transient detector manually disabled");
        }
    }
    
    #if JSTREAM_HAS_HDF5
    /// Get the HDF5 database filepath for archiving data
    adv_conf["database_path"].get(m_database_path);
    m_logger.config("Archiving to HDF5 database:\n\t", m_database_path);
    #else
    m_logger.config("HDF5 database services automatically disabled.");
    #endif //~JSTREAM_HAS_HDF5    
    
    return adv_conf_root;
}

/////
///  Parse and set the specified run configuration
ConfigTreeJsonRoot JStreamTopologyController::
    run_config_parse()
{
    ConfigTreeJsonRoot config_root("jstream_run", m_logger);    

    /// Load the JSON run configuration
    auto config = config_root.access();    
    {
        bool valid_conf_str = false;
        if(not m_run_config_str.empty()){
            /// Attempt to parse the remotely-supplied JSON string
            auto parsed = config_root.parse_string(m_run_config_str);
            if(parsed){
                m_logger[0].config("Remote configuration settings parsed successfully");
                valid_conf_str = true;
            }else{
                m_logger[0].error("Remote configuration settings parse failed!");
            }
        }
        if(not valid_conf_str){
            /// If no string has been provided or it is invalid, try loading the configuration file
            auto conf_basename = run_config_find(m_run_config_fname);
            auto parsed = config_root.parse_file(conf_basename);
            if(not parsed){
                exit(1);
            }
            m_logger.config("Loading run configuration:\n\t", conf_basename);
        }
    }

    {   /// Enable/disable IFO lock-state updates from control system
        config["monitor_ifo_state"].get(m_monitor_ifo_state);  
        if(m_monitor_ifo_state){
            m_logger.config("Listening for auto-locker IFO state updates");
        }else{
            m_logger.config("Ignoring auto-locker IFO state");
        }
    }    
    
    {   /// Set the total integration time (s)
        config["integration_time"].get(m_time_to_run_s);  
        if(m_time_to_run_s == -1){
            m_logger.config("Integration time:\tFOREVER");
        }else if(m_time_to_run_s >= 0){
            m_logger.config("Integration time:\t", m_time_to_run_s, " s"); 
        }else{
            m_logger.fatal(
                "Are you kidding me? You can't run for negative time!\n",
                "Use -1 to run indefinitely"
            );
            exit(1);
        }
    }
    
    {   /// Set the error level at which to veto CSD frames
        config["csd_veto_thres"].get(m_veto_thres, 1, 100);    
        m_logger.config("CSD veto threshold:\t", m_veto_thres);
    }

    {   /// Set the DFT size (rounded to the nearest power of two)
        unsigned fft_size_E = int_log2(m_fft_size);
        config["fft_size_E"].get(fft_size_E,  4, 28);
        m_fft_size = (unsigned(1) << int(fft_size_E));
    }

    {   /// Set the data overlap fraction for each time frame
        config["frame_overlap_frac"].get(m_overlap_frac,  0, 1);
    }

    /// Set up the data source configuration
    bool use_real_5122 = false;
    {
        auto conf = config["data_sources"];
        
        /// Set the real/virtual source switch
        conf["use_real_5122"].get(use_real_5122);

        /// Set the number of data sources
        auto devices = conf["devices"];
        if(not devices.is_array()){
            throw std::logic_error("Device list must be a JSON array");
        }
        m_num_sources = devices.array_size();
        assert(m_num_sources > 0);
        
        /// Set the total number of channels
        m_num_chs = 2 * m_num_sources;

        /// Set the channel source descriptions        
        m_source_labels.resize(m_num_chs);
        for(auto src_idx : icount(m_num_sources)){
            for(auto dev_idx : icount(2)){
                auto chn_idx = 2 * src_idx + dev_idx;
                std::string source;
                devices[src_idx]["chn_sources"][dev_idx].get(source);
                m_source_labels[chn_idx] = source;
            }
        }
    }

    {   /// Create the data sources
        long unsigned start_time_s = 0;
        if(use_real_5122){
            #if JSTREAM_HAS_NISCOPE
            /// Create the real data sources in the JSON device list
            for(auto source_idx : icount(m_num_sources)){
                /// Create a real device
                auto scope = std::make_shared<NIScope5122>(
                    config,
                    source_idx,
                    m_5122_fetch_size,
                    m_trigger_delay_s,
                    start_time_s, //value will be set for all devices by the first device
                    m_logger.child("5122_reader")
                );

                /// Get the locality assignments
                std::stringstream SS;
                SS << "data_source_buffer_" << source_idx;
                auto memory_locality = m_loc_manager->memory_locality_get(SS.str());
                auto thread_locality = m_loc_manager->thread_locality_get(SS.str());                  
                
                /// Wrap in a threaded adaptor              
                auto reader = std::make_shared<ThreadedReader>(
                    scope,
                    memory_locality,
                    thread_locality,
                    m_logger.child("threaded_adaptor")
                );
                m_data_sources.push_back(reader);
            }

            /// Check the hardware settings for consistency
            /// (Generates a warning for any inconsistent JSON settings between the data sources)
            auto conf = config["data_sources"]["devices"];
            config_error_checker(conf);
            #else //JSTREAM_HAS_NISCOPE
            
            /// Create virtual sources (without a threaded adaptor)
            m_logger.error("NiScope/Sync not compiled in, using ", m_num_sources, " virtual sources");
            for(auto source_idx : icount(m_num_sources)){
                auto scope = std::make_shared<Virtual5122>(start_time_s);
                m_data_sources.push_back(scope);
            }
            #endif //JSTREAM_HAS_NISCOPE
        }else{
            /// Create virtual sources (without a threaded adaptor)
            m_logger.config("NIScope/Sync manually disabled, using ", m_num_sources, " virtual sources");
            for(auto source_idx : icount(m_num_sources)){
                auto scope = std::make_shared<Virtual5122>(start_time_s);
                m_data_sources.push_back(scope);
            }
        }
        
        /// Set the *actual* 5122 sampling frequency
        /// (may not equal the requested JSON value)  
        m_sample_freq = m_data_sources[0]->sample_freq();      
        m_start_time_s = start_time_s;
    }

    /// Set the frame accumulation properties  
    {   
        /// The "requested" number of samples per frame
        config["accumulation_time_s"].get(m_accumulation_time_s, 0., 20.);
        m_time_frame_len = int(m_sample_freq * m_accumulation_time_s);

        /// The super-accumulation of CSD frames (number of CSD frames into one superframe)
        unsigned max_frame_samples = 8e6 / (1 << (m_num_sources - 1));
        m_super_accumulation = unsigned(std::ceil(double(m_time_frame_len) / max_frame_samples));
        
        /// If super-accumulation required, must coerce the per-timeframe accumulation down
        if(m_super_accumulation > 1){
            m_time_frame_len = max_frame_samples;
            m_accumulation_time_s = m_super_accumulation * (m_time_frame_len / m_sample_freq);
            m_logger.digest(
                "Super-accumulation automatically enabled\n\t",
                "Accumulation time: ", m_accumulation_time_s, " s"
            );
        }else{ 
            m_logger.digest("Accumulation time: ", m_accumulation_time_s, " s");
        }
    }
    
    /// Create the window function
    /// TODO: Make apodization window configurable (need an Apodization factory)
    m_apodization = std::make_shared<Apodization<float> >(
        m_sample_freq, 
        apodization_hanning<float>(m_fft_size)
    );
                    
    {   /// Report the frequency bin width
        /// TODO: Fix the accuracy of this as the window function affects it
        auto freq_res = (m_sample_freq / 1e3) / m_fft_size;
        m_logger.digest("Frequency resolution: ", freq_res, " kHz");
    }
    
    {   /// Set the HDF5 writer settings
        #if JSTREAM_HAS_HDF5    
        
        /// The HDF5 writer enable/disable switch
        auto conf = config["hdf5_writer"];        
        conf["use_hdf5_writer"].get(m_enable_hdf5_writer); 
        if(m_enable_hdf5_writer){
            /// IFO operator name
            conf["operator"].get(m_operator_name);
            m_logger.detail("IFO operator name:  ", m_operator_name);

            /// Run comments
            conf["comments"].get(m_run_comments);
            m_logger.detail("Run comments:  ", m_run_comments); 
            
            /// File write time (s)
            conf["file_write_time_s"].get(m_file_write_time_s);
            m_logger.detail("CSD file write time:  ", m_file_write_time_s, " s");            

            /// Get the time series writer settings
            m_ts_writer_config.parse_config_basic(config["hdf5_ts_writer"]);
            if(m_ts_writer_config.m_use_hdf5_ts_writer){
                m_logger.config("Time series writer enabled");
            }
        }else{
            m_logger.config("HDF5 writers manually disabled");
        }
        #else //JSTREAM_HAS_HDF5
        m_logger.config("HDF5 writers automatically disabled");
        #endif //JSTREAM_HAS_HDF5      
    }
    return config_root;
}

/////
///  Set up the data flow topology
void JStreamTopologyController::
    topology_create(ConfigTreeJsonRoot run_config)
{
    /// Create the data flow nodes
                
    /// Fetch manager
    m_fetch_manager = std::make_shared<FetchManager>(
        m_data_sources,
        m_monitor_ifo_state,
        m_veto_thres,
        m_logger.child("fetch_manager")
    );
    m_fetch_manager->hw_exception_callback_set(
        [this](){
            return this->hw_exception_handler();
        }
    );          

    /// Saturation scanner (checks for ADC clipping)
    if(m_enable_saturation_detector){
        m_saturation_detector = std::make_shared<SaturationDetector>(
            m_saturation_threshold,
            m_num_chs,
            m_veto_thres,
            m_logger.child("saturation_detector")
        );
    }

    /// CSD worker pools 
    /// (needs one in time and one in CSD, because it computes through both)
    m_csd_csd_worker_pool = std::make_shared< TCSDTaskVentilator >();
    m_time_csd_worker_pool = std::make_shared< TTimeTaskVentilator >();
    
    /// Super-accumulator
    /// (Creates a second layer of CSD averaging if m_super_accumulation > 1,
    ///  else just time-orders the original CSD frames)
    m_csd_accumulator = std::make_shared<CSDAccumulator>(
        m_super_accumulation,
        m_logger.child("csd_accumulator")
    );
    
    /// Transient detector
    if(m_enable_transient_detector){
        /// Determine which auxiliary channels are actually 
        /// present in the current run configuration
        std::vector<unsigned> aux_chs_present;
        for(auto aux_chn : m_aux_chs){
            for(auto chn_idx : icount(m_num_chs)){
                if(chn_idx == aux_chn){
                    aux_chs_present.push_back(aux_chn);
                    break;
                }
            }
        }
        if(aux_chs_present.size() > 0){
            /// Create the transient detector
            m_transient_detector = std::make_shared<TransientDetector>(
                m_transient_threshold,
                m_baseline_size,
                aux_chs_present,
                m_veto_thres,
                m_logger.child("transient_detector")
            );
        }else{
            m_logger.warning("Transient detector automatically disabled. No auxiliary chs present.");
        }
    }

    #if JSTREAM_HAS_HDF5        
    if(m_enable_hdf5_writer){
        /// Get the data source settings
        std::vector<IDataSource::DataSourceSettings> source_metadata;
        for(auto& source : m_data_sources){
            source_metadata.push_back(source->all_settings());
        }

        /// Get the database run ID
        auto db_run = m_db_manager->register_run(
                m_operator_name,
                m_run_comments,
                m_source_labels,
                run_config
        );
        
        /// Create the HDF5 CSD writer
        m_csd_writer = std::make_shared<CSDWriter>(
            source_metadata,
            db_run,
            m_file_write_time_s,
            m_accumulation_time_s,
            m_logger.child("csd_writer")
        );
        if(m_ts_writer_config.m_use_hdf5_ts_writer){
            /// Create the HDF5 time series writer
            auto hdf5_ts_writer = std::make_shared<TimeseriesWriteNode::TWriter>(
                source_metadata,
                db_run->fpath_get(),
                m_logger.child("HDF5_TS")
            );            
            m_ts_writer = std::make_shared<TimeseriesWriteNode>(
                m_ts_writer_config,
                hdf5_ts_writer,
                m_logger.child("ts_writer")
            );
        }
    }else{
        if(m_ts_writer_config.m_use_hdf5_ts_writer){
            m_logger.warning("Time series writer enabled but can't activate without CSD writer enabled");
        }
    }
    #endif //~JSTREAM_HAS_HDF5    

    /// TCP export module (maintains the global CSD averages)
    m_csd_exporter = std::make_shared<CSDExporter>(
        m_num_chs,
        m_fft_size / 2,
        m_sample_freq,
        m_overlap_frac,
        m_veto_thres,
        m_logger.child("csd_exporter")
    );
    if(m_enable_server_rt){
    	/// Set the broadcast callback a new CSD frame accumulation
        m_csd_exporter->accumulation_callback_set(
            [this](const CSDCarrierDouble &csd){
                return m_server_rt->csd_broadcaster().broadcast(csd);
            }
        );
    }
        
    /// Populate the CSD worker pool with workers
    assert(m_fft_thread_num > 0);
    for(auto idx : icount(m_fft_thread_num)){
		/// Create a worker
		std::stringstream SS;
		SS << "csd_worker_" << idx;
        auto compute_worker = std::make_shared<CSDComputeWorker>(
            idx,
            m_num_chs,
            m_sample_freq,
            m_overlap_frac,
            m_time_frame_len,
            m_apodization,
            m_logger.child(SS.str())
        );

		/// Add it to the worker pools
        m_csd_csd_worker_pool->add_peer_node(compute_worker);
        m_time_csd_worker_pool->add_peer_node(compute_worker);
    }

    /////
    ///  SET UP THE DATA FLOWS
    
    {   /// Time flow
        std::vector<std::shared_ptr<TTimeFlowNode> > nodes;
        nodes.push_back(m_fetch_manager);
        if(m_enable_saturation_detector){
            nodes.push_back(m_saturation_detector);
        }
        nodes.push_back(m_time_csd_worker_pool);
        #if JSTREAM_HAS_HDF5     
        if(m_ts_writer){
            nodes.push_back(m_ts_writer);
        }
        #endif     
        m_time_flow = std::make_shared<TTimeFlow>(nodes, true /*circular*/);
    }

    {   /// CSD subflow
        std::vector<std::shared_ptr<TCSDFlowNode> > nodes;
        nodes.push_back(m_csd_csd_worker_pool);
        nodes.push_back(m_csd_accumulator);
        m_csd_flow = std::make_shared<TCSDFlow>(nodes, true /*circular*/);
    }
    
    {   /// CSD superflow
        std::vector<std::shared_ptr<TCSDFlowNode> > nodes;
        if(m_transient_detector){
            nodes.push_back(m_transient_detector);
        }
        #if JSTREAM_HAS_HDF5     
        if(m_csd_writer){
            nodes.push_back(m_csd_writer);
        }
        #endif     
        nodes.push_back(m_csd_exporter);
        m_csd_accumulator->internal_flow_setup(nodes);
    }
}

/////
///  Reset the data flow topology to an uninitialized state
void JStreamTopologyController::
    topology_destroy(bool config_preserve)
{
    /// Reset the run configuration parameters
    /// (Advanced settings are machine-specific and should not be changed between runs)
    m_time_to_run_s             = -1;               //Integration time (s)
    m_sample_freq               = -1;               //Sample frequency (Hz)
    m_fft_size                  = unsigned(1)<<16;  //FFT size
	m_overlap_frac              = .5;               //Welch overlap fraction
    m_time_frame_len            = -1;               //Number of samples per timeframe (per channel)
    m_super_accumulation        = -1;               //Number of CSD frames per superframe
    m_enable_hdf5_writer        = false;            //HDF5 writer enable/disable switch
    m_veto_thres                = 100;              //Error level at which to veto CSD frames
    m_accumulation_time_s       = 1.0;              //CSD frame accumulation time (s) 
    m_file_write_time_s         = 60;               //CSD file write time (s)    
    m_operator_name.clear();
    m_run_comments.clear();    
    m_source_labels.clear(); 
    
    /// Release the data sources
    for(auto& source : m_data_sources){
        source.reset();
    }
    m_data_sources.erase(m_data_sources.begin(), m_data_sources.begin() + m_data_sources.size());
    
    /// Release the window function
    m_apodization.reset();

    /// Release the data flow nodes
    m_fetch_manager.reset();
    m_saturation_detector.reset();
    m_csd_csd_worker_pool.reset();
    m_time_csd_worker_pool.reset();
    m_csd_accumulator.reset();
    m_transient_detector.reset();
    #if JSTREAM_HAS_HDF5     
    m_csd_writer.reset();
    m_ts_writer.reset();
    #endif 
    m_csd_exporter.reset();

    /// Release the data flows
    m_csd_flow.reset();
    m_time_flow.reset();
    
    /// Reset the real-time server internal CSD queues
    if(m_enable_server_db){
        m_server_rt->csd_broadcaster().reset();
    }
   
    /// Reset the JSON run configuration string
    if(not config_preserve){
        m_run_config_str.clear();
    }
}

/////
///  Set the state of the TCP server(s) (ON or OFF)
void JStreamTopologyController::
    networking_state_set(bool on_off)
{
    Guard guard(m_protect);
    m_server_state = on_off;
    if(m_server_state){
        /// Start accepting connections
        if(m_enable_server_rt){
            m_server_rt->connections_begin();
        }
        if(m_enable_server_db){
            #if JSTREAM_HAS_HDF5            
            m_server_db->connections_begin();  
            #endif  
        }
    }else{
        /// Close connections
        if(m_enable_server_rt){
            m_server_rt->connections_end();
        }
        if(m_enable_server_db){           
            #if JSTREAM_HAS_HDF5            
            m_server_db->connections_end();      
            #endif
        }
    }
}

/////
///  Sets the current IFO state
void JStreamTopologyController::
    ifo_lock_state_set(bool state)
{
    if(m_fetch_manager){
        m_fetch_manager->ifo_lock_state_set(state);
    }
}

/////
///  Start or stop the current data acquisition run
void JStreamTopologyController::
    run_state_set(
        bool on_off,
        bool config_preserve
    )
{
    Guard guard(m_protect);
    
    if(m_run_state == on_off){
        /// System is already in the requested run state
        if(m_is_key_handling and not m_run_state){
            m_local_stream << "\nEnter key option:\n";
        }
        return;
    }
    
    if(m_enable_server_rt){
        /// Notify any connected clients of the run-state change
        m_server_rt->run_state_broadcaster().broadcast(on_off);
    }    
    
    /// Start/stop the run
    if(on_off){
        this->_run_start();
    }else{
        this->_run_stop(config_preserve);
    }
    m_run_state = on_off;  
}
    
/////
///  Start a data acquisition run
void JStreamTopologyController::
    _run_start()
{    
    /// Local terminal message
    if(m_is_key_handling){
        m_local_stream << "\nEnter \"stop\" at any time to abort the run.\n\n";
    }

    /// Load the user-supplied run configuration
    auto run_config = this->run_config_parse();            

    /// Set up the data flow topology
    this->topology_create(run_config);
    if(not this->time_to_run()){
        /// Exit now without starting the acquisition 
        exit(1);
    }

    /// Add frames to the time and CSD flows
    this->csd_flow_prefill();
    this->time_flow_prefill();

    /// Start the time and CSD flow queues
    m_csd_flow->flow_tasks_start();
    m_time_flow->flow_tasks_start();
    
    /// Schedule the run end time
    if(this->time_to_run()!= -1){
        /// Launch a thread to stop the run when time expires (cancellable)
        boost::thread run_timer(&JStreamTopologyController::run_timer, this);             
    }else{
        m_logger.action("Integration will continue indefinitely");
    }
}        
        
/////
///  Start a data acquisition run
void JStreamTopologyController::
    _run_stop(bool config_preserve)
{           
    /// Cancel the run timer thread (if one is running)
    m_signal_abort.notify_all();
    
    /// End the time & CSD flows
    m_time_flow->flow_tasks_end();
    m_csd_flow->flow_tasks_end();

    /// Restore the topology to its uninitialized state
    this->topology_destroy(config_preserve); 
    if(m_is_key_handling and m_system_state){
        m_local_stream << "\n\nEnter key option:\n";
    }
}

/////
///  Inner handler function for hardware exceptions
///  (Restarts the run with the same configuration settings)
void JStreamTopologyController::
    hw_exception_handler()
{
    this->run_state_set(false, true /*preserve current config*/);
    m_logger.action("Auto-restarting the DAQ...");    
    this->run_state_set(true);
}

/////
///  Timer loop for finite-duration runs
void JStreamTopologyController::
    run_timer()
{
    Mutex   local_mutex;
    Lock    lock(local_mutex);
    
    /// Report the absolute DAQ stop time
    auto date = std::chrono::system_clock::to_time_t(this->end_timepoint());
    m_logger.action("Integration will terminate:\t", ctime(&date));    
    
    /// Block until the run is aborted or time expires
    bool run_aborted = m_signal_abort.wait_until(
        lock, 
        this->end_timepoint(), 
        [this](){return not m_run_state;}
    );
    if(not run_aborted){
        /// Terminate the run
        m_logger.action("Run complete.");
        this->run_state_set(false);
    }
}

/////
///  Add CSD frames to the CSD flow and superflow queues
void JStreamTopologyController::
    csd_flow_prefill()
{
	auto locality = m_loc_manager->memory_locality_get("csd_carriers");

    /// Add CSD frames to the CSD flow queue
	/// (array allocation handled within CSDCarrier constructor)
    unsigned tot_csd_frames = m_fft_thread_num + m_fft_extra;
    for(auto t_idx : icount(tot_csd_frames)){
		/// Create a CSDCarrier frame
        auto csd = shared_ptr<TCSDCarrier>(
            new TCSDCarrier(
                m_num_chs, 
                m_fft_size/2,
                m_sample_freq,
                m_overlap_frac,
                locality
            )
        );
        m_csd_flow->start_queue_get()->push(csd);
    }

    /// Add CSD superframes to the CSDAccumulator internal queue
	/// (array allocation handled within CSDCarrier constructor)
    for(auto idx : icount(5)){
		/// Create a CSDCarrier superframe
        auto csd = shared_ptr<TCSDCarrier>(
            new TCSDCarrier(
                m_num_chs,
                m_fft_size/2,
                m_sample_freq, 
                m_overlap_frac,
                locality
            )
        );
        m_csd_accumulator->prefill_csd_add(csd);
    }
}

/////
///  Add time frames to the time flow queue
void JStreamTopologyController::
    time_flow_prefill()
{
    /// (Array allocation handled within T5122Frame constructor)
    unsigned tot_time_frames = m_fft_thread_num + m_timeframe_extra;
    auto prototype = std::make_shared<T5122Frame>(
        m_num_chs,
        m_time_frame_len,
        m_fft_size,
        unsigned(m_fft_size * m_overlap_frac),
        m_fetch_manager->locality_get()
    );
    m_fetch_manager->prefill_frame_setup(
        prototype, 
        tot_time_frames
    );
}

/////
///  Return the integration time
int JStreamTopologyController::
    time_to_run() const
{
    return m_time_to_run_s;
}

/////
///  Return the absolute start trigger time
unsigned JStreamTopologyController::
    trigger_time() const
{
    return m_data_sources[0]->trigger_time();
}

/////
///  Return the stop timepoint
std::chrono::system_clock::time_point JStreamTopologyController::
    end_timepoint() const
{
    auto trigger_time = this->trigger_time();
    auto stop_time = trigger_time + this->time_to_run() + 1;
    return std::chrono::system_clock::time_point(std::chrono::seconds(stop_time));
}

/////
///  Does not return until system-wide shutdown is signaled
void JStreamTopologyController::
    wait_for_shutdown()
{
    Mutex   local_mutex;
    Lock    lock(local_mutex);

    /// Launch a thread to accept/handle keyboard input from the local terminal
    boost::thread key_handler(
        &JStreamTopologyController::key_handler, 
        this
    );                       
    
    /// Block until system shutdown flag is set
    m_signal_shutdown.wait(
        lock, 
        [this](){return not m_system_state;}
    );
}

/////
///  Accepts/handles keyboard input from the local terminal
void JStreamTopologyController::
    key_handler()
{
    std::string command;
    
    /// Wait for a key command to arrive
    if(not m_is_key_handling){
        m_local_stream << "\n\nEnter key option:\n";
        m_is_key_handling = true;
    }
    std::cin >> command;

    /// Handle the command
    if(command == "run"){
        /// Start a new run
        this->run_state_set(true);
    }else if(command == "stop"){
        /// Stop the current run
        this->run_state_set(false);
    }else if(command == "shutdown"){
        /// Signal system-wide shutdown and quit
        m_local_stream << "\nDAQ system shutting down...\n\n";
        m_system_state = false;
        if(m_run_state){
            this->run_state_set(false);
        }
        m_signal_shutdown.notify_all();
        return;
    }else{
        m_local_stream << "\nUnrecognized key option: " << command << "\n" <<
            "\tValid options:\n" <<
            "\t run\n" <<
            "\t stop\n" <<
            "\t shutdown\n";
    }
    
    /// Recurse
    key_handler();
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
