////
///
///

#ifndef H_JSTREAM_TYPES_H
#define H_JSTREAM_TYPES_H

#include "analysis/time_framing.hpp"
#include "analysis/csd_carrier_float.hpp"

#include "data_flow/data_flow.hpp"
#include "data_flow/data_flow_ventilator.hpp"

namespace JStream {

typedef SlidingFrameTimestream< int16_t >     T5122Frame;
typedef std::shared_ptr< T5122Frame >         T5122FramePtr;

typedef CSDCarrierFloat                       TCSDCarrier;
typedef std::shared_ptr< TCSDCarrier >        TCSDCarrierPtr;

typedef IDataFlowNode< TCSDCarrierPtr >       TCSDFlowNode;
typedef IDataFlowNode< T5122FramePtr >        TTimeFlowNode;

typedef DataFlowVentilator< TCSDCarrierPtr >  TCSDTaskVentilator;
typedef DataFlowVentilator< T5122FramePtr >   TTimeTaskVentilator;

typedef DataFlow< TCSDCarrierPtr >            TCSDFlow;
typedef DataFlow< T5122FramePtr  >            TTimeFlow;

}; //~namespace JStream

#endif //H_JSTREAM_TYPES_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

