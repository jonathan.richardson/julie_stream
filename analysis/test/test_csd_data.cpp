////
///
///
///

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <iostream>

#include <thread>
#include <chrono>
#include <functional>

#include "analysis/csd_carrier_double.hpp"


using namespace JStream;
// Test the methods to help interface to numpy arrays in python
BOOST_AUTO_TEST_CASE( second_test )
{
    size_t n_channels = 4;
    unsigned length = 65536;

    size_t csdSize = length*sizeof(CSDCarrierDouble::TNum);
    CSDCarrierDouble::TNum* inCSDReal = (CSDCarrierDouble::TNum*) malloc(csdSize);
    CSDCarrierDouble::TNum* inCSDImag = (CSDCarrierDouble::TNum*) malloc(csdSize);
    CSDCarrierDouble::TNum* outCSDReal = (CSDCarrierDouble::TNum*) malloc(csdSize);
    CSDCarrierDouble::TNum* outCSDImag = (CSDCarrierDouble::TNum*) malloc(csdSize);

    size_t psdSize = length*sizeof(CSDCarrierDouble::TNum);
    CSDCarrierDouble::TNum* inPSDValues = (CSDCarrierDouble::TNum*) malloc(psdSize);
    CSDCarrierDouble::TNum* outPSDValues = (CSDCarrierDouble::TNum*) malloc(psdSize);


    CSDCarrierDouble* csdData = new CSDCarrierDouble(n_channels, length);

    // set the csd values
    for (size_t iRow=0; iRow < n_channels; iRow++) {
        for (size_t iCol=0; iCol < iRow; iCol++) {
            for (int i=0; i<length; i++) {
                inCSDReal[i] = 10*iRow+iCol;
                inCSDImag[i] = 0.00001*i;
            }
            csdData->csd_setValues(inCSDReal, length, inCSDImag, length, iRow, iCol);
        }
    }

    // set the psd values
    for (int iChannel=0; iChannel<n_channels; iChannel++) {
        for (int i=0; i<length; i++) {
            inPSDValues[i] = iChannel + 0.00001*i;
        }
        csdData->psd_setValues(inPSDValues, length, iChannel);
    }

    // get and check the csd values
    for (size_t iRow=0; iRow < n_channels; iRow++) {
        for (size_t iCol=0; iCol < iRow; iCol++) {
            csdData->csd_getValues(outCSDReal, length, outCSDImag, length, iRow, iCol);
            for (int i=0; i<length; i++) {
                std::complex<CSDCarrierDouble::TNum> value(outCSDReal[i], outCSDImag[i]);
                BOOST_CHECK_EQUAL(value, CSDCarrierDouble::TComplex(10*iRow+iCol,0.00001*i));
            }
        }
    }

    // get and check the psd values
    for (int iChannel=0; iChannel<n_channels; iChannel++) {
        csdData->psd_getValues(outPSDValues, length, iChannel);
        for (int i=0; i<length; i++) {
            CSDCarrierDouble::TNum value = iChannel + 0.00001*i;
            BOOST_CHECK_EQUAL(value,outPSDValues[i]);
        }
    }


}
// Test the setters and getters
BOOST_AUTO_TEST_CASE( initial_test )
{
    // Make a blank CSDCarrierDouble
    size_t n_channels = 4;
    unsigned length = 65536;
    CSDCarrierDouble* csdData = new CSDCarrierDouble(n_channels, length);

    // Check the frameId setter and getter
    TFrameId frameId = 12345;
    csdData->sequence_number_set(frameId);
    BOOST_CHECK_EQUAL(frameId, csdData->sequence_number_get());

    // Check the size of the PSDs and fill in values
    for (int iChannel=0; iChannel<n_channels; iChannel++) {
        const Range<CSDCarrierDouble::TNum *> &range = csdData->psd_get(iChannel);
        BOOST_CHECK_EQUAL(length, range.size());
        size_t n = range.size();
        CSDCarrierDouble::TNum* temp = (CSDCarrierDouble::TNum*) malloc(n*sizeof(CSDCarrierDouble::TNum));
        for (int i=0; i<n; i++) {
            temp[i] = iChannel + 0.00001*i;
        }
        memcpy(range.begin(), temp, n*sizeof(CSDCarrierDouble::TNum));
        free(temp);
    }

    // Check the size of the CSDs and fill in values
    for (size_t iRow=0; iRow < n_channels; iRow++) {
        for (size_t iCol=0; iCol < iRow; iCol++) {
            const Range<CSDCarrierDouble::TComplex *> &range = csdData->csd_get(iRow, iCol);
            size_t n = range.size();
            BOOST_CHECK_EQUAL(length, n);
            CSDCarrierDouble::TComplex* temp = 
                (CSDCarrierDouble::TComplex*) malloc(n*sizeof(CSDCarrierDouble::TComplex));
            for (int i=0; i<n; i++) {
                temp[i] = CSDCarrierDouble::TComplex(10*iRow+iCol,0.00001*i);
            }
            memcpy(range.begin(), temp, n*sizeof(CSDCarrierDouble::TComplex));
            free(temp);
        }
    }

    // Read back the PSD values
    for (int iChannel=0; iChannel<n_channels; iChannel++) {
        Range<CSDCarrierDouble::TNum *> range = csdData->psd_get(iChannel);
        BOOST_CHECK_EQUAL(length, range.size());
        size_t n = range.size();
        CSDCarrierDouble::TNum* temp = (CSDCarrierDouble::TNum*) malloc(n*sizeof(CSDCarrierDouble::TNum));
        memcpy(temp, range.begin(), n*sizeof(CSDCarrierDouble::TNum));
        int i = 0;
        CSDCarrierDouble::TNum testValue;
        for (int i=0; i<n; i++) {
            testValue = iChannel + 0.00001*i;
            BOOST_CHECK_EQUAL(temp[i], testValue);
        }
        free(temp);
    }

    // Read back the CSD values
    for (size_t iRow=0; iRow < n_channels; iRow++) {
        for (size_t iCol=0; iCol < iRow; iCol++) {
            const Range<CSDCarrierDouble::TComplex *> &range = csdData->csd_get(iRow, iCol);
            size_t n = range.size();
            CSDCarrierDouble::TComplex* temp = 
                (CSDCarrierDouble::TComplex*) malloc(n*sizeof(CSDCarrierDouble::TComplex));
            memcpy(temp, range.begin(), n*sizeof(CSDCarrierDouble::TComplex));
            for (int i=0; i<n; i++) {
                BOOST_CHECK_EQUAL(temp[i], CSDCarrierDouble::TComplex(10*iRow+iCol,0.00001*i));
            }
            free(temp);
        }
    }
}

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

