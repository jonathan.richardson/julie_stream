////
///
#include <memory>
#include <cstdlib>

#include <cassert>
#include <stdexcept>

#include "hardware_locality/SIMD_align.hpp"

#include "spectrum_tools.hpp"

namespace JStream{

FFTMulti::Mutex FFTMulti::s_fftw_protect;

/////
///  FFT object constructor w/ hw locality
FFTMulti::
    FFTMulti(
		unsigned n_channels,
		unsigned channel_length,
		SDT::MemoryLocalityAccess locality
    ) :
		m_locality(locality)
{
	/// Set FFT properties
    m_channels_num      = n_channels;
    m_channel_length    = channel_length;
    m_input_bsize       = simd_align_size(sizeof(TFloat) * channel_length);
    m_fft_bsize         = simd_align_size(sizeof(TComplex) * (channel_length/2+1));
	
    size_t fft_size     = channel_length/2 + 1;
    
	/// Allocate the FFT input/output arrays
    m_input_data = (char*)m_locality.allocate(this->total_block_size(m_channels_num, m_channel_length));
    m_fft_data   = (m_input_data + this->input_block_size(m_channels_num, m_channel_length));

	/// Create the FFTW plan
    int rank = 1;
    int n[] = {int(m_channel_length)};
    int howmany = m_channels_num;
    int idist = int(m_input_bsize/sizeof(TFloat));
    int odist = int(m_fft_bsize/sizeof(TComplex));
    int istride = 1;
    int ostride = 1;
    int *inembed = nullptr;
    int *onembed = nullptr;
	{Guard fftw_guard(s_fftw_protect);
		m_fft_plan = fftwf_plan_many_dft_r2c(
							  rank, 
							  n, 
							  howmany,
							  (TFloat *)m_input_data, 
							  inembed,
							  istride, 
							  idist,
							  (TFloat (*)[2])m_fft_data, 
							  onembed,
							  ostride, 
							  odist,
							  FFTW_DESTROY_INPUT | FFTW_MEASURE //FFTW_ESTIMATE
						);
	}
}

/////
///  Get size of FFT input data
unsigned FFTMulti:: 
    input_block_size(unsigned n_channels, unsigned channel_length)
{
    return simd_align_size(sizeof(TFloat) * channel_length) * n_channels;
}

/////
///  Get size of FFT output data
unsigned FFTMulti:: 
    fft_block_size(unsigned n_channels, unsigned channel_length)
{
    auto fft_size = (channel_length/2 + 1);
    return simd_align_size(sizeof(TComplex) * fft_size) * n_channels;
}

/////
/// Get total size of FFT input/output
unsigned FFTMulti:: 
    total_block_size(unsigned n_channels, unsigned channel_length)
{
    return fft_block_size(channel_length, n_channels) + input_block_size(channel_length, n_channels);
}

/////
///  FFT object destructor
FFTMulti::
    ~FFTMulti()
{
	{Guard fftw_guard(s_fftw_protect);

		fftwf_destroy_plan(m_fft_plan);

        //fftwf_free(m_input_data);
        m_locality.deallocate(m_input_data, this->total_block_size(m_channels_num, m_channel_length));
	}
}

/////
///  Compute the FFTs
void FFTMulti::
    compute_ffts()
{
    fftwf_execute(m_fft_plan);
}

/////
///  Get the input data range ptrs
Range<FFTMulti::TFloat *> FFTMulti::
    input_data(unsigned chn)
{
    return Range<TFloat *>((TFloat*)(m_input_data + m_input_bsize*chn), m_channel_length);
}

/////
///  Get the output data range pointers
Range<const FFTMulti::TComplex *> FFTMulti::
    fft_data(unsigned chn)
{
    return Range<const TComplex*>((TComplex*)(m_fft_data + m_fft_bsize*chn), m_channel_length/2 + 1);
}

}//namespace ~JStream;
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
