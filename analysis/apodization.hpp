////
///
#ifndef H_APODIZATION_H
#define H_APODIZATION_H

#include <vector>

#ifndef M_PI
#define M_PI 3.1415926535
#endif

namespace JStream {

/////
///  The window function class
template<typename TNumeric>
class Apodization
{ 
    Range<const TNumeric *> m_apodization; 				//The apodization (window function)
    double       			m_sample_freq; 				//Sampling frequency (Hz)
    double       			m_ENBW;       				//Effective noise bandwidth (Hz)
    double       			m_power_spectrum_scaling;	//Normalization factor for power/cross-spectra [V^2]

  public:
	/// Constructor
    Apodization(
		double sample_freq, 
		std::vector<TNumeric> apodization
	);

    /// Copy constructor
    Apodization(const Apodization&);

	/// Destructor
    ~Apodization();

	/////
	///  Returns the window function range (begin and end ptrs)
    Range<const TNumeric *> apodization_data() const
    { return m_apodization; }

	/////
	///  Returns the window function length
    unsigned size() const
    { return m_apodization.size(); }

	/////
	///  Returns the (normalized) noise-equivalent-bandwidth
    double ENBW_get() const
    { return m_ENBW; }

	/////
	/// Return the normalization factor for the power spectra
    double power_spectrum_scaling_get() const
    { return m_power_spectrum_scaling; }
	
};

/////
///  Vector of Hanning window values
template<typename TNumeric>
std::vector<TNumeric> apodization_hanning(unsigned len);

}; //~namespace JStream

#include "analysis/apodization_T.hpp"
#endif //H_APODIZATION_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
