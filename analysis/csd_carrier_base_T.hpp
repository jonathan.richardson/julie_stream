////
///

#include <iostream>
#include <cstring>
#include <stdexcept>

#include "hardware_locality/SIMD_align.hpp"

#include "csd_carrier_base.hpp"
#include "csd_utilities_T.hpp"

namespace JStream {


/////
///  Constructor
template<typename TNum>
CSDCarrierBase<TNum>::
    CSDCarrierBase(
        unsigned n_channels,
        unsigned length,
        double sample_freq,
        double overlap_frac,
        SDT::MemoryLocalityAccess locality
    ) :
        m_channels_num(n_channels),
        m_length(length),
        m_sample_freq(sample_freq),
        m_overlap_frac(overlap_frac),
        m_locality(locality)
{
    /// Allocate the data arrays
    this->_setup_core_block();

    /// Initialize the PSDs to zero
    for(auto chn_idx : icount(m_channels_num)){
        auto psd = this->psd_get(chn_idx).begin();  
        for(unsigned idx = 0; idx < m_length; idx++){
            psd[idx] = 0.0;
        }
    }

    /// Initialize the CSDs to zero
    for(auto row_idx : icount(m_channels_num)){
        for(auto col_idx : icount(row_idx)){
            auto csd = (TComplex *) __builtin_assume_aligned
                            (this->csd_get(row_idx, col_idx).begin(), 16);
            for(unsigned idx = 0; idx < m_length; idx++){
                csd[idx] = TComplex(0.0, 0.0); 
            }
        }
    }
 
	/// Allocate the channel voltage range buffer, initialize to zero
	m_voltage_min.resize(m_channels_num);    
	m_voltage_max.resize(m_channels_num);
	for(auto chn_idx : icount(m_channels_num)){
		m_voltage_min[chn_idx] = 0.0;        
		m_voltage_max[chn_idx] = 0.0;
	}    
}

/////
///  Allocate the data arrays
template<typename TNum>
void CSDCarrierBase<TNum>::
	_setup_core_block()
{
    auto size_UD_flattened = _detail::UDmatrix_num_elements(m_channels_num);

    ///Start with an align size extra, so that we can fix the alignment coming out of malloc
    unsigned block_size = SIMD_ALIGNMENT;

    ///PSD size in all channels;
    unsigned psd_size_in_block = simd_align_size(sizeof(TNum) * m_length);
	unsigned psd_block_size = m_channels_num * psd_size_in_block;
    block_size += psd_block_size;
    
    ///CSD size in all channels;
    unsigned csd_size_in_block = simd_align_size(sizeof(TComplex) * m_length);
    block_size += size_UD_flattened * csd_size_in_block;

    /// Make the block
    m_core_block = Range<char *>((char *)m_locality.allocate(block_size), block_size);
    m_psd_offset = (TNum *)simd_round_to_align(m_core_block.begin());
    m_csd_offset = (TComplex *)simd_round_to_align(((char *)m_psd_offset) + psd_block_size);
	return;
}

/////
///  CSD frame copy constructor (inherits source locality)
template<typename TNum>
CSDCarrierBase<TNum>::
    CSDCarrierBase(const CSDCarrierBase<TNum>& source) : 
        CSDCarrierBase(
            source,
            source.m_locality
        )
{}
    
/////
///  CSD frame copy constructor (allows new locality assignment)
template<typename TNum>
CSDCarrierBase<TNum>::
    CSDCarrierBase(
        const CSDCarrierBase<TNum>& source,
        SDT::MemoryLocalityAccess locality
    ) : 
        m_channels_num(source.m_channels_num),
        m_length(source.m_length),
        m_sample_freq(source.m_sample_freq),
        m_sequence_number(source.m_sequence_number),
        m_accumulation(source.m_accumulation),
        m_normalization_to_ps(source.m_normalization_to_ps),
        m_ENBW(source.m_ENBW),
        m_overlap_frac(source.m_overlap_frac),
        m_time_begin(source.m_time_begin),
        m_time_end(source.m_time_end),
        m_error_flag(source.m_error_flag),
        m_locality(locality)
{
    /// Copy the CSD/PSD data
    this->_setup_core_block();
    auto size = sizeof(char) * source.m_core_block.size();
    memcpy(m_core_block.begin(), source.m_core_block.begin(), size);
    
    /// Copy the channel voltage range buffers (not localized)
    m_voltage_max = source.m_voltage_max;
    m_voltage_min = source.m_voltage_min;
}

/////
/// Move constructor
///  (Acquires the data block of the source frame)
template<typename TNum>
CSDCarrierBase<TNum>::
	CSDCarrierBase(CSDCarrierBase<TNum>&& source) :
        m_channels_num(source.m_channels_num),
        m_length(source.m_length),
        m_sample_freq(source.m_sample_freq),
        m_sequence_number(source.m_sequence_number),
        m_accumulation(source.m_accumulation),
        m_normalization_to_ps(source.m_normalization_to_ps),
        m_ENBW(source.m_ENBW),
        m_overlap_frac(source.m_overlap_frac),
        m_time_begin(source.m_time_begin),
        m_time_end(source.m_time_end),
        m_error_flag(source.m_error_flag),
        m_locality(source.m_locality)    
{
    /// Copy the core block reference and parameters
    m_core_block            = source.m_core_block;
    m_psd_offset            = source.m_psd_offset;
    m_csd_offset            = source.m_csd_offset;

    /// Nullify the core block parameters in the source frame
    source.m_core_block     = Range<char *>(nullptr, nullptr);
    source.m_psd_offset     = 0;
    source.m_csd_offset     = 0;
    source.m_length         = 0;
    source.m_channels_num   = 0;    
    
    /// Copy the channel voltage range buffers
    m_voltage_max = std::move(source.m_voltage_max);
    m_voltage_min = std::move(source.m_voltage_min);
}

/////
///  CSD frame assignment operator
template<typename TNum>
CSDCarrierBase<TNum> &CSDCarrierBase<TNum>::
    operator=(const CSDCarrierBase<TNum>& source)
{
    /// Check whether the core data block needs to be set up
    bool needs_setup = false;
	if( (m_channels_num != source.m_channels_num) or
        (m_length != source.m_length)) 
    {
        if(m_core_block.begin() != nullptr){
            /// Deallocate the current data arrays (will be reallocated to new size)
            m_locality.deallocate(m_core_block.begin(), m_core_block.size());
        }
        needs_setup = true;
	}

	/// Copy the metadata (not localized)
    m_length              = source.m_length;
    m_channels_num        = source.m_channels_num;    
	m_sequence_number     = source.m_sequence_number;
	m_accumulation        = source.m_accumulation;
	m_normalization_to_ps = source.m_normalization_to_ps;
	m_ENBW                = source.m_ENBW;
    m_sample_freq         = source.m_sample_freq;
    m_overlap_frac        = source.m_overlap_frac;
    m_time_begin          = source.m_time_begin;
    m_time_end            = source.m_time_end;
    m_error_flag          = source.m_error_flag;
    
    /// Copy the channel voltage range buffers (not localized)
    m_voltage_max = source.m_voltage_max;
    m_voltage_min = source.m_voltage_min;

    /// Copy the CSD/PSD data
    if(needs_setup){
        /// Allocate the data arrays
        this->_setup_core_block();
    }
    auto size = sizeof(char) * source.m_core_block.size();
    memcpy(m_core_block.begin(), source.m_core_block.begin(), size);  
	return *this;
}

/////
///  CSD frame destructor
template<typename TNum>
CSDCarrierBase<TNum>::
    ~CSDCarrierBase()
{
    m_locality.deallocate(m_core_block.begin(), m_core_block.size());
}

/////
///  Reimplements the assignment operator in a Python-callable form
template<typename TNum>
void CSDCarrierBase<TNum>::
	deepcopy(const CSDCarrierBase<TNum>& source)
{
    *this = source;
}

/////
///  Set the voltage range of the specified channel
template<typename TNum>
void CSDCarrierBase<TNum>::
    channel_voltage_set(
        unsigned chn, 
        double V_min,
        double V_max
    )
{
    m_voltage_min[chn] = V_min; 
    m_voltage_max[chn] = V_max; 
}

/////
///  Returns the maximum voltage for the specified channel
template<typename TNum> 
double CSDCarrierBase<TNum>::
	voltage_max(unsigned chn) const
{
    return m_voltage_max[chn]; 
}

/////
///  Returns the minimum voltage for the specified channel
template<typename TNum> 
double CSDCarrierBase<TNum>::
	voltage_min(unsigned chn) const
{
    return m_voltage_min[chn]; 
}

/////
///  Return the number of CSDs
template<typename TNum>
unsigned CSDCarrierBase<TNum>::
    size_UD_matrix() const
{
    return _detail::UDmatrix_num_elements(m_channels_num); 
}

/////
///  Return the index of the specified PSD/CSD
template<typename TNum>
unsigned CSDCarrierBase<TNum>::
    index_UD_matrix(unsigned row, unsigned col) const
{
    assert(row < m_channels_num);
    assert(row >= 0);
    if(not (col < row)) {*((int *)nullptr) = 911; }
    assert(col >= 0);
    return ((row)*(row+1)/2) - col - 1;
}

/////
///  Returns the data range of the minimum voltages for all channels
template<typename TNum>
Range<const double *> CSDCarrierBase<TNum>::
	voltage_min_buffer() const
{
    auto begin = m_voltage_min.data();
	return Range<const double *>(begin, m_channels_num);
}

/////
///  Returns the data range of the maximum voltages for all channels
template<typename TNum>
Range<const double *> CSDCarrierBase<TNum>::
	voltage_max_buffer() const
{
    auto begin = m_voltage_max.data();
	return Range<const double *>(begin, m_channels_num);
}

/////
///  Set the frame ID
template<typename TNum>
void CSDCarrierBase<TNum>::
    sequence_number_set(uint64_t value) 
{
    m_sequence_number = value; 
}

/////
///  Set the frame's beginning timestamp
template<typename TNum>
void CSDCarrierBase<TNum>::
    timestamps_set(uint64_t begin, uint64_t end)
{
    m_time_begin = begin; 
    m_time_end   = end; 
}


/////
///  Get the frame's beginning timestamp
template<typename TNum>
uint64_t CSDCarrierBase<TNum>::
    timestamp_begin() const
{
    return m_time_begin; 
}

/////
///  Get the frame's ending timestamp
template<typename TNum>
uint64_t CSDCarrierBase<TNum>::
    timestamp_end() const
{
    return m_time_end; 
}

/////
///  Get the frame ID
template<typename TNum>
uint64_t CSDCarrierBase<TNum>::
    sequence_number_get() const
{
    return m_sequence_number; 
}

/////
///  Get the sampling frequency
template<typename TNum>
double CSDCarrierBase<TNum>::
	sample_freq_get() const
{
	return m_sample_freq; 
}

/////
///  Get the FFT frame overlap fraction
template<typename TNum>
double CSDCarrierBase<TNum>::
	overlap_frac_get() const
{
	return m_overlap_frac; 
}

/////
///  Get the number of channels
template<typename TNum>
unsigned CSDCarrierBase<TNum>::
    channels_num() const 
{
    return m_channels_num; 
}

////
///  Returns the CSD length
template<typename TNum>
unsigned CSDCarrierBase<TNum>::
    length() const
{
	return m_length; 
}

/////
///  Returns the range of the specified PSD
template<typename TNum>
Range<TNum *> CSDCarrierBase<TNum>::
    psd_get(unsigned idx)
{
    TNum *offset = m_psd_offset;
    offset += m_length * idx ;
    return Range<TNum *>(offset, m_length); 
}

/////
///  Returns the range of the specified PSD (reader mode)
template<typename TNum>
Range<const TNum *> CSDCarrierBase<TNum>::
    psd_get(unsigned idx) const
{
    TNum *offset = m_psd_offset;
    offset += m_length * idx ;
    return Range<const TNum *>(offset, m_length); 
}

/////
///  Returns the range of the specified CSD
template<typename TNum>
Range<typename CSDCarrierBase<TNum>::TComplex *> CSDCarrierBase<TNum>::
    csd_get(unsigned row_idx, unsigned col_idx)
{
    TComplex *offset = m_csd_offset;
    //csd data offset
    offset += m_length * this->index_UD_matrix(row_idx, col_idx);
    
    return Range<TComplex *>(offset, m_length);
}

/////
///  Returns the range of the specified CSD (reader mode)
template<typename TNum>
Range<const typename CSDCarrierBase<TNum>::TComplex *> CSDCarrierBase<TNum>::
    csd_get(unsigned row_idx, unsigned col_idx) const
{
    TComplex *offset = m_csd_offset;
    //csd data offset
    offset += m_length * this->index_UD_matrix(row_idx, col_idx);
    
    return Range<const TComplex *>(offset, m_length);
}

/////
///  Get the number of individual FFT batches that went into the frame
template<typename TNum>
uint64_t CSDCarrierBase<TNum>::
    accumulation_get() const
{
    return m_accumulation; 
}

/////
///  Set the number of individual FFT batches that went into the frame
template<typename TNum>
void CSDCarrierBase<TNum>::
    accumulation_set(uint64_t accumulation)
{
    m_accumulation = accumulation; 
}

/////
///  Get the PS normalization factor (scaling to V^2)
template<typename TNum>
double CSDCarrierBase<TNum>::
    normalization_to_ps_get() const
{
    return m_normalization_to_ps; 
}

/////
///  Set the PS normalization factor (scaling to V^2)
template<typename TNum>
void CSDCarrierBase<TNum>::
    normalization_to_ps_set(double normalization)
{
    m_normalization_to_ps = normalization; 
}

/////
///  Get the ENBW
template<typename TNum>
double CSDCarrierBase<TNum>::
    ENBW_get() const
{
    return m_ENBW; 
}

/////
///  Set the ENBW
template<typename TNum>
void CSDCarrierBase<TNum>::
    ENBW_set(double ENBW)
{
    m_ENBW = ENBW; 
}

/////
///  Set the dropped-frame(s) flag
template<typename TNum>
void CSDCarrierBase<TNum>::
    error_flag_set(unsigned code)
{
    m_error_flag = code; 
}

/////
///  Get the dropped-frame(s) flag
template<typename TNum>
unsigned CSDCarrierBase<TNum>::
    error_flag_get() const
{
    return m_error_flag;
}

/////
///  Set the values for the PSDs from the supplied array
template<typename TNum>
void CSDCarrierBase<TNum>::
    _psd_setValues(
        TNum* inData, 
        int length, 
        int iChannel
    )
{
    if(iChannel < 0)
        throw std::runtime_error("channel < 0, out of range");
    if(iChannel >= m_channels_num)
        throw std::runtime_error("channel >= num_channels, out of range");
    if(length != m_length)
        throw std::runtime_error("length must be equal to CSD length");

	const Range<TNum *> &range = this->psd_get(iChannel);
	unsigned size = range.size() * sizeof(TNum);
	memcpy(range.begin(), inData, size);
	return;
}

/////
///  Get the values for the PSDs from the supplied array.
///  (The outData array must be properly allocated before this call)
template<typename TNum>
void CSDCarrierBase<TNum>::
    _psd_getValues(
        TNum** outData, 
        int* length, 
        int iChannel
    )
{
    if(iChannel < 0)
        throw std::runtime_error("channel < 0, out of range");
    if(iChannel >= m_channels_num)
        throw std::runtime_error("channel >= num_channels, out of range");

	const Range<TNum *> &range = this->psd_get(iChannel);
	*length = range.size();
    *outData = range.begin();
}

/////
///  Set the values for the CSDs from the supplied array
template<typename TNum>
void CSDCarrierBase<TNum>::
    _csd_setValues(
		TComplex* inData,
		int length,
		int iRow,
		int iCol
    ) 
{
    if(iRow < 0)
        throw std::runtime_error("Row < 0, out of range");
    if(iRow >= m_channels_num)
        throw std::runtime_error("Row >= num_channels, out of range");
    if(iCol < 0)
        throw std::runtime_error("Col < 0, out of range");
    if(iCol >= iRow)
        throw std::runtime_error("Col >= Row, out of range");
    if(length != m_length)
        throw std::runtime_error("length must be equal to CSD length");

    const Range<TComplex *> &range = this->csd_get(iRow, iCol);
    for (int i=0; i<range.size(); i++)
        range.begin()[i] = inData[i];       
}

/////
///  Get the values for the CSDs from the supplied array.
///  (The outData array must be properly allocated before this call)
template<typename TNum>
void CSDCarrierBase<TNum>::
    _csd_getValues(
        TComplex** outData,
        int* length,
        int iRow,
        int iCol
	)
{
    if(iRow < 0)
        throw std::runtime_error("Row < 0, out of range");
    if(iRow >= m_channels_num)
        throw std::runtime_error("Row >= num_channels, out of range");
    if(iCol < 0)
        throw std::runtime_error("Col < 0, out of range");
    if(iCol >= iRow)
        throw std::runtime_error("Col >= Row, out of range");
        
	const Range<TComplex *> &range = this->csd_get(iRow, iCol);
    *length = range.size();
    *outData = range.begin();   
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
