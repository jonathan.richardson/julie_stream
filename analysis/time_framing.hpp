////
///
///
#ifndef H_JSTREAM_ANALYSIS_TIME_FRAMING_H
#define H_JSTREAM_ANALYSIS_TIME_FRAMING_H

#include <vector>
#include <cassert>

#include "hardware_locality/hwloc.hpp"
#include "hardware_locality/localization_manager.hpp"

namespace JStream {


/////
///  The 5122 time frames
///  Holds the timestream fill buffer for multiple, synchronous channels
///  Holds one \delta_t worth of raw timestream data
template<typename TFill>
class SlidingFrameTimestream
{
public:
    typedef TFill           TNum;
protected:
    unsigned                m_channels_num;     //Total number of channels
    unsigned                m_fillbuffer_len;   //Timestream channel buffer length
    unsigned                m_overlap;          //FFT overlap fraction
    unsigned                m_frame_len;        //FFT size
    unsigned                m_num_frames;       //Number of FFT batches in each channel buffer
    unsigned                m_unframed_fill;    //Remainder samples at the end of each channel buffer

	/// Frame identifier
    uint64_t	            m_sequence_number;
	
	/// Timestamps of the first and last samples in the channel buffers
	uint64_t	            m_timestamp_begin;
	uint64_t	            m_timestamp_end;
	
    /// Buffer for the timestream data
    std::shared_ptr<TFill>  m_fillbuffers;      //Access: m_fillbuffers[channel, idx]
    
	/// Buffers for channel calibration constants (for scaling ADU --> V)
    std::shared_ptr<double>	m_gain;
    std::shared_ptr<double>	m_offset;
   
    /// Buffers for maximum, minimum channel voltages
    std::shared_ptr<double>	m_voltage_min;
    std::shared_ptr<double>	m_voltage_max;
   
    /// Error flag (set to 40 for ADC clipping)
    unsigned                m_error_flag    = 0;
    
private:
    /// Hardware locality assignment
    SDT::MemoryLocalityAccess   
                            m_locality;    

public:
    SlidingFrameTimestream(
        unsigned channels_n,
        unsigned fill_len,
        unsigned frame_len,
        unsigned overlap_len,
		SDT::MemoryLocalityAccess locality
    );

    ~SlidingFrameTimestream();

    /// The copy constructor just creates a similar frame (it does not copy the data)
    SlidingFrameTimestream(const SlidingFrameTimestream&);
    
#ifndef SWIG
    /// Assignment operator
    SlidingFrameTimestream<TFill> &operator=(const SlidingFrameTimestream<TFill> &);
#endif
	
	/// Return the specified data range for the frame
    Range<TFill *> channel_timebuffer(unsigned chn);
	Range<double *> gain_buffer(unsigned chn);
	Range<double *> offset_buffer(unsigned chn);

    Range<const TFill *> channel_timebuffer(unsigned chn) const;
	Range<const double *> gain_buffer(unsigned chn) const;
	Range<const double *> offset_buffer(unsigned chn) const;

    //////
    ///  Writing interface
    void sequence_number_set(uint64_t value);
    void timestamps_set(uint64_t begin, uint64_t end);
    void error_flag_set(unsigned code);
    void channel_voltage_set(
        unsigned chn, 
        double V_min,
        double V_max
    );

    //////
    ///  Reading interface
    unsigned num_channels() const;
    uint64_t sequence_number() const;
    unsigned num_frames() const;
    CountingRange channel_counter() const;      //Returns an iterator over the number of channels
    CountingRange frame_counter() const;        //Returns an iterator over the number of FFT batches
    unsigned framed_buffer_len() const;
    uint64_t timestamp_begin() const;
    uint64_t timestamp_end() const;
	double gain(unsigned chn) const;            //ADC calibration constants
	double offset(unsigned chn) const;          //(for scaling ADU --> V)
	double voltage_max(unsigned chn) const;     //Maximum measured voltage
	double voltage_min(unsigned chn) const;     //Minimum measured voltage
    unsigned error_flag() const; 

    //////
    ///  This function is ok to use *this as prior_frame
    unsigned prefill_overlap_size(SlidingFrameTimestream &prior_frame);
    
    Range<const TFill *> frame_range(unsigned chn, unsigned frame_num) const;
};

}; //~namespace JStream

#include "analysis/time_framing_T.hpp"
#endif //H_JSTREAM_ANALYSIS_TIME_FRAMING_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
