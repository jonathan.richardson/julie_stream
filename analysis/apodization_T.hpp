////
///

#include "apodization.hpp"

namespace JStream{

/////
///  Creates a vector of Hanning window values
template<typename TNumeric>
std::vector<TNumeric> apodization_hanning(unsigned len)
{
    std::vector<TNumeric> apodization;
    apodization.reserve(len);

    for(unsigned idx = 0; idx < len; idx +=1) {
        double argument = 2 * M_PI * double(idx + 1)/double(len + 2);
        apodization.push_back((1 - cos(argument))/2);
    }
	
    return apodization;
}

/////
///  Apodization constructor
template<typename TNumeric>
Apodization<TNumeric>::
	Apodization(
		double sample_freq,
		std::vector<TNumeric> apodization
	)
	: m_sample_freq(sample_freq)
{
	/////
	///  Compute the window normalization factors
	double win_S1 = 0; //the apodization frame sum
	double win_S2 = 0; //the apodization frame sum-squares
	for(auto W : apodization) {
		win_S1 += W;
		win_S2 += W*W;
	}

	/////
	///  Set the PS normalization and the normalized effective noise bandwidth 
	/// (from the GEO600 paper)
    m_power_spectrum_scaling = 2 / pow(win_S1, 2);
	m_ENBW = sample_freq * win_S2 / pow(win_S1, 2);

	/////
	///  Copy the values of the passed-in window function to a local array
	auto apodization_len = apodization.size();
    TNumeric *apodization_buff = new TNumeric[apodization_len];
    for(auto idx : icount(apodization_len)) {
        apodization_buff[idx] = apodization[idx];
    }

    m_apodization = Range<const TNumeric *>(apodization_buff, apodization_len);
}

/////
///  Apodization copy constructor
template<typename TNumeric>
Apodization<TNumeric>::
    Apodization(const Apodization& other)
{
    m_sample_freq 			 = other.m_sample_freq;
    m_ENBW 					 = other.m_ENBW;
	m_power_spectrum_scaling = other.m_power_spectrum_scaling;

    /////
    ///  Copy the apodization buffer values to a local array
    auto apodization_len = other.m_apodization.size();
	auto apodization_ptr = new TNumeric[apodization_len];
	for(auto idx : icount(apodization_len)) {
		apodization_ptr[idx] = other.m_apodization[idx];
	}

    m_apodization = Range<const TNumeric *>(apodization_ptr, apodization_ptr + apodization_len);
}

/////
///  Apodization destructor
template<typename TNumeric>
Apodization<TNumeric>::
	~Apodization()
{
	delete[] m_apodization.begin();
}

}//namespace ~JStream;
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
