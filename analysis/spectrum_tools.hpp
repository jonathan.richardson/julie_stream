////
///
#ifndef H_JULIE_STREAM_CORE_H
#define H_JULIE_STREAM_CORE_H

#include <memory>
#include <vector>
#include <cassert>
#include <complex>

#include <fftw3.h>

#include "utilities/cpp_utilities.hpp"
#include "utilities/range.hpp"

#include "time_framing.hpp"

#include <mutex>

namespace JStream {

using std::complex;
using std::shared_ptr;

/////
///  FFT-computing object
///  Given a pointer to data, creates an output buffer and FFT plan
class FFTMulti
{
    typedef float           TFloat;
    typedef complex<TFloat> TComplex;

	typedef std::mutex Mutex;
	typedef std::lock_guard<Mutex> Guard;

	static Mutex s_fftw_protect;

    unsigned   m_channels_num;
    unsigned   m_channel_length;

    char      *m_input_data;
    unsigned   m_input_bsize;
    char      *m_fft_data;
    unsigned   m_fft_bsize;

    fftwf_plan m_fft_plan;

    SDT::MemoryLocalityAccess m_locality;

  public:
    FFTMulti(
		unsigned n_channels,
		unsigned input_size,
		SDT::MemoryLocalityAccess locality
    );

    FFTMulti(
		unsigned n_channels,
		unsigned length,
		Range<char *> input_array
    );

    FFTMulti(const FFTMulti&) = delete;
    FFTMulti &operator=(const FFTMulti&) = delete;

    ~FFTMulti();

    static unsigned input_block_size(unsigned n_channels, unsigned channel_length);
    static unsigned fft_block_size(unsigned n_channels, unsigned channel_length);
    static unsigned total_block_size(unsigned n_channels, unsigned channel_length);

    Range<TFloat *>         input_data(unsigned chn_num);
    Range<const TComplex *> fft_data(unsigned chn_num);

    void compute_ffts();
};

}; //~namespace JStream

#endif //H_JULIE_STREAM_CORE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
