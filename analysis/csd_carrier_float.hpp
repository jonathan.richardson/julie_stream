////
///

#ifndef H_JSTREAM_CSD_CARRIER_FLOAT_H
#define H_JSTREAM_CSD_CARRIER_FLOAT_H

#include "csd_carrier_base.hpp"

namespace JStream {

/////
///  Single-precision CSD container class
///  (Wraps the base carrier class in a datatype-specific layer for C++ to Numpy mapping)
class CSDCarrierFloat : public CSDCarrierBase<float>
{
public:
    /////
    ///  Wrappers for the base class constructor
    
    CSDCarrierFloat(
        unsigned n_channels     = 0,
        unsigned length         = 0,
        double sample_freq      = 0,
        double overlap_frac     = 0,
        SDT::MemoryLocalityAccess locality = SDT::memory_locality_default()
    ) :
        CSDCarrierBase<float>(n_channels, length, sample_freq, overlap_frac, locality)
    {}    

    CSDCarrierFloat(const CSDCarrierFloat &source) :
        CSDCarrierBase<float>(source) 
    {}
    
    CSDCarrierFloat(
        const CSDCarrierFloat &source,
        SDT::MemoryLocalityAccess locality
    ) :
        CSDCarrierBase<float>(source, locality) 
    {}        

#ifndef SWIG
    CSDCarrierFloat(CSDCarrierFloat &&source) :
        CSDCarrierBase(source) 
    {}

    CSDCarrierFloat &operator=(const CSDCarrierFloat & source){
        CSDCarrierBase<float>::operator=(source);
        return *this;
    }
#endif    

    /////
    ///  Adds support for co-adding single-precision frames

    void csd_frame_add(const CSDCarrierBase<float>& csd_carrier){
        _detail::add_csds_data(*this, csd_carrier);
    }   

    /////
    ///  Wrappers for the SWIG data access interface
    
    void psd_setValues(float* inData, int length, int iChannel){
        this->_psd_setValues(inData, length, iChannel);
    }

    void psd_getValues(float** outData, int* length, int iChannel){
        this->_psd_getValues(outData, length, iChannel);
    }

    void csd_setValues(std::complex<float>* inData, int length, int iRow, int iCol){
        this->_csd_setValues(inData, length, iRow, iCol);
    }

    void csd_getValues(std::complex<float>** outData, int* length, int iRow, int iCol){
        this->_csd_getValues(outData, length, iRow, iCol);
    }
    
};

}; //~namespace JStream

#endif //H_JSTREAM_CSD_CARRIER_FLOAT_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :