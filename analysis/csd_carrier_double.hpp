////
///

#ifndef H_JSTREAM_CSD_CARRIER_DOUBLE_H
#define H_JSTREAM_CSD_CARRIER_DOUBLE_H

#include "csd_carrier_base.hpp"

namespace JStream {

/////
///  Double-precision CSD container class
///  (Wraps the base carrier class in a datatype-specific layer for C++ to Numpy mapping)
class CSDCarrierDouble : public CSDCarrierBase<double>
{
public:
    /////
    ///  Wrappers for the base class constructor
    
    CSDCarrierDouble(
        unsigned n_channels     = 0,
        unsigned length         = 0,
        double sample_freq      = 0,
        double overlap_frac     = 0,
        SDT::MemoryLocalityAccess locality  = SDT::memory_locality_default()
    ) :
        CSDCarrierBase<double>(n_channels, length, sample_freq, overlap_frac, locality)
    {}

    CSDCarrierDouble(const CSDCarrierDouble &source) :
        CSDCarrierBase<double>(source) 
    {}
    
    CSDCarrierDouble(
        const CSDCarrierDouble &source,
        SDT::MemoryLocalityAccess locality
    ) :
        CSDCarrierBase<double>(source, locality) 
    {}    
    
#ifndef SWIG
    CSDCarrierDouble(CSDCarrierDouble &&source) :
        CSDCarrierBase(source)
    {}

    CSDCarrierDouble &operator=(const CSDCarrierDouble & source){
        CSDCarrierBase<double>::operator=(source);
        return *this;
    }
#endif    

    /////
    ///  Adds support for co-adding single-, double-precision frames

    void csd_frame_add(const CSDCarrierBase<float>& csd_carrier){
        _detail::add_csds_data(*this, csd_carrier);
    }   
    
    void csd_frame_add(const CSDCarrierBase<double>& csd_carrier){
        _detail::add_csds_data(*this, csd_carrier);
    }
    
    /////
    ///  Wrappers for the SWIG data access interface
    
    void psd_setValues(double* inData, int length, int iChannel){
        this->_psd_setValues(inData, length, iChannel);
    }

    void psd_getValues(double** outData, int* length, int iChannel){
        this->_psd_getValues(outData, length, iChannel);
    }

    void csd_setValues(std::complex<double>* inData, int length, int iRow, int iCol){
        this->_csd_setValues(inData, length, iRow, iCol);
    }

    void csd_getValues(std::complex<double>** outData, int* length, int iRow, int iCol){
        this->_csd_getValues(outData, length, iRow, iCol);
    }
    
};

}; //~namespace JStream

#endif //H_JSTREAM_CSD_CARRIER_DOUBLE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :