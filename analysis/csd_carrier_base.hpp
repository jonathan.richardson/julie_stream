////
///

#ifndef H_JSTREAM_CSD_CARRIER_BASE_H
#define H_JSTREAM_CSD_CARRIER_BASE_H

#include <iostream>
#include <cstring>
#include <stdexcept>
#include <complex>
#include <vector>

#include "utilities/cpp_utilities.hpp"
#include "utilities/range.hpp"

#include "hardware_locality/locality.hpp"
#include "hardware_locality/SIMD_align.hpp"

#include "analysis/csd_utilities_T.hpp"

namespace JStream {

/////
///  Base CSD container class
template <typename TPrecision>
class CSDCarrierBase
{
public:
    /// Convenience typedefs
    typedef TPrecision          
        TNum;
    typedef std::complex<TNum>  
        TComplex;

protected:
    /// Carries the memory allocated for the FFT data, CSD data, and the scaled apodization
    Range<char *>       m_core_block;
    TNum               *m_psd_offset;
    TComplex           *m_csd_offset;

    /// CSD frame parameters
    unsigned            m_length                 = 0;     //CSD length
    uint64_t            m_sequence_number        = 0;     //Frame ID number
    unsigned            m_channels_num           = 0;     //Total number of channels
    uint64_t            m_accumulation           = 0;     //Total number of FFT batches in this frame
    double              m_normalization_to_ps    = 0.0;   //Power spectrum normalization factor
    double              m_ENBW                   = 0.0;   //Effective noise bandwidth (Hz)
    double              m_sample_freq            = -1.0;  //Sample frequency (Hz)
    double 		        m_overlap_frac           = 0.0;   //FFT frame overlap fraction
    
    /// Frame timestamps
    uint64_t            m_time_begin             = 0;     //Begin timestamp
    uint64_t            m_time_end               = 0;     //End timestamp
    
    /// Error flagging
    unsigned            m_error_flag             = 0;     //CSD error flag 
    //   0:  No error
    //   10: Frame-dropping occurred 
    //          (only matters if noise non-stationary)
    //   20: Possible environmental contamination 
    //          (transient detected in aux channel(s) only)   
    //   30: Unknown transient       
    //          (detected in main channel(s) only)    
    //   31: Environmental contamination 
    //          (transient detected in both main + aux channels)
    //   40: Clipping occurred in the ADC
    
    /// Minimum, maximum channel voltages
    std::vector<double> m_voltage_min;
    std::vector<double> m_voltage_max;
    
private:
    /// Hardware locality assignment
    SDT::MemoryLocalityAccess   m_locality;

public:
    /// Constructor
    CSDCarrierBase(
        unsigned n_channels,
        unsigned length,
        double sample_freq,
        double overlap_frac,
        SDT::MemoryLocalityAccess locality
    );

    /// Copy constructor (allows new locality assignment)
    CSDCarrierBase(
        const CSDCarrierBase<TNum> & source,
        SDT::MemoryLocalityAccess locality
    );
    
    /// Copy constructor (uses source locality)
    CSDCarrierBase(const CSDCarrierBase<TNum> &);    
    
#ifndef SWIG
    /// Move constructor
    CSDCarrierBase(CSDCarrierBase<TNum> &&);

    /// Assignment operator
    CSDCarrierBase<TNum> &operator=(const CSDCarrierBase<TNum> &);
#endif

    /// Destructor
    ~CSDCarrierBase();
    
    /// Wraps the assigment operator in a Python-callable form
    void deepcopy(const CSDCarrierBase<TNum> &);        

    /////
    ///  Writer interface
    Range<TComplex *>       csd_get(unsigned row_idx, unsigned col_idx);
    Range<TNum     *>       psd_get(unsigned chn_idx);
    void                    sequence_number_set(uint64_t value);
    void                    accumulation_set(uint64_t num);
    void                    normalization_to_ps_set(double normalization);
    void                    ENBW_set(double ENBW);
    void                    timestamps_set(uint64_t begin, uint64_t end);
    void                    error_flag_set(unsigned code);
    void                    channel_voltage_set(
                                unsigned chn, 
                                double V_min,
                                double V_max
                            );
    
    /////
    ///  Reader interface
    Range<const TComplex *> csd_get(unsigned row_idx, unsigned col_idx) const;
    Range<const TNum     *> psd_get(unsigned chn_idx) const;
    unsigned                channels_num() const;
    unsigned                length() const;
    double                  sample_freq_get() const;
    double                  overlap_frac_get() const;
    uint64_t                sequence_number_get() const;
    uint64_t                accumulation_get() const;
    double                  normalization_to_ps_get() const;
    double                  ENBW_get() const;
    uint64_t                timestamp_begin() const;
    uint64_t                timestamp_end() const;
    unsigned                error_flag_get() const; 
	double                  voltage_max(unsigned chn) const;
	double                  voltage_min(unsigned chn) const;  
	Range<const double *>   voltage_min_buffer() const;   //Returns minima for all channels
	Range<const double *>   voltage_max_buffer() const;   //Returns maxima for all channels

protected:
    /// Takes arguments from 0 to the number of channels-1, where col <= row
    unsigned index_UD_matrix(unsigned row, unsigned col) const;
    unsigned size_UD_matrix() const;
    
    /// Allocates the CSD memory block
	void _setup_core_block();
    
    /////
    ///  Provide numpy-compatible data access interface
    
    void _psd_setValues(
        TNum* inData,
        int length,
        int iChannel
    );

    void _psd_getValues(
        TNum** outData,
        int* length,
        int iChannel
    );

    void _csd_setValues(
        TComplex* inData,
        int length,
        int iRow,
        int iCol
    );

    void _csd_getValues(
        TComplex** outData,
        int* length,
        int iRow,
        int iCol
    );       
    
};

}; //~namespace JStream

#include "analysis/csd_carrier_base_T.hpp"
#endif //H_JSTREAM_CSD_CARRIER_BASE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :