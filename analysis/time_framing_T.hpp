////
///

#include <iostream>

#include "time_framing.hpp"

namespace JStream {


/////
///  Time frame constructor
template<typename TFill>
SlidingFrameTimestream<TFill>::
    SlidingFrameTimestream(
        unsigned channels_n,
        unsigned fill_len,
        unsigned frame_len,
        unsigned overlap_len,
		SDT::MemoryLocalityAccess locality
    ) :
        m_locality(locality),
        m_channels_num(channels_n),
        m_fillbuffer_len(fill_len),
        m_frame_len(frame_len),
        m_overlap(overlap_len)
{
	assert(m_fillbuffer_len > m_frame_len);
	
	/// Get the number of FFT frames within each channel buffer (includes batch overlap)
	/// (Rounds down to nearest int value)
	m_num_frames = (m_fillbuffer_len - m_overlap) / (m_frame_len - m_overlap);
	assert(m_num_frames > 0);

	/// Get the number of samples that are not in a complete frame
	/// (These will be copied to the beginning of the next frame)
	m_unframed_fill = (m_fillbuffer_len - m_overlap) % (m_frame_len - m_overlap);  
    
	/// Allocate the input data buffer
	m_fillbuffers = m_locality.new_array<TFill>(m_channels_num * m_fillbuffer_len);
        
	/// Allocate the calibration constant, voltage range buffers
	/// (Initialize for case of virtual sources/no saturation scanning)
	m_gain          = m_locality.new_array<double>(m_channels_num);
	m_offset        = m_locality.new_array<double>(m_channels_num);
	m_voltage_min   = m_locality.new_array<double>(m_channels_num);    
	m_voltage_max   = m_locality.new_array<double>(m_channels_num);
	for(auto chn_idx : channel_counter()){
		m_gain.get()[chn_idx]           = 1.0;
		m_offset.get()[chn_idx]         = 0.0;
		m_voltage_min.get()[chn_idx]    = 0.0;        
		m_voltage_max.get()[chn_idx]    = 0.0;
	}
}

//////
///  The copy constructor just creates a similar frame, it does not copy the data
template<typename TFill>
SlidingFrameTimestream<TFill>::
    SlidingFrameTimestream(const SlidingFrameTimestream &S) :
        SlidingFrameTimestream(
                S.m_channels_num,
                S.m_fillbuffer_len,
                S.m_frame_len,
                S.m_overlap,
				S.m_locality
            )
{
}

/////
///  Time frame assignment operator
template<typename TFill>
SlidingFrameTimestream<TFill> &SlidingFrameTimestream<TFill>::
    operator=(const SlidingFrameTimestream<TFill> &source)
{
    /// Require the allocated timestream buffers be identical size
    assert(m_channels_num == source.m_channels_num);
    assert(m_fillbuffer_len == source.m_fillbuffer_len);
    
	/// Copy the metadata
    m_overlap           = source.m_overlap;
    m_frame_len         = source.m_frame_len;
    m_num_frames        = source.m_num_frames;
    m_unframed_fill     = source.m_unframed_fill;
    m_sequence_number   = source.m_sequence_number;
	m_timestamp_begin   = source.m_timestamp_begin;
	m_timestamp_end     = source.m_timestamp_end;
    m_error_flag        = source.m_error_flag;
    
	{   /// Copy the timestream data
        auto size = sizeof(TFill) * m_channels_num * m_fillbuffer_len;
        memcpy(m_fillbuffers.get(), source.m_fillbuffers.get(), size);
    }
    
    {   /// Copy the calibration constants, voltage ranges
        auto size = sizeof(double) * m_channels_num;
        memcpy(m_gain.get(), source.m_gain.get(), size);
        memcpy(m_offset.get(), source.m_offset.get(), size);
        memcpy(m_voltage_max.get(), source.m_voltage_max.get(), size);
        memcpy(m_voltage_min.get(), source.m_voltage_min.get(), size);        
    }
	return *this;
}

/////
///  T5122Frame destructor
template<typename TFill>
SlidingFrameTimestream<TFill>::
	~SlidingFrameTimestream()
{
}

/////
///  Returns the gain calibration for the specified channel
template<typename TFill> 
double SlidingFrameTimestream<TFill>::
	gain(unsigned chn) const
{
    return m_gain.get()[chn]; 
}

/////
///  Returns the offset calibration for the specified channel
template<typename TFill> 
double SlidingFrameTimestream<TFill>::
	offset(unsigned chn) const
{
    return m_offset.get()[chn]; 
}

/////
///  Returns the maximum voltage for the specified channel
template<typename TFill> 
double SlidingFrameTimestream<TFill>::
	voltage_max(unsigned chn) const
{
    return m_voltage_max.get()[chn]; 
}

/////
///  Returns the minimum voltage for the specified channel
template<typename TFill> 
double SlidingFrameTimestream<TFill>::
	voltage_min(unsigned chn) const
{
    return m_voltage_min.get()[chn]; 
}

/////
///  Returns the total number of channels
template<typename TFill> 
unsigned SlidingFrameTimestream<TFill>::
	num_channels() const
{
    return m_channels_num; 
}


/////
///  Set the frame's beginning and ending timestamps
template<typename TFill> 
void SlidingFrameTimestream<TFill>::
	timestamps_set(uint64_t begin, uint64_t end)
{
    m_timestamp_begin = begin; 
    m_timestamp_end = end;
}

/////
///  Get the frame's beginning & ending timestamps
template<typename TFill> 
uint64_t SlidingFrameTimestream<TFill>::
	timestamp_end() const
{
	return m_timestamp_end; 
}

template<typename TFill> 
uint64_t SlidingFrameTimestream<TFill>::
	timestamp_begin() const
{
	return m_timestamp_begin; 
}
/////
///  Set the frame number
template<typename TFill> 
void SlidingFrameTimestream<TFill>::
    sequence_number_set(uint64_t value)
{
    m_sequence_number = value; 
}

/////
///  Get the frame number
template<typename TFill> 
uint64_t SlidingFrameTimestream<TFill>::
    sequence_number() const
{
    return m_sequence_number; 
}

/////
///  Set the dropped-frame(s) flag
template<typename TFill>
void SlidingFrameTimestream<TFill>::
    error_flag_set(unsigned code)
{
    m_error_flag = code; 
}

/////
///  Set the voltage range of the specified channel
template<typename TFill>
void SlidingFrameTimestream<TFill>::
    channel_voltage_set(
        unsigned chn, 
        double V_min,
        double V_max
    )
{
    m_voltage_min.get()[chn] = V_min; 
    m_voltage_max.get()[chn] = V_max; 
}

/////
///  Get the dropped-frame(s) flag
template<typename TFill>
unsigned SlidingFrameTimestream<TFill>::
    error_flag() const
{
    return m_error_flag;
}

/////
///  Get the number of pre-fill samples
template<typename TFill>
unsigned SlidingFrameTimestream<TFill>::
	prefill_overlap_size(SlidingFrameTimestream &prior_frame)
{
	/// Assumes both sliding frames have the same sizes, just different buffers
    assert(m_frame_len == prior_frame.m_frame_len);
    assert(m_overlap == prior_frame.m_overlap);
    
	return prior_frame.m_overlap + prior_frame.m_unframed_fill;
}

/////
///  Get the data range of the specified channel time buffer
template<typename TFill>
Range<TFill *> SlidingFrameTimestream<TFill>::
    channel_timebuffer(unsigned chn_idx)
{
    auto begin = &m_fillbuffers.get()[chn_idx * m_fillbuffer_len];
	return Range<TFill *>(begin, begin + m_fillbuffer_len);
}

/////
///  Get the data range of the gain calibration constant (for scaling ADU --> V)
template<typename TFill>
Range<double *> SlidingFrameTimestream<TFill>::
	gain_buffer(unsigned chn)
{
    auto begin = &m_gain.get()[chn];
	return Range<double *>(begin, 1);
}

/////
///  Get the data range of the gain calibration constant (for scaling ADU --> V)
template<typename TFill>
Range<double *> SlidingFrameTimestream<TFill>::
	offset_buffer(unsigned chn)
{
    auto begin = &m_offset.get()[chn];
	return Range<double *>(begin, 1);
}


/////
///  Return an iterator over the number of channels
template<typename TFill>
CountingRange SlidingFrameTimestream<TFill>::
    channel_counter() const
{
    return icount(m_channels_num); 
}

/////
///  Get the data range of the specified channel time buffer
template<typename TFill>
Range<const TFill *> SlidingFrameTimestream<TFill>::
    channel_timebuffer(unsigned chn_idx) const
{
    auto begin = &m_fillbuffers.get()[chn_idx * m_fillbuffer_len];
	return Range<const TFill *>(begin, begin + m_fillbuffer_len);
}

/////
///  Get the data range of the gain calibration constant (for scaling ADU --> V)
template<typename TFill>
Range<const double *> SlidingFrameTimestream<TFill>::
	gain_buffer(unsigned chn) const
{
    auto begin = &m_gain.get()[chn];
	return Range<const double *>(begin, 1);
}

/////
///  Get the data range of the gain calibration constant (for scaling ADU --> V)
template<typename TFill>
Range<const double *> SlidingFrameTimestream<TFill>::
	offset_buffer(unsigned chn) const
{
    auto begin = &m_offset.get()[chn];
	return Range<const double *>(begin, 1);
}
/////
///  Return the number of FFT batches within the frame buffer
template<typename TFill>
unsigned SlidingFrameTimestream<TFill>::
	num_frames() const
{
    return m_num_frames;
}

/////
///  Return an iterator over the number of FFT batches
template<typename TFill>
CountingRange SlidingFrameTimestream<TFill>::
	frame_counter() const
{
    return icount(m_num_frames);
}

/////
///  Return the effective (framed) size of the channel buffers
///  (i.e., the number of samples that do fit into an FFT frame)
template<typename TFill>
unsigned SlidingFrameTimestream<TFill>::
    framed_buffer_len() const
{
    return m_fillbuffer_len - m_unframed_fill;
}

/////
/// Sets two endpoints to a range of a frame;
template<typename TFill>
Range<const TFill *> SlidingFrameTimestream<TFill>::
frame_range(unsigned chn, unsigned frame_num) const
{
	auto start = &m_fillbuffers.get()[chn * m_fillbuffer_len] + frame_num * (m_frame_len - m_overlap);
	auto end = start + m_frame_len;
	return Range<const TFill *>(start, end);
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

