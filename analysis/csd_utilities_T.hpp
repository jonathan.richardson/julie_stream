/////
///

#ifndef H_JSTREAM_CSD_UTILITIES_H
#define H_JSTREAM_CSD_UTILITIES_H

namespace JStream {

namespace _detail {


/////
///  Utility function to get the number of CSDs
inline unsigned UDmatrix_num_elements(unsigned matrix_size) 
{
	return (matrix_size * (matrix_size - 1) / 2); 
}

/////
///  Adds the data from one CSD-like object to another CSD-like object
///  (Allows for interfacing between CSDCarrier and CSDAccumulation classes)
template<typename TCSDTo, typename TCSDFrom>
void add_csds_data(TCSDTo &csd_to, const TCSDFrom &csd_from)
{
    /////
    ///  Set/update the metadata
	if(csd_to.accumulation_get() == 0){
        /// If the target instance is uninitialized, set the immutable metadata
        csd_to = TCSDTo(
            csd_from.channels_num(),
            csd_from.length(),
            csd_from.sample_freq_get(),
            csd_from.overlap_frac_get()
        );
        csd_to.normalization_to_ps_set(csd_from.normalization_to_ps_get());
		csd_to.ENBW_set(csd_from.ENBW_get());
        
        /// Set the voltage ranges of the source frame
        for(auto chn_idx : icount(csd_to.channels_num())){
            csd_to.channel_voltage_set(
                chn_idx,
                csd_from.voltage_min(chn_idx),
                csd_from.voltage_max(chn_idx)
            );
        }
	}else{
        /// Verify that the immutable metadata are consistent between the frames
        assert(
            csd_to.channels_num() == csd_from.channels_num() and
            csd_to.length() == csd_from.length() and
            csd_to.normalization_to_ps_get() == csd_from.normalization_to_ps_get() and
            csd_to.ENBW_get() == csd_from.ENBW_get() and
            csd_to.sample_freq_get() == csd_from.sample_freq_get() and
            csd_to.overlap_frac_get() == csd_from.overlap_frac_get() 
        );        
    }
    
    /// Increment the frame accumulation count
    csd_to.sequence_number_set(csd_to.sequence_number_get() + 1);

    /// Update the total frame accumulation
    csd_to.accumulation_set(csd_to.accumulation_get() + csd_from.accumulation_get());

    /// Update the error flag of the cummulative frame
    /// (Takes the highest error level of the two frames)
    auto error_flag = csd_from.error_flag_get();
    if(error_flag > csd_to.error_flag_get()){
        csd_to.error_flag_set(error_flag);
    }

    /// Set the channel voltage ranges
    for(auto chn_idx : icount(csd_to.channels_num())){
        /// Take the furthest range of the two frames
        auto min_from = csd_from.voltage_min(chn_idx);
        auto max_from = csd_from.voltage_max(chn_idx);
        auto min_to = csd_to.voltage_min(chn_idx);        
        auto max_to = csd_to.voltage_max(chn_idx);
        auto min = (min_to < min_from ? min_to : min_from);
        auto max = (max_to > max_from ? max_to : max_from);
        csd_to.channel_voltage_set(chn_idx, min, max);
    }
    
    /// Set the timestamps
    /// (Take the furthest time endpoints spanned by the two frames)
    
    /// Start time
    double time_begin;
    if( (csd_to.timestamp_begin() < csd_from.timestamp_begin()) and
        (csd_to.timestamp_begin() != 0)
    ){
        time_begin = csd_to.timestamp_begin();
    }else{
        time_begin = csd_from.timestamp_begin();
    }

    /// End time
    double time_end;
    if(csd_to.timestamp_end() > csd_from.timestamp_end()){
        time_end = csd_to.timestamp_end();
    }else{
        time_end = csd_from.timestamp_end();
    }

    csd_to.timestamps_set(time_begin, time_end);   

    /////
    ///  Add the PSDs
    for(auto chn_idx : icount(csd_to.channels_num())){
        /// Set the PSD	array pointers
        auto psd = csd_from.psd_get(chn_idx).begin(); 	
        auto accum_psd = csd_to.psd_get(chn_idx).begin(); 
        assert((void *)psd != (void *)accum_psd);

        /// Add the new PSD
        for(unsigned idx = 0; idx < csd_to.length(); idx++){
            accum_psd[idx] += psd[idx];
        }
    }

    /////
    ///  Add the CSDs
    for(auto row_idx : icount(1, csd_to.channels_num())){
        for(auto col_idx : icount(row_idx)){
            /// Set the CSD array pointers
            auto csd = (typename TCSDFrom::TComplex *) __builtin_assume_aligned
                                (csd_from.csd_get(row_idx, col_idx).begin(), 16);
            auto accum_csd = (typename TCSDTo::TComplex *) __builtin_assume_aligned
                                (csd_to.csd_get(row_idx, col_idx).begin(), 16);
            assert((void *)csd != (void *)accum_csd);

            /// Add the new CSD
            for(unsigned idx = 0; idx < csd_to.length(); idx++){
                accum_csd[idx] += csd[idx]; 
            }
        }
    }  
}

} //~namespace _detail

}; //~namespace JStream

#endif //H_JSTREAM_CSD_UTILITIES_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
