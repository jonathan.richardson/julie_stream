

FUNCTION(HYPER_OPTIMIZE source_file)
    SET(FAST_OPT "-O3")
    SET(FAST_OPT "${FAST_OPT} -fomit-frame-pointer")
    SET(FAST_OPT "${FAST_OPT} -march=native")
    SET(FAST_OPT "${FAST_OPT} -mtune=native")
    SET(FAST_OPT "${FAST_OPT} -mfpmath=both")
    SET(FAST_OPT "${FAST_OPT} -ftree-vectorize")
    #SET(FAST_OPT "${FAST_OPT} -ftree-vectorizer-verbose=8") #makes GCC say a lot about its vectorization optimizations
    SET(FAST_OPT "${FAST_OPT} -funroll-loops")
    SET(FAST_OPT "${FAST_OPT} -fstrict-aliasing")
    SET(FAST_OPT "${FAST_OPT} -fstrict-overflow")
    SET(FAST_OPT "${FAST_OPT} -malign-double")
    SET(FAST_OPT "${FAST_OPT} -ffast-math")
    SET(FAST_OPT "${FAST_OPT} -fassociative-math")
    SET(FAST_OPT "${FAST_OPT} -freciprocal-math")
    SET(FAST_OPT "${FAST_OPT} -fno-signed-zeros")
    SET(FAST_OPT "${FAST_OPT} -fsingle-precision-constant")
    SET(FAST_OPT "${FAST_OPT} -fvariable-expansion-in-unroller")
    SET(FAST_OPT "${FAST_OPT} -ftracer")
    SET(FAST_OPT "${FAST_OPT} -fselective-scheduling")
    SET(FAST_OPT "${FAST_OPT} -fsel-sched-pipelining")
    SET(FAST_OPT "${FAST_OPT} -fsel-sched-pipelining-outer-loops")
    SET(FAST_OPT "${FAST_OPT} -frename-registers")
    SET(FAST_OPT "${FAST_OPT} -fschedule-insns") #broken in mingw32 gcc4.8
    SET(FAST_OPT "${FAST_OPT} -fschedule-insns2")
    SET(FAST_OPT "${FAST_OPT} -fsched-stalled-insns=3")
    SET(FAST_OPT "${FAST_OPT} -fsched-stalled-insns-dep=3")
    SET(FAST_OPT "${FAST_OPT} --param inline-unit-growth=200")
    SET(FAST_OPT "${FAST_OPT} --param l2-cache-size=2048")
    SET(FAST_OPT "${FAST_OPT} --param l1-cache-size=64")

    #SET(FAST_OPT " ") #cancels all of the settings (if we need to debug)

    SET_SOURCE_FILES_PROPERTIES(${source_file} COMPILE_FLAGS ${FAST_OPT})
ENDFUNCTION()
