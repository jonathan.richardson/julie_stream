

FUNCTION(EXPORT_PROJECT PRIMARY_TARGET)

    ###############################################################################
    # Create the ${PROJECT_NAME}Config.cmake and ${PROJECT_NAME}ConfigVersion files
    ###############################################################################

    # Export the package for use from the build-tree
    # (this registers the build-tree with a global CMake-registry)
    export(PACKAGE ${PROJECT_NAME})

    # ... for the build tree
    IF(NOT ${PROJECT_NAME}_INCLUDE_DIRS)
        MESSAGE("${PROJECT_NAME}_INCLUDE_DIRS is empty, the packaging will not locate those directories")
    ENDIF()

    SET(PROJECT_INCLUDE_DIRS ${${PROJECT_NAME}_INCLUDE_DIRS})
    CONFIGURE_FILE(projectConfig.cmake.in
      "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake" @ONLY)

    # ... for the install tree
    FILE(RELATIVE_PATH REL_INCLUDE_DIR "${INSTALL_CMAKE_DIR_ABS}" "${INSTALL_INCLUDE_DIR_ABS}")
    SET(PROJECT_INCLUDE_DIRS "\${${PROJECT_NAME}_CMAKE_DIR}/${REL_INCLUDE_DIR}")
    CONFIGURE_FILE(projectConfig.cmake.in
      "${PROJECT_BINARY_DIR}/${CMAKE_FILES_DIRECTORY}/${PROJECT_NAME}Config.cmake" @ONLY)

    # ... for both
    CONFIGURE_FILE(projectConfigVersion.cmake.in
      "${PROJECT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake" @ONLY)

    # Install the ${PROJECT_NAME}Config.cmake and ${PROJECT_NAME}ConfigVersion.cmake
    INSTALL(FILES
      "${PROJECT_BINARY_DIR}/${CMAKE_FILES_DIRECTORY}/${PROJECT_NAME}Config.cmake"
      "${PROJECT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"
      DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev)

    # Install the export set for use with the install-tree
    INSTALL(EXPORT ${PROJECT_NAME}Targets DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev)

ENDFUNCTION()


