
IF(DEFINED ENV{SDT_PATH})
    FILE(TO_CMAKE_PATH "$ENV{SDT_PATH}" SDT_PATH)
ELSE()
    SET(SDT_PATH)
ENDIF()
SET(SDT_PATH ${SDT_PATH}
    CACHE STRING "Root path to locate SDT"
    )

#attempt to find the package through the package config registry
#using the CONFIG invocation of FIND_PACKAGE
FIND_PACKAGE(SDT CONFIG QUIET
    HINTS
    ${SDT_PATH}
    )

IF(NOT ${SDT_FOUND})
    IF(SDT_PATH)
        SET(SDT_INCLUDE_PATH 
            "${SDT_PATH}/include" 
            CACHE STRING "Directory hint for SDT include files"
            )
        SET(SDT_LIBRARY_PATH
            "${SDT_PATH}/lib"
            CACHE STRING "Directory hint for SDT libraries"
            )
    ENDIF()

    SET(SDT_LIBRARY_NAME SDT)
    FIND_LIBRARY(SDT_LIBRARY 
        ${SDT_LIBRARY_NAME}
        HINTS
            ${SDT_LIBRARY_PATH}
            ${SDT_PATH}
        )

    FIND_PATH(SDT_INCLUDE_DIRS SDT/json.h 
        HINTS 
            ${SDT_INCLUDE_PATH}
            ${SDT_PATH}
        )

    IF(SDT_LIBRARY AND SDT_INCLUDE_DIRS)
        SET(SDT_FOUND TRUE)
    ELSE()
        SET(SDT_FOUND FALSE)
    ENDIF()
ENDIF()

