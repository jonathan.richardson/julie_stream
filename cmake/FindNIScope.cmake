
IF(DEFINED ENV{NISCOPE_PATH})
    SET(NISCOPE_PATH 
        "$ENV{NISCOPE_PATH}"
        CACHE STRING "NIScope base path")
        
ELSE()
    IF(CMAKE_SYSTEM_NAME MATCHES Windows)
    SET(NISCOPE_PATH 
        "${IVIROOTDIR64}"
        CACHE STRING "NIScope base path"
        )
    ENDIF()
ENDIF()

IF(NISCOPE_PATH)
    SET(NISCOPE_INCLUDE_PATH 
        "${NISCOPE_PATH}/Include"
        CACHE STRING "NIScope path for includes"
        )
    SET(NISCOPE_LIBRARY_PATH 
        "${NISCOPE_PATH}/Bin"
        CACHE STRING "NIScope path for libraries"
        )
ENDIF()

SET(NIScope_LIBRARY_NAME niScope_64)
FIND_LIBRARY(NIScope_LIBRARY 
    ${NIScope_LIBRARY_NAME} 
    HINTS
        ${NISCOPE_LIBRARY_PATH} 
    NO_DEFAULT_PATH
    )

FIND_PATH(NIScope_INCLUDE_DIR niScope.h 
    HINTS
        ${NISCOPE_INCLUDE_PATH} 
    NO_DEFAULT_PATH
    )

IF(NIScope_LIBRARY AND NIScope_INCLUDE_DIR)
    SET(NIScope_FOUND TRUE)	
ELSE()
    SET(NIScope_FOUND FALSE)	
ENDIF()