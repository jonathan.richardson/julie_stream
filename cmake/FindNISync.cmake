
IF(DEFINED ENV{NISYNC_PATH})
    SET(NISYNC_PATH 
        "$ENV{NISYNC_PATH}"
        CACHE STRING "NISync base path")
        
ELSE()
    IF(CMAKE_SYSTEM_NAME MATCHES Windows)
    SET(NISYNC_PATH 
        "${VXIPNPPATH64}/Win64"
        CACHE STRING "NISync base path"
        )
    ENDIF()
ENDIF()

IF(NISYNC_PATH)
    SET(NISYNC_INCLUDE_PATH 
        "${NISYNC_PATH}/Include"
        CACHE STRING "NISync path for includes"
        )
    SET(NISYNC_LIBRARY_PATH
        "${NISYNC_PATH}/Bin"
        CACHE STRING "NISync path for libraries"
        )
ENDIF()

SET(NISync_LIBRARY_NAME niSync)

FIND_LIBRARY(NISync_LIBRARY 
    ${NISync_LIBRARY_NAME} 
    HINTS
        ${NISYNC_LIBRARY_PATH} 
    NO_DEFAULT_PATH
    )

FIND_PATH(NISync_INCLUDE_DIR niSync.h 
    HINTS
        ${NISYNC_INCLUDE_PATH} 
    NO_DEFAULT_PATH
    )

IF(NISync_LIBRARY AND NISync_INCLUDE_DIR)
    SET(NISync_FOUND TRUE)	
ELSE()
    SET(NISync_FOUND FALSE)	
ENDIF()