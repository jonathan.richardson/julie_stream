
IF(DEFINED ENV{FFTWF_PATH})
    SET(FFTWf_PATH "$ENV{FFTWF_PATH}"
        CACHE STRING "Root path to locate FFTWf"
        )
ELSE()
    IF(CMAKE_SYSTEM_NAME MATCHES Windows)
    SET(FFTWf_PATH "C:/jstream_environment/fftw3"
        CACHE STRING "Root path to locate FFTWf"
        )
    ENDIF()
ENDIF()

IF(FFTWf_PATH)
    SET(FFTWf_INCLUDE_PATH 
        "${FFTWf_PATH}/include" 
        CACHE STRING "Directory hint for FFTWf include files"
        )
    SET(FFTWf_LIBRARY_PATH
        "${FFTWf_PATH}/lib"
        CACHE STRING "Directory hint for FFTWf libraries"
        )
ENDIF()

IF(CMAKE_SYSTEM_NAME MATCHES Windows)
    SET(FFTWf_LIBRARY_NAME libfftw3f-3)
ELSE()
    SET(FFTWf_LIBRARY_NAME fftw3f)
ENDIF()
FIND_LIBRARY(
    FFTWf_LIBRARY 
    ${FFTWf_LIBRARY_NAME}
    HINTS
        ${FFTWf_LIBRARY_PATH}
    )
FIND_PATH(FFTWf_INCLUDE_DIR fftw3.h 
    HINTS 
        ${FFTWf_INCLUDE_PATH}
    )

IF(FFTWf_LIBRARY AND FFTWf_INCLUDE_DIR)
    SET(FFTWf_FOUND TRUE)
ELSE()
    MESSAGE("Could not find FFTWf.")
    SET(FFTWf_FOUND FALSE)
ENDIF()