
IF(DEFINED ENV{HWLOC_PATH})
    SET(HWLOC_PATH "$ENV{HWLOC_PATH}"
        CACHE STRING "Root path to locate hwloc"
        )
ELSE()
    IF(CMAKE_SYSTEM_NAME MATCHES Windows)
    SET(HWLOC_PATH "C:/jstream_environment/hwloc/hwloc-win64-build-1.8.1"
        CACHE STRING "Root path to locate hwloc"
        )
    ENDIF()
ENDIF()

IF(HWLOC_PATH)
    SET(HWLOC_INCLUDE_PATH 
        "${HWLOC_PATH}/include" 
        CACHE STRING "Directory hint for hwloc include files"
        )
    SET(HWLOC_LIBRARY_PATH
        "${HWLOC_PATH}/bin"
        CACHE STRING "Directory hint for hwloc libraries"
        )
ENDIF()

IF(CMAKE_SYSTEM_NAME MATCHES Windows)
    SET(HWLOC_LIBRARY_NAME libhwloc-5)
ELSE()
    SET(HWLOC_LIBRARY_NAME hwloc)
ENDIF()
FIND_LIBRARY(
    HWLOC_LIBRARY 
    ${HWLOC_LIBRARY_NAME} 
    HINTS
        ${HWLOC_LIBRARY_PATH}
    )

FIND_PATH(
    HWLOC_INCLUDE_DIR hwloc.h 
    HINTS 
        ${HWLOC_INCLUDE_PATH}
    )

IF(HWLOC_LIBRARY AND HWLOC_INCLUDE_DIR)
    SET(HWLOC_FOUND TRUE)
ELSE()
    MESSAGE("Hwloc Library not Found, cmake HWLOC_PATH: ${HWLOC_PATH} and ")
    SET(HWLOC_FOUND FALSE)
ENDIF()

