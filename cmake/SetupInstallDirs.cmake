

MACRO(SETUP_INSTALL_DIRS)

    # Offer the user the choice of overriding the installation directories
    SET(INSTALL_LIB_DIR lib CACHE PATH "Installation directory for libraries (relative to CMAKE_INSTALL_PREFIX)")
    SET(INSTALL_BIN_DIR bin CACHE PATH "Installation directory for executables (relative to CMAKE_INSTALL_PREFIX)")
    SET(INSTALL_INCLUDE_DIR include CACHE PATH "Installation directory for header files (relative to CMAKE_INSTALL_PREFIX)")

    IF(WIN32 AND NOT CYGWIN)
      SET(DEF_INSTALL_CMAKE_DIR CMake)
    ELSE()
      SET(DEF_INSTALL_CMAKE_DIR lib/CMake/${PROJECT_NAME})
    ENDIF()
    SET(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR} CACHE PATH "Installation directory for CMake files (relative to CMAKE_INSTALL_PREFIX)")

    # Make relative paths absolute (needed later on)
    FOREACH(p LIB BIN INCLUDE CMAKE)
      SET(var INSTALL_${p}_DIR)
      IF(NOT IS_ABSOLUTE "${${var}}")
          SET(${var}_ABS "${CMAKE_INSTALL_PREFIX}/${${var}}")
      ELSE()
          SET(${var}_ABS "$${${var}}")
      ENDIF()
    ENDFOREACH()

ENDMACRO()



