////
///

#ifndef H_DATA_SOURCE_BASE_H
#define H_DATA_SOURCE_BASE_H

#include <iostream>

#include "utilities/range.hpp"

namespace JStream {

#define STR_LEN 30

/////
///  Data source base class
class IDataSource
{
public:
    /// Data source settings container type
    typedef struct {
        char    adc_name[STR_LEN];                  //ADC MAX name
        char    adc_type[STR_LEN];                  //ADC type
        char    pll_status[STR_LEN];                //Phase-lock loop to GPS reference clock
        double	sample_freq             = -1.0;	    //Sampling frequency (Hz)
        double	input_range	            = -1.0;	    //Input signal range (V)
        double	dc_offset               = -1.0;	    //DC offset (V)
        double	scaling                 = -1.0;	    //Scaling factor for input signal
        double	bandwidth               = -1.0; 	//Maximum frequency (bandwidth, Hz)
        double 	impedance               = -1.0; 	//Channel impedance	
        int  	coupling  	            = -1;	    //Channel coupling mode
    } DataSourceSettings;

    /// Destructor
    virtual ~IDataSource() {};
	
    /// Return the number of channels on the device (one source) 
    virtual size_t channels_num() = 0;

    ///  This method states whether fetchers should drop data to maintain realtime operation
    virtual bool realtime_is() = 0;

    /// Return the sample frequency
    /// (This may return a negative number to represent an unimportant/scalable sample rate)
    virtual double sample_freq() = 0;

	/// Get the number of 5122 fetch operations required to fill a channel buffer
	/// (Not required for virtual fetcher)
	virtual size_t get_fetch_size() {};
	
	/// Set the absolute future time to start the DAQ on each 5122
	/// (Not required for virtual fetcher)
	virtual void arm_start_trigger() {};

	/// Get the time interval between 5122 samples (sec)
	/// (NOT required for virtual fetcher)
	virtual uint64_t time_increment() = 0;
    
    /// Return the data source settings
    virtual const DataSourceSettings all_settings() = 0;
    
    /// Return the DAQ start time (trigger time for real sources)
    virtual unsigned trigger_time() = 0;
    
    /// Return the decimation factor for digital down sampling
    virtual unsigned decimation() = 0;    

    /// Start/stop the DAQ
    virtual void   fetching_start() = 0;
    virtual void   fetching_finish() = 0;

    /// Fetch a batch of data
    virtual size_t fetch_data(
        uint8_t chn, 
        Range<int16_t *>  &data_fetch,
        Range<uint64_t *> &time_fetch,
        Range<double *>   &gain_fetch,
        Range<double *>   &offset_fetch
	) = 0;
};

}; //~namespace JStream

#endif //H_DATA_SOURCE_BASE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

