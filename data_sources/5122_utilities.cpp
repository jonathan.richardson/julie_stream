////
///

#include "5122_utilities.hpp"

namespace JStream {


/////
///  Checks for consistency in the 5122 settings between all data sources
///  Returns a warning for every inconsistent setting found
void config_error_checker(ConfigTreeAccess& hw_config)
{
    auto logger         = uLogger::logger_root().child("5122_config_checker");
	auto num_sources    = hw_config.array_size();

	/// No need to check consistency for only one data source
	if(num_sources == 1){
        return;
    }
	
	string		warning_msg = "Did you really mean to set this?\n\n";

    double  	input_range[num_sources];
	double      dc_offset  [num_sources];
	double      scaling    [num_sources];
	double      bandwidth  [num_sources];
				
	string		impedance  [num_sources]; 
    string      coupling   [num_sources];
	string      name_5122  [num_sources];
				
	bool		err_input_range	= false;
	bool        err_dc_offset	= false;
	bool        err_scaling 	= false;
	bool        err_bandwidth	= false;
	bool        err_coupling 	= false;
	bool        err_impedance 	= false;

	/// Load the 5122 settings for each source				
	for(auto source_idx : icount(num_sources)){
		auto config = hw_config[source_idx];
		config["source_name"].get(name_5122[source_idx]);
		config["input_range"].get(input_range[source_idx]);
		config["dc_offset"].get(dc_offset[source_idx]);
		config["scaling"].get(scaling[source_idx]);
		config["bandwidth"].get(bandwidth[source_idx]);
		config["coupling"].get(coupling[source_idx]);
		config["impedance"].get(impedance[source_idx]);		
	}
	
	/// Check whether any settings are inconsistent between the sources and flag them
	for(auto source_idx : icount(num_sources - 1)){
		if(input_range[source_idx] != input_range[source_idx+1]){
            err_input_range = true;
        }
		if(dc_offset[source_idx] != dc_offset[source_idx+1]){
            err_dc_offset   = true;
        }
		if(scaling[source_idx] != scaling[source_idx+1]){
            err_scaling     = true; 
        }
		if(bandwidth[source_idx] != bandwidth[source_idx+1]){
            err_bandwidth   = true; 
        }
		if(coupling[source_idx] != coupling[source_idx+1]){
            err_coupling    = true;
        }
		if(impedance[source_idx] != impedance[source_idx+1]){
            err_impedance   = true;
        }
	}

	/// Print a warning for any setting(s) found to be inconsistent
	if(err_input_range){
        std::stringstream SS;
		for(auto source_idx : icount(num_sources)){
			SS << "\t\tInput range = +/-" << input_range[source_idx]/2 << " V (" << name_5122[source_idx] << ")\n";
        }
        logger.warning(warning_msg, SS.str());
	}
	if(err_dc_offset){
		std::stringstream SS;
		for(auto source_idx : icount(num_sources)){
			SS << "\t\tDC offset = " << dc_offset[source_idx] << " V (" << name_5122[source_idx] << ")\n";
        }
        logger.warning(warning_msg, SS.str());
	}	
	if(err_scaling){
		std::stringstream SS;
		for(auto source_idx : icount(num_sources)){
			SS << "\t\tScaling factor = " << scaling[source_idx] << " (" << name_5122[source_idx] << ")\n"; 
        }
        logger.warning(warning_msg, SS.str());
	}
	if(err_bandwidth){
		std::stringstream SS;
		for(auto source_idx : icount(num_sources)){
			SS << "\t\tBandwidth = " << bandwidth[source_idx]/1e6 << " MHz (" << name_5122[source_idx] << ")\n"; 
        }
        logger.warning(warning_msg, SS.str());
	}
	if(err_coupling){
		std::stringstream SS;
		for(auto source_idx : icount(num_sources)){
			SS << "\t\tCoupling = " << coupling[source_idx] << " (" << name_5122[source_idx] << ")\n"; 
        }
        logger.warning(warning_msg, SS.str());
	}
	if(err_impedance){
		std::stringstream SS;
		for(auto source_idx : icount(num_sources)){
			SS << "\t\tImpedance = " << impedance[source_idx] << " (" << name_5122[source_idx] << ")\n"; 
        }
        logger.warning(warning_msg, SS.str());
	}
}
    
}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
