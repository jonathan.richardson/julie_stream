////
///

#ifndef H_5122_READER_H
#define H_5122_READER_H

#include "jstream/config.hpp"
#if JSTREAM_HAS_NISCOPE

#include <sstream>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <cstring>

#include <string>

#include <windows.h>

#include <niScope.h>
#include <niSync.h>

#include "utilities/config_tree/config_tree_json.hpp"
#include "utilities/range.hpp"

#include "data_source_base.hpp"

namespace JStream {

using namespace ConfigTree;
using std::string;


/////
///  5122 Data Source Class
class NIScope5122 : public IDataSource
{
    /// Logger
    LoggerAccess        m_logger;

	/// Device identifiers
    std::string         m_resource_name_5122;
    std::string         m_resource_name_6682;
    ViSession	        m_vi_5122				= 0;	
    ViSession           m_vi_6682 				= 0;
	
	/// Enable/disable switches
    bool     	        m_use_realtime_mode		= false; //Enable realtime mode (allows frame dropping)
	bool		        m_use_start_trig    	= false; //Enable simultaneous start trigger (requires 6682H)
	bool		        m_use_gps_sync 			= false; //Enable GPS clock synchronization (requires 6682H)
    bool                m_calibrate_5122        = false; //Enable 5122 self-calibration at start up
    bool                m_use_down_sampling     = false; //Enable sampling frequency decimation
    
	/// 5122 device settings
	DataSourceSettings  m_settings;		                 //Contains all adjustable 5122 settings
	size_t   	        m_fetch_size      		= 1<<19; //The number of samples read in one ADC fetch
    double              m_sample_clock_offset   = 0.0;   //The offset of the 5122 sample clock from zero at the trigger time
    unsigned            m_decimation            = 1;     //The decimation factor for digital down sampling
    
	/// 6682H device settings
	unsigned  	        m_trigger_delay_s		= 6;	 //Time delay before the start trigger is sent (sec)
    unsigned            m_trigger_time          = 0;     //Absolute trigger time (sec)

public:
	/// Constructor
	/// (Imports its own configuration from JSON)
    NIScope5122(
		ConfigTreeAccess& basic_conf_data,
		unsigned short source_idx,
        size_t fetch_size,
        unsigned trigger_delay,
		long unsigned& trigger_time,
        LoggerAccess logger
	);

    ~NIScope5122();
    
    /// Sets the 5122 JSON configuration
    void _json_config_set(
		ConfigTreeAccess& basic_conf_data,
        unsigned short source_idx
    );
    
	/// Creates the trigger sent from the 6682Hs to the 5122s
    /// (Allows DAQ to be simultaneously started on all 5122s)
	void arm_start_trigger();

	/// Starts/stops the DAQ into the 5122 onboard buffers
    void fetching_start();
    void fetching_finish();

	/// Fetches a batch of data from the 5122 onboard channel buffer
    size_t fetch_data(
		uint8_t chn, 
		Range<int16_t *>  &data_fetch, 
		Range<uint64_t *> &time_fetch,
		Range<double *>   &gain_fetch,
		Range<double *>   &offset_fetch
	);
	
	/// Utility functions to return DAQ info
    size_t      channels_num();		        //Number of channels on the 5122
    bool        realtime_is();		        //True if realtime mode (frame dropping) is enabled
    double      sample_freq();		        //The *actual* sampling frequency (Hz)
	size_t      get_fetch_size();	        //The number of samples read in one fetch
    uint64_t    time_increment();           //The time interval between 5122 samples (ns)
	unsigned    trigger_time();             //6682 start trigger time (ns)
    const DataSourceSettings 
                all_settings();             //Return all 5122 settings
    unsigned    decimation();               //Decimation factor for digital down sampling

protected:
	/// Initialize/configure 5122 and 6682H hardware
	void _initSync();
    void _initScope();
   
    /// Sets the absolute DAQ start time
    void _start_time_set(long unsigned& trigger_time);    
    
    /// Utility function for converting to absolute-valued timestamps
    inline uint64_t _convert_to_absolute_ns(double timestamp);
};

}; //~namespace JStream

#endif //~JSTREAM_HAS_NISCOPE

#endif //H_5122_READER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
