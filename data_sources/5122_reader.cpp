////
///

#include "5122_reader.hpp"

namespace JStream {


/////
///  CONSTRUCTOR
///  (Imports its own configuration from json)
NIScope5122::
    NIScope5122(
		ConfigTreeAccess& basic_conf_data,
		unsigned short source_idx,
        size_t fetch_size,
        unsigned trigger_delay,
		long unsigned& trigger_time,
        LoggerAccess logger
	) :
        m_fetch_size(fetch_size),
        m_trigger_delay_s(trigger_delay),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
    
    /// Set the JSON configuration
    this->_json_config_set(basic_conf_data, source_idx);
		
	/// Configure the devices
	_initSync();        //6682H
	_initScope();       //5122
    
    /// Set the DAQ start time
    _start_time_set(trigger_time);
}

/////
///  Set the JSON configuration
void NIScope5122::
    _json_config_set(
		ConfigTreeAccess& basic_conf_data,
        unsigned short source_idx
    )
{
	{	/// First get the enabled hardware functionality
        auto config = basic_conf_data["data_sources"];
        
        /// Enable/disable run modes
		config["use_realtime_mode"].get(m_use_realtime_mode); 
		config["use_gps_sync"].get(m_use_gps_sync);	  
        config["use_start_trig"].get(m_use_start_trig);
		config["calibrate_5122"].get(m_calibrate_5122);
        config["use_down_sampling"].get(m_use_down_sampling);   
        if(source_idx == 0){
            /// Only the first device needs to report these global settings
			if(not m_use_realtime_mode){
                m_logger.action("Real-time mode manually disabled"); 
            }
			if(not m_use_gps_sync){ 
                m_logger.action("GPS clock synchronization manually disabled");
            }            
            if(not m_use_start_trig){             
                m_logger.action("Simultaneous start triggering manually disabled");  
            }
            if(not m_use_down_sampling){
                m_logger.config("Digital down-sampling manually disabled");
            }
        }
	}
    
	{	/// Import the basic hardware settings
        auto config = basic_conf_data["data_sources"]["devices"][source_idx];

		/// Get the device names
		config["source_name"].get(m_resource_name_5122);	//5122
		config["gps_name"].get(m_resource_name_6682);	    //6682H

		/// Get the 5122 settings
        config["sample_freq"].get(m_settings.sample_freq);  //Sample frequency (Hz)
		config["input_range"].get(m_settings.input_range);  //Input signal range (V)	
		config["dc_offset"].get(m_settings.dc_offset);      //DC offset (V)
		config["scaling"].get(m_settings.scaling);	        //Scaling factor for input signal
		config["bandwidth"].get(m_settings.bandwidth);      //Maximum frequency (bandwidth, in Hz)

		/// Channel coupling mode
		std::string coupling_str;
		config["coupling"].get(coupling_str);
		if(coupling_str == "NISCOPE_VAL_DC"){
            m_settings.coupling = NISCOPE_VAL_DC;
        }else if(coupling_str == "NISCOPE_VAL_AC"){
            m_settings.coupling = NISCOPE_VAL_AC;
        }else if(coupling_str == "NISCOPE_VAL_GND"){
            m_settings.coupling = NISCOPE_VAL_GND; 
        }else{
			m_logger.error(
                "Invalid coupling mode specified, setting default\n",
                "Valid options:\n\t", 
                "NISCOPE_VAL_DC\n\t", 
                "NISCOPE_VAL_AC\n\t", 
                "NISCOPE_VAL_GND"
            );
		}

		/// Channel impedance
		string impedance_str;
		config["impedance"].get(impedance_str);
		if(impedance_str == "NISCOPE_VAL_1_MEG_OHM"){
            m_settings.impedance = NISCOPE_VAL_1_MEG_OHM; 
        }else if(impedance_str == "NISCOPE_VAL_50_OHMS"){
            m_settings.impedance = NISCOPE_VAL_50_OHMS;
        }else{
            m_logger.error(
                "Invalid channel impedance specified, setting default\n",
                "Valid options:\n\t",
                "NISCOPE_VAL_50_OHMS\n\t",
                "NISCOPE_VAL_1_MEG_OHM"
            );
		}
	}
}

/////
///  DESTRUCTOR
NIScope5122::
    ~NIScope5122()
{
    niScope_close(m_vi_5122);
	niSync_close(m_vi_6682);	
}

/////
///  CONFIGURE THE 6682H
void NIScope5122::
    _initSync()
{
	/// Return immediately if the 6682H is totally disabled
	if( (not m_use_start_trig) and 
        (not m_use_gps_sync)
    ){
        return;
    }

    ViStatus 	error = VI_SUCCESS;
    ViChar  	errorSource[MAX_FUNCTION_NAME_SIZE];
    ViChar   	errorMessage[MAX_ERROR_DESCRIPTION] = " ";

	ViInt32	 	gps_status  = 0;
	ViInt32	 	num_sat     = 0;
	ViUInt32 	clock_ticks = 50; //This fixes the 6682H clock frequency to 1 MHz (not modifiable)
    std::string gps_string;

	/// Initialize the 6682H
    handleErr(
        niSync_init(
			const_cast<char *>(m_resource_name_6682.c_str()), 
            NISCOPE_VAL_FALSE, 
            NISCOPE_VAL_FALSE, 
            &m_vi_6682
        )
    );

    /// Discipline the onboard clock to GPS
    handleErr(niSync_SetTimeReferenceGPS(m_vi_6682));

    /// Check the GPS status
    handleErr(
        niSync_GetAttributeViInt32(
            m_vi_6682, 
            "", 
            NISYNC_ATTR_GPS_STATUS, 
            &gps_status
        )
    );
    handleErr(
        niSync_GetAttributeViInt32(
            m_vi_6682, 
            "", 
            NISYNC_ATTR_GPS_SATELLITES_AVAILABLE, 
            &num_sat
        )
    );
    if((gps_status != 10) or 
       (num_sat < 4) 
    ){
        m_logger.warning("Weak GPS signal on ", m_resource_name_6682, ". Timestamps may be inaccurate");
    }

	/// Start outputting a GPS-disciplined reference clock for the 5122 (1 MHz)
	if(m_use_gps_sync){		 
        handleErr(
            niSync_CreateClock(
                m_vi_6682, 
                NISYNC_VAL_PFI0, 
                clock_ticks, 
                clock_ticks, 
                0, 0, 0, 0, 0, 0
            )
        );
	}

	/// Reporting
    if(gps_status == 10){
        std::stringstream SS;
        SS << "GPS status:\tReady";
        gps_string = SS.str();
	}else{
        std::stringstream SS;
        SS << "GPS status:\t" << gps_status;
        gps_string = SS.str();
    }
    m_logger[2].config(
        "GPS Receiver:\t", m_resource_name_6682, "\n\t",
        gps_string, "\n\t",
        "Tracking:\t", num_sat, " satellites"
    );
    return;
Error:
    {   std::stringstream SS;
        SS << "Device:\t" << m_resource_name_6682 << "\n";
		niSync_error_message(m_vi_6682, error, errorMessage);
        SS << "\tError:\t" << errorMessage << std::endl;
        m_logger.fatal(SS.str());
        throw std::runtime_error("6682 initialization failure");
    }
}

/////
///  CONFIGURE THE 5122
void NIScope5122::
    _initScope()
{
    ViStatus 	    error = VI_SUCCESS;
    ViChar  	    errorSource[MAX_FUNCTION_NAME_SIZE],
                    errorMessage[MAX_ERROR_DESCRIPTION],
                    device_type[MAX_FUNCTION_NAME_SIZE];
    ViConstString	channel_list 	= "0,1";	
	ViBoolean		phase_locked 	= VI_FALSE;
    std::string     impedance_string,
                    coupling_string,
                    pll_string;
                    
    //Specifies the reference clock frequency (Must be 1MHz)
	ViReal64 		f_ref 			= 1000000.0; 

	/// Initialize the 5122
    m_resource_name_5122.copy(
        m_settings.adc_name, 
        (m_resource_name_5122.size() < STR_LEN)? m_resource_name_5122.size() : STR_LEN
    );
    handleErr(
        niScope_init(
            const_cast<char *>(m_resource_name_5122.c_str()), 
            NISCOPE_VAL_FALSE, 
            NISCOPE_VAL_FALSE, 
            &m_vi_5122
        )
    );

    /// Get the device type
    handleErr(
        niScope_GetAttributeViString(
            m_vi_5122, 
            VI_NULL, 
            NISCOPE_ATTR_INSTRUMENT_MODEL, 
            MAX_FUNCTION_NAME_SIZE, 
            device_type
        )
    );
    std::strncpy(
        m_settings.adc_type, 
        device_type,
        STR_LEN
    );
			 
	/// Perform a self-calibration, if the flag has been set
	if(m_calibrate_5122){
        m_logger.action("Self-calibrating ", m_resource_name_5122, "...");
		handleErr(
            niScope_CalSelfCalibrate(
                m_vi_5122, 
                channel_list,
                VI_NULL
            )
        );
		m_logger.digest("Calibration of ", m_resource_name_5122, " complete");
	}

	/// Configure the vertical settings
    handleErr(
        niScope_ConfigureVertical(
            m_vi_5122,
            channel_list,
            m_settings.input_range,
            m_settings.dc_offset,
            m_settings.coupling,
            m_settings.scaling,
            VI_TRUE
        )
    );

	/// Get the actual input voltage range (may be larger than config value)
	handleErr(
        niScope_GetAttributeViReal64(
            m_vi_5122, 
            "",
            NISCOPE_ATTR_VERTICAL_RANGE,
            &m_settings.input_range
        )
    );

	/// Configure the channel properties
    handleErr(
        niScope_ConfigureChanCharacteristics(
            m_vi_5122,
            channel_list,
            m_settings.impedance,
            m_settings.bandwidth
        )
    );
						 
	/// Get the actual bandwidth (may be larger than config value)
	handleErr(
        niScope_GetAttributeViReal64(
            m_vi_5122, 
            channel_list, 
            NISCOPE_ATTR_MAX_INPUT_FREQUENCY, 
            &m_settings.bandwidth
        )
    );	

	/// Configure the timing properties
	/// *Note: The record parameters have no use in cts acquisition mode
    if(m_use_down_sampling){
        /// Force the full sample rate (100 MHz)
        /// (Timestream will be digitally decimated by this factor downstream)
        m_decimation = 1.0E8 / m_settings.sample_freq;
        m_settings.sample_freq = 1.0E8;
    }
    handleErr(
        niScope_ConfigureHorizontalTiming(
            m_vi_5122,
            m_settings.sample_freq,
            1, 0.0, 1,
            VI_TRUE
        )
    );
						  
    /// Get the actual sampling frequency of the hardware
    handleErr(
        niScope_SampleRate(
            m_vi_5122, 
            &m_settings.sample_freq
        )
    );

	/// Configure the stop trigger (never sent in continuous acquisition mode)
    handleErr(
        niScope_ConfigureTriggerSoftware(
            m_vi_5122,
            0.0, 0.0
        )
    );

	/// Set read pointer for continuous acquisition
    handleErr(
        niScope_SetAttributeViInt32(
            m_vi_5122,
            VI_NULL,
            NISCOPE_ATTR_FETCH_RELATIVE_TO,
            NISCOPE_VAL_READ_POINTER
        )
    );
					
	/// Configure the start trigger (will be sent by 6682Hs to both 5122s simultaneously)
	/// If disabled, each 5122 starts DAQ immediately
	if(m_use_start_trig){
		handleErr(
            niScope_SetAttributeViString(
                m_vi_5122, 
                "", 
				NISCOPE_ATTR_ACQ_ARM_SOURCE, 
				NISCOPE_VAL_RTSI_0
            )
        );
	}

	/// Phase lock to the GPS-disciplined reference clock
	/// If disabled, each 5122 sample clock free-runs
	if(m_use_gps_sync){
		/// Set the reference clock frequency
		niScope_SetAttributeViReal64(
            m_vi_5122, 
            "", 
            NISCOPE_ATTR_REF_CLK_RATE, 
            f_ref
        );

		/// Configure the reference clock
		handleErr(
            niScope_ConfigureClock(
                m_vi_5122, 
                NISCOPE_VAL_CLK_IN, 
                NISCOPE_VAL_NO_SOURCE, 
                NISCOPE_VAL_NO_SOURCE, 
                VI_FALSE
            )
        );

		/// Apply the clock settings to hardware now
		handleErr(niScope_Commit(m_vi_5122));

		/// Check whether the sample clock is now phase-locked to the reference clock
		niScope_GetAttributeViBoolean(
            m_vi_5122, 
            "", 
            NISCOPE_ATTR_PLL_LOCK_STATUS, 
            &phase_locked
        );
		if(not phase_locked){
			std::stringstream SS;
			SS << m_resource_name_5122 << " unable to phase lock to reference clock!\n";
            m_logger.fatal(SS.str());
			throw std::runtime_error(SS.str());
		}
	}

	/// Reporting
           
    /// Create impedance string
    if(m_settings.impedance == NISCOPE_VAL_1_MEG_OHM){
        std::stringstream SS;
        SS << "1 MOhm";
        impedance_string = SS.str();
    }else if(m_settings.impedance == NISCOPE_VAL_50_OHMS){
        std::stringstream SS;
        SS << "50 Ohm";
        impedance_string = SS.str();
    }
    
    /// Create coupling mode string
	if(m_settings.coupling == NISCOPE_VAL_DC){
        std::stringstream SS;
        SS << "DC";
        coupling_string = SS.str();
    }else if(m_settings.coupling == NISCOPE_VAL_AC){
        std::stringstream SS;
        SS << "AC";
        coupling_string = SS.str();
    }else{
        std::stringstream SS;
        SS << "GND";
        coupling_string = SS.str();
    }
    
    /// Create phase-lock loop string
	if(m_use_gps_sync){
        std::stringstream SS;
        SS << "Phase-locked to " << m_resource_name_6682;
        pll_string = SS.str();
    }else{
        std::stringstream SS;
        SS << "NOT phase-locked to " << m_resource_name_6682;
        pll_string = SS.str();
    }
    pll_string.copy(
        m_settings.pll_status,
        (pll_string.size() < STR_LEN)? pll_string.size() : STR_LEN
    );        
    m_logger.config(
        "Device name:\t", m_settings.adc_name, "\n\t",
        "Device type:\t", m_settings.adc_type, "\n\t",
        "Sample rate:\t", m_settings.sample_freq/1.0e6, " MHz\n\t",
        "Bandwidth:\t", m_settings.bandwidth/1.0e6, " MHz\n\t",
        "Voltage range:\t+/-", m_settings.input_range/2, " V\n\t",
        "DC offset:\t", m_settings.dc_offset, " V\n\t",
        "Input scaling:\t", m_settings.scaling, "\n\t",
        "Impedance:\t", impedance_string, "\n\t",
        "Coupling mode:\t", coupling_string, "\n\t",
        m_settings.pll_status
    );
    return;
Error:
    {   std::stringstream SS;
        SS << "Device:\t" << m_resource_name_5122 << "\n";
		niScope_errorHandler(m_vi_5122, error, errorSource, errorMessage);
        SS << "\tError:\t" << errorMessage << std::endl;
        m_logger.fatal(SS.str());
        throw std::runtime_error("5122 initialization failure");
    }
}

/////
///  SET THE ABSOLUTE DAQ START TIME
void NIScope5122::
	_start_time_set(long unsigned& trigger_time)
{
    ViStatus 	error = VI_SUCCESS;
    ViChar  	errorSource[MAX_FUNCTION_NAME_SIZE];
    ViChar   	errorMessage[MAX_ERROR_DESCRIPTION] = " ";
    ViUInt32 	ns    = 0;
	ViUInt16	subns = 0;
    
    /// Set the absolute trigger time
    if(!trigger_time){
        /// First device sets the common trigger time for all sources
    	if(m_use_start_trig){
            /// Initialize the onboard clock to the current system time
            handleErr(
                niSync_SetTime(
                    m_vi_6682, 
                    NISYNC_VAL_INIT_TIME_SRC_SYSTEM_CLK, 
                    0, 0, 0
                )
            );        
        
			/// Take the current onboard 6682 time + delay
			handleErr(
                niSync_GetTime(
                    m_vi_6682, 
                    &trigger_time, 
                    &ns, 
                    &subns
                )
            );
			trigger_time += m_trigger_delay_s;
            trigger_time += 35; // TAI to UTC offset
		}else{
            /// Take the current time as the start time
            auto current_time = std::chrono::system_clock::now().time_since_epoch();
            trigger_time = std::chrono::duration_cast<std::chrono::seconds>(current_time).count();            
        }
    }
    m_trigger_time = trigger_time;  
    return;
Error:
    {   std::stringstream SS;
        SS << "Device:\t" << m_resource_name_6682 << "\n";
		niSync_error_message(m_vi_6682, error, errorMessage);
        SS << "\tError:\t" << errorMessage << std::endl;
        m_logger.fatal(SS.str());
        throw std::runtime_error("6682 initialization failure");
    }
}

/////
///  SET A COMMON FUTURE TRIGGER TIME FOR THE 5122 DAQ
void NIScope5122::
	arm_start_trigger()
{
	/// Return immediately if start triggering is disabled
	if(not m_use_start_trig){
        return; 
    }

    ViStatus 	error = VI_SUCCESS;
    ViChar  	errorSource[MAX_FUNCTION_NAME_SIZE];
    ViChar   	errorMessage[MAX_ERROR_DESCRIPTION] = " ";    

	/// Create the future start trigger event
	handleErr(
        niSync_CreateFutureTimeEvent(
            m_vi_6682, 
            NISYNC_VAL_PXITRIG0, 
            NISYNC_VAL_LEVEL_HIGH,
            m_trigger_time,
            0, 0
        )
    );
    return;
Error:
    {   std::stringstream SS;
        SS << "Device:\t" << m_resource_name_6682 << "\n";
		niSync_error_message(m_vi_6682, error, errorMessage);
        SS << "\tError:\t" << errorMessage << std::endl;
        m_logger.fatal(SS.str());
        throw std::runtime_error("Triggering failure");
    }
}

/////
///  CHECK WHETHER REALTIME MODE IS SET
///  (Realtime will allow frame dropping)
bool NIScope5122::
    realtime_is()
{
    return m_use_realtime_mode;
}

/////
///  Returns decimation factor for digital down sampling
unsigned NIScope5122::
    decimation()
{
    return m_decimation;
}

/////
///  GET SAMPLING FREQUENCY (Hz)
inline double NIScope5122::
    sample_freq()
{
    return m_settings.sample_freq;
}

/////
///  RETURNS THE 6682 START TRIGGER TIME (s)
unsigned NIScope5122::
    trigger_time()
{
    return (m_trigger_time - 35); // TAI to UTC offset;
}

/////
///  INITIATE THE DAQ
///  If start trigger disabled, DAQ begins immediately
void NIScope5122::
    fetching_start()
{
    ViStatus error = VI_SUCCESS;
    ViChar   errorSource[MAX_FUNCTION_NAME_SIZE];
    ViChar   errorMessage[MAX_ERROR_DESCRIPTION] = " ";
    
    handleErr(niScope_InitiateAcquisition(m_vi_5122));
    return;
Error:
    {   std::stringstream SS;
        SS << "Device:\t" << m_resource_name_5122 << "\n";
		niScope_errorHandler(m_vi_5122, error, errorSource, errorMessage);
        SS << "\tError:\t" << errorMessage << std::endl;
        m_logger.fatal(SS.str());
        throw std::runtime_error("Initiate acquisition failure");    
    }
}

/////
///  END THE ACQUISITION
void NIScope5122::
    fetching_finish()
{ 
    niScope_Abort(m_vi_5122); 
}

/////
///  GET THE NUMBER OF CHANNELS PER ADC
size_t NIScope5122::
    channels_num()
{ 
    return 2; 
}

/////
///  GET THE FETCH SIZE FOR THE 5122
size_t NIScope5122::
	get_fetch_size()
{ 
    return m_fetch_size; 
}

/////
///  RETURN ALL 5122 SETTINGS
const IDataSource::DataSourceSettings NIScope5122::
	all_settings()
{ 
    return m_settings; 
}

/////
///  GET THE SAMPLE TIME INTERVAL (ns)
inline uint64_t NIScope5122::
	time_increment()
{
    return (1e9 / m_settings.sample_freq); 
}

/////
///  FETCH A BATCH OF DATA FROM THE SPECIFIED CHANNEL
size_t NIScope5122::
    fetch_data(
		uint8_t chn, 
		Range<int16_t *>  &data_fetch, 
		Range<uint64_t *> &time_fetch,
		Range<double *>   &gain_fetch,
		Range<double *>   &offset_fetch
	)
{
    ViStatus 		error = VI_SUCCESS;
    ViChar			errorSource[MAX_FUNCTION_NAME_SIZE];
    ViChar   		errorMessage[MAX_ERROR_DESCRIPTION] = " ";
    niScope_wfmInfo wfmInfo;
	ViBoolean		phase_locked = VI_FALSE;

	/// Range of data_fetch in which to copy the 5122 data
	/// Fetcher will throw an error if less than "fetch_size" number of points are returned
	auto read_range = LeftLimitedRange(
        data_fetch, 
        m_fetch_size
    );

	/// Get the channel number
    const char *chn_str;
    switch(chn){
        case 0:
            chn_str = "0";
            break;
        case 1:
            chn_str = "1";
            break;
        default:
			std::stringstream SS;
			SS << "Bad channel number : " << chn;
            m_logger.fatal(SS.str());
            throw std::logic_error(SS.str());
    }

	/// Verify that the 5122 clock is still phase-locked to the 6682H reference clock.
	if(m_use_gps_sync){
		niScope_GetAttributeViBoolean(
            m_vi_5122, 
            "", 
            NISCOPE_ATTR_PLL_LOCK_STATUS, 
            &phase_locked
        );
		if(not phase_locked){
			std::stringstream SS;
            SS << "Device:\t" << m_resource_name_5122 << "\n";
            SS << "\tError:\tLost phase lock to reference clock!\n\n";            
			throw std::runtime_error(SS.str());
		}
	}

	/// Fetch a batch of data from the 5122
    handleErr(
        niScope_FetchBinary16(
            m_vi_5122, 
            chn_str, 
            10.0, //time out after 10 s
            read_range.size(),
            read_range.begin(), 
            &wfmInfo
        )
    );

	/// Copy the timestamp & calibration constant values, if they are requested for this fetch batch
    if(time_fetch.size() != 0){
		/// Copy the value of the 5122 timestamp & the calibration coefficients
		*(time_fetch.begin())   = _convert_to_absolute_ns(wfmInfo.absoluteInitialX);  
		*(gain_fetch.begin())   = wfmInfo.gain;
		*(offset_fetch.begin()) = wfmInfo.offset;

		/// Increment the external buffer ranges
		time_fetch.begin_shift(1);
		gain_fetch.begin_shift(1);
		offset_fetch.begin_shift(1);
	}
	
	/// Increment the begin_ptr of the channel buffer range
	data_fetch.begin_shift(read_range.size());
    return read_range.size();
Error:
    {   std::stringstream SS;
        SS << "Device:\t" << m_resource_name_5122 << "\n";
		niScope_errorHandler(m_vi_5122, error, errorSource, errorMessage);
        SS << "\tError:\t" << errorMessage << std::endl; 
        throw std::runtime_error(SS.str());
    }
}

/////
///  Converts raw timestamp to absolute local time in ns since 
///   the UNIX epoch (00:00:00 Jan. 1, 1970)
inline uint64_t NIScope5122::
    _convert_to_absolute_ns(double timestamp)
{
    /// Subtract the initial offset of the sample clock from zero
    if(not m_sample_clock_offset){
        /// Set the offset for the device on the first acquisition (guaranteed to be >0)
        m_sample_clock_offset = timestamp;
    }
    timestamp -= m_sample_clock_offset;
    
    /// Add the zero-based timestamp to the absolute DAQ start time (ns)
    uint64_t timestamp_ns  = (timestamp * pow(10, 9)) + 0.5;
    uint64_t start_time_ns = uint64_t(m_trigger_time) * 1e9;
    return (timestamp_ns + start_time_ns);
}

}; //~namespace JS
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

