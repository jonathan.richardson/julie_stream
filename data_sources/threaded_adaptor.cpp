////
///

#include "threaded_adaptor.hpp"

namespace JStream {


/////
///  Constructor
ThreadedReader::
    ThreadedReader(
        shared_ptr<IDataSource> internal_source,
        SDT::MemoryLocalityAccess memory_locality,
        SDT::ThreadLocalityAccess thread_locality,
        LoggerAccess logger,
        unsigned hunk_size,
        unsigned num_hunks
    ) :
        m_internal_source(internal_source),
        m_memory_locality(memory_locality),
        m_thread_locality(thread_locality),
        m_logger(logger),
        m_decimation(internal_source->decimation()),
        m_hunk_size(hunk_size),
        m_num_hunks(num_hunks)
{
    /// The channel buffer size MUST be an integer multiple >= 1 of the 5122 fetch size
    assert(m_num_hunks > 0);
    assert(m_hunk_size > 0);
    assert(m_decimation > 0);
    assert(m_hunk_size >= m_internal_source->get_fetch_size());
    assert(m_hunk_size % m_internal_source->get_fetch_size() == 0);  
    assert(m_hunk_size % m_decimation == 0);
    
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);

	/// Queues circulating buffer elements to/from fetcher
    m_queues_to_source = std::vector<TDataQueue>(this->channels_num());
    m_queues_from_source = std::vector<TDataQueue>(this->channels_num());
    auto decimated_size = m_hunk_size / m_decimation;
    for(auto &chn_queue : m_queues_to_source){ //loop over the source channel queues
        for(auto hunk_idx : icount(m_num_hunks)){ //loop over the number of elements in the queue	
            /// Allocate and inject a channel buffer element
			TBufferQueueElement inject_data;
            inject_data.buffer = TFillRange(
                (TFill *) m_memory_locality.allocate(sizeof(TFill) * decimated_size, m_logger), 
                decimated_size
            );          
            inject_data.timestamp = TTimeRange(
                (uint64_t *) m_memory_locality.allocate(sizeof(uint64_t), m_logger), 
                1
            );
            inject_data.gain = TCalRange(
                (double *) m_memory_locality.allocate(sizeof(double), m_logger), 
                1
            );
            inject_data.offset = TCalRange(
                (double *) m_memory_locality.allocate(sizeof(double), m_logger), 
                1
            );
			chn_queue.push(inject_data);
		}
	}     

	/// Pointers to the current batch of data to be copied by the Fetch Manager
	/// (The double-pointer scheme allows the buffer to be copied incrementally
	///  with multiple calls to ThreadedReader::fetch_data() )
    m_current_hunk = std::vector<TBufferQueueElement>(this->channels_num());
    m_current_holder = std::vector<TBufferQueueElement>(this->channels_num());
	for(auto chn_idx : icount(this->channels_num())){
		m_current_hunk[chn_idx].buffer      = TFillRange(nullptr, nullptr);
		m_current_hunk[chn_idx].timestamp   = TTimeRange(nullptr, nullptr);
		m_current_hunk[chn_idx].gain        = TCalRange (nullptr, nullptr);
		m_current_hunk[chn_idx].offset      = TCalRange (nullptr, nullptr);
		m_current_holder[chn_idx].buffer    = TFillRange(nullptr, nullptr);
		m_current_holder[chn_idx].timestamp = TTimeRange(nullptr, nullptr);
		m_current_holder[chn_idx].gain      = TCalRange (nullptr, nullptr);
		m_current_holder[chn_idx].offset    = TCalRange (nullptr, nullptr);
	}
}

/////
///  Destructor
ThreadedReader::
    ~ThreadedReader()
{
}

/////
///  Returns the number of channels on the underlying source (not the total)
size_t ThreadedReader::
    channels_num()
{ 
    return m_internal_source->channels_num(); 
}

/////
///  Returns "true" if realtime mode is enabled
bool ThreadedReader::
    realtime_is()
{ 
    return m_internal_source->realtime_is(); 
}

/////
///  Returns decimation factor for digital down sampling
unsigned ThreadedReader::
    decimation()
{
    return m_internal_source->decimation();
}

/////
///  Returns the sampling frequency of the underlying source
///  (with appropriate decimation)
inline double ThreadedReader::
    sample_freq()
{ 
    return (m_internal_source->sample_freq() / m_decimation); 
}

/////
///  Returns the time interval between 5122 samples (sec)
inline uint64_t ThreadedReader::
	time_increment()
{
    return (m_internal_source->time_increment() * m_decimation);
}

/////
///  Returns the ADC settings
const IDataSource::DataSourceSettings ThreadedReader::
    all_settings()
{
    return m_internal_source->all_settings();
}

/////
///  Returns the 6682 start trigger time
unsigned ThreadedReader::
    trigger_time()
{
    return m_internal_source->trigger_time();
}

/////
///  Start fetching data from the data source buffer
void   ThreadedReader::
    fetching_start()
{
    /// This protects the thread from being launched twice
    if(not m_background_loop.joinable()){
        /// Launch the background fetch loop
        m_background_loop = std::thread(
            std::bind(
                &ThreadedReader::_background_fetch_loop, 
                this
            )
        );
    }
}

/////
///  Release the internal queue resources
void   ThreadedReader::
    fetching_finish()
{
	/// Wait for the resources using the queues and buffers to finish
    for(auto &to_queue : m_queues_to_source){
        to_queue.deny_wait_set();
    }
    m_background_loop.join();
    for(auto &to_queue : m_queues_to_source){
        to_queue.deny_wait_clear();
    }

    /// Clean up the buffers and queues
    for(auto chn_idx : icount(this->channels_num())){
		/// Return the buffer to the queue for cleaning
        if(not m_current_holder[chn_idx].buffer.empty()){
            m_current_hunk[chn_idx].buffer    = TFillRange(nullptr, nullptr);
			m_current_hunk[chn_idx].timestamp = TTimeRange(nullptr, nullptr);
			m_current_hunk[chn_idx].gain      = TCalRange (nullptr, nullptr);
			m_current_hunk[chn_idx].offset    = TCalRange (nullptr, nullptr);
            m_queues_to_source[chn_idx].push(std::move(m_current_holder[chn_idx]));
        }
        
		/// Deallocate the queues
		TBufferQueueElement element;
        while(m_queues_to_source[chn_idx].pull(element)){
			m_memory_locality.deallocate(element.buffer.begin(), element.buffer.binary_size());
			m_memory_locality.deallocate(element.timestamp.begin(), element.timestamp.binary_size());
			m_memory_locality.deallocate(element.gain.begin(), element.gain.binary_size());
			m_memory_locality.deallocate(element.offset.begin(), element.offset.binary_size());
		}
		while(m_queues_from_source[chn_idx].pull(element)){
			m_memory_locality.deallocate(element.buffer.begin(), element.buffer.binary_size());
			m_memory_locality.deallocate(element.timestamp.begin(), element.timestamp.binary_size());
			m_memory_locality.deallocate(element.gain.begin(), element.gain.binary_size());
			m_memory_locality.deallocate(element.offset.begin(), element.offset.binary_size());
		}
	}
}

/////
///  Fetch a batch of ADC data from the ThreadedReader internal channel buffer
size_t ThreadedReader::
    fetch_data(
		uint8_t chn_idx, 
		Range<TFill *> &data_fetch, 
		Range<uint64_t *> &time_fetch,
		Range<double *> &gain_fetch,
		Range<double *> &offset_fetch
	)
{
	if(m_current_hunk[chn_idx].buffer.empty()){
		/// If m_current_holder is not empty, first push the memory back to the fetcher
		if(not m_current_holder[chn_idx].buffer.empty()){
			m_queues_to_source[chn_idx].push(m_current_holder[chn_idx]);
		}
        
        /// Pull in the next element from the output queue
        /// (Blocking but cancellable)
		TBufferQueueElement element;
        auto did_pull = m_queues_from_source[chn_idx].pull_spinwait(element);
        if(not did_pull){
            /// Forward the error to the fetch manager
            std::rethrow_exception(m_error);    
        }
        
		/// The data ranges contained in m_current_holder, m_current_hunk will point to the 
		/// *same* underlying region of memory
        m_current_holder[chn_idx] = element;
        m_current_hunk[chn_idx] = element;
	}

	/// Copy the data for the specified channel into "data_fetch"
	/// (Copy the minimum of the number of free points left in the external buffer
	///  or the number of filled points left in the internal queue element)
	TFillRange &chn_range = m_current_hunk[chn_idx].buffer;
    unsigned cpy_length = std::min(data_fetch.size(), chn_range.size());
    memcpy(data_fetch.begin(), chn_range.begin(), sizeof(TFill) * cpy_length);

	/// Write the timestamp for the specified channel into "time_fetch", if it is requested
    if(time_fetch.size()){
		auto timestamp = *(m_current_holder[chn_idx].timestamp.begin());
		auto gain 	   = *(m_current_holder[chn_idx].gain.begin());
		auto offset    = *(m_current_holder[chn_idx].offset.begin());

		/// Infer the sample time of the first data point in the range being fetched
		uint64_t adjusted_timestamp;
		{
			auto buffer_beginning  = m_current_holder[chn_idx].buffer.begin();
			unsigned length_offset = chn_range.begin() - buffer_beginning;      //Number of points from buffer beginning to current position	
			uint64_t time_offset   = length_offset * this->time_increment();    //The amount of time since the 5122 timestamp (ns)
			adjusted_timestamp     = timestamp + time_offset;			        //The adjusted timestamp (ns)
		}

		/// Copy the timestamp & ADC calibration constants into the external buffers
		memcpy(time_fetch.begin(), &adjusted_timestamp, sizeof(uint64_t));
		memcpy(gain_fetch.begin(), &gain, sizeof(double));
		memcpy(offset_fetch.begin(), &offset, sizeof(double));

		/// Increment the external buffer ranges
		time_fetch.begin_shift(1);
		gain_fetch.begin_shift(1);
		offset_fetch.begin_shift(1);
	}

	/// Increment the internal and external channel buffer ranges
    chn_range.begin_shift(cpy_length);
	data_fetch.begin_shift(cpy_length);
	
	/// Return the element to the fetcher if all of its data has been transferred out
	if(m_current_hunk[chn_idx].buffer.empty()){
        m_queues_to_source[chn_idx].push(m_current_holder[chn_idx]);
        m_current_hunk[chn_idx].buffer		= TFillRange(nullptr, nullptr);
		m_current_hunk[chn_idx].timestamp	= TTimeRange(nullptr, nullptr);
		m_current_hunk[chn_idx].gain	 	= TCalRange (nullptr, nullptr);
		m_current_hunk[chn_idx].offset 	  	= TCalRange (nullptr, nullptr);
        m_current_holder[chn_idx].buffer    = TFillRange(nullptr, nullptr);
		m_current_holder[chn_idx].timestamp = TTimeRange(nullptr, nullptr);
		m_current_holder[chn_idx].gain 		= TCalRange (nullptr, nullptr);
		m_current_holder[chn_idx].offset 	= TCalRange (nullptr, nullptr);
    }
	return cpy_length;
}

/////
///  Copies data from source to target with decimation
inline void ThreadedReader::
    _decimated_memcpy(
        TBufferQueueElement &target,
        TBufferQueueElement &source
    )
{
    auto source_idx = 0;
    auto bitshift = 1;
    switch(m_decimation){
        case 1:
            /// No decimation
            memcpy(target.buffer.begin(), source.buffer.begin(), sizeof(TFill) * m_hunk_size);
            break;
    
        case 2:
            /// x2 down sampling
            bitshift = 2;
            for(auto target_idx : icount(target.buffer.size())){
                TFill inner_sum =   (source.buffer[source_idx    ] / bitshift) + 
                                    (source.buffer[source_idx + 1] / bitshift);
                target.buffer[target_idx] = inner_sum;
                source_idx += 2;
            }
            
            break;
            
        case 4:
            /// x4 down sampling
            bitshift = 4;
            for(auto target_idx : icount(target.buffer.size())){
                TFill inner_sum =   (source.buffer[source_idx    ] / bitshift) + 
                                    (source.buffer[source_idx + 1] / bitshift) +
                                    (source.buffer[source_idx + 2] / bitshift) + 
                                    (source.buffer[source_idx + 3] / bitshift);
                target.buffer[target_idx] = inner_sum;
                source_idx += 4;
            }
            break;

        default:
            throw std::runtime_error("Down sampling only supported for x2, x4");
    }
    target.gain[0]      = bitshift * source.gain[0] / m_decimation;
    target.offset[0]    = source.offset[0];
    target.timestamp[0] = source.timestamp[0];
}

/////
///  Fetch loop
void ThreadedReader::
    _background_fetch_loop()
{
    /// Bind the work thread
	m_thread_locality.thread_locality_apply();
    
	/// Channel buffers for the current batch of data
    auto current_hunk = std::vector<TBufferQueueElement>(this->channels_num());
	auto current_holder = std::vector<TBufferQueueElement>(this->channels_num());
	for(auto chn_idx : icount(this->channels_num())){
        /// Allocate each channel buffer
        TBufferQueueElement inject_data;
        inject_data.buffer = TFillRange(
            (TFill *) m_memory_locality.allocate(sizeof(TFill) * m_hunk_size, m_logger), 
            m_hunk_size
        );
        inject_data.timestamp = TTimeRange(
            (uint64_t *) m_memory_locality.allocate(sizeof(uint64_t), m_logger), 
            1
        );
        inject_data.gain = TCalRange(
            (double *) m_memory_locality.allocate(sizeof(double), m_logger), 
            1
        );
        inject_data.offset = TCalRange(
            (double *) m_memory_locality.allocate(sizeof(double), m_logger), 
            1
        );
        
        /// Set the ranges in current_holder, current_hunk to point to the 
        /// *same* underlying region of memory
        current_holder[chn_idx] = inject_data;
        current_hunk[chn_idx] = inject_data;        
	}

	/// Start the 5122 listening for the start trigger
	/// (DAQ starts immediately if: start trigger disabled, virtual source(s), only one 5122)
    m_internal_source->fetching_start();

	/// Configure the 6682H to send the start trigger at a specified future time
	/// (Does nothing if start trigger disabled or virtual source)
    /// (MUST be called from this thread, which is already bound)
    m_internal_source->arm_start_trigger();

    while(true){
        try{
            /// Read data from the 5122 into the local channel buffers
            /// (Continues reading batches until the current_hunk buffers are filled)
            uint32_t chn_bitset = 0;
            uint32_t channels_full_bitset = (1 << this->channels_num()) - 1;
            while(chn_bitset != channels_full_bitset){
                for(auto chn_idx : icount(this->channels_num())){ //loop over the device channels
                    /// Read data from the 5122 into the current_hunk buffer
                    /// (Automatically increments the begin ptrs of current_hunk)
                    TFillRange &chn_buffer    = current_hunk[chn_idx].buffer;
                    TTimeRange &time_buffer   = current_hunk[chn_idx].timestamp;
                    TCalRange  &gain_buffer   = current_hunk[chn_idx].gain;
                    TCalRange  &offset_buffer = current_hunk[chn_idx].offset;    
                    m_internal_source->fetch_data(
                        chn_idx, 
                        chn_buffer, 
                        time_buffer,
                        gain_buffer,
                        offset_buffer
                    );
                    if(chn_buffer.empty()){
                        /// This channel buffer is filled
                        chn_bitset |= 1 << (chn_idx);
                    }
                } //~channel loop
            } //~buffer fill loop
        }catch(std::runtime_error&){
            /// Get the error & cancel the channel buffer queues, then return
            m_error = std::current_exception();
            for(auto chn : icount(this->channels_num())){ //loop over all channels
                m_queues_from_source[chn].deny_wait_set();
            }
            break;
        }

        try{
            /// Copy the current batch from the local buffers to the queue element buffers
            for(auto chn_idx : icount(this->channels_num())){
                /// Pull in the next input queue element for filling
                TBufferQueueElement element;
                auto did_pull = m_queues_to_source[chn_idx].pull_spinwait(element);
                if(not did_pull){
                    throw std::runtime_error("Run has been shut down");
                }

                /// Copy the current data to the queue element with decimation
                /// (This is the digital down sampling)
                this->_decimated_memcpy(element, current_holder[chn_idx]);

                /// Push the filled queue element onto the output queue
                m_queues_from_source[chn_idx].push(element);

                /// Reset the ranges of the local channel buffers
                /// (The original range of the buffers is stored in current_holder)
                current_hunk[chn_idx].buffer    = current_holder[chn_idx].buffer;
                current_hunk[chn_idx].timestamp = current_holder[chn_idx].timestamp;
                current_hunk[chn_idx].gain      = current_holder[chn_idx].gain;
                current_hunk[chn_idx].offset    = current_holder[chn_idx].offset;
            }
        }catch(std::runtime_error&){
            /// Run has shut down
            break;
        }
    } //~work loop    
    
    /// Return any partially-filled buffer ranges to the queues for cleaning
    for(auto chn_idx : icount(this->channels_num())){
        if(not current_holder[chn_idx].buffer.empty()){
            m_queues_to_source[chn_idx].push(current_holder[chn_idx]);
        }
    }
    
    /// Shut down the inner data source
	m_internal_source->fetching_finish();
}

}; //~namespace JStream
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :