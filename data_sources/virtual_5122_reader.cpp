////
///

#include <limits>
#include "virtual_5122_reader.hpp"

namespace JStream {
using namespace uLogger;

auto logger_vds = logger_root().child("fetch_logger");

Virtual5122::
	Virtual5122(long unsigned& trigger_time) :
		m_CHN1_Fetch_N(0),
		m_CHN2_Fetch_N(0),
		m_mod_rate(1024)
{
    /// Set the data source type
    auto type = std::string("Virtual");
    std::strncpy(
        m_settings.adc_type,
        type.c_str(),
        (type.size() < STR_LEN)? type.size() : STR_LEN
    );

    /// Set the DAQ start time, if it has not already been set by another source 
    if(!trigger_time){
        /// Take the current time as the start time
        auto current_time = std::chrono::system_clock::now().time_since_epoch();
        trigger_time = std::chrono::duration_cast<std::chrono::seconds>(current_time).count();            
    }
    m_trigger_time = trigger_time; 
}

size_t Virtual5122::
    channels_num()
{
    return 2;
}

bool Virtual5122::
    realtime_is()
{
    return false;
}

/////
///  Returns decimation factor for down sampling
unsigned Virtual5122::
    decimation()
{
    return 1;
}


double Virtual5122::
    sample_freq()
{
    return 1.0E8; 
}

/////
///  Returns the time increment
///  (Taken as 10 ns, the increment for a 100 MHz sample rate)
uint64_t Virtual5122::
	time_increment()
{
    return 10; 
}

/////
///  Returns the fetch size
size_t Virtual5122::
	get_fetch_size()
{ 
    return (1 << 19);
}

/////
///  Returns the trigger time
///  (Taken as the current time, since virtual sources start immediately)
unsigned Virtual5122::
	trigger_time()
{
    return m_trigger_time;
    //return duration_cast<seconds>(system_clock::now().time_since_epoch()).count();
}

/////
///  Return all device settings
const IDataSource::DataSourceSettings Virtual5122::
	all_settings()
{ 
    return m_settings; 
}

void Virtual5122::
	fetching_start()
{
	m_CHN1_Fetch_N = 0;
	m_CHN2_Fetch_N = 0;
}

void Virtual5122::
	fetching_finish()
{ }

size_t Virtual5122::
	fetch_data(
		uint8_t chn, 
		Range<int16_t *>  &data_fetch,
		Range<uint64_t *> &time_fetch,
		Range<double *>   &gain_fetch,
		Range<double *>   &offset_fetch
	)
{
    uint64_t data_count;
    uint64_t begin_count;

    /// Increment the begin_ptr of the data buffer range
    if(chn == 0){
        data_count = m_CHN1_Fetch_N;
        begin_count = m_CHN1_Fetch_N;
        m_CHN1_Fetch_N += data_fetch.size();
    }else{
        data_count = m_CHN2_Fetch_N;
        begin_count = m_CHN2_Fetch_N;
        m_CHN2_Fetch_N += data_fetch.size();
    }

	/// Copy the data values
	for(auto &data_el : data_fetch){
        data_el = 999 + ((data_count) / 100000000);
        data_count += 1;
    }    

	/// Copy the timestamp value
	/// One timestamp returned for each call to Virtual5122::fetch_data()
    if(not time_fetch.empty()){
        /// Timestamp the current system time (ns)
        *(time_fetch.begin()) = m_trigger_time * 1e9 + begin_count / (this->sample_freq() / 1e9);

        /// Increment the begin_ptr of the timestamp buffer range
        time_fetch.begin_shift(1);
    }

    data_fetch.begin_shift(data_fetch.size());
    return data_fetch.size();
}

}; //~namespace JS

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

