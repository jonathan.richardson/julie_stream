////
///
#ifndef H_HARDWARE_CONFIG_UTILITIES_H
#define H_HARDWARE_CONFIG_UTILITIES_H

#include <string>

#include "jstream_types.hpp"

#include "utilities/config_tree/config_tree_json.hpp"
#include "utilities/range.hpp"

namespace JStream {
using namespace ConfigTree;

/// Checks the 5122 settings for mutual consistency
void config_error_checker(ConfigTreeAccess&);

}; //~namespace JStream

#endif //H_HARDWARE_CONFIG_UTILITIES_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
