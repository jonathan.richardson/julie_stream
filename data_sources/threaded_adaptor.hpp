////
///
///

#ifndef H_JSTREAM_THREADED_ADAPTOR_H
#define H_JSTREAM_THREADED_ADAPTOR_H

#include <boost/thread.hpp>

#include <thread>
#include <vector>
#include <memory>
#include <algorithm>
#include <functional>
#include <cstring>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <sstream>

#include <sdt/utilities/logger.hpp>

#include "hardware_locality/locality.hpp"

#include "queues/queue.hpp"

#include "jstream_types.hpp"
#include "data_source_base.hpp"

namespace JStream {
using std::shared_ptr;


/////
///  Wraps the underlying data source in a threading layer
class ThreadedReader : public IDataSource
{
    /// Convenience typedefs
    typedef int16_t 			    
        TFill;	
    typedef Range<TFill *> 		    
        TFillRange;
	typedef Range<uint64_t *>       
        TTimeRange;
	typedef Range<double *>   	    
        TCalRange;

    /// Internal channel queue element type
	typedef struct {  
		TFillRange	buffer;
		TTimeRange	timestamp;
		TCalRange	gain;
		TCalRange 	offset;
	}   
        TBufferQueueElement;
        
    typedef WaitingQueue<TBufferQueueElement>	
        TDataQueue;

    /// Logger
    LoggerAccess                m_logger; 
 
    /// Hardware error status
    std::exception_ptr          m_error;
 
	/// Hardware localization
    SDT::MemoryLocalityAccess 	m_memory_locality;
    SDT::ThreadLocalityAccess   m_thread_locality;

	/// The length of the internal channel buffers
    unsigned 	                m_hunk_size;
    unsigned                    m_decimation;
    
	/// The number of elements in the internal queues
    unsigned 	                m_num_hunks;

	/// The time interval between consecutive 5122 samples (ns)
	uint64_t	                m_time_increment_ns;

    /// Thread for the compute loop
    std::thread                 m_background_loop;
    
    /// Internal queue parameters
    std::vector<TDataQueue>	            
                                m_queues_to_source;
    std::vector<TDataQueue> 	        
                                m_queues_from_source;
    std::vector<TBufferQueueElement>    
                                m_current_hunk;
    std::vector<TBufferQueueElement>   	
                                m_current_holder;
    
    /// The underlying data source
    shared_ptr<IDataSource>	    m_internal_source;

public:
    ThreadedReader(
        shared_ptr<IDataSource> internal_source,
        SDT::MemoryLocalityAccess memory_locality,
        SDT::ThreadLocalityAccess thread_locality,
        LoggerAccess logger,
        unsigned hunk_size = (unsigned(1)<<21),
        unsigned num_hunks = 5
    );

    virtual ~ThreadedReader();

	/// Returns the time interval between 5122 samples (ns)
	virtual uint64_t time_increment();
	
    /// Returns the number of channels on the source it wraps (one ADC)
    virtual size_t channels_num();

    /// Returns whether fetchers will drop data to maintain realtime operation
    virtual bool realtime_is();

    /// Returns the decimation factor for digital down sampling 
    virtual unsigned decimation();
    
    /// Returns the sample frequency
    /// (This may return a negative number to represent an unimportant/scalable sample rate)
    virtual double sample_freq();
    
    /// Returns the ADC settings
    const DataSourceSettings all_settings();
    
    /// Returns the 6682 start trigger time (TAI ns)
    unsigned trigger_time();

    /// Starts/stops the DAQ
    virtual void fetching_start();
    virtual void fetching_finish();

    /// Fetches a batch of data
    virtual size_t fetch_data(
		uint8_t chn, 
		Range<TFill *> &data_fetch, 
		Range<uint64_t *> &time_fetch,
		Range<double *> &gain_fetch,
		Range<double *> &offset_fetch
	);

protected:
    /// Contains the work loop
    void _background_fetch_loop();
    
    /// Decimated memcpy of channel buffer
    void _decimated_memcpy(
        TBufferQueueElement &target,
        TBufferQueueElement &source
    );
};

}; //~namespace JStream

#endif //H_JSTREAM_THREADED_ADAPTOR_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
