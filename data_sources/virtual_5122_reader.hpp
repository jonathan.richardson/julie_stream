////
///
#ifndef H_VIRTUAL_5122_READER_H
#define H_VIRTUAL_5122_READER_H

#include <string>
#include <stdint.h>
#include <chrono>
#include <cstring>

#include <sdt/utilities/logger.hpp>

#include "utilities/range.hpp"

#include "data_source_base.hpp"

namespace JStream {
using std::string;
using namespace std::chrono;

/////
///  Virtual data source class
class Virtual5122 : public IDataSource
{
	int         m_CHN1_Fetch_N;
	int         m_CHN2_Fetch_N;
	int         m_mod_rate;
	
	/// Absolute start time in sec
	unsigned    m_trigger_time = 0;
    
    /// Contains all source settings
	DataSourceSettings    m_settings;

public:
    Virtual5122(long unsigned& trigger_time);

    size_t      channels_num();
    bool        realtime_is();
    double      sample_freq();
	uint64_t    time_increment();
    unsigned    trigger_time();
    size_t      get_fetch_size();
    const DataSourceSettings
                all_settings();
    unsigned    decimation();          

    void   fetching_start();
    void   fetching_finish();

    size_t fetch_data(
        uint8_t chn, 
        Range<int16_t *>  &data_fetch, 
        Range<uint64_t *> &time_fetch,
        Range<double *>   &gain_fetch,
        Range<double *>   &offset_fetch
    );
    
};

}; //~namespace JS

#endif //H_VIRTUAL_5122_READER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

