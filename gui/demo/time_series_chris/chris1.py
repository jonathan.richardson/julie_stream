#!/usr/bin/env python

import os
import numpy as np
from functools import partial as BIND
import json
import math
import copy
import time
import datetime

from julie_csd import CSDCarrierFloat, CSDCarrierDouble

from realtime_interface.utilities.QtVariant import QtGui, QtCore
from realtime_interface.utilities import csd_utils as cUtil
from realtime_interface.utilities import gui_utils as gUtil
from realtime_interface.utilities import plot_utils as pUtil
from realtime_interface.utilities import interface_factories as factory

from realtime_interface.fetcher.fetch_thread import FetchThread

from realtime_interface.plotter.figure_canvas_time import FigureCanvasTimeSeries


#####
###  GUI class
###  (Contains all of the standard Qt widget methods + our custom methods)
class Chris1(QtGui.QMainWindow):
    
    ###
    ## Constructor
    def __init__(self):
        # Call the constructor of each inherited class
        super(Chris1, self).__init__()

        # Create the GUI window  
        self.__main_frame = QtGui.QWidget()
        self.setCentralWidget(self.__main_frame)
        self.__layout = QtGui.QGridLayout()
        self.__main_frame.setLayout(self.__layout)

        # Bind the GUI window to the widest available monitor
        self.__geometry = gUtil.screen_autodetect()
        self.move(
            self.__geometry.left(), 
            self.__geometry.top()
        )
        
        # Start the timer loop (functions as the producer)
        self.timer = QtCore.QTimer()
        self.timer.connect(self.timer, QtCore.SIGNAL("timeout()"), self.__plot_update)
        
        # Launch the welcome screen
        self.show()          
        self.__welcome_screen()              
        return
        

    ###
    ## Pass new data to the figure canvas
    def __plot_update(self):   
        print 'Plot update!'
        for key in self.__ydata.keys():
            self.__ydata[key] += .5
        self.__canvas.draw_lines(self.__ydata)
        return
        
        
    ###
    ## Launch the main GUI screen
    def __welcome_screen(self):    
        # Reset/reconfigure the window  
        gui_size = [
            self.__geometry.width()  - 17, 
            self.__geometry.height() - 40
        ]
        gui_size = [2030, 1230]
        self.__reset_layout(
            'Chris1',
            gui_size,
            6
        )
        #self.move(1920, 0)
        self.move(1024, 0)

        # Add a button to start the GUI
        object = factory.std_button(
            'Click to start the demo',
            self.__main_screen
        )
        self.__layout.addWidget(object, 0, 0)
        
        # Show the window content
        self.__main_frame.show()
        return
       

    ###
    ## Launch the main GUI screen
    def __main_screen(self):    
        # Reset/reconfigure the window  
        gui_size = [
            self.__geometry.width()  - 17, 
            self.__geometry.height() - 40
        ]
        gui_size = [2030, 1230]
        self.__reset_layout(
            'Chris1',
            gui_size,
            6
        )
        #self.move(1920, 0)
        self.move(1024, 0)
        
        # Rescale y-range button
        object = factory.std_button(
            'Click to rescale the axes',
            BIND(self.__display_mode_set, None)
        )
        self.__layout.addWidget(object, 0, 0)        
        
        # Figure canvas 
        num_chs = 8
        self.__canvas = FigureCanvasTimeSeries(num_chs)
        self.__layout.addWidget(self.__canvas, 1, 0)
        self.__layout.setRowStretch(1, 10)
     
        # Set up the figure canvas
        # (Must draw the Qt window first to get the correct canvas size)
        self.__main_frame.show()
        self.__canvas.fig_size_autoset()
        
        self.__ydata = {}
        num_pts = 100
        for chn_idx in range(num_chs):
            self.__ydata[chn_idx] = np.ones(num_pts) * 1.5
            
        self.__canvas.subplot_setup(
            self.__ydata,
            length       = num_pts,
            time_max     = num_pts
        )
        
        # Start the timer (data updates)
        self.timer.start(1000)                
        return
    
    
    ###
    ## Convenience method to reset/reconfigure the main window
    def __reset_layout(
        self,
        title,
        size,
        spacing
    ):
        # Hide the window content
        self.__main_frame.hide()
        QtGui.QApplication.processEvents()    

        # Clear the window layout
        while self.__layout.count():
            child = self.__layout.takeAt(0)
            child.widget().deleteLater()
        self.__objects = {} 
        
        # Set the window size and object spacing
        self.setFixedSize(size[0], size[1])
        self.__layout.setVerticalSpacing(spacing)   
        
        # Set the window title
        self.setWindowTitle(title)
        return     
        
        
    ###
    ## Display setting on/off callback
    def __display_mode_set(
        self,
        setting
    ):
        # Update the display setting and redraw the figure         
        self.__canvas.display_mode_toggle(
            self.__ydata,
            setting
        )
        return        

#~Chris1 class