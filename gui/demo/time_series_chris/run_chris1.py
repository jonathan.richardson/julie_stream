#!/usr/bin/env python

import sys

from realtime_interface.utilities.QtVariant import QtGui
from chris1 import Chris1
               
####################
# THE MAIN EXECUTION
####################

if __name__ == '__main__':

    # Construct the GUI
    app = QtGui.QApplication([])
    gui = Chris1()
    print 'starting event loop'
    # Execute the GUI event-processing loop
    # (Blocks until the GUI shuts down)
    sys.exit(app.exec_())
