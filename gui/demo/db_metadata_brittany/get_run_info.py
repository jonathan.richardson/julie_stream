#!/usr/bin/env python

"""
Demo program to transfer run metadata from the HDF5 archive
"""

###################################
# Run ID whose metadata to transfer
RUN_ID = "00373"
###################################

import json
from julie_csd import DatabaseClient

# Database server address
address = 'tcp://julie2.fnal.gov:8702'

if __name__ == '__main__':
    # Create a database client
    client = DatabaseClient(address)
    
    # Connect to the database server
    # (This will block until a connection is established)
    client.start()
    if not client.wait_for_connection():
        raise RuntimeError('Unable to connect to %s' % address)
        
    # Send the packaged run ID request to the server
    requested_run = json.dumps({"run_id" : RUN_ID})
    print requested_run
    metadata = client.search_send(requested_run)

    print metadata