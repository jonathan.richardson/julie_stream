#!/usr/bin/env python

import sys, psutil, os
import argparse

from realtime_interface.utilities.QtVariant import QtGui

from realtime_interface.gui import GUI

# Server addresses
address_rt = 'tcp://julie2.fnal.gov:8710'
address_db = 'tcp://julie2.fnal.gov:8711'
           
####################
# THE MAIN EXECUTION
####################

if __name__ == '__main__':
    # Get the command line argument(s)
    parser = argparse.ArgumentParser(description='JStream GUI client')
    parser.add_argument('-f', '--fname_config', nargs='?', type=str, help='Run settings JSON file')
    args = parser.parse_args()

    # Elevate the OS priority of this process
    priority = 0 if os.name == 'posix' else psutil.HIGH_PRIORITY_CLASS
    psutil.Process(os.getpid()).nice(priority)

    # Create a Qt event-processing loop
    app = QtGui.QApplication([])

    # Launch the GUI window
    gui = GUI(
        address_rt, 
        address_db,
        fname_config = args.fname_config
    )
    
    # Start the event processing-loop
    # (Blocks until the GUI window shuts down)
    sys.exit(app.exec_())
