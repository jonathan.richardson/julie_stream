#!/usr/bin/env python

import json
import copy
import os
from os.path import join as PATH
from os.path import normpath as NORM

from PyQt4.Qt import *

from julie_csd import CSDCarrierFloat, CSDCarrierDouble

from .utilities import gui_utils as gUtil
from .utilities.interface_factories import *

from .data_fetchers.live_streamer import LiveStreamer
from .data_fetchers.database_access import DatabaseAccess

from .screens.welcome_screen import WelcomeScreen
from .screens.database_screen import DatabaseScreen
from .screens.main_options_screen import MainOptionsScreen
from .screens.time_options_screen import TimeOptionsScreen
from .screens.hw_options_screen import HWOptionsScreen
from .screens.source_options_screen import SourceOptionsScreen
from .screens.writer_options_screen import WriterOptionsScreen
from .screens.main_screen import MainScreen

###############################################################
# JSON configuration filepaths
PATH_CONFIG     = PATH(os.getenv('JSTREAM_PATH'), NORM('settings'))
PATH_SETTINGS   = PATH(os.getenv('JSTREAM_PATH'), NORM('gui/realtime_interface/settings'))
FNAME_CONFIG    = 'run_settings.json'
FNAME_RUN_OPTS  = 'supported_run_settings.json'
FNAME_HW_OPTS   = 'supported_5122_settings.json'
FNAME_HW_INCOMP = 'incompatible_5122_settings.json'
FNAME_DISP_CONF = 'default_display.json'
FNAME_LOGO      = 'holo_logo_anim.gif'
FNAME_IFO_OPS   = 'ifo_operators.json'
###############################################################


class GUI(QMainWindow):
    """ Main GUI class """

    def __init__(
        self,
        addr_rt,
        addr_db,
        fname_config = None
    ):
        """ Constructor """
        super(GUI, self).__init__()
        self._initialized = False
        self._run_state = False
        self._actv_scrnm = None

        # Create the data fetcher for the real-time stream, connect to server.
        self._streamer = LiveStreamer(addr_rt)
        self._streamer.callback_add('connect', self._connection_handler)    
        self._streamer.callback_add('run_status', self._run_status_handler)        
        self._streamer.callback_add('err', self._err_handler)
        self._streamer.callback_add('csd', self._csd_handler)
        self._streamer.callback_add('msg', self._msg_handler)

        # Create the data fetcher for database access, connect to server.
        # (The fetchers share a data model)
        self._database = DatabaseAccess(
            addr_db,
            model = self._streamer.model
        )

        # Bind the main window to the widest available monitor
        geometry = gUtil.screen_autodetect(widest = True)
        self.move(geometry.left(), geometry.top())
        self._size = [
            geometry.width()  - 17,
            geometry.height() - 40
        ]

        # Get the path of the run settings JSON file
        if fname_config is None:
            fpath = PATH(PATH_CONFIG, FNAME_CONFIG)
        else:
            # Look in the local run directory
            fpath = fname_config
            if not os.path.isfile(fpath):
                # Look in the JSTREAM_LOCAL directory
                fpath = PATH(os.getenv('JSTREAM_LOCAL', 'none'), fname_config)
                if not os.path.isfile(fpath):
                    # Use the system copy
                    fpath = PATH(PATH_CONFIG, FNAME_CONFIG)
        print 'Loading run settings from:\n', fpath
    
        # Load the initial (default) run configuration                    
        self._config = json.loads(open(fpath).read())
        self._config['data_sources']['use_real_5122']   = True  # Only allow real hardware
        self._config['hdf5_writer']['write_interval_s'] = 60    # Hardwire 1 min file length
        self._data_sources = copy.deepcopy(self._config['data_sources']['devices'])

        # Create the switchable GUI screens
        self._screens = {}
        self.setCentralWidget(QStackedWidget())
        self._screens['welcome'] = self.centralWidget().addWidget(
            WelcomeScreen(self)
        )
        self._screens['main_options'] = self.centralWidget().addWidget(
            MainOptionsScreen(self, PATH(PATH_SETTINGS, FNAME_RUN_OPTS))
        )
        self._screens['time_options'] = self.centralWidget().addWidget(
            TimeOptionsScreen(self, PATH(PATH_SETTINGS, FNAME_RUN_OPTS))
        )
        self._screens['hw_options'] = self.centralWidget().addWidget(
            HWOptionsScreen(self)
        )
        self._screens['writer_options'] = self.centralWidget().addWidget(
            WriterOptionsScreen(self, PATH(PATH_SETTINGS, FNAME_IFO_OPS))
        )
        self._screens['database'] = self.centralWidget().addWidget(
            DatabaseScreen(self, PATH(PATH_SETTINGS, FNAME_IFO_OPS))
        )
        for src in self._data_sources:
            self._screens[src['source_name']] = self.centralWidget().addWidget(
                SourceOptionsScreen(
                    self,
                    src,
                    PATH(PATH_SETTINGS, FNAME_HW_OPTS),
                    PATH(PATH_SETTINGS, FNAME_HW_INCOMP)
                )
            )

        # Launch the welcome screen (non-blocking)
        self.screen_selector('welcome')

        # Start the network client
        self._streamer.connection_init()     
        return

    def screen_selector(self, name):
        """ Switches between screens 
        """
        # Set the specified screen
        self._actv_scrnm = name
        screen_idx = self._screens[name]
        self.centralWidget().setCurrentIndex(screen_idx)

        # Reconfigure the window
        self.setWindowTitle(self.centralWidget().currentWidget().title)
        self.setFixedSize(*self.centralWidget().currentWidget().size)
        self.show()
        return

    def connection_status(self):
        """ Queries the realtime server connection state
        """
        try:
            state = self._streamer.connection_status()
        except RuntimeError as error:
            return False
        return state        
        
    def run_status(self):
        """ Queries the realtime server run state
        """
        try:
            state = self._streamer.run_status()
        except RuntimeError as error:
            return False
        return state
        
    def live_streaming_select(self):
        """ Queries the realtime server run state before allowing/disallowing a new run 
        """
        try:
            if not self._streamer.run_status():
                # Launch the options screen for a new run
                self.screen_selector('main_options')
            else:
                # Join the run in progress
                self._channel_sources = {}
                self._accepting_data = True
                self._streamer.run_start(None)
                self.hide()
        except RuntimeError as error:
            # Report the error
            QMessageBox.critical(self, 'Runtime Error', '%s' % error)
        return

    @gUtil.synchronized
    def freq_res_set(self, smoothing_len_E):
        """ Sets the frequency bin averaging """
        self._streamer.model.freq_averaging_set(smoothing_len_E)
        return

    def live_mode_set(self, t_window_s):
        """ Sets the live-window time interval """
        self._streamer.model.live_mode_set(t_window_s)
        return

    def monitor_ifo_state(self, on_off):
        """ Enables/disables locking DAQ to auto-locker-provided IFO state """
        self._config['monitor_ifo_state'] = on_off
        return

    def run_mode_on_off(
        self,
        setting,
        on_off
    ):
        """ Enables/disables a run setting """
        self._config['data_sources'][setting] = on_off
        if  setting == 'calibrate_5122' \
        and on_off:
            # Warn the user about calibrating
            msg = 'Self-calibrating the digitizers will take approximately 10 minutes and cannot be ' \
                    + 'interrupted. Calibration only needs to be performed if the ambient temperature ' \
                    + 'has changed by more than 5 degrees Celsius.'
            QMessageBox.warning(self, 'Warning', msg)
        return

    @gUtil.synchronized
    def _csd_handler(self, *args):
        """ Callback function for new CSD data """
        if not self._accepting_data:
            return

        if not self._initialized:
            # Get the initial display settings
            use_default_display = True 
            if os.getenv('JSTREAM_LOCAL') is not None:
                # Try to find a local display settings file
                fpath_display = PATH(os.getenv('JSTREAM_LOCAL'), FNAME_DISP_CONF)
                if os.path.exists(fpath_display):
                    use_default_display = False
            if use_default_display:    
                # Use the system default display settings file
                fpath_display = PATH(PATH_SETTINGS, FNAME_DISP_CONF)
                
            # Create and load the main GUI screen
            self._screens['main'] = self.centralWidget().addWidget(
                MainScreen(
                    self,
                    self._streamer.model,
                    fpath_display,
                    streaming = True
                )
            )
            self.screen_selector('main')
            self._initialized = True
        #~if(init)
            
        # Update the main GUI screen display
        self.centralWidget().currentWidget().update_view()
        return

    def _connection_handler(self, state):
        """ Handler for an established connection """
        if self._actv_scrnm == 'welcome':       
            self.centralWidget().currentWidget().connection_handler(state)
        return
            
    def _run_status_handler(self, state):
        """ Handler for a run state change """
        self._run_state = state
        if self._actv_scrnm == 'welcome':       
            self.centralWidget().currentWidget().run_status_handler(state)            
        return
        
    def _err_handler(self, msg):
        """ Handler for a data stream error """
        error_msg = 'Error:  ' + msg + \
                    '\n\nThe data feed has terminated on the server side. Close the GUI?'
        reply = QMessageBox.question(
            self,
            'Data Stream Error',
            error_msg,
            QMessageBox.Yes | QMessageBox.No
        )
        if reply == QMessageBox.Yes:
            # Shut down the GUI
            self.close()
        return

    def _msg_handler(self, msg):
        """ Handler for a logger message """
        msg = msg[:-1] if (msg[-1] == "\n") else msg
        print msg
        if self._initialized:
            # Update the main window display
            self.centralWidget().currentWidget().update_log(msg)
        return

    def closeEvent(self, event):
        """ Reimplemented handler for a window close event """
        end_run = False
        self._accepting_data = False
        if self._streamer.connection_status():    
            if self._streamer.run_status() and \
               self._actv_scrnm == 'main':
                # Ask the user whether to end the DAQ run on the server
                reply = QMessageBox.question(
                    self,
                    'Confirm Quit',
                    'A data acquisition is currently in progress. Kill the run?',
                    QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel
                )
                if reply == QMessageBox.Yes:
                    end_run = True
                elif reply == QMessageBox.No:
                    end_run = False
                else:
                    # Cancelled: Return to the GUI screen
                    self._accepting_data = True
                    event.ignore()
                    return
        self._streamer.stop(end_run)
        return
        
    @property
    def run_state(self):
        """ Returns the DAQ server run state
        """
        return self._run_state
        
    def kill_run(self):
        """ Signal the DAQ server to terminate the run
        """
        reply = QMessageBox.question(
            self,
            'Run Terminate Confirm',
            "Kill the run currently in progress?",
            QMessageBox.Yes | QMessageBox.No
        )
        if reply == QMessageBox.Yes:
            self._streamer.stop(True)
        return

    def run_config_update(
        self,
        setting,
        value
    ):
        """ Updates a run configuration setting """
        self._config[setting] = value
        return

    def writer_config_update(
        self,
        setting,
        value
    ):
        """ Updates an HDF5 writer configuration setting """
        self._config['hdf5_writer'][setting] = value
        return

    def source_config_update(
        self,
        source_name,
        setting,
        value
    ):
        """ Updates a data source configuration setting """
        needs_update = [self._data_sources, self._config['data_sources']['devices']]
        for conf in needs_update:
            idx = [src['source_name'] for src in conf].index(source_name)
            conf[idx][setting] = value
        return

    def source_on_off(
        self,
        source_name,
        on_off
    ):
        """ Adds/removes a data source to/from the run configuration """
        conf = self._config['data_sources']['devices']
        if on_off:
            # Add the data source to the run config
            idx = [src['source_name'] for src in self._data_sources].index(source_name)
            source = self._data_sources[idx]
            conf.append(source)
        else:
            # Remove the data source from the run config
            idx = [src['source_name'] for src in conf].index(source_name)
            conf.pop(idx)
        return

    def channel_source_update(
        self,
        chn_idx,
        value
    ):
        """ Updates the source description for the supplied channel """
        src_idx, dev_idx = divmod(chn_idx, 2)
        src_name = self._data_sources[src_idx]['source_name']
        run_conf = self._config['data_sources']['devices']
        run_idx = [src['source_name'] for src in run_conf].index(src_name)
        needs_update = [self._data_sources[src_idx], run_conf[run_idx]]
        for conf in needs_update:
            conf['chn_sources'][dev_idx] = value
        return

    def run_start(self):
        """ Submits new data acquisition job to server """
        try:
            # Package the run configuration as a JSON string for transmission
            if not len(self._config['data_sources']['devices']) > 0:
                raise RuntimeError('At least one data source must be enabled')
            settings = json.dumps(
                self._config,
                sort_keys = True,
                indent = 4,
                separators = (',', ': ')
            )

            # Get the channel sources
            self._channel_sources = {}
            for src_idx, source in enumerate(self._config['data_sources']['devices']):
                for dev_idx in range(2):
                    chn_idx = 2 * src_idx + dev_idx
                    self._channel_sources[chn_idx] = source['chn_sources'][dev_idx]

            # Connect to DAQ server and start the run
            # (Fails if not connected)
            self._accepting_data = True
            self._streamer.run_start(settings)
            self.hide()
        except RuntimeError as error:
            # Report the error
            QMessageBox.critical(self, 'Runtime Error', '%s' % error)
        return

    def pause(self, on_off):
        """ Pauses/unpauses data updating """
        self._accepting_data = not on_off
        return

    def database_search(self, search):
        """ Connects to DAQ server and performs a remote database search """
        results = self._database.search(search)
        return results

    def database_transfer_to_disk(
        self,
        query,
        loc_path,
        write_all
    ):
        """ Connects to the DAQ server and performs a remote database transfer.
            Writes each received frame to local disk. """
        self._database.transfer_to_disk(
            query,
            loc_path,
            write_all
        )
        return

    def database_transfer_to_model(self, runs):
        """ Connects to the DAQ server and performs a remote database transfer.
            Loads accumulated frame into the data model. """

        # Get the channel source info for this run
        self._channel_sources = {}
        sources = self._database.search(runs[0])[0].get("sources", [])
        for chn_idx, source in enumerate(sources):
            self._channel_sources[chn_idx] = source

        # Transfer the time-averaged frame to the data model
        self._database.transfer_to_model(runs)

        # Launch the main GUI screen
        self.hide()
        self._screens['main'] = self.centralWidget().addWidget(
            MainScreen(
                self,
                self._streamer.model,
                PATH(PATH_SETTINGS, FNAME_DISP_CONF),
                streaming = False
            )
        )
        self.screen_selector('main')
        self.centralWidget().currentWidget().update_view()
        return

    def register_monitor(self, callback):
        """ Registers a pop-up window to receive CSD updates """
        self._streamer.callback_add('csd', callback)
        return

    @property
    def config(self):
        """ Read-only run configuration """
        return self._config

    @property
    def data_sources(self):
        """ Read-only list of available data sources """
        return self._data_sources

    @property
    def size(self):
        """ Main window size """
        return self._size

    @property
    def channel_sources(self):
        """ Read-only list of channel source descriptions """
        return self._channel_sources

#~GUI class
