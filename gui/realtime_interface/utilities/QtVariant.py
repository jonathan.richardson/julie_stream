'''
******
Assuming you're using python 2.x, the following wrapper makes your code compatible with both PySide and PyQt4:
******
Both PyQt4 and PySide have very similar mappings to the Qt API. There are, however, some differences, my opinions 
on which are described below:

Maintenance

They are both well maintained. The PySide produces a lot more regular releases at the moment: I think it's more 
closely linked to Qt than PyQt4 and as a newer project has a more active community at the moment. However, that 
is just my impression and it may be wrong.

PyQt4 has the option of commercial support available (I don't know whether this is true for PySide or not).

Licence

PyQt4 is released under either a commercial licence or the GPL; PySide is released under the LGPL. For commercial 
applications, this is a significant difference.

APIs and Python versions

PyQt4 supports two different APIs. API version 1 is the default for python 2.x applications and API version 2 is 
the default for python 3.x applications.

PySide only supports one API, which is roughly equivalent to PyQt4's API version 2. API version 2 (or the PySide 
API) is much nicer to work with than PyQt4's API version 1. In API version 1 you have a lot of code that casts 
python strings to QtCore.QStrings and back again. In API version 2 (and PySide) you just use python strings 
throughout. See below for a simple way of switching between PyQt4 and PySide if you want to play with both.

Most code I write seems to work equally well in PyQt4 and PySide. Historically, I had always used PyQt4 for python 
GUIs but most new stuff I write now uses PySide (mainly due to the more flexible licensing). I'd certainly 
recommend you try both and see how you find them. If you use QtVariant.py (below), switching between them is 

trivial and when you make a decision there will only be one file that needs updating.
Documentation

The documentation for both PyQt4 and PySide are auto-generated from the main Qt documentation. In my opinion, the 
PySide documentation is a better representation of what you actually use, but in practice I tend to just use the 
Qt documentation anyway (it's quite easy to mentally translate the C++ documentation to python).
External Libraries

If you're using external libraries, some do not work with PySide yet. There aren't many that you need to work with 
PySide to be honest, but a couple of years ago I wrote some code that used twisted (with the Qt reactor) and 
matplotlib and that forced me down the line of using PyQt4 rather than PySide. I think it's quite likely that these 
libraries will have been updated to support both by now, but I haven't checked.
'''

import sys
import os

default_variant = 'PySide'

env_api = os.environ.get('QT_API', 'pyqt')
if '--pyside' in sys.argv:
    variant = 'PySide'
elif '--pyqt4' in sys.argv:
    variant = 'PyQt4'
elif env_api == 'pyside':
    variant = 'PySide'
elif env_api == 'pyqt':
    variant = 'PyQt4'
else:
    variant = default_variant

if variant == 'PySide':
    from PySide import QtGui, QtCore
    # This will be passed on to new versions of matplotlib
    os.environ['QT_API'] = 'pyside'
    def QtLoadUI(uifile):
        from PySide import QtUiTools
        loader = QtUiTools.QUiLoader()
        uif = QtCore.QFile(uifile)
        uif.open(QtCore.QFile.ReadOnly)   
        result = loader.load(uif)
        uif.close()
        return result
elif variant == 'PyQt4':
    import sip
    api2_classes = [
            'QData', 'QDateTime', 'QString', 'QTextStream',
            'QTime', 'QUrl', 'QVariant',
            ]
    for cl in api2_classes:
        sip.setapi(cl, 1)
    from PyQt4 import QtGui, QtCore  
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.QString = str
    os.environ['QT_API'] = 'pyqt'
    def QtLoadUI(uifile):
        from PyQt4 import uic
        return uic.loadUi(uifile)
else:
    raise ImportError("Python Variant not specified")

__all__ = [QtGui, QtCore, QtLoadUI, variant]