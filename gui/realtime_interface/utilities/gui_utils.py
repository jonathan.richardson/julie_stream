#!/usr/bin/env python

from PyQt4.Qt import *

from threading import RLock


""" Provides cross-module access to shared data """
data_access = RLock()
def synchronized(func):
    """ Synchronization decorator """
    def new_func(*args, **kwargs):
        with data_access:
            return func(*args, **kwargs)
    return new_func


def screen_reset(
    title   = None,
    size    = None,
    spacing = None
):
    """ Window reset decorator """
    def wrap(func):
        def new_func(self, *args, **kwargs):
            # Hide the window content
            self.hide()
            QApplication.processEvents()

            # Reset the window layout
            self._objects = {}
            QWidget().setLayout(self.layout())
            self.setLayout(QGridLayout())

            # Reconfigure the window
            if title is not None:
                self.setWindowTitle(title)   
            if size is not None:
                self.setFixedSize(*size)
            if spacing is not None:
                self.layout().setVerticalSpacing(spacing)

            # Generate the new window content
            func(self, *args, **kwargs)
            
            # Show the window content
            self.show()
        return new_func
    return wrap  

    
###
## Returns the geometry of the widest available monitor
def screen_autodetect(widest = True):        
    if widest:
        func = max
    else:
        func = min
    num_screens = QDesktopWidget().numScreens()        
    widths = [QDesktopWidget().screenGeometry(idx).width() for idx in range(num_screens)]
    screen_idx = widths.index(func(widths))
    geometry = QDesktopWidget().screenGeometry(screen_idx)
    return geometry
    
    
###
## Enables/disables all options on the supplied screen/panel
def selected_options_enable(
    enabled,
    screen,
    *option_types
):
    for option_type in option_types:
        for child in screen.findChildren(option_type):
            # Enable/disable the option
            child.setEnabled(enabled)
            if type(child) is QLineEdit:
                validator = child.validator()
                if validator is not None:
                    # Update the textbox color
                    if enabled:
                        valid, _ = validator.validate(child.text(), 0)
                        if valid == QValidator.Acceptable:
                            color = 'background-color: lightgreen'
                        else:
                            color = 'background-color: red'
                    else:
                        color = 'background-color: '
                    child.setStyleSheet(color)
            elif type(child) is QPushButton:
                # Update the button color
                if enabled:
                    if child.isChecked():
                        color = 'background-color: lightgreen'
                    else:
                        color = 'background-color: red'
                else:
                    color = 'background-color: '
                child.setStyleSheet(color)                    
    return
