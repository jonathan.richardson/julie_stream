#!/usr/bin/env python

import math
from functools import partial as BIND
from collections import OrderedDict
from PyQt4.Qt import *

from .QRangeSlider import QRangeSlider
from .QLed import QLed
from . import plot_utils as pUtil
    

def std_button(
    label,
    callback,
    font_size   = 14,
    enabled     = True    
):
    """ Creates a standard push button """
    font = QFont('Arial', font_size)    
    button = QPushButton(label)
    button.setFont(font)
    button.setEnabled(enabled)
    button.clicked.connect(callback)
    return button 

def on_off_button(
    label,
    callback,
    on_off      = True,    
    enabled     = True,
    font_size   = 14,
    style       = None
):
    """ Creates an enable/disable button """
    if style is None:
        style = 'QPushButton:checked {background-color: lightgreen}' + \
                'QPushButton {background-color: #ff8080}'

    # Create the button
    button = std_button(
        label,
        callback,
        font_size,
        enabled    
    )    
    button.setStyleSheet(style)
    button.setCheckable(True)
    button.setChecked(on_off)
    return button     
    
def dropdown_menu_button(
    label,
    callback,
    menu_items,
    default_val = None,
    font_size   = 14,
    enabled     = True,
):
    """ Creates a standard button with a drop-down menu """

    # Create the button
    font = QFont('Arial', font_size)    
    button = QPushButton(label)
    button.setFont(font)
    button.setEnabled(enabled)  

    # Set the drop-down menu 
    menu  = QMenu()    
    group = QActionGroup(button) 
    for key in menu_items.keys():
        # Add each menu item
        value = menu_items[key]
        item = QAction(key, button)
        item.setFont(font)
        item.setCheckable(True)
        if value == default_val:
            item.setChecked(True)           
        item.triggered.connect(BIND(callback, value))
        group.addAction(item)
        menu.addAction(item)
    button.setMenu(menu)
    return button
  
def navigation_button(
    label,
    callback,
    font_size   = 12, 
    checkable   = False,
    enabled     = True
):
    """ Creates a screen navigation button """
    style = 'QPushButton {color: black; background-color: lightblue}' + \
            'QPushButton:disabled {}'    
    button = std_button(
        label,
        callback,
        font_size,
        enabled    
    )
    button.setCheckable(checkable)
    button.setStyleSheet(style)
    return button

def embedded_image(fpath):
    """ Creates an embedded image """
    pixmap = QPixmap(fpath)
    image = QLabel()
    image.setPixmap(pixmap)   
    return image
    
def embedded_image_anim(fpath):
    """ Creates an animated embedded image """
    
    # Load the file into a QMovie
    movie = QMovie(fpath)
    movie.setCacheMode(QMovie.CacheAll)
    movie.setSpeed(100)
    movie.start()    
    
    # Embed the movie in a label
    image = QLabel()
    image.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
    image.setAlignment(Qt.AlignCenter)
    image.setMovie(movie)
    return image    
    
def std_label(
    text    = None,
    size    = 14,
    bold    = False,
    font    = 'Arial'
):
    """ Creates a text label """
    if text is not None:
        label = QLabel(text)
    else:
        label = QLabel()
    if bold:
        weight = QFont.Bold
    else:  
        weight = QFont.Normal
    label.setFont(QFont(font, size, weight))
    return label    
    
def menu_button_panel(
    options,
    label       = None,
    font_size   = 16,
    flat        = False,
    enabled     = True
):   
    """ Creates a panel of menu buttons """
     
    # Create the menu panel
    font = QFont('Arial', font_size)
    if label is not None:
        panel = QGroupBox(label)
    else:
        panel = QGroupBox()
    panel.setFlat(flat)
    panel.setFont(font)
    panel.setEnabled(enabled)
    
    # Add the menu buttons
    layout = QVBoxLayout()           
    for opt in options:
        button = std_button(*opt)
        layout.addWidget(button)       
    layout.setSpacing(15)
    layout.addStretch(1)      
    panel.setLayout(layout)    
    return panel    
    
def on_off_panel(
    options,
    label           = None,
    on_off          = False,
    flat            = False,    
    mstr_callback   = None
):    
    """ Create a panel of on/off buttons """
    
    # Create the panel
    if label is None:
        panel = QGroupBox()
    else:
        panel = QGroupBox(label)        
    panel.setFlat(flat)        
    if mstr_callback is None: 
        font = QFont('Arial', 14)
    else:
        font = QFont('Arial', 12)
        panel.setCheckable(True)
        panel.setChecked(on_off)
        panel.clicked.connect(
            BIND(
                mstr_callback,
                panel
            )
        )
    panel.setFont(font)
    
    # Add the on/off buttons
    layout = QVBoxLayout()           
    for opt in options:
        button = on_off_button(*opt)
        layout.addWidget(button)
    layout.setSpacing(15)
    layout.addStretch(1)      
    panel.setLayout(layout)    
    return panel
    
def server_state_panel(kill_run_callback):
    """ Create the DAQ server state panel """
    objects = {}
    font = QFont('Arial', 14)
    font_size_sm = 13
    font_sm = QFont('Arial', font_size_sm)     
    
    # Create the panel
    layout = QGridLayout()      
    panel = QGroupBox(
        'DAQ server status', 
        font = font
    )     

    # Connection status indicator
    objects["connection"] = QLed(onColour=1, value=True)
    label = QLabel('Online', font = font_sm)
    layout.addWidget(objects["connection"], 0, 0)
    layout.addWidget(label, 0, 1) 
    
    # Run status inidicator
    objects["run_status"] = QLed(onColour=2, value=False)
    label = QLabel('Run in progress', font = font_sm)
    objects["terminator"] = std_button(
        'Kill current run',
        kill_run_callback,
        font_size = font_size_sm,
        enabled = False
    )
    layout.addWidget(objects["run_status"], 1, 0)
    layout.addWidget(label, 1, 1)     
    layout.addWidget(objects["terminator"], 2, 1)    

    # Configure the layout   
    layout.setColumnStretch(1, 1) 
    panel.setLayout(layout)    
    return panel, objects

def textbox_panel(
    label,
    default         = '',
    font_size       = 14,
    input_type      = str,
    valid_range     = [None, None],
    placeholder     = None,
    spec_callback   = None,
    fwd_if_invalid  = False,
    footnote        = None,
    enabled         = True
):     
    """ Creates a text box panel """
    
    # Create the panel
    font = QFont('Arial', font_size)
    panel = QGroupBox(label)
    panel.setFont(font)

    # Create the input text box
    box = textbox(
        default         = default,
        font_size       = font_size,
        input_type      = input_type,
        valid_range     = valid_range,
        placeholder     = placeholder,
        spec_callback   = spec_callback,
        fwd_if_invalid  = fwd_if_invalid,
        enabled         = enabled
    )   
    if footnote is not None:
        # Create the footnote label
        fnote = std_label(
            text = footnote,
            size = 12
        )
    
    # Set the panel layout
    layout = QVBoxLayout()
    layout.addWidget(box)
    if footnote is not None:    
        layout.addWidget(fnote)
    layout.addStretch(1)
    panel.setLayout(layout)
    return panel, box
    
def textbox(
    default         = '',
    font_size       = 14,
    input_type      = str,
    valid_range     = [None, None],
    placeholder     = None,
    spec_callback   = None,
    fwd_if_invalid  = False,
    enabled         = True
):        
    """ Creates a textbox widget """
    
    # Create the input textbox
    font = QFont('Arial', font_size)
    box = QLineEdit(default)
    box.setFont(font)
    if placeholder is not None:
        box.setPlaceholder(placeholder)
        box.setMaxLength(len(placeholder))
    box.setEnabled(enabled)
    box.textEdited.connect(
        BIND(
            textbox_callback,
            box,
            spec_callback,
            fwd_if_invalid,
            input_type
        )
    )    
    if input_type is float or \
       input_type is int:
        # Set a numerical range enforcer for input
        if input_type is float:
            validator = QDoubleValidator(None)
        else:
            validator = QIntValidator(None)        
        if valid_range[0] is not None:
            validator.setBottom(input_type(valid_range[0]))
        if valid_range[1] is not None:
            validator.setTop(input_type(valid_range[1]))   
        box.setValidator(validator)
        if enabled:
            # Validate the current text (color the background)
            valid, _ = box.validator().validate(box.text(), 0)
            if valid == QValidator.Acceptable:
                color = 'background-color: lightgreen'
            else:
                color = 'background-color: #ff8080'
            box.setStyleSheet(color)
    return box

def textbox_callback(
    box,
    spec_callback,
    fwd_if_invalid,
    input_type,
    text
):
    """ Textbox general callback """
    if box.validator():
        # Validate the input text
        valid, _ = box.validator().validate(text, 0)
        if valid == QValidator.Acceptable:
            # Color the background green
            color = 'background-color: lightgreen'
            box.setStyleSheet(color)
        else:
            # Color the background red
            color = 'background-color: #ff8080'
            box.setStyleSheet(color)
            if not fwd_if_invalid:
                return

    # Optionally call a specialized callback
    if spec_callback is not None:
        spec_callback(
            input_type,
            text
        )
    return       
    
def dropdown_list(
    label,
    options,
    callback    = None,
    default     = None,
    font_size   = 14,
    enabled     = True
):
    """ Creates a drop-down list panel """
    
    # Order the supplied options by value
    ordered_opts = []
    if isinstance(options, list):
        keys = vals = options
    else:
        keys = options.keys()
        vals = options.values()
    for val in sorted(vals):
       idx = vals.index(val)
       ordered_opts.append((keys[idx], vals[idx]))
    menu_items = OrderedDict(ordered_opts)

    # Create the panel
    font = QFont('Arial', font_size)
    panel = QGroupBox(label)
    panel.setFont(font)

    # Create the drop-down list
    button = QComboBox()
    button.setFont(font)
    button.addItems(menu_items.keys())
    button.setEnabled(enabled)

    # Set the default option
    if default in menu_items.values():
        default_idx = menu_items.values().index(default)
    else:
        default_idx = 0
    button.setCurrentIndex(default_idx)  
    if callback is not None:
        # Set the callback (must come after setCurrentIndex)
        button.currentIndexChanged.connect(callback)

    # Set the panel layout
    layout = QVBoxLayout()
    layout.addWidget(button)
    layout.addStretch(1)
    panel.setLayout(layout)
    return panel, button

def database_search_panel(
    callbacks,
    ifo_operators
): 
    """ Creates the database search interface """
    objects = {}
    font_sm = QFont('Arial', 12)
    font_lg = QFont('Arial', 14) 
    
    # Create the panel & assign it a layout
    panel = QGroupBox("1. Search remote database")
    panel.setFont(font_lg)    
    layout = QGridLayout()
    panel.setLayout(layout)

    # Search by run ID
    key = 'id'
    objects[key] = QRadioButton("Search by five-digit run ID")
    objects[key].setFont(font_sm)
    objects[key].clicked.connect(callbacks[key]) 
    layout.addWidget(objects[key], 0, 0)  
    
    # Text box subfield
    key = 'id_textbox'
    objects[key] = QLineEdit()
    objects[key].setMaxLength(5)
    objects[key].setText('00000')
    objects[key].setFont(font_sm)     
    layout.addWidget(objects[key], 1, 0)

    # Search by operator name
    key = 'operator'
    objects[key] = QRadioButton("Search by operator name")
    objects[key].setFont(font_sm)
    objects[key].clicked.connect(callbacks[key])
    layout.addWidget(objects[key], 2, 0) 

    # IFO operator drop-down subfield
    key = 'op_list'
    objects[key] = QComboBox()
    objects[key].setFont(font_sm)
    objects[key].addItems(sorted(ifo_operators))
    layout.addWidget(objects[key], 3, 0)
    
    # Search by date/time range
    key = 'date'
    objects[key] = QRadioButton("Search by date range")
    objects[key].setFont(font_sm)
    objects[key].clicked.connect(callbacks[key])
    layout.addWidget(objects[key], 4, 0)
    
    # Minimum time subfield
    key = 'time1'
    date_fmt = 'MM/dd/yyyy hh:mm:ss AP'
    objects[key] = QDateTimeEdit(QDateTime(2014, 1, 1, 0, 0, 0))
    objects[key].setCalendarPopup(True)
    objects[key].setFont(font_sm)
    objects[key].setDisplayFormat(date_fmt)
    layout.addWidget(objects[key], 5, 0)
    
    # Maximum time subfield  
    key = 'time2'
    objects[key] = QDateTimeEdit(QDateTime.currentDateTime())       
    objects[key].setCalendarPopup(True)
    objects[key].setFont(font_sm)
    objects[key].setDisplayFormat(date_fmt)
    layout.addWidget(objects[key], 6, 0)
        
    # Submit search request
    key = 'search'
    objects[key] = navigation_button('Search', callbacks[key])
    layout.addWidget(objects[key], 7, 0)
    return panel, objects
    
def database_transfer_panel(callbacks):
    """ Creates the data transfer interface """
    objects = {}
    font_vsm = QFont('Arial', 11)
    font_sm = QFont('Arial', 12)
    font_lg = QFont('Arial', 14)

    # Create the panel & assign it a layout
    panel = QGroupBox("2. Select run")
    panel.setFont(font_lg)
    layout = QGridLayout()
    panel.setLayout(layout)
    
    # Search results display box
    key = 'list'
    objects[key] = QTreeWidget()
    objects[key].setFont(font_sm)
    objects[key].setUniformRowHeights(True)
    objects[key].setSelectionMode(QAbstractItemView.ExtendedSelection)    
    objects[key].clicked.connect(callbacks[key])
    layout.addWidget(objects[key], 0, 0, 1, 2)
        
    # Set up the displayed data fields
    header = {
        0 : ['Run ID', 100],
        1 : ['Duration', 150],
        2 : ['Operator', 160], 
        3 : ['Start Time', 290],
        4 : ['End Time', 290],
        5 : ['Comments', 600],      
        6 : ['Channel Sources', 1500]
    }
    labels = [header[col_idx][0] for col_idx in sorted(header.keys())]
    objects[key].setHeaderItem(QTreeWidgetItem(labels))             
    objects[key].setColumnCount(len(header))
    objects[key].setFont(QFont('Lucida Console', 12))
    for col_idx in header.keys():
        objects[key].setColumnWidth(col_idx, header[col_idx][1])

    # Selected run label
    key = 'label'
    objects[key] = std_label(size = 12, bold = True)
    objects[key].setStyleSheet('QLabel {color : blue}') 
    layout.addWidget(objects[key], 1, 0, 1, 2)      
    
    # Selected run time range
    key = 'slider'
    objects[key] = QRangeSlider()
    objects[key].setFixedHeight(18)
    objects[key].setMin(0)
    objects[key].setRange(0, objects[key].max())
    objects[key].setEnabled(False)
    layout.addWidget(objects[key], 2, 0, 1, 2)  

    # Receiver accumulation mode
    key = 'accum'
    objects[key] = QCheckBox("Display the selected data (averaged over the selected time range)")
    objects[key].setFont(font_sm)
    objects[key].setChecked(True)       
    objects[key].setEnabled(False)
    layout.addWidget(objects[key], 3, 0, 1, 2)  
    
    # Receiver write mode
    key = 'write_data'
    objects[key] = QCheckBox("Write the selected data to local disk (creates an HDF5 file)")
    objects[key].setFont(font_sm)
    objects[key].setEnabled(False)
    objects[key].clicked.connect(callbacks[key])
    layout.addWidget(objects[key], 4, 0, 1, 1)  
    
    # Write data average option
    key = 'write_avg'
    objects[key] = QRadioButton("Write the final averages only")
    objects[key].setFont(font_vsm)
    objects[key].setEnabled(False)
    objects[key].setChecked(True)
    layout.addWidget(objects[key], 4, 1, 1, 1) 

    # Write all frames option
    key = 'write_all'
    objects[key] = QRadioButton("Write all frames in the selected time range")
    objects[key].setFont(font_vsm)
    objects[key].setEnabled(False)
    layout.addWidget(objects[key], 5, 1, 1, 1)
    
    # Receiver write mode for run metadata
    key = 'write_meta'
    objects[key] = QCheckBox("Write the run meta-data to local disk (creates a JSON file)")
    objects[key].setFont(font_sm)
    objects[key].setEnabled(False)
    layout.addWidget(objects[key], 6, 0, 1, 1)     
    
    # Submit transfer request 
    key = 'transfer'
    objects[key] = navigation_button(
        'Transfer data',
        callbacks[key],
        enabled = False
    )
    objects[key].setStyleSheet('QPushButton {}')
    layout.addWidget(objects[key], 7, 0, 1, 2)
    return panel, objects
    
def integration_time_panel(
    initial_value,
    label = "Wall time",
    objname = 'time',
):  
    """ Creates the run control panel """
    objects = {}

    # Create the panel & assign it a layout
    font = QFont('Arial', 12)
    panel = QGroupBox(label)
    panel.setFont(font)
    layout = QVBoxLayout()
    panel.setLayout(layout)

    # Integration time label
    objects[objname] = std_label(
        text = initial_value,
        size = 18, 
        font = 'Lucida Console'
    )
    layout.addWidget(objects[objname])

    return panel, objects
    
def run_control_panel(
    callbacks,
    stream_mode
):  
    """ Creates the control panel for run settings """
    objects     = {}
    font_size   = 10

    # Create the panel & assign it a layout
    panel = QGroupBox()
    layout = QGridLayout()
    panel.setLayout(layout)
    
    # Stop button
    key = 'stop'
    objects[key] = std_button(
        'Stop',
        callbacks[key],
        font_size   = font_size
    )
    layout.addWidget(objects[key], 0, 0)    
    
    # Pause button
    key = 'pause'
    style = 'QPushButton:checked {background-color: yellow}'
    objects[key] = on_off_button(
        'Pause',
        callbacks[key],
        font_size   = font_size,
        on_off      = False,
        style       = style,
        enabled     = stream_mode
    )    
    layout.addWidget(objects[key], 1, 0)   
 
    # Live view mode button
    key = 'live'
    times = [1, 5, 10, 30, 60]
    options = [('OFF', 0)] + [(str(tm) + ' s', tm) for tm in times]
    menu_items = OrderedDict(options)
    objects[key] = dropdown_menu_button(
        'Live view',
        callbacks[key],
        menu_items,
        default_val     = None,
        font_size       = font_size,
        enabled         = stream_mode
    )    
    layout.addWidget(objects[key], 2, 0) 
    
    # Save figure button
    key = 'save'
    objects[key] = std_button(
        'Save figure',
        callbacks[key],
        font_size   = font_size
    )
    layout.addWidget(objects[key], 0, 1)    
    return panel, objects
 
def monitor_panel(callbacks, stream_mode):  
    """ Creates add-on monitor launch panel """
    objects     = {}
    font_size   = 10

    # Create the panel & assign it a layout
    font = QFont('Arial', 12)
    panel = QGroupBox('Monitors')
    panel.setFont(font)    
    layout = QGridLayout()
    layout.setAlignment(Qt.AlignTop)      
    panel.setLayout(layout)
    
    # Transient monitor button
    key = 'transient'
    style = 'QPushButton:checked {background-color: lightgreen}'
    objects[key] = on_off_button(
        'Transient detector',
        callbacks[key],
        font_size   = font_size,
        on_off      = False,
        style       = style,
        enabled     = stream_mode
    )
    layout.addWidget(objects[key], 0, 0)
    
    # # Time correlator button
    # key = 'correlation'
    # style = 'QPushButton:checked {background-color: lightgreen}'
    # objects[key] = on_off_button(
        # 'Time correlation',
        # callbacks[key],
        # font_size   = font_size,
        # on_off      = False,
        # style       = style,
        # enabled     = False
    # )
    layout.addWidget(objects[key], 1, 0)    
    return panel, objects 

def display_control_panel(
    callbacks,
    display_settings
):    
    """ Creates the control panel of on/off display options """
    objects     = {}
    font_size   = 10
    num_chs     = display_settings['channels'].values().count(True)

    # Create the panel & assign it a layout
    panel = QGroupBox()
    layout = QGridLayout()
    panel.setLayout(layout)
 
    # Coherence button
    if num_chs > 1:
        enabled = True
    else: 
        enabled = False
    key = 'coherence'        
    objects[key] = on_off_button(
        'Coherence',
        callbacks[key],
        font_size   = font_size,
        enabled     = enabled,
        on_off      = display_settings[key]
    )
    layout.addWidget(objects[key], 0, 0)    
 
    # X-axis log/linear scale button
    key = 'xlog'
    objects[key] = on_off_button(
        'Log scale X',
        callbacks[key],
        font_size   = font_size,
        on_off      = display_settings[key]
    )
    layout.addWidget(objects[key], 1, 0)       

    # Y-axis log/linear scale button
    key = 'ylog'
    objects[key] = on_off_button(
        'Log scale Y',
        callbacks[key],
        font_size   = font_size,
        on_off      = display_settings[key]
    )
    layout.addWidget(objects[key], 2, 0) 
    
    # Common Y-range button
    if num_chs > 1:
        enabled = True
    else:
        enabled = False
    key = 'common_y'
    objects[key] = on_off_button(
        'Common Y',
        callbacks[key],
        font_size   = font_size,
        enabled     = enabled,
        on_off      = display_settings[key]
    )
    layout.addWidget(objects[key], 3, 0)    

    # Rescale button
    key = 'rescale'
    objects[key] = std_button(
        'Rescale',
        callbacks[key],
        font_size = font_size
    )
    layout.addWidget(objects[key], 0, 1)     
    
    # Grid button
    key = 'grid'
    objects[key] = on_off_button(
        'Grid lines',
        callbacks[key],
        font_size   = font_size,
        on_off      = display_settings[key]
    )
    layout.addWidget(objects[key], 1, 1)
    
    # Channel labels button
    key = 'source_labels'
    objects[key] = on_off_button(
        'Source labels',
        callbacks[key],
        font_size   = font_size,
        on_off      = display_settings[key]
    )
    layout.addWidget(objects[key], 2, 1)    

    # Holographic noise overlay button
    key = 'holo_trace'
    objects[key] = on_off_button(
        'Hogan noise',
        callbacks[key],
        font_size   = font_size,
        on_off      = display_settings[key],
        enabled     = False
    )
    layout.addWidget(objects[key], 3, 1)    
    return panel, objects

def freq_range_panel(
    callbacks,
    valid_range_hz,
    cur_range_hz,
    label = None
):    
    """ Creates the frequency range control panel """
    objects         = {}
    font_size       = 10
    valid_range_mhz = [bound / 1.e6 for bound in valid_range_hz]
    cur_range_mhz   = [bound / 1.e6 for bound in cur_range_hz]
    if label is None:
        label = 'Freq range (MHz)'
        
    # Create the panel & assign it a layout
    font = QFont('Arial', 12)
    panel = QGroupBox(label)
    panel.setFont(font)
    layout = QGridLayout()
    panel.setLayout(layout)    
    
    # Frequency range minimum textbox   
    key = 'freq_min'
    objects[key] = textbox(
        default         = str(cur_range_mhz[0]),
        font_size       = 12,
        input_type      = float,
        valid_range     = valid_range_mhz,
        spec_callback   = callbacks[key],
        fwd_if_invalid  = True
    )       
    layout.addWidget(objects[key], 0, 0)
    
    # Min/max separating label
    key = 'freq_label'
    objects[key] = std_label(
        text = ':',
        size = 12,
        bold = True
    )
    layout.addWidget(objects[key], 0, 0, 1, 2, Qt.AlignHCenter)
    
    # Frequency range maximum textbox
    key = 'freq_max'
    objects[key] = textbox(
        default         = str(cur_range_mhz[1]),
        font_size       = 12,
        input_type      = float,
        valid_range     = valid_range_mhz,
        spec_callback   = callbacks[key],
        fwd_if_invalid  = True
    )       
    layout.addWidget(objects[key], 0, 1, Qt.AlignRight)

    # Set frequency range button
    key = 'freq_set'
    objects[key] = navigation_button(
        'Set',
        callbacks[key],
        font_size = font_size
    )
    layout.addWidget(objects[key], 1, 0)
    
    # Clear frequency range button
    key = 'freq_reset'
    objects[key] = navigation_button(
        'Clear',
        callbacks[key],
        font_size = font_size
    )
    layout.addWidget(objects[key], 1, 1)
    return panel, objects
    
def freq_resolution_panel(
    callback,
    sample_freq,
    dft_size_E,
    smoothing_len_E
):
    """ Creates the frequency resolution control panel """
    objects = {}
    
    # Get the formatted frequency resolution options    
    options = []
    for bin_size_E in range(dft_size_E - 3):
        dft_size = 1 << int(dft_size_E - bin_size_E)
        res_str = pUtil.freq_resolution_str(sample_freq, dft_size)
        options.append((res_str, sample_freq / dft_size))
    menu_items = OrderedDict(options)
    
    # Create the drop-down control panel
    panel, objects['freq_res'] = dropdown_list(
        'Freq bin width',
        menu_items,
        callback    = callback,
        default     = smoothing_len_E,
        font_size   = 12
    )
    return panel, objects
    
def chn_selection_panel(
    callbacks,
    active_chs,
    incl_update_button = True
):   
    """ Creates the channel selection panel """
    objects     = {}
    font_size   = 10

    # Create the panel & assign it a layout
    font = QFont('Arial', 12)
    panel = QGroupBox('Select channel(s)')
    panel.setFont(font)
    layout = QGridLayout()
    layout.setAlignment(Qt.AlignTop)       
    panel.setLayout(layout)    

    # Individual subplot enable/disable buttons    
    style = 'QPushButton:checked {background-color: lightgreen}'
    chn_list = [key for key in callbacks.keys() if isinstance(key, int)]
    for chn_idx in chn_list:
        label = 'Chn %d' % chn_idx            
        on_off = True if chn_idx in active_chs else False
        objects[chn_idx] = on_off_button(
            label,
            callbacks[chn_idx],
            font_size   = font_size,
            on_off      = on_off,
            style       = style
        )        
        layout.addWidget(
            objects[chn_idx], 
            math.floor(chn_idx / 2.),
            chn_idx % 2
        )
    #~for(chn_idx)

    if incl_update_button:
        # "Update view" button
        key = 'chs_set'
        objects[key] = navigation_button(
            'Update view',
            callbacks[key],
            font_size   = font_size
        )
        layout.addWidget(
            objects[key], 
            math.ceil(len(callbacks.keys()) / 2), 
            0, 1, 2
        )        
    return panel, objects

def lookback_time_panel(
    callbacks,
    valid_range,
    cur_val,
    label = None
):
    """ Creates the lookback time control panel """
    objects     = {}
    font_size   = 10
    if label is None:
        label = 'Lookback Time (s)'
        
    # Create the panel & assign it a layout
    font = QFont('Arial', 12)
    panel = QGroupBox(label)
    panel.setFont(font)
    layout = QGridLayout()
    panel.setLayout(layout)
    
    # Lookback time textbox   
    key = 'lookback_val'
    objects[key] = textbox(
        default         = str(cur_val),
        font_size       = 12,
        input_type      = int,
        valid_range     = valid_range,
        spec_callback   = callbacks[key],
        fwd_if_invalid  = True
    )
    layout.addWidget(objects[key], 0, 0)

    # Set lookback time button
    key = 'lookback_set'
    objects[key] = navigation_button(
        'Set',
        callbacks[key],
        font_size = font_size,
        enabled = False
    )    
    layout.addWidget(objects[key], 1, 0)
    return panel, objects

def transient_control_panel(callbacks):  
    """ Creates the control panel for transient monitor """
    objects     = {}
    font_size   = 10

    # Create the panel & assign it a layout
    panel = QGroupBox()
    layout = QGridLayout()
    panel.setLayout(layout)

    # Pause button
    key = 'pause'
    style = 'QPushButton:checked {background-color: yellow}'
    objects[key] = on_off_button(
        'Pause',
        callbacks[key],
        font_size   = font_size,
        on_off      = False,
        style       = style
    )    
    layout.addWidget(objects[key], 0, 0) 

    # Save figure button
    key = 'save'
    objects[key] = std_button(
        'Save figure',
        callbacks[key],
        font_size   = font_size
    )
    layout.addWidget(objects[key], 1, 0)        
    
    # Update baseline button
    key = 'baseline'
    objects[key] = std_button(
        'Update baseline',
        callbacks[key],
        font_size = font_size
    )
    layout.addWidget(objects[key], 0, 1)       
    
    # Rescale figure button
    key = 'rescale'
    objects[key] = std_button(
        'Rescale',
        callbacks[key],
        font_size   = font_size
    )
    layout.addWidget(objects[key], 1, 1)        
    return panel, objects  

def logger_message_box():
    """ Creates the display panel for logger messages """
    box = QPlainTextEdit()
    box.setMaximumBlockCount(10000)
    box.setReadOnly(True)
    box.setFont(QFont('Lucida Console', 10))
    return box
