#!/usr/bin/env python

import numpy as np


def sec_to_time_str(
    seconds,
    compact = False
):
    """ Convert seconds to days, hours, minutes, seconds"""
    sec = int(seconds)

    days = sec / 86400
    sec -= 86400*days
    
    hrs = sec / 3600
    sec -= 3600*hrs

    mins = sec / 60
    sec -= 60*mins

    if compact:
        string = "%02d:%02d:%02d:%02d" % (days, hrs, mins, sec)
    else:
        string = "%d days    %d hrs    %d min    %d s" % (days, hrs, mins, sec)
    return string


def round_floor(val, round_to):
    """Rounds the supplied value to the floor of the specified increment"""
    return val - val % round_to


def round_ceil(val, round_to):
    """Rounds the supplied value to the ceiling of the specified increment"""
    if val % round_to == 0.:
        return val
    else:
        return val + round_to - val % round_to


def tick_maker(
        axis_range,
        is_log
):
    """Creates an array of tick values in the specified data range"""
    min_val = axis_range[0]
    max_val = axis_range[1]
    assert min_val < max_val
    if is_log:
        assert min_val > 0
    
    if is_log:
        # Assign power of 10 tick values
        min_E = np.log10(min_val)
        max_E = np.log10(max_val)
        if int(max_E - min_E) > 1:
            # If at least two ticks will be produced, return them
            E_range = range(
                int(round_ceil(min_E, 1.)), 
                int(round_floor(max_E, 1.) + 1)
            )
            ticks = [10**E for E in E_range]
            return ticks

    # Assign linearly increasing tick values
    diff = max_val - min_val
    diff_E = np.log10(diff)
    tick_increment = 10**(int(diff_E))
    if (diff / tick_increment) < 2:
        # If less than two ticks will be produced, decrease the step size
        tick_increment /= 10.
    if (diff / tick_increment) > 8:
        # If more than eight ticks will be produced, increase the step size
        tick_increment *= 2.
        
    # Generate the tick values in the specified range with the specified step size
    ticks = np.arange(
        round_ceil(min_val, tick_increment),
        round_floor(max_val, tick_increment) + tick_increment,
        tick_increment
    )
    if is_log:
        # If using linear ticks with log scale, use only every other tick
        ticks = ticks[::2]
    return ticks


def tick_labeler(
    tick_vals, 
    is_log = False
):
    """Create LaTeX labels for the supplied set of tick values"""
    tick_labels = []    
    
    # Get the number of significant digits needed
    power = 0
    if (tick_vals[0] > 0) and \
       (len(tick_vals) > 1):
        while (tick_vals[-1] / tick_vals[0] - 1) < (0.2 / 10**power):
            power += 1
    sig_figs = 2 + power
    
    # Generate a label for each tick value
    for tick_val in tick_vals:
        # Determine which label style to use
        if tick_val == 0:
            case = 1
        elif is_log:
            tick_E = np.log10(tick_val)               
            if abs(tick_E - int(tick_E)) == 0:
                case = 3
            elif (abs(tick_val) > 100) or (abs(tick_val) < .01):            
                case = 2
                sign = 1. 
            else:
                case = 1            
        else:                
            if (abs(tick_val) > 100) or (abs(tick_val) < .01): 
                case = 2
                if tick_val < 0:
                    sign = -1.
                    tick_val = np.abs(tick_val)
                else:
                    sign = 1.                             
                tick_E = np.log10(tick_val)                  
            else:
                case = 1
        
        # Create the label
        if case == 1:
            # Standard linear label
            tick_labels.append(
                r'${ival:1.{prec}f}$'.
                format(
                    ival = tick_val, 
                    prec = max(2, sig_figs),
                )
            )  
        elif case == 2:
            # Linear label in subscript "e" notation
            tick_labels.append(
                r'${ival:1.{prec}f}_{{\rm e{power}}}$'.
                format(
                    ival = sign * tick_val / 10**int(tick_E), 
                    prec = sig_figs - 1, 
                    power = int(tick_E)
                )
            )  
        elif case == 3:
            # Power of 10 label
            tick_labels.append(
                r'$10^{{{0}}}$'.
                format(int(tick_E))
            )        
        else:
            raise LogicError("SHOULD NEVER REACH")
    #~for(tick_val)
    return tick_labels

    
def sample_freq_str(sample_freq_hz):
    """Returns the sample frequency as a properly formatted string"""
    if sample_freq_hz < 1.e3:
        string = '%.2f Hz' % sample_freq_hz
    elif sample_freq_hz < 1.e6:
        string = '%.2f kHz' % (sample_freq_hz / 1.e3)
    else:
        string = '%.2f MHz' % (sample_freq_hz / 1.e6)        
    return string         
    
    
def freq_resolution_str(
    sample_freq_hz,
    dft_size
): 
    """Returns the frequency bin width as a properly formatted string"""
    bin_width = sample_freq_hz / dft_size       
    if bin_width < 1.e3:
        string = '%.2f Hz' % bin_width
    elif bin_width < 1.e6:
        string = '%.2f kHz' % (bin_width / 1.e3)
    else:
        string = '%.2f MHz' % (bin_width / 1.e6)      
    return string     
    
    
def autowrap_text(event):
    """Auto-wraps all text objects in a MPL figure at draw-time"""
    import matplotlib as mpl
    
    fig = event.canvas.figure

    for object in fig.get_children():    
        if isinstance(object, mpl.text.Text):
            # If it's a text artist, wrap it
            wrap_textbox(object, event.renderer, fig)
        else:
            # If it has children, check whether they're text artists
            for child in object.get_children():
                if isinstance(child, mpl.text.Text):
                    # If it's a text artist, wrap it
                    wrap_textbox(child, event.renderer, fig)

    # Temporarily disconnect any callbacks to the draw event...
    # (To avoid recursion)
    func_handles = fig.canvas.callbacks.callbacks[event.name]
    fig.canvas.callbacks.callbacks[event.name] = {}
    # Re-draw the figure..
    fig.canvas.draw()
    # Reset the draw event callbacks
    fig.canvas.callbacks.callbacks[event.name] = func_handles

    
def wrap_textbox(
    textobj, 
    renderer, 
    fig
):
    """Wraps the given matplotlib text object so that it exceed the boundaries
    of the axis it is plotted in."""
    import textwrap
    
    # Get the starting position of the text in pixels...
    x0, y0 = textobj.get_transform().transform(textobj.get_position())
    
    # Get the extents of the current axis in pixels...
    owner = textobj.get_axes()
    if owner is None:
        owner = fig
    clip = owner.get_window_extent()
    
    # Set the text to rotate about the left edge (doesn't make sense otherwise)
    textobj.set_rotation_mode('anchor')

    # Get the amount of space in the direction of rotation to the left and 
    # right of x0, y0 (left and right are relative to the rotation, as well)
    rotation = textobj.get_rotation()
    right_space = min_dist_inside((x0, y0), rotation, clip)
    left_space = min_dist_inside((x0, y0), rotation - 180, clip)

    # Use either the left or right distance depending on the horiz alignment.
    alignment = textobj.get_horizontalalignment()
    if alignment is 'left':
        new_width = right_space 
    elif alignment is 'right':
        new_width = left_space
    else:
        new_width = 2 * min(left_space, right_space)

    # Estimate the width of the new size in characters...
    aspect_ratio = 0.5 # This varies with the font!! 
    fontsize = textobj.get_size()
    pixels_per_char = aspect_ratio * renderer.points_to_pixels(fontsize)

    # If wrap_width is < 1, just make it 1 character
    wrap_width = max(1, new_width // pixels_per_char)
    try:
        wrapped_text = textwrap.fill(textobj.get_text(), wrap_width)
    except TypeError:
        # This appears to be a single word
        wrapped_text = textobj.get_text()
    textobj.set_text(wrapped_text)

    
def min_dist_inside(point, rotation, box):
    """Gets the space in a given direction from "point" to the boundaries of
    "box" (where box is an object with x0, y0, x1, & y1 attributes, point is a
    tuple of x,y, and rotation is the angle in degrees)"""
    from math import sin, cos, radians
    
    x0, y0 = point
    rotation = radians(rotation)
    distances = []
    threshold = 0.0001 
    if cos(rotation) > threshold: 
        # Intersects the right axis
        distances.append((box.x1 - x0) / cos(rotation))
    if cos(rotation) < -threshold: 
        # Intersects the left axis
        distances.append((box.x0 - x0) / cos(rotation))
    if sin(rotation) > threshold: 
        # Intersects the top axis
        distances.append((box.y1 - y0) / sin(rotation))
    if sin(rotation) < -threshold: 
        # Intersects the bottom axis
        distances.append((box.y0 - y0) / sin(rotation))
    return min(distances)    