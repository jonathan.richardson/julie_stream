#!/usr/bin/env python

import numpy as np




###
## Returns the Poisson amplitude coherence level
## (Statistical limit if the two channel streams are random and uncorrelated)
def poisson_coherence(
    accumulation,
    smoothing_len
):
    # Poisson amplitude coherence = 1/N^(-1/4)
    if smoothing_len > 1:
        # Account for the frequency bin averaging
        accumulation *= smoothing_len
    min_coherence = accumulation**(-0.25)
    return min_coherence

    
###
## Returns the magnitude of the requested PSD/CSD in V/rtHz
def csd_mag(
    csd,
    ch1,
    ch2
):
    norm            = csd.normalization_to_ps_get() / csd.accumulation_get()
    enbw            = csd.ENBW_get()
    if ch1 == ch2:
        spectrum    = csd.psd_getValues(ch1)            
    else:
        spectrum    = csd.csd_getValues(ch1, ch2)        
        spectrum    = np.absolute(spectrum)              
    ps              = spectrum * norm                    #[V^2]
    psd             = ps / enbw                          #[V^2/Hz]
    asd             = np.sqrt(psd)                       #[V/rtHz]
    return asd   
    
    
###
## Returns the phase of the requested CSD in deg
def csd_phase(
    csd,
    ch1,
    ch2
):
    assert ch1 != ch2
    spectrum    = csd.csd_getValues(ch1, ch2)
    csd_phase   = np.angle(spectrum, deg = True)     #[deg]
    return csd_phase
    
        
###
## Returns the amplitude coherence of the requested CSD
def coherence(
    csd,
    ch1,
    ch2
):
    assert ch1 != ch2
    spectrum    = csd.csd_getValues(ch1, ch2)
    cs_mag      = np.absolute(spectrum)
    ps_ch1      = csd.psd_getValues(ch1)
    ps_ch2      = csd.psd_getValues(ch2)
    coherence   = np.sqrt(cs_mag / np.sqrt(ps_ch1 * ps_ch2))
    return coherence
   