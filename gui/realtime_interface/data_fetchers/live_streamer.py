#!/usr/bin/env python

"""
Contains the data fetcher classes
"""

from time import sleep
import json
from functools import partial as BIND
from PyQt4.Qt import *

from julie_csd import  RealtimeClient

from ..utilities import gui_utils as gUtil

from .data_model import DataModel


class LiveStreamer(QThread):
    """ Live-stream fetcher class """

    def __init__(
        self,
        addr,
        model = None,
        logging = False
    ):
        """ Constructor """
        super(LiveStreamer, self).__init__()
        self._logging = logging

        # Create the realtime client
        self._addr = addr
        self._client = RealtimeClient(addr)
        self._client.message_handler_set(BIND(self._event_handler, 'msg'))
        self._client.connection_handler_set(BIND(self._event_handler, 'connect'))   
        self._client.run_status_handler_set(BIND(self._event_handler, 'run_status'))   
         
        # Create the CSD data model
        self._model = model if model is not None else DataModel()
        return

    def callback_add(
        self,
        event,
        callback
    ):
        """ Registers a receiver callback for a fetch thread event.
            Allows callback to execute on receiver thread. 
        """
        events = ['connect', 'run_status', 'csd', 'err', 'msg']
        if event not in events:
            raise RuntimeError('Unsupported event %s' % event)
        self.connect(self, SIGNAL(event), callback)
        return

    def connection_init(self):
        """ Establishes a connection to the real-time server (non-blocking)
        """
        self._client.start()
        return
        
    def connection_status(self):
        """ Queries the connection state
        """
        run_state = self._client.is_connected()
        return run_state        
        
    def run_status(self):
        """ Queries the run state (checks whether a run is already in progress) 
        """
        if not self._client.is_connected():
            raise RuntimeError('Unable to connect to %s' % self._addr)
        run_state = self._client.run_state_query()
        return run_state

    def run_start(self, settings):
        """ Starts/joins a DAQ run 
        """
        # Start the data fetch loop
        self.start()
        if not self.run_status():
            # Start a new remote run
            self._client.run_settings_send(settings)
            self._client.run_state_send(True)
        return

    def stop(self, end_run = False):
        """Stop fetching"""
        self._should_quit = True
        if end_run:
            # End the DAQ run on the server 
            # (Pause to receive final messages before disconnecting)
            self._client.run_state_send(False)
            sleep(1)
        return

    def run(self):
        """ Work loop (This method should not be called directly) """
        self._should_quit = False
        try:
            while not self._should_quit:
                # Fetch the next data update from the server (blocking)
                csd = self._client.csd_next()
                if csd is None:
                    # Failed pull
                    if not self._should_quit:
                        msg = 'Lost connection to %s.\n\nThis most likely means the DAQ server has crashed.' % self._addr
                        raise RuntimeError(msg)
                    continue
                elif self._client.num_csd_ready() > 0:
                    # Outdated frame
                    continue

                # Update the data model only if the GUI is idle
                if gUtil.data_access.acquire(blocking = False):
                    # Apply the data update to the model
                    self.model.update(csd)
                    if self.model.initialized:
                        # Signal the receivers that the model has updated
                        self.emit(SIGNAL("csd"), csd)
                    gUtil.data_access.release()
            #~while(not quit)
        except RuntimeError as error:
            # Report the error and shut down
            self.emit(SIGNAL('err'), str(error))
        return

    @property
    def model(self):
        """ Provides read-only public access to the data model """
        return self._model

    def _event_handler(
        self, 
        name, 
        event
    ):
        """ Forwards event data to receiver(s) """
        self.emit(SIGNAL(name), event)
        if (name == 'msg') and self._logging:
            msg = event[:-1] if (event[-1] == "\n") else event
            print msg
        return        

#~LiveStreamer
