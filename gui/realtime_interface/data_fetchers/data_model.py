#!/usr/bin/env python

"""
Contains the data model classes
"""

import numpy as np
import math
from collections import deque

from julie_csd import CSDCarrierDouble


def freq_avg_deepcopy(source, target):
    """ Deep copies the frequency-averaged source data into the target container """
    assert  source.channels_num() == target.channels_num() \
        and source.sample_freq_get() == target.sample_freq_get() \
        and source.overlap_frac_get() == target.overlap_frac_get() \
        and math.log(source.length(), 2) % 1 == 0 \
        and math.log(target.length(), 2) % 1 == 0 \
        and source.length() > target.length()
    smoothing_len = source.length() / target.length()

    # Copy the mutable source metadata into the target
    target.sequence_number_set(source.sequence_number_get())
    target.accumulation_set(source.accumulation_get() * smoothing_len)
    target.normalization_to_ps_set(source.normalization_to_ps_get())
    target.ENBW_set(source.ENBW_get())
    target.timestamps_set(source.timestamp_begin(), source.timestamp_end())
    target.error_flag_set(source.error_flag_get())

    # Copy the frequency-averaged source data into the target
    for row_idx in range(source.channels_num()):
        for col_idx in range(row_idx + 1):
            if row_idx == col_idx:
                data = source.psd_getValues(row_idx)
            else:
                data = source.csd_getValues(row_idx, col_idx)
            rebinned_data = np.sum(
                data.reshape(-1, smoothing_len),
                axis = 1
            )
            if row_idx == col_idx:
                target.psd_setValues(rebinned_data, row_idx)
            else:
                target.csd_setValues(rebinned_data, row_idx, col_idx)
    return

def accum_time_s(csd):
    """ Returns the CSD frame accumulation time in s """
    t_overlap = (csd.overlap_frac_get() * 2. * csd.length()) / csd.sample_freq_get()
    num_frames = csd.sequence_number_get()
    t_integ = 1.E-9 * (csd.timestamp_end() - csd.timestamp_begin())
    return ((t_integ - (num_frames - 1) * t_overlap) / num_frames)


class DataModel(object):
    """ Parent CSD model class. Contains the container-specific methods. """

    def __init__(
        self,
        t_window_s      = 0,
        smoothing_len_E = 0
    ):
        """ Constructor """
        self._t_window_s        = t_window_s
        self._smoothing_len_E   = smoothing_len_E
        self._submodel          = None
        return

    def _check_initialized(func):
        """ Enforces model initialization """
        def wrapped(self, *args, **kwds):
            if not self.initialized:
                raise RuntimeError("Model uninitialized")
            return func(self, *args, **kwds)
        return wrapped

    def update(
        self,
        csd,
        coadd = False
    ):
        """ Updates the master data and forwards to the submodel """
        self._csd_validator(csd)
        if self._submodel is None:
            self._mstr_csd = CSDCarrierDouble(csd)
            self._submodel = self._model_autocreate()
        else:
            if coadd:
                t_integ_ns = (self._mstr_csd.timestamp_end() - self._mstr_csd.timestamp_begin()) \
                                + (csd.timestamp_end() - csd.timestamp_begin())
                self._mstr_csd.csd_frame_add(csd)
                self._mstr_csd.timestamps_set(0, t_integ_ns)
            else:
                self._mstr_csd.deepcopy(csd)
            self._submodel.update(self._mstr_csd)
        return

    def live_mode_set(self, t_window_s):
        """ Sets the live time window and creates the submodel """
        self._window_validator(t_window_s)
        self._t_window_s = t_window_s
        self._submodel = self._model_autocreate()
        return

    def freq_averaging_set(self, smoothing_len_E):
        """ Sets the frequency bin averaging and applies it to the submodel """
        self._smoothing_validator(smoothing_len_E)
        self._smoothing_len_E = smoothing_len_E
        self._submodel = self._model_autocreate()
        return

    @_check_initialized
    def psd(self, chn):
        """ Routes to the current submodel """
        self._channel_validator(chn)
        return self._submodel.psd(chn)

    @_check_initialized
    def csd_mag(self, ch1, ch2):
        """ Routes to the current submodel """
        self._channel_validator(ch1, ch2)
        return self._submodel.csd_mag(ch1, ch2)

    @_check_initialized
    def csd_phase(self, ch1, ch2):
        """ Routes to the current submodel """
        self._channel_validator(ch1, ch2)
        return self._submodel.csd_phase(ch1, ch2)

    @_check_initialized
    def coherence(self, ch1, ch2):
        """ Routes to the current submodel """
        self._channel_validator(ch1, ch2)
        return self._submodel.coherence(ch1, ch2)

    @property
    @_check_initialized
    def poisson_coh(self):
        """ Routes to the current submodel """
        return self._submodel.poisson_coh()

    @_check_initialized
    def poisson_amp(self, ch1, ch2):
        """ Routes to the current submodel """
        return self._submodel.poisson_amp(ch1, ch2)

    @property
    @_check_initialized
    def wall_time(self):
        """ Routes to the current submodel """
        return self._submodel.wall_time()

    @property
    @_check_initialized
    def integ_time(self):
        """ Routes to the current submodel """
        return self._submodel.integ_time()

    @property
    @_check_initialized
    def duty_cycle(self):
        """ Routes to the current submodel """
        return self.integ_time / self.wall_time

    @property
    @_check_initialized
    def accum(self):
        """ Total frame accumulation """
        return (self._mstr_csd.accumulation_get() * 2**self._smoothing_len_E)

    @property
    @_check_initialized
    def num_chs(self):
        """ Number of channels """
        return self._num_chs

    @property
    @_check_initialized
    def length(self):
        """ CSD length """
        return (self._length / 2**self._smoothing_len_E)

    @property
    @_check_initialized
    def sample_freq(self):
        """ Sample frequency in Hz """
        return self._sample_freq

    @property
    @_check_initialized
    def accum_time(self):
        """ Frame accumulation time in s """
        return accum_time_s(self._mstr_csd)

    @property
    @_check_initialized
    def enbw(self):
        """ ENBW in Hz """
        return self._enbw

    @property
    @_check_initialized
    def normalization(self):
        """ Normalization factor to V^2 """
        return self._normalization

    @property
    @_check_initialized
    def overlap_frac(self):
        """ Frame Welch overlap fraction """
        return self._overlap_frac

    @property
    def initialized(self):
        """ Routes to the current submodel, if one exists """
        if self._submodel is not None:
            state = self._submodel.initialized()
        else:
            state = False
        return state

    def _model_autocreate(self):
        """ Returns the appropriate submodel for the current state """
        if self._t_window_s != 0:
            model = DataModelLive(
                self._mstr_csd,
                self._t_window_s,
                self._smoothing_len_E
            )
        else:
            model = DataModelAvg(
                self._mstr_csd,
                self._smoothing_len_E
            )
        return model

    def _attr(self, attr, default_val):
        """ Returns the value of a private attribute, or creates the attribute with the
            default value if it does not exist """
        attr = "_" + attr
        try:
            value = getattr(self, attr)
        except AttributeError:
            setattr(self, attr, default_val)
            value = default_val
        return value

    def _channel_validator(self, *chns):
        """ Channel input validator """
        for chn in chns:
            assert  chn < self.num_chs \
                and chns.count(chn) == 1
        return

    def _smoothing_validator(self, smoothing_len_E):
        """ Smoothing length validator """
        csd_len_E = math.log(self._length, 2)
        assert  csd_len_E % 1 == 0 \
            and csd_len_E >= smoothing_len_E
        return

    def _csd_validator(self, csd):
        """ CSD consistent metadata validator """
        new_values = {
            "num_chs"          : csd.channels_num(),
            "length"           : csd.length(),
            "sample_freq"      : csd.sample_freq_get(),
            "enbw"             : csd.ENBW_get(),
            "normalization"    : csd.normalization_to_ps_get(),
            "overlap_frac"     : csd.overlap_frac_get()
        }
        for attr, new_val in new_values.items():
            if new_val != self._attr(attr, new_val):
                raise RuntimeError('Run configuration has changed!')
        return

    @staticmethod
    def _window_validator(t_window_s):
        """ Sliding time window validator """
        if t_window_s != -1:
            assert  t_window_s >= 0 \
                and t_window_s <= 600
        return

#~class DataModel


class DataModelAvg(object):
    """ Submodel class for global averages """

    def __init__(
        self,
        csd,
        smoothing_len_E
    ):
        """ Constructor """
        self._freq_avg = True if smoothing_len_E > 0 else False
        self._csd = CSDCarrierDouble(
            csd.channels_num(),
            csd.length() / 2**smoothing_len_E,
            csd.sample_freq_get(),
            csd.overlap_frac_get()
        )
        self.update(csd)
        return

    def update(self, csd):
        """ Copies the frequency-averaged data into the local container """
        self._write_to_buf(csd)
        return

    def psd(self, chn):
        """ Returns the requested power spectrum in V/rtHz """
        raw_data = self._csd.psd_getValues(chn)
        return self._normalize(raw_data)

    def csd_mag(self, ch1, ch2):
        """ Returns the magnitude of the requested cross-spectrum in V/rtHz """
        raw_data = np.absolute(self._csd.csd_getValues(ch1, ch2))
        return self._normalize(raw_data)

    def csd_phase(self, ch1, ch2):
        """ Returns the phase of the requested CSD """
        raw_data = self._csd.csd_getValues(ch1, ch2)
        return np.angle(raw_data, deg = True)

    def coherence(self, ch1, ch2):
        """ Returns the amplitude coherence of the requested CSD """
        csd_mag = np.absolute(self._csd.csd_getValues(ch1, ch2))
        psd1 = self._csd.psd_getValues(ch1)
        psd2 = self._csd.psd_getValues(ch2)
        coh = csd_mag / np.sqrt(psd1 * psd2)
        return np.sqrt(coh)

    def poisson_coh(self):
        """ Returns the Poisson amplitude coherence = N^(-1/4) """
        return (self._csd.accumulation_get()**(-.25))

    def poisson_amp(self, ch1, ch2):
        """ Returns the Poisson amplitude level in V/rtHz of the requested CSD """
        return (np.sqrt(self.psd(ch1) * self.psd(ch2)) * self.poisson_coh())

    def wall_time(self):
        """ Returns the total wall time in s """
        return ((self._csd.timestamp_end() - self._csd.timestamp_begin()) / 1e9)

    def integ_time(self):
        """ Returns the total integration time in s """
        frac = 1 - self._csd.overlap_frac_get()
        accum_conv = frac * (2*self._csd.length() / self._csd.sample_freq_get())
        return accum_conv * self._csd.accumulation_get()

    def initialized(self):
        """ Returns the initialization state """
        return (True if self._csd.sequence_number_get() > 0 else False)     
        
    def _normalize(self, raw_data):
        """ Normalizes a raw spectrum to V/rtHz """
        ps = raw_data * self._csd.normalization_to_ps_get() / self._csd.accumulation_get()  #[V^2]
        psd = ps / self._csd.ENBW_get()                                                     #[V^2/Hz]
        return np.sqrt(psd)                                                                 #[V/rtHz]
        
    @property
    def _write_to_buf(self):
        """ Returns the current CSD data-copy method """
        if self._freq_avg:
            method = lambda csd: freq_avg_deepcopy(csd, self._csd)
        else:
            method = self._csd.deepcopy
        return method

#~class DataModelAvg


class DataModelLive(object):
    """ Submodel class for time windowing """

    def __init__(
        self,
        csd,
        t_window_s,
        smoothing_len_E
    ):
        """ Constructor """
        self._freq_avg  = True if smoothing_len_E > 0 else False
        self._idx       = 0
        self._t_accum   = accum_time_s(csd)
        self._size      = max(int(math.ceil(t_window_s / self._t_accum)), 2)
        self._csd_deque = deque(maxlen = self._size)
        self._buffer    = []
        for idx in range(self._size):
            carrier = CSDCarrierDouble(
                csd.channels_num(),
                csd.length() / 2**smoothing_len_E,
                csd.sample_freq_get(),
                csd.overlap_frac_get()
            )
            self._buffer.append(carrier)
        self.update(csd)
        return

    def update(self, csd):
        """ Copies the supplied data into the CSD deque """
        self._write_to_buf(csd)
        self._csd_deque.append(self._buffer[self._idx])
        self._idx += 1
        if self._idx == self._size:
            self._idx = 0
        return

    def psd(self, chn):
        """ Returns the requested power spectrum in V/rtHz """
        raw_data = self._end.psd_getValues(chn) - self._beg.psd_getValues(chn)
        return self._normalize(raw_data)

    def csd_mag(self, ch1, ch2):
        """ Returns the magnitude of the requested cross-spectrum in V/rtHz """
        raw_data = np.absolute(self._end.csd_getValues(ch1, ch2) - self._beg.csd_getValues(ch1, ch2))
        return self._normalize(raw_data)

    def csd_phase(self, ch1, ch2):
        """ Returns the phase of the requested CSD in deg """
        raw_data = self._end.csd_getValues(ch1, ch2) - self._beg.csd_getValues(ch1, ch2)
        return np.angle(raw_data, deg = True)

    def coherence(self, ch1, ch2):
        """ Returns the amplitude coherence of the requested CSD """
        csd_mag = np.absolute(self._end.csd_getValues(ch1, ch2) - self._beg.csd_getValues(ch1, ch2))
        psd1 = self._end.psd_getValues(ch1) - self._beg.psd_getValues(ch1)
        psd2 = self._end.psd_getValues(ch2) - self._beg.psd_getValues(ch2)
        coh = csd_mag / np.sqrt(psd1 * psd2)
        return np.sqrt(coh)

    def poisson_coh(self):
        """ Returns the Poisson amplitude coherence N^(-1/4) """
        return ((self._end.accumulation_get() - self._beg.accumulation_get())**(-.25))

    def poisson_amp(self, ch1, ch2):
        """ Returns the Poisson amplitude level in V/rtHz of the requested CSD """
        return (np.sqrt(self.psd(ch1) * self.psd(ch2)) * self.poisson_coh())

    def wall_time(self):
        """ Returns the total wall time in s """
        return ((self._end.timestamp_end() - self._beg.timestamp_end()) / 1e9)

    def integ_time(self):
        """ Returns the total integration time in s """
        frac = 1 - self._end.overlap_frac_get()
        accum_conv = frac * (2*self._end.length() / self._end.sample_freq_get())
        return accum_conv * (self._end.accumulation_get() - self._beg.accumulation_get())

    def initialized(self):
        """ Returns the initialization state of the submodel """
        return (True if len(self._csd_deque) == self._size else False)

    @property
    def _beg(self):
        return self._csd_deque[0]

    @property
    def _end(self):
        return self._csd_deque[-1]

    def _normalize(self, raw_data):
        """ Normalizes a raw spectrum to V/rtHz """
        ps = raw_data * self._beg.normalization_to_ps_get() / \
                    (self._end.accumulation_get() - self._beg.accumulation_get())   #[V^2]
        psd = ps / self._beg.ENBW_get()                                             #[V^2/Hz]
        return np.sqrt(psd)                                                         #[V/rtHz]

    @property
    def _write_to_buf(self):
        """ Returns the current CSD data-copy method """
        if self._freq_avg:
            method = lambda csd: freq_avg_deepcopy(csd, self._buffer[self._idx])
        else:
            method = self._buffer[self._idx].deepcopy
        return method

#~class DataModelLive
