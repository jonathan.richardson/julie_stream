#!/usr/bin/env python

"""
Contains the data fetcher classes
"""

from time import sleep
import json
from PyQt4.Qt import *

from julie_csd import DatabaseClient, CSDCarrierDouble

from ..utilities import gui_utils as gUtil

from .data_model import DataModel


class DatabaseAccess(object):
    """ Fetcher class for database access """

    def __init__(
        self,
        addr,
        model = None
    ):
        """Constructor"""
        super(DatabaseAccess, self).__init__()
        self._addr = addr
        self._client = DatabaseClient(addr)
        self._model = model if model is not None else DataModel()
        return

    def search(self, run):
        """ Connect to server and perform a database search (blocking) """
        self._client.start()
        if not self._client.wait_for_connection():
            raise RuntimeError('Unable to connect to %s' % self._addr)
        hits = self._client.search_send(json.dumps(run))

        # Parse the raw JSON strings, ignoring any extraneous characters at the end
        results = [json.JSONDecoder().raw_decode(hit)[0] for hit in hits]
        return results

    def transfer_to_disk(
        self,
        run,
        local_path,
        write_all
    ):
        """ Connect to server and transfer data for the requested run to local disk (blocking) """
        self._client.start()
        if not self._client.wait_for_connection():
            raise RuntimeError('Unable to connect to %s' % self._addr)
        self._client.transfer_to_disk_send(
            json.dumps(run),
            local_path,
            write_all
        )
        return

    @gUtil.synchronized
    def transfer_to_model(self, runs):
        """ Connect to server and transfer pre-accumulated data to data model (blocking) """

        # Connect to the database server
        self._client.start()
        if not self._client.wait_for_connection():
            raise RuntimeError('Unable to connect to %s' % self._addr)

        # Transfer data to model (accumulates multiple requested runs)
        # *This ONLY checks for consistency of DFT-related metadata (not, e.g., channel sources)*
        for run in runs:
            csd = self._client.transfer_averages_send(json.dumps(run))
            if csd is None:
                raise RuntimeError("No data available for run %s" % run['run_id'])
            self.model.update(csd, coadd = True)
        return

    @property
    def model(self):
        """ Provides read-only public access to the data model """
        return self._model

#~DatabaseAccess
