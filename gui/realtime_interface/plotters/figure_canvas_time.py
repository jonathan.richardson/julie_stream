#!/usr/bin/env python

import numpy as np
import math
import textwrap
import sys
import warnings

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
with warnings.catch_warnings(RuntimeWarning):
    # Suppress an unncessary warning generated on Linux machines
    warnings.simplefilter("ignore")
    from matplotlib.pyplot import Subplot

# Matploptlib config parameters
from matplotlib import rcParams
if sys.platform != 'win32':
    rcParams['text.usetex'] = True
rcParams['font.family']     = 'sans-serif'
rcParams['grid.linestyle']  = '-'
rcParams['grid.alpha']      = .3
rcParams['figure.dpi'] = 80

from ..utilities import plot_utils as pUtil
from ..utilities import csd_utils as cUtil


class FigureCanvasTime(FigureCanvasQTAgg):
    """ Figure canvas class for plotting time-domain cross-correlation """
    
    _colors = [
        '#D768ED',
        '#AD9728',
        '#80AD28', 
        '#AD283E',
        '#5528AD',
        '#28AD97'        
    ]

    def __init__(self, model):
        """ Constructor """
    
        # Initialize the canvas
        self._fig = Figure(dpi = rcParams['figure.dpi'])
        super(FigureCanvasTime, self).__init__(self._fig)

        # Set the underlying data model
        self._model = model
        return    

    def _active_chs(self):
        """ Returns a list of active data channels """
        actv_chs = [chn_idx for chn_idx, on_off in self._chs.iteritems() if on_off]
        return actv_chs
                
    def setup(self):
        """ Sets up the canvas """
        
        # Clear the figure
        self._fig.clear()
    
        # Autoset the figure size
        # (Note: Must be called after Qt window has been drawn)
        dpi         = rcParams['figure.dpi']
        size_pix    = [self.width(), self.height()]
        size_in     = [dim / dpi for dim in size_pix]
        self._fig.set_size_inches(*size_in)
        
        # Create the plot object        
        self._ax = self._fig.add_subplot(111)
        self._fig.subplots_adjust(
            left    = .09,
            right   = .87,
            bottom  = .10,
            top     = .95
        )
        
        # Create the animated channel lines
        # self._lines = {}
        # placeholder = np.ones(self._xvals.size)
        # for chn_idx, on_off in self._chs.iteritems():
            # if on_off:
                # label = '$\\rm Ch \; %d$' % chn_idx
            # else:
                # label = None
            # self._lines[chn_idx], = self._ax.plot(
                # self._xvals,
                # placeholder,            
                # color       = self._colors[chn_idx], 
                # animated    = True,
                # visible     = on_off,
                # label       = label,
                # linewidth   = 1.5
            # )
        #~for(channels)
        
        # # Null line (zero fluctuation)
        # self._ax.axhline(
            # 0., 
            # ls = '--', 
            # color = '0.5',
            # linewidth = 1.5
        # ) 

        # Set the immutable plot formatting
        fontsize = 13
        self._ax.set_xlabel(
            "$\\rm Time \; Delay\; (s)$", 
            fontsize = fontsize
        )        
        self._ax.set_ylabel(
            '$\\rm Correlation$', 
            fontsize = fontsize
        )
        self._ax.tick_params(
            axis = 'both', 
            reset = True, 
            which = 'both', 
            length = 3, 
            width = 1.25, 
            labelsize = fontsize
        )        
        self._ax.grid(True)                
        
        # Apply the plot formatting and draw the figure
        self._draw_figure()
        return

    def save_figure(
        self, 
        fname,
        ydata
    ):
        """ Saves the figure """
    
        # Overplot the data as non-animated lines
        # (MPL WORKAROUND: Animated lines cannot be saved in the Qt backend on Linux)
        self._draw_lines_nonanimated(ydata)

        # Save the figure
        success = True            
        try:
            self._fig.savefig(fname)
        except:
            # Can occur if either the filename or comments are invalid
            success = False

        # Redraw the figure with animated lines
        self.setup(ydata)
        return success
      
    def _draw_lines_nonanimated(self, ydata):
        """ Overplots the data as non-animated lines.
            (Needed b/c animated lines cannot be saved in the Qt MPL backend) """
        for chn_idx in self._active_chs():
            self._ax.plot(
                self._xvals[:len(ydata[chn_idx])],
                ydata[chn_idx],            
                color = self._colors[chn_idx], 
                linewidth = 1.5
            )
        return

    def rescale(self, ydata):
        """ Rescales the canvas """
        self._draw_figure(ydata)
        return
        
    def _yrange_get(self, ydata):
        """ Returns the Y-range for the figure """
        abs_max_global = 0
    
        # Take the furthest y-range spanned by all active channels      
        for chn_idx in self._active_chs():    
            # Get the maximum absolute Y-value for this channel
            abs_max = np.max(np.abs(ydata[chn_idx]))
            if abs_max != 0:
                rounded_abs_max = 10**(math.ceil(np.log10(abs_max / 5.)))
            else:
                rounded_abs_max = .01
            if rounded_abs_max > abs_max_global:
                abs_max_global = rounded_abs_max
        yrange = [-5. * abs_max_global, 5. * abs_max_global]
        return yrange

    def _draw_figure(self):
        """ Updates the plot formatting and redraws the entire figure.
            (This method is EXPENSIVE and should only be called indirectly 
            through the axis change handlers) """
    
        # # Apply the X-axis attributes         
        # xrange = [self._xvals[0], self._xvals[-1]]
        # xtick_vals = pUtil.tick_maker(xrange, False)
        # xtick_labs = pUtil.tick_labeler(xtick_vals, is_log = False)        
        # self._ax.set_xticks(xtick_vals)
        # self._ax.set_xticklabels(xtick_labs)
        # self._ax.set_xlim(reversed(xrange))
        
        # # Apply the Y-axis attributes
        # yrange = self._yrange_get(ydata)
        # ytick_vals = pUtil.tick_maker(yrange, False)
        # ytick_labs = pUtil.tick_labeler(ytick_vals, is_log = False)
        # self._ax.set_yticks(ytick_vals)
        # self._ax.set_yticklabels(ytick_labs)
        # self._ax.set_ylim(yrange)

        # # Set the figure legend
        # legend = self._ax.legend(
            # bbox_to_anchor  = (1.01, 1.), 
            # loc             = 2, 
            # borderaxespad   = 0.,
            # fancybox        = True
        # )
        # legend.get_frame().set_facecolor('#FEF3DD')
                         
        # Draw the figure
        self._fig.canvas.draw()
        
        # Capture the new background
        self._backgnd = self._fig.canvas.copy_from_bbox(self._fig.bbox)

        # Draw the animated lines
        #self.draw_lines(ydata)
        return

    def draw_lines(self, ydata):
        """ Draws the animated data curves """
    
        # Restore the figure background
        self._fig.canvas.restore_region(self._backgnd)

        # Update the animated curves
        for chn_idx, on_off in self._chs.iteritems():
            if not on_off:
                continue
            self._lines[chn_idx].set_data(
                self._xvals[:len(ydata[chn_idx])],
                ydata[chn_idx]
            )
            self._ax.draw_artist(self._lines[chn_idx])
            
        # Call the updated line artists
        self._fig.canvas.blit(self._fig.bbox)
        return
        
    def channel_on_off(
        self,
        chn_idx,
        ydata
    ):
        """ Turns on/off plotting for the specified channel """
        
        # Update the plot settings
        self._chs[chn_idx] = not self._chs[chn_idx]
        label = ('$\\rm Ch \; %d$' % chn_idx) if self._chs[chn_idx] else None
        self._lines[chn_idx].set_label(label)
        self._lines[chn_idx].set_visible(self._chs[chn_idx])
        
        # Redraw the figure
        self._draw_figure(ydata)
        return

#~FigureCanvasTime class