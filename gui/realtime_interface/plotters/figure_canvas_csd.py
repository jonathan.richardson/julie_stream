#!/usr/bin/env python

import numpy as np
import math
import textwrap
import sys
import warnings

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
with warnings.catch_warnings(RuntimeWarning):
    # Suppress an unncessary warning generated on some Linux machines
    warnings.simplefilter("ignore")
    from matplotlib.pyplot import Subplot

from ..utilities import plot_utils as pUtil

# Matploptlib config parameters
from matplotlib import rcParams
if sys.platform != 'win32':
    rcParams['text.usetex'] = True
rcParams['font.family']     = 'sans-serif'
rcParams['grid.linestyle']  = '-'
rcParams['grid.alpha']      = .3
rcParams['figure.dpi']      = 80


class FigureCanvasCSD(FigureCanvasQTAgg):
    """ Figure canvas class for CSD data """

    # Supported canvas display settings
    _supported_opts = [
        'xlog', 
        'ylog', 
        'coherence', 
        'grid', 
        'common_y', 
        'holo_trace', 
        'freq_range_hz', 
        'source_labels',
        'channels'
    ]

    # Line colors
    _color_psd     = '#262F63'
    _color_mag     = '#DE4D14'
    _color_phase   = '#6F7272'    

    _labels    = {}


    def __init__(
        self,
        settings,
        model
    ): 
        """ Constructor """
        
        # Initialize the canvas
        self._fig = Figure(dpi = rcParams['figure.dpi'])
        super(FigureCanvasCSD, self).__init__(self._fig)

        # Make all figure text auto-wrap
        self._fig.canvas.mpl_connect('draw_event', pUtil.autowrap_text)
        
        # Set the underlying data model
        self._model = model        

        # Set the display settings
        self._settings = self._settings_validate(
            settings,
            self._supported_opts,
            model
        )
        return  
        
    def all_settings_get(self):
        """ Returns all the display settings """
        return self._settings
        
    def setting_get(
        self,
        setting,
    ):
        """ Returns the requested display setting """
        return self._settings[setting]
        
    def active_channels(self):
        """ Returns a list of enabled channels """
        chn_list = [chn_idx for chn_idx, on_off in self._settings['channels'].items() if on_off]
        return sorted(chn_list)

    def setup(self):  
        """ Renders the entire canvas, creating all plot objects.
            (This method MUST be called after the Qt window has been drawn) """
        
        # Set the appropriate figure size
        dpi = rcParams['figure.dpi']
        size_pix = [self.width(), self.height()]
        size_in = [dim / dpi for dim in size_pix]
        self._fig.set_size_inches(*size_in)
        
        # Create the frequency bins
        self.freq_bins_create()
        
        # Create the plots and animated lines, then draw the figure
        self.plots_create()        
        return   
            
    def display_mode_toggle(self, setting):
        """ Toggles an on/off display setting and redraws the figure """
        if setting is not None:
            self._settings[setting] = not self._settings[setting]
        self._draw_figure()
        return            
        
    def _model_map(self, key):  
        """ Retrieves the y-data for the specified plot from the model """
        chs = [int(chn) for chn in key[:2]]
        if chs[0] == chs[1]:
            data = self._model.psd(chs[0])
        else:
            if 'p' in key:
                data = self._model.csd_phase(*chs)
            elif self._settings['coherence']:
                data = self._model.coherence(*chs)         
            else:
                data = self._model.csd_mag(*chs)
        return data
        
    def draw_lines(self):
        """ Draw the subplot data curves """  
  
        # Restore the figure background
        self._fig.canvas.restore_region(self._background)

        # Update the animated PSD/CSD curves
        for key in self._axes.keys():
            data = self._model_map(key)[self._offset:]
            self._lines[key].set_ydata(data)
            self._axes[key].draw_artist(self._lines[key])

        if self._poisson.keys():
            # Update the animated Poisson coherence/amplitude limits        
            if self._settings['coherence']:
                data = np.ones(self._num_displayed_bins) * self._model.poisson_coh
            for key in self._poisson.keys():
                if not self._settings['coherence']:
                    chs = [int(chn) for chn in key[:2]]
                    data = self._model.poisson_amp(*chs)[self._offset:]
                self._poisson[key].set_ydata(data)
                self._axes[key].draw_artist(self._poisson[key])
       
        # Call the updated line artists
        self._fig.canvas.blit(self._fig.bbox)
        return
        
    def freq_range_set(
        self,
        freq_min,
        freq_max
    ):
        """ Sets the frequency range """
        
        # Map the supplied limits to the closest supported values
        self._settings['freq_range_hz'] = self.freq_range_map(freq_min, freq_max)

        # Redraw the figure
        self._draw_figure()
        return   
        
    def freq_range_map(
        self,
        freq_min,
        freq_max
    ):
        """Maps the supplied limits to the closest supported values"""
        xmin = freq_min
        xmax = freq_max
        assert xmin >= 0 and \
               xmax > xmin
           
        if xmin <= self._freqs[self._offset]:
            # Set the minimum displayable bin
            xmin = self._freqs[self._offset]
        else:
            # Set the closest bin <= xmin
            idx  = np.argmin(self._freqs < xmin) - 1
            xmin = self._freqs[idx]
        if xmax >= self._freqs[-1]:
            # Set the maximum displayable bin
            xmax = self._freqs[-1]
        else:
            # Set the closest bin >= xmax
            idx = np.argmax(self._freqs > xmax)
            xmax = self._freqs[idx]
        return [xmin, xmax]
 
    def channels_set(self, active_chs):
        """ Enables plotting for the specified channels """
    
        # Update the channel on/off settings
        for chn_idx in self._settings['channels'].keys():
            on_off = True if chn_idx in active_chs else False
            self._settings['channels'][chn_idx] = on_off
                                 
        # Create the new subplots and animated lines
        self.plots_create()   
        return        
        
    def _draw_figure(self):
        """ Updates the plot formatting and redraws the entire figure
            (This method is EXPENSIVE and should only be called indirectly 
             through the axis change handlers) """    

        # Set the plot attributes
        fontsize = 12 if self._settings['bigscreen'] else 8
        for key in self._axes:
            attr = self._attributes_get(key)             
            ax = self._axes[key]

            # Apply the X-axis attributes
            ax.set_xlabel(attr['xlabel'], fontsize = fontsize)
            ax.set_xscale(attr['xlog_str'])
            ax.set_xticks(attr['xtick_vals'])
            ax.set_xticklabels(attr['xtick_labs'])
            ax.set_xlim(attr['xrange'])

            # Apply the Y-axis attributes
            ax.set_ylabel(attr['ylabel'], fontsize = attr['text_size'])
            ax.set_yscale(attr['ylog_str'])
            ax.set_yticks(attr['ytick_vals'])
            ax.set_yticklabels(attr['ytick_labs'])
            ax.set_ylim(attr['yrange'])
            
            # Apply the general attributes
            ax.grid(self._settings['grid'])
            ax.tick_params(
                axis = 'both', 
                reset = True, 
                which = 'both', 
                length = 3, 
                width = 1.25, 
                labelsize = fontsize
            )
            '''
            if key[0] == 'c':
                # Set the holographic noise trace visibility
                self.traces[key].set_visible(self._settings['holo_trace'])  
                if self._settings['holo_trace']:
                    # Set the holographic noise trace values
                    if self._settings['coherence']:
                        signal = self.holo_coh
                    else:
                        signal = self.holo_asd
                    self.traces[key].set_ydata(signal)
            '''
        #~for(key)
            
        # Show/hide the channel source labels
        for label in self._labels.values():
            label.set_visible(self._settings['source_labels'])
        
        # Auto-lay out the canvas
        self._fig.tight_layout()            

        # Draw the figure & capture the new background
        self._fig.canvas.draw()
        #self._fig.canvas.blit(self._fig.bbox) #(FIX: This calls any mpl artists that still need to be updated)        
        self._background = self._fig.canvas.copy_from_bbox(self._fig.bbox)
        
        # Draw the animated lines
        self.draw_lines()
        return                
        
    def freq_bins_create(self): 
        """ Creates the frequency bins (x-values) """

        # Create the bins
        self._freqs = np.linspace(
            0.,
            self._model.sample_freq / 2.,
            self._model.length
        )

        # Set the frequency bin offset (hides the DC bins)
        self._offset = 3
        self._num_displayed_bins = self._freqs[self._offset:].size               
        if self._settings['freq_range_hz'][0] < self._freqs[self._offset]:
            # Set the minimum displayable bin
            self._settings['freq_range_hz'][0] = self._freqs[self._offset]   
            if self._settings['freq_range_hz'][1] < self._freqs[self._offset]:
                self._settings['freq_range_hz'][1] = self._freqs[self._offset + 1]     
        return
       
    def lines_resize(self):
        """ Updates the length of the animated lines """

        # Create the new frequency bins
        self.freq_bins_create()

        # Set the new size of the animated lines
        placeholder = np.ones(self._num_displayed_bins)
        lines = self._lines.values() + self._poisson.values()
        for line in lines:
            line.set_data(
                self._freqs[self._offset:],     # new frequency bins
                placeholder                     # y-values placeholder
            )              
            
        # Redraw the figure
        self._draw_figure()            
        return
        
    def plots_create(self):
        """ Creates the plot objects and animated lines """
    
        # Clear the figure
        self._axes      = {}
        self._lines     = {}
        self._poisson   = {}
        self._traces    = {}
        self._labels    = {}
        self._fig.clear()        

        # Create the plot objects and animated lines    
        active_chs = self.active_channels()
        num_chs = len(active_chs)
        grid = GridSpec(num_chs, num_chs)
        row_pos = 0
        for row_idx in active_chs:
            col_pos = 0
            for col_idx in [idx for idx in active_chs if idx <= row_idx]:
                key = str(row_idx) + str(col_idx)            
                plot_func = self._psd_plot_create if row_idx == col_idx else self._csd_plot_create                    
                plot_func(key, grid[row_pos, col_pos])
                col_pos += 1
            row_pos += 1          
            
        # Draw the figure
        self._draw_figure()
        return
        
    def _psd_plot_create(
        self,
        key,
        grid_loc
    ):
        """ Creates a PSD plot (single-panel) """
        
        # Create the plot
        placeholder = np.ones(self._num_displayed_bins)      
        self._axes[key] = self._fig.add_subplot(grid_loc)
        self._lines[key], = self._axes[key].plot(
            self._freqs[self._offset:],   # frequency bins
            placeholder,                    # y-values placeholder           
            color = self._color_psd, 
            animated = True
        )
        
        # Create the channel source label
        chn_idx = int(key[0])
        fontsize = 12 if self._settings['bigscreen'] else 8
        self._labels[chn_idx] = self._axes[key].text(
            0.05, 
            0.95, 
            self._settings["chn_sources"][chn_idx], 
            transform = self._axes[key].transAxes, 
            fontsize = fontsize,
            va ='top', 
            bbox = dict(boxstyle = 'round', facecolor = 'wheat', alpha = 0.5)
        )
        return
  
    def _csd_plot_create(
        self,
        key,
        grid_loc
    ):
        """ Creates a two-panel CSD plot """
        placeholder = np.ones(self._num_displayed_bins)  

        # Create a subgrid for CSD magnitude & phase
        subgrid = GridSpecFromSubplotSpec(
            2, 1, 
            subplot_spec = grid_loc, 
            height_ratios = [2, 1],
            hspace = .25
        )
            
        # CSD magnitude panel
        self._axes[key] = Subplot(self._fig, subgrid[0, 0])
        self._fig.add_subplot(self._axes[key])
        self._lines[key], = self._axes[key].plot(
            self._freqs[self._offset:],     # frequency bins
            placeholder,                    # y-values placeholder
            color = self._color_mag, 
            animated = True
        )

        # CSD phase panel
        key_p = key + 'p'
        self._axes[key_p] = Subplot(self._fig, subgrid[1, 0])
        self._fig.add_subplot(self._axes[key_p])
        self._lines[key_p], = self._axes[key_p].plot(
            self._freqs[self._offset:],     # frequency bins
            placeholder,                    # y-values placeholder
            color = self._color_phase, 
            animated = True
        )
       
        # Line for the Poisson statistical correlation limit 
        self._poisson[key], = self._axes[key].plot(
            self._freqs[self._offset:],
            placeholder,
            color = 'k',
            linestyle = '--',
            animated = True
        )

        '''                      
        # Holographic noise trace
        self.traces[key], = ax.plot(
            self._freqs[self._offset:],               # x-values
            np.ones(self._num_displayed_bins), # y-values placeholder
            color = '.6'
        )
        '''            
        return
        
    def _attributes_get(self, plot_key):
        """ Returns the applicable subplot attributes """
        attr = {}
        ch1 = plot_key[0]
        ch2 = plot_key[1]
        last_chn_row = str(self.active_channels()[-1])
        if ch1 == ch2:
            # PSD x-attributes
            attr['xrange']         = self._xrange_get()
            attr['xlog']           = self._settings['xlog']     
            if attr['xlog']:
                attr['xlog_str']   = 'log'
            else:
                attr['xlog_str']   = 'linear'            
            attr['xtick_vals']     = pUtil.tick_maker(attr['xrange'], attr['xlog'])
            if ch1 == last_chn_row:
                attr['xlabel']     = r'$\rm Frequency \; [Hz]$'
                attr['xtick_labs'] = pUtil.tick_labeler(attr['xtick_vals'], is_log = attr['xlog'])
            else:
                attr['xlabel']     = ''
                attr['xtick_labs'] = []

            # PSD y-attributes                
            attr['ylabel']         = r'$\rm A_{' + ch1 + ch2 + r'} \; \left[ V/\sqrt{Hz} \right]$'
            if self._settings['bigscreen']:
                attr['text_size']  = 12
            else:
                attr['text_size']  = 8
            attr['ylog']           = self._settings['ylog']
            if attr['ylog']:
                attr['ylog_str']   = 'log'
            else:
                attr['ylog_str']   = 'linear'
            attr['yrange']         = self._yrange_get(plot_key)  
            attr['ytick_vals']     = pUtil.tick_maker(attr['yrange'], attr['ylog'])
            attr['ytick_labs']     = pUtil.tick_labeler(attr['ytick_vals'], is_log = attr['ylog'])    
        elif len(plot_key) == 2:
            # CSD magnitude/coherence x-attributes    
            attr['xrange']         = self._xrange_get()            
            attr['xlog']           = self._settings['xlog']
            if attr['xlog']:
                attr['xlog_str']   = 'log'
            else:
                attr['xlog_str']   = 'linear'
            attr['xtick_vals']     = pUtil.tick_maker(attr['xrange'], attr['xlog'])
            attr['xlabel']         = ''
            attr['xtick_labs']     = [] 
            
            # CSD magnitude/coherence y-attributes   
            if self._settings['coherence']:
                attr['ylabel']     = r'$\rm Coh(' + ch1 + r',' + ch2 + r')$'            
            else:
                attr['ylabel']     = r'$\rm A_{' + ch1 + ch2 + r'} \; \left[ V/\sqrt{Hz} \right]$'
            if self._settings['bigscreen']:
                attr['text_size']  = 12
            else:
                attr['text_size']  = 8
            attr['ylog']           = self._settings['ylog']    
            if attr['ylog']:
                attr['ylog_str']   = 'log'
            else:
                attr['ylog_str']   = 'linear'
            attr['yrange']         = self._yrange_get(plot_key)
            attr['ytick_vals']     = pUtil.tick_maker(attr['yrange'], attr['ylog'])
            attr['ytick_labs']     = pUtil.tick_labeler(attr['ytick_vals'], is_log = attr['ylog'])                 
        elif plot_key[2] == 'p':   
            # CSD phase x-attributes
            attr['xrange']         = self._xrange_get()
            attr['xlog']           = self._settings['xlog']
            if attr['xlog']:
                attr['xlog_str']   = 'log'
            else:
                attr['xlog_str']   = 'linear'            
            attr['xtick_vals']     = pUtil.tick_maker(attr['xrange'], attr['xlog'])
            if ch1 == last_chn_row:
                attr['xlabel']     = r'$\rm Frequency \; [Hz]$'
                attr['xtick_labs'] = pUtil.tick_labeler(attr['xtick_vals'], is_log = attr['xlog'])
            else:
                attr['xlabel']     = ''
                attr['xtick_labs'] = []

            # CSD phase y-attributes
            attr['ylabel']         = r'$\phi_{' + ch1 + ch2 + r'}$'
            if self._settings['bigscreen']:
                attr['text_size']  = 12
            else:
                attr['text_size']  = 8
            attr['ylog']           = False
            attr['ylog_str']       = 'linear'
            attr['yrange']         = [-180, 180]
            attr['ytick_vals']     = [-180, 0, 180]
            attr['ytick_labs']     = [r'$-180^\circ$', r'$0^\circ$', r'$+180^\circ$']             
        else:
            raise ValueError('Invalid subplot key: %s' % plot_key)
        return attr                       
    
    def _xrange_get(self):   
        """ Get the X-range for the supplied subplot """
        x_range = self._settings['freq_range_hz']
        if x_range[0] < self._freqs[self._offset]:
            # Set the minimum displayable bin
            x_range[0] = self._freqs[self._offset]
        return x_range
        
    def ordered_keylist(
        self,
        filter = None
    ):
        """ Returns the list of enabled PSDs or CSDs (numerically ordered by row, then col) """
        keylist = []
        active_chs = self.active_channels()
        for row_idx in active_chs:
            if filter == 'psd':
                cols = [row_idx]
            elif filter == 'csd':
                cols = [idx for idx in active_chs if idx < row_idx]
            else:
                cols = [idx for idx in active_chs if idx <= row_idx]
            for col_idx in cols:
                key = str(row_idx) + str(col_idx)
                keylist.append(key)
        return keylist
    
    def _yrange_get(self, plot_key):
        """ Get the Y-range for the supplied PSD or CSD magnitude subplot.
            (Note: CSD phase is not supported by this method) """
        assert len(plot_key) == 2

        # Get the list of plot keys over which to iterate            
        if self._settings['common_y']:
            # Take all enabled PSDs or CSDs
            filter = 'psd' if plot_key[0] == plot_key[1] else 'csd'
            keylist = self.ordered_keylist(filter = filter)     
        else:
            # Take only this particular plot
            keylist = [plot_key]
            
        # Get the index range of bins to include       
        xmin, xmax = self._xrange_get()
        idx_min = np.where(self._freqs <= xmin)[0][-1]
        idx_max = np.where(self._freqs >= xmax)[0][0]
               
        # Take the furthest y-range spanned by all plots in the list               
        yrange = None
        for key in keylist:
            # Get the data
            data = self._model_map(key)[idx_min : idx_max]
            if data[data <= 0].size > 0:
                # Power measurements should always be >0
                warnings.warn("Negative/zero-valued data: %s" % key)
                return [1, 2]
                    
            # Get the Y-range of this data                    
            ymin = 10**(math.floor(np.log10(np.min(data))))
            ymax = 10**(math.ceil(np.log10(np.max(data))))
            if yrange is None:
                yrange = [ymin, ymax]
                continue
            if ymin < yrange[0]:
                yrange[0] = ymin
            if ymax > yrange[1]:
                yrange[1] = ymax
            
        # Expand the y-range, if the min and max are almost the same
        if (yrange[1] / yrange[0]) - 1 < 0.1:
            if self._settings['ylog']:
                yrange[0] /= 10.
                yrange[1] *= 10.
            else:
                yrange[0] *= 0.9
                yrange[1] *= 1.1
        return yrange         
        
    def save_figure(
        self, 
        filename,
        remarks
    ):
        """ Saves the figure """
    
        # Overplot the data as non-animated lines
        # (MPL BUG WORKAROUND: Animated lines cannot be saved in the Qt backend on Linux)
        self._draw_lines_nonanimated()
                     
        # Add the metadata label
        self._add_metadata_label(remarks)

        # Save the figure
        success = True            
        try:
            self._fig.savefig(filename)
        except:
            success = False

        # Redraw the plots with animated lines
        self.plots_create()
        return success

    def _draw_lines_nonanimated(self):
        """ Overplots the data as non-animated lines
            (Needed b/c animated lines cannot be saved in the Qt MPL backend) """
            
        # Plot non-animated data curves
        for key in self._axes.keys():
            if key[0] == key[1]:
                color = self._color_psd
            elif len(key) == 2:
                color = self._color_mag
            elif key[2] == 'p':
                color = self._color_phase
            else:
                raise ValueError('Invalid key: %s' % key)
            self._axes[key].plot(
                self._freqs[self._offset:], 
                self._model_map(key)[self._offset:],
                color = color
            )
            
        if self._poisson.keys():
            # Plot non-animated Poisson coherence/amplitude limits     
            if self._settings['coherence']:
                data = np.ones(self._num_displayed_bins) * self._model.poisson_coh
            for key in self._poisson.keys():
                if not self._settings['coherence']:
                    chs = [int(chn) for chn in key[:2]]
                    data = self._model.poisson_amp(*chs)[self._offset:]
                self._axes[key].plot(
                    self._freqs[self._offset:],
                    data,
                    linestyle = '--', 
                    color = 'k'
                )       
            
        '''
        if self._settings['holo_trace']:
            # Overplot a holographic noise trace
            if self._settings['coherence']:
                signal = self.holo_coh
            else:
                signal = self.holo_asd                 
            self._axes[key].plot(
                self._freqs[self._offset:],
                signal,
                color = '.6'
            )
        '''
        return
        
    def _add_metadata_label(self, remarks):
        """ Stamps the figure with a metadata label """
        x_pos       = 0.80
        y_pos       = 0.89
        delta_y     = 0.05
        max_chars   = 38      
        font_size   = 14 if self._settings['bigscreen'] else 10

        # Integration time range 
        int_time_str = pUtil.sec_to_time_str(self._model.integ_time)
        self._fig.text(
            x_pos, 
            y_pos, 
            'Integ time:  ' + int_time_str,
            va = 'top',
            size = font_size
        )
        
        # Total accumulation (number of FFT batches)
        accumulation_str = '%.2e' % self._model.accum
        self._fig.text(
            x_pos, 
            y_pos - 1 * delta_y, 
            'Accum:  ' + accumulation_str,
            va = 'top',
            size = font_size
        )

        # Sample frequency
        sample_freq_str = pUtil.sample_freq_str(self._model.sample_freq)
        self._fig.text(
            x_pos, 
            y_pos - 2 * delta_y, 
            'Sample freq:  ' + sample_freq_str,
            va = 'top',
            size = font_size
        )

        # Frequency bin width
        freq_res_str = pUtil.freq_resolution_str(
            self._model.sample_freq, 
            self._model.length * 2
        )
        self._fig.text(
            x_pos, 
            y_pos - 3 * delta_y, 
            'Freq bin width:  ' + freq_res_str,
            va = 'top',
            size = font_size
        )

        # Figure comments
        # if remarks:
            # if len(remarks) > max_chars:
                # # Break the comment into multiple lines
                # remarks = textwrap.fill(remarks, width = max_chars)
        self._fig.text(
            x_pos, 
            y_pos - 4 * delta_y, 
            'Remarks:  ' + remarks,
            va = 'top',
            size = font_size
        )
        return        
        
    @staticmethod
    def _settings_validate(
        settings,
        supported_opts,
        model
    ): 
        """ Validates the supplied display settings """
        for opt_key in supported_opts:
            if opt_key == 'freq_range_hz':
                # Validate the frequency range setting
                frange = settings['freq_range_hz']
                valid = True
                if type(frange) != list:
                    valid = False
                elif len(frange) != 2:
                    valid = False
                elif frange[1] > model.sample_freq / 2. or \
                     frange[0] < 0                or \
                     frange[0] >= frange[1]:
                    valid = False
                if not valid:
                    settings['freq_range_hz'] = [0., model.sample_freq / 2.]               
            elif opt_key == 'channels':
                # Validate the channel on/off states (default value is ON)
                for chn_idx in range(model.num_chs):
                    settings['channels'][chn_idx] = settings['channels'].get(str(chn_idx), True)
                old_keys = [key for key in settings['channels'].keys() if key not in range(model.num_chs)]
                for old_key in old_keys:
                    del settings['channels'][old_key]
                    
                # Validate the channel sources
                for chn_idx in range(model.num_chs):                    
                    settings["chn_sources"][chn_idx] = settings["chn_sources"].get(chn_idx, "N/A")
            else:
                # Set/validate general on/off option (default value is OFF)
                settings[opt_key] = settings.get(opt_key, False)        
        return settings

    """   
        # Create the Hogan noise traces
        n_shot  = 1.5e-7                                                    # [V/rtHz]
        len_arm = 40.                                                       # [m]
        f_c = 2.39e7 / len_arm                                              # [Hz]
        norm = 2. * f_c**2 * (n_shot / 150.)                                # [V/Hz^3/2]
        f = self._freqs[self._offset:]                                        # [Hz]
        self.holo_asd = norm * np.divide(1 - np.cos(f / f_c), np.square(f)) # [V/rtHz]
        self.holo_coh = self.holo_asd / n_shot                              # [amplitude coherence]
    """        
        
#~FigureCanvasCSD class
