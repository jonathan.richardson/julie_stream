#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import os
import math
import copy
import numpy as np
from collections import deque
from functools import partial as BIND

from PyQt4.Qt import *

from julie_csd import CSDCarrierFloat, CSDCarrierDouble

from ..utilities import plot_utils as pUtil
from ..utilities import gui_utils as gUtil
from ..utilities import interface_factories as factory

from ..plotters.figure_canvas_transient import FigureCanvasTransient
from ..plotters.figure_canvas_time import FigureCanvasTime

from ..data_fetchers.data_model import DataModel


class TimeCorrelator(QWidget):
    """ Time-domain cross-correlator class """

    _size   = [1000, 700]
    _title  = "Time Cross-Correlation"
        
    def __init__(
        self, 
        close_callback  = None
    ):
        """ Constructor """
        super(TimeCorrelator, self).__init__()
        if close_callback is not None:
            # Create the PyQt4 signal for window close
            # (Unregisters this child with the main window)
            self.connect(
                self, 
                SIGNAL('close'), 
                close_callback,
            )
        
        # Set the window properties
        self._layout = QGridLayout()
        self.setLayout(self._layout)
        self.setFixedSize(*self._size)
        self.setWindowTitle(self._title)
        
        # Bind the GUI window to the smallest available monitor
        self._geometry = gUtil.screen_autodetect(widest = False)
        self.move(self._geometry.left(), self._geometry.top())        
        
        # Create the underlying data model
        self._model = DataModel()
        
        # Set the initial monitor state
        self._paused = False
        self._initialized = False            
        return

    def closeEvent(self, event):
        """ Signal close to the parent window """        
        self.emit(SIGNAL('close'), False)
        return        

    def _rescale(self):  
        """ Rescales the canvas """
        self._canvas.rescale(self._pow_deq)
        return
        
    def _pause(self):
        """ Pause button callback """
        self._paused = not self._paused
        return
        
    def _window_launch(self):
        """ Sets up the monitor screen """
        self._objects = {}
        
        # Integration time display panel
        int_time_str = pUtil.sec_to_time_str(self._model.integ_time, compact = True) 
        label = "Integration time"
        panel, objects = factory.integration_time_panel(int_time_str, label = label)
        self._layout.addWidget(panel, 0, 0, 1, 2)
        self._objects.update(objects)

        # Figure canvas
        self._canvas = FigureCanvasTime(self._model)
        self._layout.addWidget(self._canvas, 1, 0, 1, 7)
        
        # Set the row & column sizing policies, then show the window
        for col_idx in range(self._layout.columnCount()):
            self._layout.setColumnStretch(col_idx, 1)
        self._layout.setRowStretch(1, 10)
        self.show()
        
        # Set up the figure canvas
        # (Must render the Qt window first to get the correct sizing)
        self._canvas.setup()
        return

    def _channel_on_off(
        self,
        chn_idx,
        on_off
    ):
        """ Enables/disables the supplied data channel curve """
        if  (len(self._active_chs) == 1) \
        and (self._active_chs[0] == chn_idx):
            # Do not allow all channels to be turned off
            self._objects[chn_idx].setChecked(True)
            return
    
        # Add/remove channel from the active list
        if on_off:
            self._active_chs.append(chn_idx)
        else:
            self._active_chs.remove(chn_idx)
        
        # Update the figure to show/hide the specified curve
        self._canvas.channel_on_off(chn_idx, self._pow_deq)
        return
        
    def _save_figure(self):
        """ Saves the figure as currently configured on the canvas """
        initially_paused = self._paused
        if not initially_paused:
            self._pause()
    
        # Get the image formats supported by the canvas backend
        filter = ''
        formats = self._canvas.get_supported_filetypes_grouped()
        for ftype in sorted(formats.keys()):
            for suffix in formats[ftype]:
                filter += "%s (*.%s);;" % (ftype, suffix)
        
        try:
            # Prompt the user for a filename 
            fname = unicode(
                QFileDialog.getSaveFileName(
                    caption = "Save file",
                    filter  = filter,
                    selectedFilter = "Portable Network Graphics (*.png)"
                )
            )
            if not fname:
                raise RuntimeError("Normal control flow")

            # Save the figure
            saved = self._canvas.save_figure(fname, self._pow_deq)
            if not saved:
                # Report the error
                msg = 'Filename is invalid or comment contains illegal character sequence.'
                QMessageBox.critical(
                    self, 
                    'Save Failed', 
                    msg
                )
        except RuntimeError:
            pass
        if not initially_paused:
            self._pause()
        return
        
    @gUtil.synchronized
    def update(self, csd):
        """ Updates the window display """
        if self._paused:
            return    
    
        # Apply the data update to the model
        self._model.update(csd)
    
        if not self._initialized:
            # Initialize the window
            self._initialize()
            return

        # Compute the time cross-correlation     
        self._time_correlation_set()

        # Update the integration time label         
        int_time_str = pUtil.sec_to_time_str(self._model.integ_time, compact = True)        
        self._objects['time'].setText(int_time_str)
            
        # Update the plot line(s)
        #self._canvas.draw_lines(self._pow_deq)
        return
        
    def _time_correlation_set(self):
        """ Compute the time cross-correlation """     
        return
        
    def _initialize(self):
        """ Initializes the window """
        '''
        # Set the channels to monitor
        if not self._active_chs:
            self._active_chs = [chn_idx for chn_idx in range(self._model.num_chs)]

        # Create the frequency bins
        self._freq = np.linspace(
            0., 
            self._model.sample_freq / 2., 
            self._model.length
        )
        
        # Set the integration bandwidth
        if not self._bandwidth:
            self._bandwidth = [self._freq[0], self._freq[-1]]
    
        # Create, set the P_rms baselines and fluctuation deques
        self._pow_deq = {}
        self._win_len = int(math.ceil(self._win_time_s / self._model.accum_time))      
        for chn_idx in range(self._model.num_chs):
            self._pow_deq[chn_idx] = deque(maxlen = self._win_len)    
        self._baselines_set() 
        self._fluctuations_set()
        '''
        # Launch the window         
        self._window_launch()
        self._initialized = True            
        return        

#~class TimeCorrelator