#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import os
import math
import copy
import numpy as np
from collections import deque
from functools import partial as BIND

from PyQt4.Qt import *

from julie_csd import CSDCarrierFloat, CSDCarrierDouble

from ..utilities import plot_utils as pUtil
from ..utilities import gui_utils as gUtil
from ..utilities import interface_factories as factory

from ..plotters.figure_canvas_transient import FigureCanvasTransient

from ..data_fetchers.data_model import DataModel


class TransientMonitor(QWidget):
    """ Real-time transient monitor class """

    _size   = [1000, 700]
    _title  = "Real-Time Transient Monitor"
        
    def __init__(
        self, 
        close_callback  = None,
        win_time_s      = 60,
        bandwidth       = [],
        active_chs      = []
    ):
        """ Constructor """
        super(TransientMonitor, self).__init__()
        self._win_time_s = win_time_s
        self._bandwidth = bandwidth
        self._active_chs = active_chs
        if close_callback is not None:
            # Create the PyQt4 signal for window close
            # (Unregisters this child with the main window)
            self.connect(
                self, 
                SIGNAL('close'), 
                close_callback,
            )
        
        # Set the window properties
        self._layout = QGridLayout()
        self.setLayout(self._layout)
        self.setFixedSize(*self._size)
        self.setWindowTitle(self._title)
        
        # Bind the GUI window to the smallest available monitor
        self._geometry = gUtil.screen_autodetect(widest = False)
        self.move(self._geometry.left(), self._geometry.top())        
        
        # Create the underlying data model
        # (Supplying time window = -1 forces a CSD deque of minimum size = 2)
        self._model = DataModel(t_window_s = -1)
        
        # Set the initial monitor state
        self._paused        = False
        self._visible       = False
        self._initialized   = False            
        return

    def closeEvent(self, event):
        """ Signal close to the parent window """        
        self.emit(SIGNAL('close'), False)
        return
        
    def start_stop(self, on_off):
        """ Hides/shows window and stops/starts updating """
        self._visible = on_off
        if self._initialized:
            # Set the window visibility
            if self._visible:
                # Reset the plotted data curves
                self._baselines_set()
                self._canvas.draw_lines(self._pow_deq)
            self.setVisible(self._visible)
        return
                     
    def _baselines_set(self):
        """ Sets the baseline integrated in-band RMS power for each channel
            (This is an average over the entire integration time) """
    
        # Reset the power deques
        for deq in self._pow_deq.values():
            deq.clear()
    
        # Compute the new baselines
        self._p0 = {}
        for chn_idx in self._pow_deq.keys(): 
            self._p0[chn_idx] = self._power_rms(chn_idx)
        return

    def _rescale(self):  
        """ Rescales the canvas """
        self._canvas.rescale(self._pow_deq)
        return
        
    def _pause(self, on_off):
        """ Pause button callback """

        # Set the monitor state
        self._paused = on_off
        if not self._paused:
            # Reset the power buffers & baseline
            self._baselines_set()
            
        # Enable/disable the live options
        self._objs['baseline'].setDisabled(self._paused)
        self._objs['bandwidth'].setDisabled(self._paused)
        return
        
    def _window_launch(self):
        """ Sets up the monitor screen """
        self.hide()
        self._objs = {}
        
        # Integration time display panel
        int_time_str = pUtil.sec_to_time_str(self._model.integ_time, compact = True) 
        label = "Time resolution"
        panel, objects = factory.integration_time_panel(int_time_str, label = label)
        self._layout.addWidget(panel, 0, 0, 1, 2)
        self._objs.update(objects)
        
        # Control panel for monitor options
        callbacks = {
            'pause'     : self._pause,
            'save'      : self._save_figure,
            'baseline'  : self._baselines_set,
            'rescale'   : self._rescale
        }
        panel, objects = factory.transient_control_panel(callbacks)
        self._layout.addWidget(panel, 1, 0, 1, 2) 
        self._objs.update(objects)
        
        # Lookback time control panel
        callbacks = {
            'lookback_set'  : self._lookback_time_set,
            'lookback_val'  : self._lookback_time_validate,
        }
        valid_range_s = [1, 3600]    
        self._objs['lookback'], objects = factory.lookback_time_panel(
            callbacks,
            valid_range_s,
            self._win_time_s,
            label = 'Lookback time (s)'
        )
        self._layout.addWidget(self._objs['lookback'], 0, 2, 2, 1) 
        self._objs.update(objects)        
        
        # Frequency range control panel
        callbacks = {
            'freq_set'      : BIND(self._freq_range_set, False),
            'freq_reset'    : BIND(self._freq_range_set, True),
            'freq_min'      : BIND(self._freq_range_validate, 'freq_min'),
            'freq_max'      : BIND(self._freq_range_validate, 'freq_max')
        }
        valid_range_hz = [self._freq[0], self._freq[-1]]      
        self._objs['bandwidth'], objects = factory.freq_range_panel(
            callbacks,
            valid_range_hz,
            self._bandwidth,
            label = 'Integration bandwidth (MHz)'
        )
        self._layout.addWidget(self._objs['bandwidth'], 0, 3, 2, 2) 
        self._objs.update(objects)
        self._objs['freq_set'].setEnabled(False)
        self._objs['freq_reset'].setEnabled(False)
        
        # Channel selection panel
        callbacks = {}
        for chn_idx in range(self._model.num_chs):
            callbacks[chn_idx] = BIND(self._channel_on_off, chn_idx)
        self._objs['chn_select'], objects = factory.chn_selection_panel(
            callbacks,
            self._active_chs,
            incl_update_button = False
        )
        self._layout.addWidget(self._objs['chn_select'], 0, 5, 2, 2)
        self._objs.update(objects)
        
        # Figure canvas
        self._canvas = FigureCanvasTransient(
            self._win_time_s,
            self._win_len,
            self._model.num_chs,
            self._active_chs
        )
        self._layout.addWidget(self._canvas, 2, 0, 1, 7)
        
        # Set the row & column sizing policies
        for col_idx in range(self._layout.columnCount()):
            self._layout.setColumnStretch(col_idx, 1)
        self._layout.setRowStretch(2, 10)
        
        # Set up the figure canvas
        # (Must render the Qt window first to get the correct sizing)
        self.show()
        self._canvas.setup(self._pow_deq)
        return

    def _channel_on_off(
        self,
        chn_idx,
        on_off
    ):
        """ Enables/disables the supplied data channel curve """
        if  (len(self._active_chs) == 1) \
        and (self._active_chs[0] == chn_idx):
            # Do not allow all channels to be turned off
            self._objs[chn_idx].setChecked(True)
            return
    
        # Add/remove channel from the active list
        if on_off:
            self._active_chs.append(chn_idx)
        else:
            self._active_chs.remove(chn_idx)
        
        # Update the figure to show/hide the specified curve
        self._canvas.channel_on_off(chn_idx, self._pow_deq)
        return
        
    def _save_figure(self):
        """ Saves the figure as currently configured on the canvas """
        initially_paused = self._paused
        if not initially_paused:
            self._pause()
    
        # Get the image formats supported by the canvas backend
        filter = ''
        formats = self._canvas.get_supported_filetypes_grouped()
        for ftype in sorted(formats.keys()):
            for suffix in formats[ftype]:
                filter += "%s (*.%s);;" % (ftype, suffix)
        
        try:
            # Prompt the user for a filename 
            fname = unicode(
                QFileDialog.getSaveFileName(
                    caption = "Save file",
                    filter  = filter,
                    selectedFilter = "Portable Network Graphics (*.png)"
                )
            )
            if not fname:
                raise RuntimeError("Normal control flow")

            # Save the figure
            saved = self._canvas.save_figure(fname, self._pow_deq)
            if not saved:
                # Report the error
                msg = 'Filename is invalid or comment contains illegal character sequence.'
                QMessageBox.critical(
                    self, 
                    'Save Failed', 
                    msg
                )
        except RuntimeError:
            pass
        if not initially_paused:
            self._pause()
        return

    def _power_rms(self, chn_idx): 
        """ Computes the integrated in-band RMS power for the requested channel.
            (Properly normalized to units of V^2) """    
        in_band = np.where(
            (self._freq >= self._bandwidth[0]) & 
            (self._freq <= self._bandwidth[1])
        )
        psd = self._model.psd(chn_idx)[in_band]
        p_rms = np.sum(psd**2)**.5
        return p_rms
        
    def _freq_range_set(self, restore_full_range):
        """ Sets/clears the integration bandwidth used to compute P_rms """
    
        # Enable/disable the "clear range" & "set" options
        self._objs['freq_reset'].setDisabled(restore_full_range)
        self._objs['freq_set'].setEnabled(False) 
  
        if restore_full_range:
            # Restore the full bandwidth
            self._bandwidth = [self._freq[0], self._freq[-1]]
            self._objs['freq_min'].setText(str(self._bandwidth[0] * 1.E-6))
            self._objs['freq_max'].setText(str(self._bandwidth[1] * 1.E-6))
        else:
            # Set the supplied range
            self._bandwidth = [
                float(self._objs['freq_min'].text()) * 1.e6,
                float(self._objs['freq_max'].text()) * 1.e6
            ]
    
        # Set the new baseline integrated RMS power
        self._baselines_set()
        
        # Uncheck the set, reset buttons
        self._objs['freq_reset'].setChecked(False)
        self._objs['freq_set'].setChecked(False)         
        return

    def _freq_range_validate(
        self,
        active_key,        
        input_type,
        text
    ):
        """ Validates a change in supplied integration bandwidth """
        box_min     = self._objs['freq_min']
        box_max     = self._objs['freq_max']
        box_active  = self._objs[active_key]
        
        # Enable/disable the "set range" option
        valid_min, _ = box_min.validator().validate(box_min.text(), 0)
        valid_max, _ = box_max.validator().validate(box_max.text(), 0)
        if  (valid_min == QValidator.Acceptable) \
        and (valid_max == QValidator.Acceptable):
            # Require xmin < xmax; new_range != current_range
            new_range = [
                input_type(box_min.text()) * 1.E6, 
                input_type(box_max.text()) * 1.E6
            ]
            if new_range[0] < new_range[1]:
                enabled = True if new_range != self._bandwidth else False
            else:
                # Color the background of the active textbox red
                color = 'background-color: #ff8080'
                box_active.setStyleSheet(color)            
                enabled = False
        else:
            enabled = False
        self._objs['freq_set'].setEnabled(enabled)   
        return

    def _lookback_time_set(self):
        """ Sets the monitor lookback time """
        
        # Get the new value
        self._win_time_s = int(self._objs['lookback_val'].text())
        self._objs['lookback_set'].setEnabled(False) 
        
        # Update the deque lengths
        self._win_len = math.ceil(self._win_time_s / self._model.accum_time)   
        for chn_idx in range(self._model.num_chs):
            # Copy data from old deque into new deque until old deck is emptied or new deque is filled
            new_deque = deque(maxlen = self._win_len)
            while   (len(self._pow_deq[chn_idx]) > 0) \
                and (len(new_deque) < self._win_len):
                new_deque.append(self._pow_deq[chn_idx].popleft())
            self._pow_deq[chn_idx] = new_deque

        # Update the canvas x-range (the lookback time range)
        self._canvas.time_range_set(
            self._win_time_s,
            self._win_len,
            self._pow_deq
        )
        
        # Uncheck the set button
        self._objs['lookback_set'].setChecked(False)
        return

    def _lookback_time_validate(
        self,       
        input_type,
        text
    ):
        """ Validates a change in supplied lookback time """ 
        box = self._objs['lookback_val']
        valid, _ = box.validator().validate(box.text(), 0)
        if valid == QValidator.Acceptable:
            value = input_type(text)
            enabled = True if value != self._win_time_s else False
        else:
            enabled = False
        self._objs['lookback_set'].setEnabled(enabled)  
        return           
        
    @gUtil.synchronized
    def update(self, csd):
        """ Updates the window display """
        if  self._paused \
        or  not self._visible:
            return

        # Apply the data update to the model
        self._model.update(csd)
        if not self._model.initialized:
            return
        
        if self._initialized:
            # Compute the P_rms fluctuations      
            self._fluctuations_set()

            # Update the integration time label         
            int_time_str = pUtil.sec_to_time_str(self._model.integ_time, compact = True)        
            self._objs['time'].setText(int_time_str)
                
            # Update the plot line(s)
            self._canvas.draw_lines(self._pow_deq)
        else:
            # Initialize the buffers and canvas
            self._initialize()
        return
        
    def _fluctuations_set(self):
        """ Computes the fractional fluctuations in P_rms and appends them 
            to the power deques """
        for chn_idx in self._pow_deq.keys():
            delta_rms = (self._power_rms(chn_idx) - self._p0[chn_idx]) / self._p0[chn_idx]
            self._pow_deq[chn_idx].appendleft(delta_rms)
        return
        
    def _initialize(self):
        """ Initializes the monitor """
        
        # Set the channels to monitor
        if not self._active_chs:
            self._active_chs = [chn_idx for chn_idx in range(self._model.num_chs)]

        # Create the frequency bins
        self._freq = np.linspace(
            0., 
            self._model.sample_freq / 2., 
            self._model.length
        )
        
        # Set the integration bandwidth
        if not self._bandwidth:
            self._bandwidth = [self._freq[0], self._freq[-1]]
    
        # Create, set the P_rms baselines and fluctuation deques
        self._pow_deq = {}
        self._win_len = int(math.ceil(self._win_time_s / self._model.accum_time))      
        for chn_idx in range(self._model.num_chs):
            self._pow_deq[chn_idx] = deque(maxlen = self._win_len)    
        self._baselines_set() 
        self._fluctuations_set()

        # Launch the window         
        self._window_launch()
        self._initialized = True            
        return        

#~class TransientMonitor