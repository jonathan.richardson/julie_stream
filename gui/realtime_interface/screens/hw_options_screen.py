#!/usr/bin/env python

import os
from functools import partial as BIND
from PyQt4.Qt import *    

from ..utilities.interface_factories import *
    

class HWOptionsScreen(QWidget):
    """ Hardware options screen """
        
    title = 'Hardware Settings'
    
    @property
    def size(self):
        """ Window size dynamically determined by number of data sources present """
        return self._size        
    
    def __init__(self, parent):
        """ Generator """
        super(HWOptionsScreen, self).__init__()
        self.setLayout(QGridLayout())
        self.layout().setVerticalSpacing(10)   
        
        # Set the window size for this screen
        window_len = 60 * (len(parent.data_sources) + 1)
        self._size = [400, min(window_len, 750)]

        # Data source menu panel
        options = []
        for source in parent.data_sources:
            options.append(
                [   source['source_name'], 
                    BIND(parent.screen_selector, source['source_name']), 
                    14,
                    parent.config['data_sources']['use_real_5122']
                ]     
            )
        object = menu_button_panel(options)
        self.layout().addWidget(object, 0, 0)
            
        # Nagivation button (returns to the main options screen)
        object = navigation_button(
            'Return',
            BIND(parent.screen_selector, 'main_options') 
        )               
        self.layout().addWidget(object, 1, 0)
        return
#~class HWOptionsScreen
