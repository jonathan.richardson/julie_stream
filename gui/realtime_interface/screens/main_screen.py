#!/usr/bin/env python

import math
import os
from functools import partial as BIND
from PyQt4.Qt import *
import json

from ..utilities import plot_utils as pUtil
from ..utilities import gui_utils as gUtil
from ..utilities.interface_factories import *

from ..plotters.figure_canvas_csd import FigureCanvasCSD

from ..monitors.transient_monitor import TransientMonitor
from ..monitors.time_correlator import TimeCorrelator
    

class MainScreen(QWidget):
    """ Main GUI screen """

    title = 'Holometer Data Acquisition System'

    @property
    def size(self):
        """ Dynamically determine window size based on monitor size """    
        return self.parent.size               
    
    def __init__(
        self, 
        parent,
        model,
        filepath,
        streaming = True
    ):
        """ Generator """
        super(MainScreen, self).__init__()
        self.setLayout(QGridLayout())
        self.layout().setVerticalSpacing(6)   
        self.parent = parent
        self.model = model
        self._objs = {}
        self._suspended  = []  
        self._initialized = False
        self._needs_resize = False
        self._fig_comment = ''         

        # Assemble the display configuration (these settings passed to canvas)
        display = json.loads(open(filepath).read())      
        display['bigscreen'] = True if self.parent.size[1] > 800 else False
        display['chn_sources'] = parent.channel_sources
        display['live'] = False        

        # Create the pop-up monitor windows
        self._monitors = {
            'transient'     :   TransientMonitor#,
            #'correlation'   :   TimeCorrelator
        }
        for key, constructor in self._monitors.items():
            self._monitors[key] = constructor(
                close_callback = BIND(self._window_manager, key)
            )          
            self.parent.register_monitor(self._monitors[key].update)
        
        # Figure canvas 
        self._canvas = FigureCanvasCSD(display, model)
        self.layout().addWidget(self._canvas, 4, 0, 1, 7)

        # Integration time display panel
        integ_time_str = pUtil.sec_to_time_str(
            model.integ_time, 
            compact = True
        )
        panel, objects = integration_time_panel(
            integ_time_str,
            label = 'Integrated Time',
            objname='Itime'
        )
        self.layout().addWidget(panel, 0, 0, 1, 1)
        self._objs.update(objects)
            
        wall_time_str = pUtil.sec_to_time_str(
            model.wall_time, 
            compact = True
        )
        panel, objects = integration_time_panel(
                wall_time_str,
                label = 'Wall Time',
                objname='time'
                )
        self.layout().addWidget(panel, 0, 1, 1, 1)
        self._objs.update(objects)

        duty_cycle_str = "{0: .1f}%".format(model.duty_cycle * 100)
        panel, objects = integration_time_panel(
                duty_cycle_str,
                label = 'Duty Cycle',
                objname='duty_cycle'
                )
        self.layout().addWidget(panel, 0, 2, 1, 1)
        self._objs.update(objects)

        # Control panel for run options
        callbacks = {
            'stop'      :   self._close,
            'pause'     :   self._pause,
            'live'      :   self._live_mode_set,
            'save'      :   self._save_figure
        }
        panel, objects = run_control_panel(callbacks, streaming)
        self.layout().addWidget(panel, 1, 0, 3, 2) 
        self._objs.update(objects)

#        # Monitor add-on window panel            
        callbacks = {}
        for key in self._monitors.keys():
            callbacks[key] = BIND(self._window_manager, key)        
        panel, objects = monitor_panel(callbacks, streaming)
        self.layout().addWidget(panel, 1, 2, 3, 1)
        self._objs.update(objects)

        # Display control panel
        callbacks = {
            'coherence'     :   BIND(self._display_mode_set, 'coherence'),
            'xlog'          :   BIND(self._display_mode_set, 'xlog'),
            'ylog'          :   BIND(self._display_mode_set, 'ylog'),
            'common_y'      :   BIND(self._display_mode_set, 'common_y'),
            'grid'          :   BIND(self._display_mode_set, 'grid'),
            'holo_trace'    :   BIND(self._display_mode_set, 'holo_trace'),
            'rescale'       :   BIND(self._display_mode_set,  None),
            'source_labels' :   BIND(self._display_mode_set, 'source_labels')
        }
        panel, objects = display_control_panel(
            callbacks,
            self._canvas.all_settings_get()
        )
        self.layout().addWidget(panel, 0, 3, 4, 2)
        self._objs.update(objects)

        # Frequency range control panel
        callbacks = {
            'freq_set'      :   BIND(self._freq_range_set, False),
            'freq_reset'    :   BIND(self._freq_range_set, True),
            'freq_min'      :   BIND(self._freq_range_validate, 'freq_min'),
            'freq_max'      :   BIND(self._freq_range_validate, 'freq_max')
        }
        valid_range_hz = [0., model.sample_freq / 2.]
        cur_range_hz = self._canvas.setting_get('freq_range_hz')
        self._objs['freq_range_hz'], objects = freq_range_panel(
            callbacks,
            valid_range_hz,
            cur_range_hz
        )
        self.layout().addWidget(self._objs['freq_range_hz'], 0, 5, 2, 1) 
        self._objs.update(objects)
            
        # Frequency resolution control panel
        dft_size_E = int(math.log(model.length * 2, 2))
        panel, objects = freq_resolution_panel(
            self._freq_res_set,
            model.sample_freq,
            dft_size_E,
            0
        )
        self.layout().addWidget(panel, 2, 5, 2, 1)
        self._objs.update(objects)
            
        # Subplot selection panel
        callbacks = {'chs_set' : self._channels_set}
        for chn_idx in range(model.num_chs):
            callbacks[chn_idx] = BIND(self._channel_on_off, chn_idx)       
        self._active_chs = self._canvas.active_channels()            
        self._objs['chn_select'], objects = chn_selection_panel(
            callbacks,
            self._active_chs
        )
        self.layout().addWidget(self._objs['chn_select'], 0, 6, 4, 1)
        self._objs.update(objects)
        
        # Logger display panel
        self._objs['logger'] = logger_message_box()
        self.layout().addWidget(self._objs['logger'], 5, 0, 1, 7)

        # Set the row & column sizing policies, then show the window
        for col_idx in range(self.layout().columnCount()):
            self.layout().setColumnStretch(col_idx, 1)     
        self.layout().setRowStretch(4, 10)
        self.layout().setRowStretch(5, 1)
        return
        
    def _close(self):
        """ Handler for close event """
        
        # Close all pop-up monitor windows
        for monitor in self._monitors.values():
            monitor.start_stop(False)
            
        # Close the parent window
        self.parent.close()
        return
        
    def _pause(self, on_off):
        """ Handler for pause button """
        self.parent.pause(on_off)
        self._objs['live'].setEnabled(not on_off)
        return
        
    def _live_mode_set(self, t_window_s):
        """ Handler for the live mode controller """
        
        # Update the live mode state
        self.parent.live_mode_set(t_window_s)        
        
        # Update the button appearance
        style = 'background-color: lightgreen' if t_window_s > 0 else 'background-color:'  
        self._objs['live'].setStyleSheet(style)
        if t_window_s > 0:
            # Disable data-dependent display options while the CSD deque is filling
            exclude = ['stop', 'live', 'time']
            for key, object in self._objs.items():
                if  key not in exclude \
                and object.isEnabled():
                    object.setEnabled(False)
                    self._suspended.append(object)        
        return        
        
    @gUtil.synchronized   
    def _save_figure(self, *args):
        """ Saves the figure as currently configured on the canvas """

        # Get the image formats supported by the canvas backend
        filter = ''
        formats = self._canvas.get_supported_filetypes_grouped()
        for ftype in sorted(formats.keys()):
            for suffix in formats[ftype]:
                filter += "%s (*.%s);;" % (ftype, suffix)
                
        # Prompt the user for a filename 
        filename = unicode(
            QFileDialog.getSaveFileName(
                caption = "Save file",
                filter  = filter,
                selectedFilter = "Portable Network Graphics (*.png)"
            )
        )
        if not filename:
            return
            
        # Prompt the user to enter optional figure comments
        text, valid_input = QInputDialog.getText(
            self, 
            'Comments', 
            'Enter figure remarks (optional):',
            text = self._fig_comment
        )
        if not valid_input:
            return
        self._fig_comment = str(text)

        # Save the figure
        saved = self._canvas.save_figure(
            filename,       
            self._fig_comment
        )
        if not saved:
            # Report the error
            msg = 'Filename is invalid or comment contains illegal character sequence.'
            QMessageBox.critical(self, 'Save Failed', msg)
        return        
        
    @gUtil.synchronized
    def _display_mode_set(self, setting, *args):
        """ Updates the supplied display setting in the figure """         
        self._canvas.display_mode_toggle(setting)
        return        
        
    def _freq_range_validate(
        self,
        active_key,        
        input_type,
        text
    ):
        """ Validates a change in supplied frequency range & updates the controller """
        box_min     = self._objs['freq_min']
        box_max     = self._objs['freq_max']
        box_active  = self._objs[active_key]
        
        # Enable/disable the "set range" option
        valid_min, _ = box_min.validator().validate(box_min.text(), 0)
        valid_max, _ = box_max.validator().validate(box_max.text(), 0)
        if valid_min == QValidator.Acceptable and \
           valid_max == QValidator.Acceptable:
            # Require xmin < xmax
            freq_min = input_type(box_min.text()) * 1.E6 
            freq_max = input_type(box_max.text()) * 1.E6
            if freq_min < freq_max:
                # Require new_range != current_range
                new_range = self._canvas.freq_range_map(freq_min, freq_max)
                cur_range = self._canvas.setting_get('freq_range_hz')
                enabled = True if new_range != cur_range else False
            else:
                # Color the background of the active textbox red
                color = 'background-color: #ff8080'
                box_active.setStyleSheet(color)
                enabled = False
        else:
            enabled = False
        self._objs['freq_set'].setEnabled(enabled)   
        return
 
    @gUtil.synchronized
    def _freq_range_set(self, clear_range, *args):
        """ Sets/resets the frequency range """
        if clear_range:
            freq_range = [0., self.model.sample_freq / 2.]
        else:
            freq_range = [
                float(self._objs['freq_min'].text()) * 1.e6, 
                float(self._objs['freq_max'].text()) * 1.e6
            ]
        if self._canvas.freq_range_map(*freq_range) != self._canvas.setting_get('freq_range_hz'):
            # Set the supplied frequency range
            self._canvas.freq_range_set(*freq_range)
            if clear_range:              
                # Update the panel display  
                self._objs['freq_min'].setText(str(freq_range[0] * 1.E-6))
                self._objs['freq_max'].setText(str(freq_range[1] * 1.E-6)) 
        #~if(freq_range)
        key = 'freq_reset' if clear_range else 'freq_set'
        self._objs[key].setChecked(False)
        return        
        
    @gUtil.synchronized
    def _freq_res_set(self, smoothing_len_E):
        """ Sets the frequency bin averaging """
        self.parent.freq_res_set(smoothing_len_E)
        if self.model.initialized:
            # Update the canvas line size
            self._canvas.lines_resize()
        else:
            # Defer the canvas update (when in live view)
            # Disable data-dependent display options while the CSD deque is filling
            exclude = ['stop', 'live', 'time']
            for key, object in self._objs.items():
                if  key not in exclude \
                and object.isEnabled():
                    object.setEnabled(False)
                    self._suspended.append(object) 
            self._needs_resize = True                    
        #~if(init)
        return
                
    def _channel_on_off(
        self,
        chn_idx,
        on_off
    ):
        """ Adds/removed the supplied channel to/from the active list """
        if on_off:
            # Add the channel to the list
            self._active_chs.append(chn_idx)
        else:
            # Remove the channel from the list
            if  len(self._active_chs) == 1 \
            and not on_off:
                # Do not allow the last channel to be turned off
                self._objs[chn_idx].setChecked(True)
                return
            self._active_chs.remove(chn_idx)
        return
    
    @gUtil.synchronized
    def _channels_set(self, *args):
        """ Updates the displayed channels in the canvas """
        if self._active_chs != self._canvas.active_channels():            
            # Update the figure to show the requested channels
            self._canvas.channels_set(self._active_chs)  

            # Enable/disable display options
            if len(self._active_chs) == 1:
                enable = {'common_y' : False, 'coherence' : False}
            else:
                enable = {'common_y' : True,  'coherence' : True} 
            for key, state in enable.items():
                self._objs[key].setEnabled(state)
        #~if(active_chs)
        self._objs['chs_set'].setChecked(False)
        return  
        
    @gUtil.synchronized
    def update_view(self, *args):
        """ Initializes/updates the screen display """
        if self._initialized:
            self._update()
        else:
            self._setup()
            self._initialized = True
        return
        
    def update_log(self, msg):
        """ Updates the logger message display panel (HTML) 
            (Colors warning messages red)
        """
        if len(msg) == 0:
            return
        elif msg[0] == "\t":
            msg = '&nbsp;&nbsp;&nbsp;&nbsp;' + msg[1:]
        else:
            if  msg[:7] == "WARNING" \
            or  msg[:5] == "FATAL":
                self._color = 'red'
            else:
                self._color = 'black'
        msg = '<font color="' + self._color + '">' + msg + '</font>'
        self._objs['logger'].appendHtml(msg)
        return
        
    @gUtil.synchronized
    def _update(self, *args):
        """ Updates the displayed screen data """
            
        # Update the integration time label       
        wall_time_str = pUtil.sec_to_time_str(
            self.model.wall_time, 
            compact = True
        )
        self._objs['time'].setText(wall_time_str)
        
        integ_time_str = pUtil.sec_to_time_str(
            self.model.integ_time, 
            compact = True
        )
        self._objs['Itime'].setText(integ_time_str)

        duty_cycle_str = "{0: .1f}%".format(self.model.duty_cycle * 100)
        self._objs['duty_cycle'].setText(duty_cycle_str)
        
        # Update the figure canvas
        if self._needs_resize:
            # Update the line sizes and redraw the figure
            # (Deferred resize needed for frequency bin averaging in live view)
            self._canvas.lines_resize()
            self._needs_resize = False
        else:
            # Update only the animated lines
            self._canvas.draw_lines()
            
        if self._suspended:
            # Re-enable the suspended display options
            for object in self._suspended:
                object.setEnabled(True)
            del self._suspended[:]            
        return
        
    @gUtil.synchronized
    def _setup(self, *args):
        """ Renders the embedded mpl canvas """
        self._canvas.setup()
        return
        
    def _window_manager(
        self,
        key,
        on_off
    ):
        """ Handler for add-on window open/close """
        self._objs[key].setChecked(on_off)
        self._monitors[key].start_stop(on_off)
        return        
        
#~class MainScreen
        
