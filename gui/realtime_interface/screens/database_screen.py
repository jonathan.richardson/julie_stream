#!/usr/bin/env python

import os
from os.path import join as PATH
from functools import partial as BIND
from PyQt4.Qt import *
import json
import time
import datetime

from ..utilities.interface_factories import *    


class DatabaseScreen(QWidget):
    """ Database access screen """
    
    title   = 'Remote Database Access'
    size    = [1000, 750]
    
    def __init__(
        self, 
        parent,
        filepath
    ):
        """ Generator """
        super(DatabaseScreen, self).__init__()
        self.setLayout(QGridLayout())
        self.layout().setVerticalSpacing(15)
        self.parent = parent
        self._objs = {}
        
        # Load the list of IFO operators
        ifo_operators = json.loads(open(filepath).read())['operators']        
        
        # Create the database search interface
        callbacks = {
            'id'        : BIND(self._search_option_select, 'id'),
            'operator'  : BIND(self._search_option_select, 'operator'),            
            'date'      : BIND(self._search_option_select, 'date'),
            'search'    : self._database_search
        }
        panel, objects = database_search_panel(
            callbacks,
            ifo_operators
        )
        self.layout().addWidget(panel, 0, 0)
        self._objs.update(objects)
        
        # Set the default search method
        self._objs['id'].click()
        
        # Create the database transfer interface
        callbacks = {
            'list'          : self._run_select,
            'transfer'      : self._database_transfer,
            'write_data'    : self._write_option_on_off
        }
        panel, objects = database_transfer_panel(callbacks)
        self.layout().addWidget(panel, 1, 0)
        self._objs.update(objects)
        
        # Cancel button (returns to welcome screen)
        object = navigation_button(
            'Cancel', 
            BIND(parent.screen_selector, 'welcome')
        )
        self.layout().addWidget(object, 2, 0)
        self._objs['cancel'] = object    
        return        
        
    def _search_option_select(self, selection):
        """ Search method selection handler """ 
        search_options = {
            'id'        : ['id_textbox'],
            'operator'  : ['op_list'],
            'date'      : ['time1', 'time2']
        }
        for key in search_options.keys():
            on_off = True if key == selection else False
            for field in search_options[key]:
                self._objs[field].setEnabled(on_off)
        return
        
    def _write_option_on_off(self, on_off):
        """ Write data on/off handler """         
        dep_options = ['write_avg', 'write_all']
        for field in dep_options:
            self._objs[field].setEnabled(on_off)
        return
        
    def _database_search(self):
        """ Database search handler """
        
        # Disable all options while we try to connect
        for object in self._objs.values():
            object.setEnabled(False)
            
        # Get the selected search method
        active = None
        search_methods = ['id', 'operator', 'date']
        for key in search_methods:
            if self._objs[key].isChecked():
                active = key
                break
            
        try:
            # Package the search query
            search = {}
            if active == 'id':
                # Use run ID as search parameter
                search["run_id"] = str(self._objs['id_textbox'].text())
                if len(search["run_id"]) != 5:
                    raise RuntimeError('Invalid run ID: Must be five digits.')
            elif active == "operator":
                # Use operator name as search parameter
                search["operator"] = str(self._objs['op_list'].currentText())
                if len(search["operator"]) == 0:
                    raise RuntimeError('Invalid operator name: None supplied.')
            elif active == "date":
                # Use date/time range as search parameter
                search["time_beg"] = self._objs['time1'].dateTime().toTime_t()
                search["time_end"] = self._objs['time2'].dateTime().toTime_t()
                if not search["time_beg"] < search["time_end"]:
                    raise RuntimeError('Invalid time range: Run start time must be less than end time.')
            else:
                raise ValueError('SHOULD NEVER REACH')
                
            # Search the remote database
            # (Blocks until search completes and returns the results)                
            results = self.parent.database_search(search)
            self.listed_results_raw = list(reversed(results))
            
            # Parse/format the search results
            fmtd_results = self._results_format(results)
                
            # Update the database screen to show the search results
            list_items = [QTreeWidgetItem(hit) for hit in reversed(fmtd_results)]
            self._objs['list'].clear()
            self._objs['label'].clear()
            self._objs['transfer'].setStyleSheet('QPushButton {}')
            self._objs['list'].addTopLevelItems(list_items)
        except RuntimeError as error:
            # Report the connection error
            QMessageBox.critical(self, 'Runtime Error', '%s' % error)

        # Return to the database screen
        exclude = [
            'transfer', 
            'accum', 
            'write_data', 
            'slider', 
            'write_avg', 
            'write_all',
            'write_meta'
        ]
        for key, object in self._objs.items():
            if key not in exclude:
                object.setEnabled(True)
        self._objs['search'].setChecked(False)
        self._search_option_select(active)
        return        
        
    def _run_select(self, *args):
        """ Run selection handler """
        
        # Get the selected run(s)     
        self._runs = []
        for run in self._objs['list'].selectedItems() :
            run_id = str(run.text(0))
            if run_id == 'No hits':
                return            
            duration_s = self.string_to_seconds(str(run.text(1)))
            run_info = {
                'run_id'    : run_id, 
                'time_beg'  : 0,
                'time_end'  : duration_s
            }
            self._runs.append(run_info)
            
        # Get the new data transfer panel state
        if len(self._runs) == 0:
            # No run selected
            msg = ""
            enabled = False
        elif len(self._runs) == 1:
            # Single run selected
            msg = 'Run %s: Slide to select the integration time range (sec)' % self._runs[0]['run_id']            
            if self._runs[0]['time_end'] > 0:
                enabled = True
                self._objs['slider'].setMax(self._runs[0]['time_end'])
                self._objs['slider'].setRange(0, self._runs[0]['time_end'])
                self._objs['slider'].repaint()      
            else:
                enabled = False
        elif len(self._runs) > 1:
            # Multiple runs selected
            msg = 'Co-add runs:  '
            for idx, run in enumerate(self._runs):
                msg += run['run_id']
                if idx < len(self._runs) - 1:
                    msg += ',  '  
            enabled = True
        else:
            raise ValueError("SHOULD NEVER REACH")
            
        # Update integration time range selector
        self._objs['label'].setText(msg)            
        self._objs['slider'].setEnabled(enabled)
        
        # Update transfer options
        self._objs['accum'].setEnabled(enabled)                        
        self._objs['write_data'].setEnabled(enabled)
        if self._objs['write_data'].isChecked():
            write_opts = ['write_avg', 'write_all']
            for field in write_opts:
                self._objs[field].setEnabled(enabled)
        self._objs['write_meta'].setEnabled(enabled)
        
        # Update the transfer button
        if enabled:
            style = 'QPushButton {color: black; background-color: lightblue}'
        else:
            style = 'QPushButton {}'
        self._objs['transfer'].setStyleSheet(style)
        self._objs['transfer'].setEnabled(enabled)
        return

    def _database_transfer(self):
        """ Database transfer handler 
        """
        # Disable all options while we try to connect
        for object in self._objs.values():
            object.setEnabled(False)

        try:
            if len(self._runs) == 1:
                # Get the integration time range
                self._runs[0]["time_beg"], self._runs[0]["time_end"] = self._objs["slider"].getRange()
                
            if self._objs['write_data'].isChecked() or \
               self._objs["write_meta"].isChecked():
                # Get the local write directory
                loc_path = str(QFileDialog.getExistingDirectory(caption = "Select Write Directory"))
                if not loc_path:
                    raise RuntimeError("Cancelled by user")            
            
            if self._objs['write_data'].isChecked():
                # Transfer the CSD data for each selected run to local disk 
                write_all = True if self._objs["write_all"].isChecked() else False     
                for run in self._runs:                
                    self.parent.database_transfer_to_disk(
                        run, 
                        loc_path,
                        write_all
                    )
                
            if self._objs["write_meta"].isChecked():
                # Transfer the metadata for each selected run to local disk
                for run in self._runs:
                    metadata, = self.parent.database_search(run)
                    metadata = json.dumps(metadata, indent=4, sort_keys=True)
                    fname = run["run_id"] + "_run_metadata.json"
                    fpath = PATH(loc_path, fname)
                    with open(fpath, "w") as file_out:
                        file_out.write(metadata)
            
            if self._objs['accum'].isChecked():
                # Transfer the time-averaged CSD data to the internal data model
                self.parent.database_transfer_to_model(self._runs)
                return
        except RuntimeError as error:
            # Report the error
            QMessageBox.critical(self, 'Runtime Error', '%s' % error)
                
        # Return to the database screen
        exclude = []
        if not self._objs['write_data'].isChecked():
            exclude = ['write_avg', 'write_all']
        for key, object in self._objs.items():
            if key not in exclude:
                object.setEnabled(True)
        self._objs['transfer'].setChecked(False)
        return        
        
    @staticmethod
    def string_to_seconds(time_str):   
        """ Returns the duration in seconds of the selected database entry (run) """
        day = int(time_str[:2])
        hour = int(time_str[3:5])
        mins = int(time_str[6:8])
        secs = int(time_str[9:])
        duration_s = day*24*3600 + hour*3600 + mins*60 + secs
        return duration_s         

    @staticmethod
    def seconds_to_string(t_secs):
        try:
            val = int(t_secs)
        except ValueError:
            return "!!!ERROR: ARGUMENT NOT AN INTEGER!!!"
        pos = abs(int(t_secs))
        day = pos / (3600*24)
        rem = pos % (3600*24)
        hour = rem / 3600
        rem = rem % 3600
        mins = rem / 60
        secs = rem % 60
        res = '%02d:%02d:%02d:%02d' % (day, hour, mins, secs)
        if int(t_secs) < 0:
		    res = "-%s" % res
        return res

    def _results_format(self, results):
        """ Parses/packages database search results for display """
        fmtd_results = []
        date_fmt = 'ddd MM/dd/yyyy hh:mm:ss AP'
        for hit in results:                    
            # Run ID
            run_id = hit["run_id"]
            
            # Total integration time (relative sec)
            hit.setdefault("time_beg", 0)
            hit.setdefault("time_end", hit["time_beg"])
            t_diff_s = hit["time_end"] - hit["time_beg"]
            t_integ = self.seconds_to_string(t_diff_s)

            # Operator name
            operator = hit.get("operator", "N/A")            

            # Begin, end times (absolute sec)
            times = {}
            keys = ["time_beg", "time_end"]
            for key in keys:
                tm = QDateTime()
                tm.setTime_t(hit[key])
                times[key] = tm.toString(date_fmt)
 
            # End time (absolute sec)
            tm = QDateTime()
            tm.setTime_t(hit["time_end"])
            t_end = tm.toString(date_fmt)
             
            # Comments
            comment = hit.get("comments", "N/A")    
            
            # Channel sources
            sources = hit.get("sources", [])
            if sources:
                source_str = ""
                for chn_idx, source in enumerate(sources):
                    source_str += "%d: %s" % (chn_idx, source)
                    if chn_idx < (len(sources) - 1):
                        source_str += "; "
            else:
                source_str = "N/A"
                   
            # Append the formatted search result
            fmtd_hit = [
                run_id, 
                t_integ, 
                operator, 
                times["time_beg"], 
                times["time_end"], 
                comment, 
                source_str
            ]
            fmtd_results.append(fmtd_hit)
        #~for(raw_results)
        if len(fmtd_results) == 0:
            fmtd_results.append(['No hits'])
        return fmtd_results
        
#~class DatabaseScreen
