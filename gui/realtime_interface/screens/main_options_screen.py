#!/usr/bin/env python

import os
import json
from functools import partial as BIND
from PyQt4.Qt import *

from ..utilities.interface_factories import *


class MainOptionsScreen(QWidget):
    """ Main run options screen """

    title   = 'Run Configuration Settings'
    size    = [400, 620]
  
    def __init__(
        self, 
        parent,
        filepath
    ):
        """ Generator """
        super(MainOptionsScreen, self).__init__()
        self.setLayout(QGridLayout())
        self.layout().setVerticalSpacing(20)
        self.parent = parent
        self._objs = {}
        
        # Load the run options
        run_options = json.loads(open(filepath).read())        
        #print filepath
        #print json.dumps(run_options, sort_keys=True, indent=4, separators=(', ', ': '))
        
        # Hardware on/off button panel
        options = [
            [   'Lock to IFO state',
                parent.monitor_ifo_state,
                parent.config['monitor_ifo_state']
            ],         
            [   'Allow down-sampling',
                BIND(parent.run_mode_on_off, 'use_down_sampling'),
                parent.config['data_sources']['use_down_sampling']
            ],                  
            [   'Use real-time mode',
                BIND(parent.run_mode_on_off, 'use_realtime_mode'),
                parent.config['data_sources']['use_realtime_mode']
            ],                    
            [ 'Use GPS synchronization',
                BIND(parent.run_mode_on_off, 'use_gps_sync'),                
                parent.config['data_sources']['use_gps_sync']
            ],
            [ 'Use GPS triggering',
                BIND(parent.run_mode_on_off, 'use_start_trig'),               
                parent.config['data_sources']['use_start_trig']
            ],
            [ 'Self-calibrate digitizers',
                BIND(parent.run_mode_on_off, 'calibrate_5122'),           
                parent.config['data_sources']['calibrate_5122']
            ]            
        ]
        object = on_off_panel(
            options,
            on_off = parent.config['data_sources']['use_real_5122']
        )
        self.layout().addWidget(object, 0, 0, 1, 2)
        
        # Options menu
        options = [
            [   'Time settings', 
                BIND(parent.screen_selector, 'time_options'), 
                14
            ],[ 'Hardware settings', 
                BIND(parent.screen_selector, 'hw_options'),
                14
            ],[ 'Writer settings', 
                BIND(parent.screen_selector, 'writer_options'),
                14
            ]
        ]
        object = menu_button_panel(options, flat = True)
        self.layout().addWidget(object, 1, 0, 1, 2)
        
        # CSD error veto threshold dropdown list
        setting = 'csd_veto_thres'
        callback = BIND(
            self._dropdown_wrapper, 
            setting,
            sorted(run_options[setting].values())
        )
        object, _ = dropdown_list(
            'Error threshold (listed in increasing severity)',
            run_options[setting],
            callback = callback,
            default = parent.config[setting]
        )
        self.layout().addWidget(object, 2, 0, 1, 2)        

        # Navigation button #1 (connects to server)
        object = navigation_button(
            'Submit job', 
            parent.run_start
        )
        self.layout().addWidget(object, 3, 0, 1, 1)
        self._objs['submit'] = object
        
        # Navigation button #2 (returns to welcome screen)
        object = navigation_button(
            'Cancel', 
            BIND(parent.screen_selector, 'welcome')
        )
        self.layout().addWidget(object, 3, 1, 1, 1)
        return

    def _dropdown_wrapper(
        self, 
        setting,
        options,
        option_idx    
    ):
        """ Handler for a dropdown-list-supplied config setting """
        value = options[option_idx]
        self.parent.run_config_update(setting, value)
        return        
        
#~class MainOptionsScreen
