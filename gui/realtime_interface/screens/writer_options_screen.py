#!/usr/bin/env python

import os
import json
from functools import partial as BIND
from PyQt4.Qt import *

from ..utilities.interface_factories import *
    

class WriterOptionsScreen(QWidget):
    """ HDF5 writer options screen """

    title = 'Writer Settings'

    @property
    def size(self):
        """ Enables/disables source labels based on the state of each source """
        conf = self.parent.config['data_sources']['devices']
        num_sources = len(self.parent.data_sources)
        for src_idx in range(num_sources):
            src_name = self.parent.data_sources[src_idx]['source_name']
            on_off = src_name in [src['source_name'] for src in conf]      
            for dev_idx in range(2):
                chn_idx = 2 * src_idx + dev_idx
                self._objs[chn_idx].setEnabled(on_off)
        return self._size
    
    def __init__(
        self, 
        parent,
        filepath
    ):
        """ Generator """
        super(WriterOptionsScreen, self).__init__()
        self.setLayout(QGridLayout())
        self.layout().setVerticalSpacing(10)   
        self.parent = parent
        self._objs = {}
        num_sources = len(parent.config['data_sources']['devices'])
        num_chs = 2 * num_sources            
        
        # Load the list of IFO operators
        ifo_operators = json.loads(open(filepath).read())['operators']
        
        # Set the window size for this screen
      
        window_len = min(75 * (num_sources + 4), 730)    
        self._size = [750, window_len]
        
        # Get the writer state
        writer_state = parent.config['hdf5_writer']['use_hdf5_writer']
        
        # Boxes for channel source descriptions
        for src_idx in range(num_sources):
            for dev_idx in range(2):
                chn_idx = 2 * src_idx + dev_idx
                src_name = parent.data_sources[src_idx]['source_name']
                label = 'Chn %d\t\t\t\t[%s]' % (chn_idx, src_name)
                setting = 'ch%d_source' % dev_idx
                default_val = parent.data_sources[src_idx]['chn_sources'][dev_idx]
                callback = BIND(self._channel_source_update, chn_idx)
                object, _ = textbox_panel(
                    label,
                    default         = default_val,
                    spec_callback   = callback
                )
                self.layout().addWidget(object, src_idx, dev_idx)   
                self._objs[chn_idx] = object

        # HDF5 writer on/off option
        object = on_off_button(
            'HDF5 writer',
            self._writer_on_off,
            on_off = writer_state
        )
        self.layout().addWidget(object, num_chs, 0, 1, 2)

        # IFO operator drop-down list
        setting = 'operator'
        callback = BIND(
            self._dropdown_wrapper, 
            setting,
            sorted(ifo_operators)
        )    
        default = parent.config['hdf5_writer'][setting]
        if default not in ifo_operators:
            default = sorted(ifo_operators)[0]
            parent.config['hdf5_writer'][setting] = default
        object, self._objs[setting] = dropdown_list(
            'Operator',
            ifo_operators,
            callback    = callback,
            default     = default,
            enabled     = writer_state
        )
        self.layout().addWidget(object, num_chs + 1, 0, 1, 2)                
        
        # Box for other run comments
        setting = 'comments'
        callback = BIND(self._textbox_wrapper, setting)
        object, self._objs[setting] = textbox_panel(
            'Other comments',
            spec_callback   = callback,
            default         = str(parent.config['hdf5_writer'][setting]),
            input_type      = str,
            enabled         = writer_state
        )
        self.layout().addWidget(object, num_chs + 2, 0, 1, 2)
        
        # Navigation button (returns to options screen)
        object = navigation_button(
            'Return', 
            BIND(parent.screen_selector, 'main_options')
        )
        self.layout().addWidget(object, num_chs + 3, 0, 1, 2)
        return
        
    def _channel_source_update(
        self,
        chn_idx,
        input_type,
        text
    ):
        """ Updates the source description for the supplied channel """
        value = input_type(text)
        self.parent.channel_source_update(chn_idx, value)
        return         
        
    def _writer_on_off(self, on_off):
        """ Handler for writer on/off switch """
    
        # Update the run configuration
        self.parent.writer_config_update('use_hdf5_writer', on_off)
        
        # Enable/disable the writer options
        self._objs['operator'].setEnabled(on_off)
        self._objs['comments'].setEnabled(on_off)
        return  

    def _textbox_wrapper(
        self,
        setting,
        input_type,
        text
    ):
        """ Handler for a textbox-supplied config setting """
        value = input_type(text)
        self.parent.writer_config_update(setting, value)
        return      
        
    def _dropdown_wrapper(
        self, 
        setting,
        options,
        option_idx    
    ):
        """ Handler for a dropdown-list-supplied config setting """
        value = options[option_idx]
        self.parent.writer_config_update(setting, value)
        return
        
#~class WriterOptionsScreen
