#!/usr/bin/env python

import os
from functools import partial as BIND
from PyQt4.Qt import *
import json

from ..utilities.interface_factories import *
    

class TimeOptionsScreen(QWidget):
    """ Time options screen """

    title   = 'Time Settings'
    size    = [400, 460]

    def __init__(
        self, 
        parent,
        filepath
    ):
        super(TimeOptionsScreen, self).__init__()
        self.setLayout(QGridLayout())
        self.layout().setVerticalSpacing(15)
        self.parent = parent
        
        # Load the run options
        run_options = json.loads(open(filepath).read())

        # Integration time option
        setting = 'integration_time'
        callback = BIND(self._textbox_wrapper, setting) 
        object, _ = textbox_panel(
            'Integration time',
            spec_callback   = callback,
            default         = str(parent.config[setting]),
            footnote        = 'Valid values: > 0 s (-1 for unlimited)',
            input_type      = int,
            valid_range     = [-1, None]           
        )                     
        self.layout().addWidget(object, 0, 0)

        # Accumulation time option
        setting = 'accumulation_time_s'
        callback = BIND(self._textbox_wrapper, setting)  
        object, _ = textbox_panel(
            'Accumulation time',
            spec_callback   = callback,
            default         = str(parent.config[setting]),
            footnote        = 'Valid values: 0.02 - 20 s',
            input_type      = float,
            valid_range     = [0.02, 20]           
        )        
        self.layout().addWidget(object, 1, 0)
        
        # Welch overlap fraction option
        setting = 'frame_overlap_frac'
        callback = BIND(self._textbox_wrapper, setting)        
        object, _ = textbox_panel(
            'Welch overlap fraction',
            spec_callback   = callback,
            default         = str(parent.config[setting]),
            footnote        = 'Valid values: 0.0 - 1.0',
            input_type      = float,
            valid_range     = [0, 1]           
        )        
        self.layout().addWidget(object, 2, 0)        
        
        # DFT size option
        setting = 'fft_size_E'
        callback = BIND(
            self._dropdown_wrapper, 
            setting,
            sorted(run_options[setting].values())
        )
        object, _ = dropdown_list(
            'DFT size',
            run_options[setting],
            callback    = callback,
            default     = parent.config[setting]
        )                  
        self.layout().addWidget(object, 3, 0)

        # Navigation button (returns to main options screen)
        object = navigation_button(
            'Return',
            BIND(parent.screen_selector, 'main_options')
        )        
        self.layout().addWidget(object, 7, 0)
        return
        
    def _textbox_wrapper(
        self,
        setting,
        input_type,
        text
    ):
        """ Handler for a textbox-supplied config setting """
        value = input_type(text)
        self.parent.run_config_update(setting, value)
        return      
        
    def _dropdown_wrapper(
        self, 
        setting,
        options,
        option_idx    
    ):
        """ Handler for a dropdown-list-supplied config setting """
        value = options[option_idx]
        self.parent.run_config_update(setting, value)
        return
        
#~class TimeOptionsScreen
