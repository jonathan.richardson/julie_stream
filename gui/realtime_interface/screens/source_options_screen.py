#!/usr/bin/env python

import os
from functools import partial as BIND
from PyQt4.Qt import *
import json

from ..utilities import gui_utils as gUtil
from ..utilities.interface_factories import *

# Configuration file paths/names
PATH_RUN_SETTINGS   = os.path.join(os.getenv('JSTREAM_PATH'), os.path.normpath('settings'))
PATH_GUI_SETTINGS   = os.path.join(os.getenv('JSTREAM_PATH'), os.path.normpath('gui/realtime_interface/settings'))
FNAME_RUN_OPTS      = 'supported_run_settings.json'
FNAME_HW_OPTS       = 'supported_5122_settings.json'
FNAME_HW_INCOMP     = 'incompatible_5122_settings.json' 
FNAME_DISP_CONF     = 'default_display.json'
FNAME_RUN_CONF      = 'run_settings.json'   
FNAME_LOGO          = 'holo_logo_anim.gif'
FNAME_IFO_OPS       = 'ifo_operators.json'    
    

class SourceOptionsScreen(QWidget):
    """ Options screen for a particular data source """

    size = [400, 750]
    
    @property
    def title(self):
        """ Dynamically determine window title based on device name """
        return (self._source['source_name'] + ' Settings')
    
    def __init__(
        self, 
        parent, 
        source,
        fpath_options,
        fpath_incompat
    ):
        """ Generator """
        super(SourceOptionsScreen, self).__init__()
        self.setLayout(QGridLayout())
        self.layout().setVerticalSpacing(15) 
        self.parent = parent
        self._objs = {}
        
        # Load the supported values for each hardware setting
        self._hw_options = json.loads(open(fpath_options).read())   
        
        # Load the incompatible hardware settings combinations
        self._hw_incompat = json.loads(open(fpath_incompat).read())['unsupported']        
    
        # Get the source state
        self._source = source
        conf = parent.config['data_sources']['devices']
        source_state = source['source_name'] in [src['source_name'] for src in conf]

        # Source enable/disable option
        object = on_off_button(
            'Enable ' + source['source_name'],
            self._source_on_off,
            on_off = source_state
        )
        self.layout().addWidget(object, 0, 0)  

        # Sample frequency option
        setting = 'sample_freq'
        callback = BIND(
            self._dropdown_wrapper,
            setting,
            sorted(self._hw_options[setting].values())
        )        
        object, self._objs[setting] = dropdown_list(
            'Sample frequency',
            self._hw_options[setting],
            callback    = callback,
            default     = source[setting],
            enabled     = source_state
        )                
        self.layout().addWidget(object, 1, 0)

        # Coupling mode option
        setting = 'coupling'
        callback = BIND(
            self._dropdown_wrapper,
            setting,
            sorted(self._hw_options[setting].values())
        )                
        object, self._objs[setting] = dropdown_list(
            'Coupling mode',
            self._hw_options[setting],
            callback    = callback,
            default     = source[setting],
            enabled     = source_state
        )                        
        self.layout().addWidget(object, 2, 0)

        # Input voltage range option  
        setting = 'input_range'
        callback = BIND(
            self._dropdown_wrapper,
            setting,
            sorted(self._hw_options[setting].values())
        )        
        object, self._objs[setting] = dropdown_list(
            'Input voltage range',
            self._hw_options[setting],
            callback    = callback,
            default     = source[setting],
            enabled     = source_state
        )                     
        self.layout().addWidget(object, 3, 0)               

        # DC offset option
        setting = 'dc_offset'
        callback = BIND(self._textbox_wrapper, setting)
        object, _ = textbox_panel(
            'DC offset',
            spec_callback   = callback,
            default         = str(source[setting]),
            footnote        = 'Valid values: -0.1 - 0.1 V',
            input_type      = float,
            valid_range     = [-0.1, 0.1],
            enabled         = source_state
        )            
        self.layout().addWidget(object, 4, 0)

        # Anti-aliasing option      
        setting = 'bandwidth'
        callback = BIND(
            self._dropdown_wrapper,
            setting,
            sorted(self._hw_options[setting].values())
        )        
        object, self._objs[setting] = dropdown_list(
            'Anti-aliasing filter',
            self._hw_options[setting],
            callback    = callback,
            default     = source[setting],
            enabled     = source_state
        )
        self.layout().addWidget(object, 5, 0)

        # Impedance option
        setting = 'impedance'
        callback = BIND(
            self._dropdown_wrapper,
            setting,
            sorted(self._hw_options[setting].values())
        )
        object, self._objs[setting] = dropdown_list(
            'Impedance',
            self._hw_options[setting],
            callback    = callback,
            default     = source[setting],
            enabled     = source_state
        )        
        self.layout().addWidget(object, 6, 0)
        
        # Scaling factor option
        setting = 'scaling'
        callback = BIND(self._textbox_wrapper, setting)   
        object, _ = textbox_panel(
            'Probe attenuation factor',
            spec_callback   = callback,
            default         = str(source[setting]),
            footnote        = 'Valid values: > 0 (use 1.0 for cable inputs)',
            input_type      = float,
            valid_range     = [1e-6, None],
            enabled         = source_state
        )
        self.layout().addWidget(object, 7, 0)
       
        # Navigation button (returns to hardware settings screen)
        object = navigation_button(
            'Return',
            BIND(parent.screen_selector, 'hw_options')
        )
        self.layout().addWidget(object, 8, 0)
        
        # Validate the initial settings
        # (Enables/disables unsupported combinations of options)
        self._settings_validate()        
        return
        
    def _source_on_off(self, on_off):
        """ Handler for data source on/off switch """
        
        # Update the run configuration
        self.parent.source_on_off(
            self._source['source_name'], 
            on_off
        )
        
        # Enable/disable the device settings options
        option_types = [QComboBox, QLineEdit]
        gUtil.selected_options_enable(
            on_off,
            self,
            *option_types
        )
        return    

    def _textbox_wrapper(
        self,
        setting,
        input_type,
        text
    ):
        """ Handler for a textbox-supplied config setting """
                
        # Update the configuration
        value = input_type(text)
        self.parent.source_config_update(
            self._source['source_name'], 
            setting, 
            value
        )
        
        # Validate the new settings
        self._settings_validate()            
        return
        
    def _dropdown_wrapper(
        self,
        setting,
        options,
        option_idx    
    ):
        """ Handler for a dropdown-list-supplied config setting """     
        
        # Update the configuration
        value = options[option_idx]
        self.parent.source_config_update(
            self._source['source_name'], 
            setting, 
            value
        )
        
        # Validate the new settings
        self._settings_validate()           
        return
        
    def _settings_validate(self):
        """ Disallows unsupported combinations of 5122 hardware settings """
        for combo in self._hw_incompat: 
            setting1    = combo['ref_setting']['setting']
            value1      = combo['ref_setting']['value']

            setting2    = combo['adj_setting']['setting']
            value2      = combo['adj_setting']['value']            
            new_value   = combo['adj_setting']['adj_value']

            # Enable/disable the value2 option for setting2
            opt_idx = sorted(self._hw_options[setting2].values()).index(value2)
            option = self._objs[setting2].model().index(opt_idx, 0)
            if self._source[setting1] == value1:
                option_state = QVariant.Invalid
            else:
                option_state = QVariant(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self._objs[setting2].model().setData(
                option, 
                option_state, 
                Qt.UserRole - 1
            )
            
            # If setting2=value2, must change it to the next supported value
            if  self._source[setting1] == value1 \
            and self._source[setting2] == value2:
                new_idx = sorted(self._hw_options[setting2].values()).index(new_value)                
                if type(self._objs[setting2]) == QComboBox:
                    # Change the dropdown-list option 
                    # (Calls dropdown handler and then this validator again)
                    self._objs[setting2].setCurrentIndex(new_idx)
                    break
                else:
                    raise NotImplementedError('Need to set up other option types')
        #~for(combo)
        return
        
#~class SourceOptionsScreen        
