#!/usr/bin/env python

import os
from functools import partial as BIND
from PyQt4.Qt import *

from ..utilities.interface_factories import *
    
# Configuration file paths/names
PATH_RUN_SETTINGS   = os.path.join(os.getenv('JSTREAM_PATH'), os.path.normpath('settings'))
PATH_GUI_SETTINGS   = os.path.join(os.getenv('JSTREAM_PATH'), os.path.normpath('gui/realtime_interface/settings'))
FNAME_RUN_OPTS      = 'supported_run_settings.json'
FNAME_HW_OPTS       = 'supported_5122_settings.json'
FNAME_HW_INCOMP     = 'incompatible_5122_settings.json' 
FNAME_DISP_CONF     = 'default_display.json'
FNAME_RUN_CONF      = 'run_settings.json'   
FNAME_LOGO          = 'holo_logo_anim.gif'
FNAME_IFO_OPS       = 'ifo_operators.json'    
    
    
class WelcomeScreen(QWidget):
    """ Welcome screen """

    title   = 'Welcome'
    size    = [400, 740]
    
    def __init__(self, parent):
        """ Generator """
        super(WelcomeScreen, self).__init__()
        self.setLayout(QGridLayout())
        self.layout().setVerticalSpacing(15)
        self.parent = parent
        self._objs = {}

        # System label
        object = std_label(
            text = 'Holometer Data Acquisition System',
            size = 18
        )
        self.layout().addWidget(object, 0, 0, Qt.AlignHCenter)
        
        # Logo
        fpath = os.path.join(PATH_GUI_SETTINGS, FNAME_LOGO)
        object = embedded_image_anim(fpath)
        self.layout().addWidget(object, 1, 0, Qt.AlignHCenter)
        
        # DAQ server state panel
        panel, objs = server_state_panel(parent.kill_run)
        self.layout().addWidget(panel, 2, 0)
        self._objs.update(objs)        
        
        # Run mode options panel
        options = [
            [   'Live streaming', 
                parent.live_streaming_select, 
                14
            ],[ 'Database access', 
                BIND(parent.screen_selector, 'database'), 
                14
            ]
        ]
        panel = menu_button_panel(options, enabled=False)
        self.layout().addWidget(panel, 3, 0)
        self._objs['menu'] = panel
        return
        
    def connection_handler(self, state):
        """ Updates the DAQ server panel display for an established/dropped connection
        """
        # Update the menu panel
        self._objs['menu'].setEnabled(state)
        
        # Update the "Connection status" indicator
        color = 2 if state else 1
        self._objs['connection'].setOnColour(color)
        
        # Update the "Run status" indicator
        run_status = self.parent.run_status()
        self._objs['run_status'].setValue(run_status)
        self._objs['terminator'].setEnabled(run_status)
        return
        
    def run_status_handler(self, state):
        """ Updates the DAQ server panel display for a change in run state
        """
        # Update the "Run status" indicator
        self._objs['run_status'].setValue(state)
        self._objs['terminator'].setEnabled(state)
        return

#~class WelcomeScreen
