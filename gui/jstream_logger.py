#!/usr/bin/env python

import sys, psutil, os

from realtime_interface.data_fetchers.live_streamer import LiveStreamer

# Server address
address_rt = 'tcp://julie2.fnal.gov:8710'
           

####################
# THE MAIN EXECUTION
####################

if __name__ == '__main__':
    # Elevate the OS priority of this process
    priority = 0 if os.name == 'posix' else psutil.HIGH_PRIORITY_CLASS
    psutil.Process(os.getpid()).nice(priority)

    # Create the data fetcher for the real-time stream, connect to server.
    client = LiveStreamer(address_rt, logging=True)
    client.connection_init()

    # Block until interrupted by the user
    raw_input('Press any key to exit...\n\n')
    print 'Interrupted by user.'
