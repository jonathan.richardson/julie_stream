import julie_csd
import time
q = julie_csd.JStreamBridgeClient("tcp://localhost:8700")
q.start()
retval = q.wait_for_connection()
print "after wait_for_connection:  retval=",retval
iRead = 0;
for iLoop in range(20):
    print "-------->iLoop=",iLoop
    csd = q.csd_next()
    if not csd:
        time.sleep(0.1)
    else:
        print "csd read: ",csd.sequence_number_get(),csd.timestamp_begin(),csd.timestamp_end()
print "done with loop in example_client.py"
#q.stop()
