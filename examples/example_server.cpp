////
///
///
///


#include <iostream>

#include <thread>
#include <chrono>
#include <functional>

#include <boost/asio.hpp> //needs to be first for winsock defs
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/thread/thread.hpp> 
#include <boost/program_options.hpp>
#include <boost/random.hpp>
#include "sdt/utilities/logger.hpp"

#include "queues/csd_accumulation_queue.hpp"
#include "jstream_topology.hpp"

using namespace JStream;

namespace argparse = boost::program_options;
//BOOST_AUTO_TEST_CASE( initial_test )
//{
//}

namespace{
    auto logger = uLogger::logger_root().child("julie_stream");
}

int main(int argc, const char* argv[]) 
{	
    std::cout << "<<< example_server with JulieStream v" << JSTREAM_VERSION << " >>>" << std::endl ;

    /////
	///  First get the command-line options
	//TLogLevels 	verbosity_level;		//Verbosity level of the code output messages

	unsigned nChannels;
	unsigned length;
	double duration = 1.0;
	double sendFrequency = 1.0;
	double psdMean, psdSigma;
    try {
      // Get what you need from the command line
        argparse::options_description desc("Allowed options");
        desc.add_options()
            ("help,h", 
                  "produce help message"
            )
            ("verbose,v", boost::program_options::value<int>()->default_value(5)->implicit_value(8),
                  "enable verbosity"      "\n"
                  " -10: fatal"           "\n"
                  " -5: error"            "\n"
                  " -2: warnings"         "\n"
                  "  0: digest"           "\n"
                  "  2: configuration"    "\n"
                  "  5: runtime"          "\n"
                  "  10: programming"     "\n"
                  "  10: debugging"
            )
	  ("nChannels,n", boost::program_options::value<unsigned>()->default_value(4),
	   " Number of channels" "\n"
	   )
	  ("length,l", boost::program_options::value<unsigned>()->default_value(65536),
	   " number of samples per csd" "\n"
	   )
	  ("sendFrequency,s",boost::program_options::value<double>()->default_value(1.0),
	   " frequency to send accumulations (Hz)" "\n"
	   )
	  ("duration,d",boost::program_options::value<double>()->default_value(1.0),
	   " duration of one accumulation step" "\n"
	   )
	  (
	   "psdMean,pm",boost::program_options::value<double>()->default_value(1.0),
	   " mean value of psd in Volts/rtHz" "\n"
	   )
	  (
	   "psdSigma,ps",boost::program_options::value<double>()->default_value(0.01),
	   " sigma of psd in Volts/rtHz" "\n"
	   )
        ;

        argparse::variables_map option_variables;
        argparse::store(argparse::command_line_parser(argc, argv).options(desc).run(), 
			option_variables);
        argparse::notify(option_variables);
   
        if (option_variables.count("help")) {
            std::cout << desc;
            return 0;
        }
		
        uLogger::TLoggingLevels verbosity_level = 
	  option_variables["verbose"].as<TLoggingLevels>();

	nChannels = option_variables["nChannels"].as<unsigned>();
	length = option_variables["length"].as<unsigned>();
	sendFrequency = option_variables["sendFrequency"].as<double>();
	duration = option_variables["duration"].as<double>();
	psdMean = option_variables["psdMean"].as<double>();
	psdSigma = option_variables["psdSigma"].as<double>();
    }
    catch(std::exception& e){
        logger.fatal(e.what());
        return -1;
    }
    std::cout <<"nChannels="<<nChannels<< " length="<<length<<std::endl;

    PersistentJStreamBridgeServer m_server_connector(logger.child("csd_stream"));
    m_server_connector.accept_address_add("tcp://*:8700");
    std::shared_ptr<JStreamBridgeServer> m_server = std::make_shared<JStreamBridgeServer>(logger);
    m_server->prefill_queues(nChannels, length);
    m_server_connector.server_set(m_server);
    m_server_connector.connections_begin();

    CSDAccumulation ca = CSDAccumulation(nChannels, length);
    std::cout <<"ca:  ca.n_channels="<<ca.channels_num()<<"  length="<<ca.length()<<std::endl;
    TFrameId sequenceNumber = 0;
    double beginTime = 0.0;
    double endTime = beginTime + duration;
    char buffer[1000];



    for (unsigned iStep=0; iStep<10000; iStep++) {
      // sleep half a second
      int millisec = 1000/sendFrequency;
      boost::this_thread::sleep(boost::posix_time::milliseconds(millisec)); 
      // say what we are doing
      snprintf(buffer, 1000, "sequenceNumber=%d\n",sequenceNumber);
      logger.action(buffer);
      // set the metadata
      ca.sequence_number_set(sequenceNumber++);
      ca.timestamps_set(beginTime, endTime+=duration);
      ca.accumulation_set(iStep);
      ca.normalization_to_ps_set(1.0);

      boost::mt19937 rng(iStep);
      boost::normal_distribution<> nd(psdMean, psdSigma);
      boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > 
	var_nor(rng, nd);
      // set the psds
      for (unsigned iChannel=0; iChannel<nChannels; iChannel++) {
	Range<double *> psd = ca.psd_get(iChannel);
	for (unsigned i=0; i<psd.size(); i++) {
	  psd[i] = (iStep*psd[i] + var_nor())/(iStep+1);
	}
      }
      // send this accumulation
      bool retval = m_server.get()->transfer_csd_accumulation(ca);
      // say what happened
      std::cout << "after transfer:  retval="<<retval<<std::endl;
    }
    m_server_connector.connections_end();
    std::cout <<"done"<<std::endl;
}


//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

