#!/usr/bin/env python

"""
This file contains the HDF5 data reader methods.
"""

import numpy as np
import h5py

def print_logger(*args):
    """ Forms a string from the supplied arguments and prints 
        to screen. """    
    print ''.join((str(a) + ' ') for a in args)
    return

def read_h5_file(filename):
    """ This reader returns the accumulation(average) of all CSD
        frames contained in the supplied  file. """

    # Open the HDF5 file in read-only mode
    dbfile = h5py.File(filename, 'r')
    print_logger(dbfile.file)

    # Read in the metadata (accessed as a dict)
    run_params = dbfile['Metadata/Configuration']
    timestamps = dbfile['Metadata/Timestamps']

    # Create a simple dict to hold the read-in CSD data
    csd = {
        # Include the number of channels
        "num_chs" : run_params["num_chs"][0],    
    
        # Include the frequency bins
        "freq_bins" : np.linspace(
            0.,
            run_params['sample_freq'][0] / 2., 
            num = run_params['num_freq_bins'][0]
        ),
        
        # Include the ENBW (Hz)
        "ENBW" : run_params["ENBW"][0]
    }
    for row_idx in range(run_params['num_chs'][0]):
        for col_idx in range(row_idx + 1):
            # Include each PSD/CSD as a numpy array
            dtype = np.float64 if row_idx == col_idx else np.complex128
            csd[row_idx, col_idx] = np.zeros(run_params['num_freq_bins'][0], dtype = dtype)
 
    # Read in the raw CSD data, accumulating frame-by-frame
    read_ct = 0
    for frame in sorted(dbfile['Data'].keys()):
        for row_idx in range(run_params['num_chs'][0]):
            for col_idx in range(row_idx + 1):
                dset_name = "Data/%s/<%d%d>" % (frame, row_idx, col_idx)
                data = dbfile[dset_name]
                if row_idx == col_idx:
                    # Co-add the power spectrum (real)
                    csd[row_idx, col_idx] += data
                else:
                    # Co-add the cross-spectrum (complex)
                    csd[row_idx, col_idx] += data[:,0] + 1j*data[:,1]
        read_ct += 1
    #~FRAME READ LOOP
    
    # Apply the normalizing factors to get PSDs, CSDs in units of V^2
    tot_accum = run_params['frame_accumulation'][0] * read_ct
    for row_idx in range(run_params['num_chs'][0]):
        for col_idx in range(row_idx + 1):  
            csd[row_idx, col_idx] *= run_params['normalization_to_ps'][0] / tot_accum

    ## Report the results of the read
    print_logger('-->', read_ct, 'valid frames read')
    print_logger('\tNum channels:\t', run_params['num_chs'][0])
    print_logger('\tSample freq:\t', '%.1f' % (run_params['sample_freq'][0] / 1e6), 'MHz')
    print_logger('\tENBW:\t\t', '%.2f' % (run_params['ENBW'][0] / 1e3), 'kHz')
    print_logger('\tAccumulation:\t', '%.2e' % float(tot_accum))
    return csd      
