#!/usr/bin/env python

"""
This demo code reads the CSD frames from the supplied 
HDF5 file. It plots the time-average of all frames
for all channels.
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt

import hdf5_reader_template as reader

if __name__ == "__main__":
    ####################
    # THE MAIN EXECUTION
    ####################
    
    # Get the input HDF5 filename from the command line
    # (Should include full path if file is not in the current directory)
    parser = argparse.ArgumentParser(description = __doc__)
    parser.add_argument(
        dest = 'filename', 
        type = str,
        help = 'HDF5 filename (includes path)'
    )
    filename = parser.parse_args().filename
    
    # Read the HDF5 data into a simple dict
    csd = reader.read_h5_file(filename)
    
    # Plot some PSD, CSD curves
    plt.plot(csd['freq_bins'], csd[0, 0], label = 'PSD ch0')
    plt.plot(csd['freq_bins'], csd[1, 1], label = 'PSD ch1')
    plt.plot(csd['freq_bins'], np.abs(csd[1, 0]), label = 'CSD mag chs0,1')
    ax = plt.gca()
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel('Power (V^2)')
    ax.legend()
    plt.show()
    