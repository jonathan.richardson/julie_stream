from matplotlib.widgets import Button, CheckButtons, RadioButtons
import matplotlib.pyplot as plt
import numpy as np
import time
import math
import julie_csd
import json, sys, os.path

class HV:
    def __init__(self, stateFileName="hv.json", dataSource="simulate"):
        self.logScalings = ['linear','log']
        self.keepGoing = True
        self.paused = False
        self.q = None
        # not real properties -- get rid of these
        self.length = 65536
        self.setupState(stateFileName,dataSource)
        self.setupCanvas()
        # go!
        self.mainLoop()

# functions called directly in __init__
    def setupState(self, stateFileName="hv.json", dataSource="simulate"):
        self.stateFileName = stateFileName
        if (os.path.isfile(self.stateFileName)):
            self.s = json.loads(open(self.stateFileName).read())
            print "after json.load:s  s=",self.s
        else:
            show = {'a0':True,  
                    'a1':False,
                    'a2':False,
                    'a3':False,
                    'c01':False,
                    'c02':False,
                    'c03':False,
                    'c12':False,
                    'c13':False,
                    'c23':False
                    }

            self.s = {'xLogScaling':0,
                      'yLogScaling':0,
                      'xLim':(0,1), 
                      'yLim':(0,1),
                      'mode':'amp',
                      'show':show,
                      'dataSource':'simulate'
                      }
        self.s['dataSource'] = dataSource
        self.keyList = self.s['show'].keys()
        self.keyList.sort()

    def setupCanvas(self):
        self.fig, self.ax = plt.subplots()
        self.fig.subplots_adjust(left=0.20)
        self.getNextCa() # sets self.ca
        self.lines = {}
        for key in self.keyList:
            self.lines[key], = self.ax.plot(self.freqs,
                                            np.zeros(self.freqs.size),
                                            label=key)

        self.ax.set_xlim(self.getXLimits(self.s['xLim']))
        self.ax.set_ylim(self.getYLimits(self.s['yLim']))
        self.showLegend()
        stopAx = plt.axes([0.01, 0.9, 0.1, 0.04])
        self.stopButton = Button(
            stopAx, "Stop", color='red',hovercolor='darkred')
        self.stopButton.on_clicked(self.stopClicked)
            
        pauseAx = plt.axes([0.01, 0.85, 0.1, 0.04])
        self.pauseButton = Button(
            pauseAx, "Pause", color='gold',hovercolor='goldenrod')
        self.pauseButton.on_clicked(self.pauseClicked)
        
        xLogAx = plt.axes([0.01, 0.76, 0.1, 0.08])
        self.xLogButton = RadioButtons(
            xLogAx, ["xlin","xlog"], active=self.s['xLogScaling'])
        self.xLogButton.on_clicked(self.xLogClicked)
        
        yLogAx = plt.axes([0.01, 0.68, 0.1, 0.08])
        self.yLogButton = RadioButtons(
            yLogAx, ["ylin","ylog"], active=self.s['yLogScaling'])
        self.yLogButton.on_clicked(self.yLogClicked)
            
        xAutoAx = plt.axes([0.01, 0.62, 0.1, 0.04])
        self.xAutoButton = Button(
            xAutoAx, "Auto X", color='lightblue', hovercolor='skyblue')
        self.xAutoButton.on_clicked(self.xAutoClicked)
            
        yAutoAx = plt.axes([0.01, 0.58, 0.1, 0.04])
        self.yAutoButton = Button(
            yAutoAx, "Auto Y", color='lightblue', hovercolor='skyblue')
        self.yAutoButton.on_clicked(self.yAutoClicked)

        checkAx = plt.axes([0.01, 0.18, 0.1, 0.4])
        showBooleans = []
        for key in self.keyList:
            showBooleans.append(self.s['show'][key])
        self.checkButtons = CheckButtons(checkAx, self.keyList, showBooleans)
        self.checkButtons.on_clicked(self.checkButtonsClicked)

        modeAx = plt.axes([0.01,0.06, 0.1, 0.12], axisbg='pink')
        modeList = ['amp','coh']
        try:
            iMode = modeList.index(self.s['mode'])
        except ValueError:
            iMode = 0
            self.s['mode'] = modeList[iMode]
        self.modeRadio = RadioButtons(modeAx, modeList, active=iMode,
                                      activecolor='darkred')
        self.modeRadio.on_clicked(self.modeRadioClicked)

        plt.show(block=False)
        self.fig.canvas.draw()

    def mainLoop(self):
        """ Loop, plot, respond to events"""
        tstart = time.time()
        num_plots = 0        
        while self.keepGoing:
            if not self.paused:
                self.getNextCa()
                self.ax.draw_artist(self.ax.patch)
                for key in self.s['show'].keys():
                    if self.s['show'][key]:
                        self.lines[key].set_ydata(self.getCaValues(key))
                        self.ax.draw_artist(self.lines[key])
                self.ax.draw_artist(self.legend)
                self.fig.canvas.blit(self.ax.bbox)
                self.fig.canvas.flush_events()
            else:
                time.sleep(0.1)
            
            num_plots += 1
            tend = time.time()
            if tend-tstart > 2:
                if not self.paused:
                    print " plots per second =",num_plots/(tend-tstart),self.s['mode']
                tstart = tend
                num_plots = 1
                f = open(self.stateFileName,"w") 
                f.write(json.dumps(self.s))
                f.close()

# Define what to do on key clicks
    def stopClicked(self,event):
        self.keepGoing = False

    def pauseClicked(self,event):
        self.paused = not self.paused
        print "in pauseClicked:  self.paused=",self.paused
        if (self.paused):
            self.pauseButton.color = "green"
        else:
            self.pauseButton.color = "yellow"

    def xLogClicked(self, event):
        if event == "xlog": self.s['xLogScaling'] = 1
        if event == "xlin": self.s['xLogScaling'] = 0
        self.ax.set_xscale(self.logScalings[self.s['xLogScaling']])
        self.ax.set_xlim(self.getXLimits(self.s['xLim']))
        self.fig.canvas.draw()


    def yLogClicked(self, event):
        if event == "ylog": self.s['yLogScaling'] = 1
        if event == "ylin": self.s['yLogScaling'] = 0
        self.ax.set_yscale(self.logScalings[self.s['yLogScaling']])
        self.ax.set_ylim(self.getYLimits(self.s['yLim']))
        self.fig.canvas.draw()

    def xAutoClicked(self, event):
        self.s['xLim'] = self.getXRange()
        self.ax.set_xlim(self.s['xLim'])
        self.fig.canvas.draw()

    def yAutoClicked(self, event):
        self.s['yLim'] = self.getYRange()
        self.ax.set_ylim(self.getYLimits(self.s['yLim']))
        self.fig.canvas.draw()

    def checkButtonsClicked(self,event):
        self.s['show'][event] = not self.s['show'][event]

    def modeRadioClicked(self,event):
        self.s['mode'] = event

# utilities
    def getXRange(self):
        return (self.freqs[0],self.freqs[-1]) 

    def getYRange(self):
        """ return the Y min and max values to be plotted"""
        ymin = float("inf")
        ymax = -ymin
        nTrue = 0;
        for key in self.s['show'].keys():
            if self.s['show'][key]:
                nTrue += 1
                ymin = min(ymin,np.min(self.lines[key].get_ydata()))
                ymax = max(ymax,np.max(self.lines[key].get_ydata()))
        if nTrue == 0:
            return (0,1)
        else:
            if (ymin==ymax): return (ymin*0.99, ymax*1.01)
            return (ymin,ymax)


    def getXLimits(self, limits):
        xmin = self.freqs[0]
        xmax = self.freqs[-1]
        margin = 0.1 # 10% hardwired here
        if self.s['xLogScaling'] == 0:
            deltax = xmax - xmin
            return (xmin-margin*deltax,xmax+margin*deltax)
        if self.s['xLogScaling'] == 1:
            return (xmin*(1-margin), xmax*(1+margin))
            return limits
        return limits

    def getYLimits(self, limits):
        ymin = limits[0]
        ymax = limits[1]
        margin = 0.1 # 10% hardwired here
        if self.s['yLogScaling'] == 0:
            deltay = ymax-ymin
            return (ymin-margin*deltay,ymax+margin*deltay) 
        if self.s['yLogScaling'] == 1:
            return (limits[0]*(1-margin), limits[1]*(1+margin))
            return limits
        return limits
        
# data source
    def getNextCa(self):
        if self.s['dataSource'] == 'simulate':
            try:
                self.nSimulated
            except AttributeError:
                self.nSimulated = 0
            self.nSimulated += 1
            self.ca = julie_csd.CSDAccumulation(4,self.length)
            sqrtn = math.sqrt(self.nSimulated)
            a = [1,2,3,4]
            c = 0.5
            for iRow in range(4):
                psd = math.pow(a[iRow],2)+np.random.randn(self.length)/sqrtn
                self.ca.psd_setValues(psd, iRow)
            i8 = 1
            for iRow in range(4):
                psdRow = math.pow(a[iRow],2)
                for iCol in range(iRow):
                    psdCol = math.pow(a[iCol],2)
                    radius = np.sqrt(c*psdRow*psdCol) + \
                        np.random.randn(self.length)/sqrtn

                    theta = np.deg2rad(10.0*i8*np.ones(self.length))
                    theta[0:((i8)/8.0)/theta.size] *= -1
                    real = radius*np.cos(theta)
                    imag = radius*np.sin(theta)
                    comp = real + 1j*imag
                    self.ca.csd_setValues(comp,iRow,iCol)
                    i8 += 1

        else:
            if (self.q == None):
                print "Now open JStreamBridgeClient for ",self.s['dataSource']
                self.q = julie_csd.JStreamBridgeClient(self.s['dataSource'])
                self.q.start()
                self.q.wait_for_connection()
            temp = self.q.csd_next()
            # should we block here instead??
            if temp != None:
                self.ca = temp
        sfm = self.ca.sample_freq_get()
        if sfm == 0.0 : sfm = 100
        df = 0.5*sfm*1e6/self.ca.length()
        self.freqs = df*np.arange(self.ca.length())

    def getCaValues(self,key):
        if key[0] == 'a':
            if self.s['mode'] == 'amp':
                return self.ca.psd_getValues(int(key[1]))
            elif self.s['mode'] == 'coh':
                return np.ones(self.ca.length())
            
        if key[0] == 'c':
            if self.s['mode'] == 'amp':
                a = self.ca.csd_getValues(int(key[2]),int(key[1]))
                return np.absolute(a)
            elif self.s['mode'] == 'coh':
                a = self.ca.csd_getValues(int(key[2]),int(key[1]))
                dr = self.ca.psd_getValues(int(key[2]))
                dc = self.ca.psd_getValues(int(key[1]))
                coh = np.square(np.absolute(a))/(dr*dc)
                return coh


    def showLegend(self):
        self.legend = self.ax.legend(prop={"size":6},ncol=10)
        self.legend.get_frame().set_alpha(0.4)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        stateFileName = "hv.json"
        dataSource = "simulate"
    if len(sys.argv) == 2:
        stateFileName = sys.argv[1]
        dataSource = "simulate"
    if len(sys.argv) == 3:
        stateFileName = sys.argv[1]
        dataSource = sys.argv[2]
    HV(stateFileName, dataSource)
