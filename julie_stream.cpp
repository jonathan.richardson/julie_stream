//////
///  @file julie_stream.cpp
///  @date
///  @breif 
///  
///  @author Lee McCuller, Jonathan Richardson
///
///  @copyright @ref LICENSE
///
///  @ingroup ALL
///  
///  @todo 
///  @bug

#include "jstream_topology.hpp"

using namespace JStream;
using namespace ConfigTree;

namespace{
    auto logger = uLogger::logger_root().child("jstream");
}

int main(int argc, const char* argv[])
{
    /// Create the stream to the local terminal 
    /// (This stream is not forwarded to remote clients)
    std::ostream local_stream(std::cout.rdbuf());
    local_stream << "[ Holometer Data Acquisition System  v" << JSTREAM_VERSION << " ]\n";
    
#if JSTREAM_USES_WIN
    /// Elevate the Windows priority of this process 
    SetPriorityClass(
        GetCurrentProcess(), 
        REALTIME_PRIORITY_CLASS
    );

    /// Prevent Windows from launching a dialog box after a crash
    DWORD dwMode = SetErrorMode(SEM_NOGPFAULTERRORBOX);
    SetErrorMode(dwMode | SEM_NOGPFAULTERRORBOX);
#endif //~JSTREAM_USES_WIN

    /// Create the topology manager
    /// (Loads the advanced JSON configuration and sets the thread affinities)
    JStreamTopologyController controller(
        argc, 
        argv, 
        logger, 
        local_stream
    );

    /// Bind the main thread
    auto loc_manager = localization_manager();
    auto thread_locality_main = loc_manager->thread_locality_get(logger.context());
    thread_locality_main.thread_locality_apply();

    /// Activate network modules
    controller.networking_state_set(true);

    /// Block the main thread until the DAQ controller shuts down
    controller.wait_for_shutdown();

    /// Stop network modules
    controller.networking_state_set(false);
    return 0;
}

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :