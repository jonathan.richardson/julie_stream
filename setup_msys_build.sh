#!/usr/bin/sh

if [ ! $(gcc --version | grep -o 4.7) ] && [ ! $(gcc --version | grep -o 4.8) ] 
then
echo 'Must use GCC version >=4.7'
exit 2
fi

mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE:STRING=RelWithDebInfo -G "MSYS Makefiles" ..

echo "Now attempting to make"
make

