#!/usr/bin/sh

# Keep restarting the DAQ code until it exits with status code 0 
# (i.e., a controlled shutdown)
status=-1
while [ $status != 0 ]
do
    echo -e ">>> Auto-starting\n"
    julie_stream.exe
    status=$?
    echo -e "\n>>> Exit status: $status\n"
done