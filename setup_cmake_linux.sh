py2_interp=`python2 -c "import os,sys; print(os.path.realpath(sys.executable))"`
py2_inc="${py2_interp/\/bin\///include/}"
py2_lib=`ldd ${py2_interp} | grep python | cut -d " " -f 3 | sed 's/\(.*.so\).*/\1/'`

cmake \
    -DPYTHON_EXECUTABLE:PATH="${py2_interp}" \
    -DPYTHON_INCLUDE_DIR:PATH="${py2_inc}" \
    -DPYTHON_LIBRARY:FILEPATH="${py2_lib}" \
    $@
