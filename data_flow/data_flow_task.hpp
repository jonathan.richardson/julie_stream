////
///
///

#ifndef H_DATA_FLOW_TASK_H
#define H_DATA_FLOW_TASK_H

#include <thread>
#include <mutex>
#include <atomic>
#include <memory>

#include "data_flow.hpp"

namespace JStream {

/////
///
///  Virtual Inheritance of BackgroundTask is needed in order to have the 
///  Subclasses able to work as multiple DataFlowTask's at once. (Avoids the
///  dreaded multiple inheritance diamond problem that afflicts C++).
template<typename TCirculating>
class DataFlowTask : public virtual IDataFlowNode<TCirculating>, 
                     public virtual BackgroundTask
{
    typedef IDataFlowNode<TCirculating> IDataFlowSuper;
    typedef typename IDataFlowSuper::TCirculationQueuePtr TCirculationQueuePtr;
	
  private:
   TCirculationQueuePtr m_input_queue;
   TCirculationQueuePtr m_output_queue;

  public:
    DataFlowTask(const string &thread_name);
    DataFlowTask(const SDT::Context &requested_context);
    DataFlowTask(const DataFlowTask &) = delete;
    DataFlowTask &operator=(const DataFlowTask &) = delete;

    virtual ~DataFlowTask();

    virtual void queue_input_set (const TCirculationQueuePtr &);
    virtual void queue_output_set(const TCirculationQueuePtr &);

  protected:
    TCirculationQueuePtr queue_input_get()  {return m_input_queue; }
    TCirculationQueuePtr queue_output_get() {return m_output_queue; }

    /////
    ///  Fast helper for subclasses needing to determine if they should quit
    inline bool should_quit()
    {return m_input_queue->deny_wait_status(); }
};


}; //~namespace JStream

#include "data_flow_task_T.hpp"
#endif //H_DATA_FLOW_TASK_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

