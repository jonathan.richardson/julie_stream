////
///
///

#ifndef H_DATA_FLOW_VENTILATOR_H
#define H_DATA_FLOW_VENTILATOR_H

#include <thread>
#include <mutex>
#include <atomic>
#include <memory>

#include "background_task.hpp"
#include "data_flow.hpp"

namespace JStream {

/////
///  DataFlow class which ventilates data to worker-peers.
///  This allows the creation of process pools
///
///  public interface to this class is NOT threadsafe
template<typename TCirculating>
class DataFlowVentilator : public virtual IDataFlowNode<TCirculating>,
                           public virtual IBackgroundTask
{
    typedef IDataFlowNode<TCirculating> IDataFlowSuper;
    typedef typename IDataFlowSuper::TCirculationQueuePtr TCirculationQueuePtr;

    typedef shared_ptr< IDataFlowNode<TCirculating> > TNodePtr;

    typedef std::mutex              Mutex;
    typedef std::lock_guard<Mutex>  Guard;

    bool                    m_is_joinable = false;
    Mutex                   m_state_protect;

    std::vector< TNodePtr > m_flow_peers;

    TCirculationQueuePtr m_input_queue;
    TCirculationQueuePtr m_output_queue;

public:
    virtual ~DataFlowVentilator(){}

    virtual void queue_input_set (const TCirculationQueuePtr &);
    virtual void queue_output_set(const TCirculationQueuePtr &);

    void add_peer_node(TNodePtr node);

    /////
    ///  Start computation. May be called while computation is in progress
    ///  returns true if thread was started, false if it was already running.
    virtual bool computing_start();

    virtual void computing_end();

    virtual bool computing_is() const;
};

}; //~namespace JStream

#include "data_flow_ventilator_T.hpp"
#endif //H_DATA_FLOW_VENTILATOR_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

