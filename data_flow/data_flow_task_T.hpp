////
///
///

#ifndef H_JSTREAM_DATA_FLOW_TASK_IMPL_H
#define H_JSTREAM_DATA_FLOW_TASK_IMPL_H

namespace JStream {

/////
///  Constructor
template<typename TCirculating>
DataFlowTask<TCirculating>::
    DataFlowTask(const string &thread_name) :
		BackgroundTask(thread_name)
{
}

/////
///  Constructor overload
///  (takes context rather than thread name)
template<typename TCirculating>
DataFlowTask<TCirculating>::
    DataFlowTask(const SDT::Context &requested_context) :
		BackgroundTask(requested_context)
{
}

/////
///  Destructor
template<typename TCirculating>
DataFlowTask<TCirculating>::
    ~DataFlowTask()
{
}

template<typename TCirculating>
void DataFlowTask<TCirculating>::
    queue_input_set(const TCirculationQueuePtr &queue)
{
    if(this->computing_is()){
        throw std::logic_error("Cannot change the queues of a live DataFlowTask");
    }
    m_input_queue = queue;
}

template<typename TCirculating>
void DataFlowTask<TCirculating>::
    queue_output_set(const TCirculationQueuePtr &queue)
{
    if(this->computing_is()){
        throw std::logic_error("Cannot change the queues of a live DataFlowTask");
    }
    m_output_queue = queue;
}

}; //~namespace JStream

#endif //H_JSTREAM_DATA_FLOW_TASK_IMPL_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

