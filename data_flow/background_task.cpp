////
///
///
///
#include <stdexcept>
#include <iostream>

#include "data_flow/data_flow.hpp"

#include "hardware_locality/localization_manager.hpp"

namespace JStream {

/////
///  Constructor
BackgroundTask::
    BackgroundTask(const string &thread_name) :
        m_num_excecutions(0),
        m_thread_name(thread_name),
        m_cancel_interrupt(false),
        m_compute_thread()
{
    auto loc_manager = localization_manager();
    m_thread_locality = loc_manager->thread_locality_get(m_thread_name);
    m_memory_locality = loc_manager->memory_locality_get(m_thread_name);
}

/////
///  Constructor overload
///  (takes context rather than thread name)
BackgroundTask::
    BackgroundTask(const SDT::Context &requested_context) :
        m_num_excecutions(0),
        m_thread_name(requested_context.full_name_dotted()),
        m_cancel_interrupt(false),
        m_compute_thread()
{
    auto loc_manager = localization_manager();
    m_thread_locality = loc_manager->thread_locality_get(requested_context);
    m_memory_locality = loc_manager->memory_locality_get(requested_context);
}

/////
///  Destructor
BackgroundTask::
    ~BackgroundTask()
{
}

/////
///  Return the task's hardware locality assignment
SDT::MemoryLocalityAccess BackgroundTask::
	locality_get() const
{
    return m_memory_locality; 
}

/////
///  Return the task's thread name
const string &BackgroundTask::
	thread_name_get() const
{
    return m_thread_name; 
}

/////
///  Launch the compute loop in a background thread
bool BackgroundTask::
    computing_start()
{
    if(not m_compute_thread.joinable()){
        Guard state_guard(m_state_protect);
        m_cancel_interrupt.store(false);
        m_num_excecutions += 1;
        m_compute_thread = std::thread( std::bind(&BackgroundTask::_compute_loop_start, this));
    }
}

/////
///  Wait for the background thread to terminate
void BackgroundTask::
    computing_end()
{
    bool prev = m_cancel_interrupt.exchange(true);
    if(prev == false){
        m_compute_thread.join();
    }
}

/////
///  Returns true if the compute thread is currently running
bool BackgroundTask::
    computing_is() const
{
    ///Not sure if this is the correct operation for this...
    return m_compute_thread.joinable();
}

/////
///  Launches the compute loop
void BackgroundTask::
    _compute_loop_start()
{
    /// Bind the thread
	m_thread_locality.thread_locality_apply();

    /// Start the compute loop
    this->compute_loop();
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

