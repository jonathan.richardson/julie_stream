////
///
///

#ifndef H_JSTREAM_DATA_FLOW_VENT_IMPL_H
#define H_JSTREAM_DATA_FLOW_VENT_IMPL_H

#include "data_flow_ventilator.hpp"

namespace JStream {

template<typename TCirculating>
void DataFlowVentilator<TCirculating>::
    queue_input_set(const TCirculationQueuePtr &queue)
{
    Guard state_guard(m_state_protect);
    if(this->computing_is()){
        throw std::logic_error("Cannot change the queues of a live DataFlowTask");
    }
    m_input_queue = queue;
    for(auto &node_peer_ptr : m_flow_peers){
        node_peer_ptr->queue_input_set(m_input_queue);
    }
}

template<typename TCirculating>
void DataFlowVentilator<TCirculating>::
    queue_output_set(const TCirculationQueuePtr &queue)
{
    Guard state_guard(m_state_protect);
    if(this->computing_is()){
        throw std::logic_error("Cannot change the queues of a live DataFlowTask");
    }
    m_output_queue = queue;
    for(auto &node_peer_ptr : m_flow_peers){
        node_peer_ptr->queue_output_set(m_output_queue);
    }
}

template<typename TCirculating>
void DataFlowVentilator<TCirculating>::
    add_peer_node(TNodePtr node)
{
    Guard state_guard(m_state_protect);

    m_flow_peers.push_back(node);

    node->queue_input_set(m_input_queue);
    node->queue_output_set(m_output_queue);
    if(this->computing_is()) {
        node->computing_start();
    }
}

/////
///  Start computation. May be called while computation is in progress
///  returns true if thread was started, false if it was already running.
template<typename TCirculating>
bool DataFlowVentilator<TCirculating>::
    computing_start()
{
    Guard state_guard(m_state_protect);
    for(auto &node_peer_ptr : m_flow_peers) {
        node_peer_ptr->computing_start();
    }
    m_is_joinable = true;
}

template<typename TCirculating>
void DataFlowVentilator<TCirculating>::
    computing_end()
{
    Guard state_guard(m_state_protect);
    m_is_joinable = false;
    for(auto &node_peer_ptr : m_flow_peers){
        node_peer_ptr->computing_end();
    }
}

template<typename TCirculating>
bool DataFlowVentilator<TCirculating>::
    computing_is() const
{
    return m_is_joinable;
}

}; //~namespace JStream

#endif //H_JSTREAM_DATA_FLOW_VENT_IMPL_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

