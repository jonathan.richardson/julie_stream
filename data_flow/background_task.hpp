////
///
///

#ifndef H_JSTREAM_BACKGROUND_TASK_H
#define H_JSTREAM_BACKGROUND_TASK_H

#include <thread>
#include <mutex>
#include <atomic>
#include <memory>
#include <string>

#include "hardware_locality/hwloc.hpp"

namespace JStream {

using std::shared_ptr;
using std::string;

class IBackgroundTask
{
  public:
    virtual ~IBackgroundTask() {}

    /////
    ///  Start computation. May be called while computation is in progress.
    ///  Returns true if thread was started, false if it was already running.
    virtual bool computing_start() = 0;
    virtual void computing_end() = 0;
    virtual bool computing_is() const = 0;
};

class BackgroundTask : public 
                       virtual IBackgroundTask
{
    typedef std::mutex              Mutex;
    typedef std::lock_guard<Mutex>  Guard;

    Mutex             m_state_protect;
    std::thread       m_compute_thread;

    std::atomic<bool> m_cancel_interrupt;

    int               m_num_excecutions;

    string               m_thread_name;
    SDT::ThreadLocalityAccess m_thread_locality;
    SDT::MemoryLocalityAccess m_memory_locality;

  public:
    BackgroundTask(const string &thread_name);
    BackgroundTask(const SDT::Context &requested_context);
    BackgroundTask(const BackgroundTask &) = delete;
    BackgroundTask &operator=(const BackgroundTask &) = delete;

    virtual ~BackgroundTask();

    /////
    ///  Start computation. May be called while computation is in progress.
    ///  Returns true if thread was started, false if it was already running.
    virtual bool computing_start();
    virtual void computing_end();
    virtual bool computing_is() const;

    SDT::MemoryLocalityAccess locality_get() const;
    const string &thread_name_get() const;

  protected:
    /////
    ///  Subclasses override this 
    virtual void compute_loop() = 0;

    /////
    ///  Indicates that a thread is waiting to join. Can/should be used in the
    ///  compute_loop as a signal to quit.
    inline bool join_waiting()
    {
        return m_cancel_interrupt.load(); 
    }

    /////
    ///  Helper method for subclasses needing to coordinate state across threads
    Mutex &state_protector()
    {
        return m_state_protect; 
    }

  private:
    void _compute_loop_start();
};


}; //~namespace JStream

#endif //H_JSTREAM_BACKGROUND_TASK_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

