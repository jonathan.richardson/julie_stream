////
///
///
#ifndef H_JSTREAM_DATA_FLOW_IMPL_H
#define H_JSTREAM_DATA_FLOW_IMPL_H

#include "utilities/range.hpp"
#include <iostream>

namespace JStream {

/////
///  Constructor
template<typename TCirculating>
DataFlow<TCirculating>::
    DataFlow(
		size_t diagram_size, 
		bool circular
	) : 
        m_is_circular(circular),
        m_flow_diagram(diagram_size)
{
	/// Set the number of nodes in the flow
    int queue_size = diagram_size;
    if(not m_is_circular) {
        queue_size += 1; 
    }

    m_queue_chain.reserve(queue_size);
    for(auto idx : icount(queue_size)) {
        m_queue_chain.push_back( std::make_shared<TCirculationQueue>() );
    }

}

/////
///  Constructor
template<typename TCirculating>
DataFlow<TCirculating>::
    DataFlow(
		std::vector<TNodePtr> flows, 
		bool circular
	) : 
    DataFlow(flows.size(), circular)
{
    for(auto idx : icount(flows.size())) {
        this->flow_node_set(idx, flows[idx]);
    }
}

/////
///  Destructor
template<typename TCirculating>
DataFlow<TCirculating>::
    ~DataFlow()
{
}

/////
///  Set a new node in the data flow queue
template<typename TCirculating>
void DataFlow<TCirculating>::
    flow_node_set(size_t location, TNodePtr flow_node)
{
    Guard state_guard(m_state_protect);

    if(m_is_joinable) {
		throw std::runtime_error("Can not set flow node with a live flow"); 
	}
    if(location < 0) {
		throw std::runtime_error("Array out of bounds"); 
	}
    if(location >= m_flow_diagram.size()) {
		throw std::runtime_error("Array out of bounds"); 
	}

    /// Indexes into the flow diagram to get the inner vector, the node is inserted into it
    m_flow_diagram[location] = flow_node;
    if(     (not m_is_circular) 
        or  (location != m_flow_diagram.size() - 1)
        ) {
        flow_node->queue_input_set(m_queue_chain[location]);
        flow_node->queue_output_set(m_queue_chain[location + 1]);
    } else {
        flow_node->queue_input_set(m_queue_chain[location]);
        flow_node->queue_output_set(m_queue_chain[0]);
    }
}

/////
///  Start the data flow between the nodes
template<typename TCirculating>
void DataFlow<TCirculating>::
    flow_tasks_start()
{
    Guard state_guard(m_state_protect);
	
    /// Start computation in reverse so that the later elements are
    /// ready and waiting on the earlier elements
    for(auto idx : icount(m_flow_diagram.size())){
        auto b_idx = m_flow_diagram.size() - idx - 1;
        if(not m_flow_diagram[b_idx]){
            throw(std::logic_error("Not all flow objects were set, preventing crash"));
        }
        m_flow_diagram[b_idx]->computing_start();
    }
}

/////
///  Shut down the data flow
template<typename TCirculating>
void DataFlow<TCirculating>::
    flow_tasks_end()
{
    Guard state_guard(m_state_protect);
    ///////
    ///  TODO The queues need another wait method to know when they are empty, this
    ///  will allow the computing_end section to know when to end so as to leave all of the 

    /// End computation in order so that earlier elements stop feeding later elements
    for(auto idx : icount(m_flow_diagram.size())) {
        /// Prevent the workers from being able to wait on their queues
        m_queue_chain[idx]->deny_wait_set();
        m_flow_diagram[idx]->computing_end();
    }

    /// Allow the queues to fill again
    for(auto &queueptr : m_queue_chain) {
        queueptr->deny_wait_clear();
    }
}

/////
///  Return the node reference at the front of the queue
template<typename TCirculating>
typename DataFlow<TCirculating>::TCirculationQueuePtr DataFlow<TCirculating>::
    start_queue_get()
{return m_queue_chain.front(); }

/////
///  Return the node reference at the end of the queue
template<typename TCirculating>
typename DataFlow<TCirculating>::TCirculationQueuePtr DataFlow<TCirculating>::
    end_queue_get()
{
    if(m_is_circular){
        throw(std::logic_error("No end queue when flow is circular"));
    }
    return m_queue_chain.back();
}

}; //~namespace JStream

#endif //H_JSTREAM_DATA_FLOW_IMPL_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

