////
///
//
#ifndef H_JSTREAM_DATA_FLOW_H
#define H_JSTREAM_DATA_FLOW_H

#include <thread>
#include <mutex>
#include <atomic>
#include <memory>
#include <vector>

#include "hardware_locality/queue.hpp"
#include "background_task.hpp"

namespace JStream {

using std::shared_ptr;

/////
///
///  Virtual Inheritance of IBackgroundTask is needed in order to have the 
///  Subclasses able to work as multiple DataFlowTask's at once. (Avoids the
///  dreaded multiple inheritance diamond problem that afflicts C++).
template<typename TCirculating>
class IDataFlowNode : public virtual IBackgroundTask
{
  public:
    typedef WaitingQueuePage<TCirculating>  TCirculationQueue;
    typedef shared_ptr<TCirculationQueue>  	TCirculationQueuePtr;

    virtual ~IDataFlowNode() {}

    virtual void queue_input_set (const TCirculationQueuePtr &) = 0;
    virtual void queue_output_set(const TCirculationQueuePtr &) = 0;
};

/////
///  Public interface to this class is NOT threadsafe
template<typename TCirculating>
class DataFlow //: public virtual IBackgroundTask //not clear that this should be task-like
{
    typedef WaitingQueuePage<TCirculating>     			TCirculationQueue;
    typedef shared_ptr<TCirculationQueue>  				TCirculationQueuePtr;
    typedef shared_ptr< IDataFlowNode<TCirculating> > 	TNodePtr;

    typedef std::mutex              Mutex;
    typedef std::lock_guard<Mutex>  Guard;

    bool                    m_is_joinable = false;
    Mutex                   m_state_protect;

    std::vector<TCirculationQueuePtr>      m_queue_chain;
    std::vector< TNodePtr >                m_flow_diagram;
    const bool                             m_is_circular;

  public:
    DataFlow(size_t diagram_size,   bool circular = false);
    DataFlow(std::vector<TNodePtr>, bool circular = false);

    DataFlow(const DataFlow &) = delete;
    DataFlow &operator=(const DataFlow &) = delete;

    virtual ~DataFlow();

    void flow_node_set(size_t location, TNodePtr);

    void flow_tasks_start();
    void flow_tasks_end();

    TCirculationQueuePtr start_queue_get();
    TCirculationQueuePtr end_queue_get();
	
  protected:
};

}; //~namespace JStream

#include "data_flow_T.hpp"
#endif //H_JSTREAM_DATA_FLOW_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

