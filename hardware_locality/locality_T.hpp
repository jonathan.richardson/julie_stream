/////
///
///

#ifndef H_SDT_LOCALITY_T_H
#define H_SDT_LOCALITY_T_H

namespace SDT {

ThreadLocalityAccess::
    ThreadLocalityAccess() :
        m_locality_impl()
{
}

ThreadLocalityAccess::
    ThreadLocalityAccess(std::shared_ptr<IThreadLocalityImpl> locality) :
        m_locality_impl(locality)
{
}

ThreadLocalityAccess::
    ThreadLocalityAccess(const ThreadLocalityAccess &other) :
        m_locality_impl(other.m_locality_impl)
{
}

ThreadLocalityAccess &ThreadLocalityAccess::
    operator=(const ThreadLocalityAccess &other)
{
    m_locality_impl = other.m_locality_impl;
}

ThreadLocalityAccess::
    ThreadLocalityAccess(ThreadLocalityAccess &&other):
        m_locality_impl(std::move(other.m_locality_impl))
{
}

void ThreadLocalityAccess::
    thread_locality_apply()
{
    return m_locality_impl->thread_locality_apply();
}

MemoryLocalityAccess::
    MemoryLocalityAccess() :
        m_locality_impl()
{
}

MemoryLocalityAccess::
    MemoryLocalityAccess(std::shared_ptr<IMemoryLocalityImpl> locality) :
        m_locality_impl(locality)
{
}

MemoryLocalityAccess::
    MemoryLocalityAccess(const MemoryLocalityAccess &other) :
        m_locality_impl(other.m_locality_impl)
{
}

MemoryLocalityAccess &MemoryLocalityAccess::
    operator=(const MemoryLocalityAccess &other)
{
    m_locality_impl = other.m_locality_impl;
}

MemoryLocalityAccess::
    MemoryLocalityAccess(MemoryLocalityAccess &&other):
        m_locality_impl(std::move(other.m_locality_impl))
{
}

void *MemoryLocalityAccess::
    allocate(size_t size)
{
    return m_locality_impl->allocate(size);
}

void *MemoryLocalityAccess::
    allocate(size_t size, LoggerAccess logger)
{
    return m_locality_impl->allocate(size, logger);
}

void MemoryLocalityAccess::
    deallocate(void *mem, unsigned size)
{
    return m_locality_impl->deallocate(mem, size);
}

template<typename TType>
NewObjectFunctor<TType> MemoryLocalityAccess::
    new_object_logging(LoggerAccess logger)
{
    return NewObjectFunctor<TType>(*this, logger);
}

template<typename TType, typename ... TArgs>
std::unique_ptr<TType, LocalizedObjectDeleter<TType> > MemoryLocalityAccess::
    new_object(TArgs &&... args)
{
    void *mem = this->allocate(sizeof(TType));
    new (mem) TType(args...);
    return std::unique_ptr<TType, LocalizedArrayDeleter<TType> >(
            static_cast<TType *>(mem), LocalizedObjectDeleter<TType>(*this)
            );
}

template<typename TType>
NewArrayFunctor<TType> MemoryLocalityAccess::
    new_array_logging(LoggerAccess logger)
{
    return NewArrayFunctor<TType>(*this, logger);
}

template<typename TType>
std::unique_ptr<TType, LocalizedArrayDeleter<TType> > MemoryLocalityAccess::
    new_array(unsigned num)
{
    void *mem = this->allocate(num * sizeof(TType));
    new (mem) TType[num];
    return std::unique_ptr<TType, LocalizedArrayDeleter<TType> >(
            static_cast<TType *>(mem), LocalizedArrayDeleter<TType>(*this, num)
            );
}

template<typename TType>
NewObjectFunctor<TType>::
    NewObjectFunctor(MemoryLocalityAccess &loc, LoggerAccess logger) : 
        m_locality(loc),
        m_logger(logger)
{
}


template<typename TType>
template<typename... TArgs>
std::unique_ptr<TType, LocalizedObjectDeleter<TType> > NewObjectFunctor<TType>::
    operator()(TArgs... args)
{
    void *mem = m_locality.allocate(sizeof(TType), m_logger);
    new (mem) TType(args...);
    return std::unique_ptr<TType, LocalizedArrayDeleter<TType> >(
            static_cast<TType *>(mem), LocalizedObjectDeleter<TType>(m_locality)
            );
}

template<typename TType>
NewArrayFunctor<TType>::
    NewArrayFunctor(MemoryLocalityAccess &loc, LoggerAccess logger) : 
        m_locality(loc),
        m_logger(logger)
{
}

template<typename TType>
std::unique_ptr<TType, LocalizedArrayDeleter<TType> > NewArrayFunctor<TType>::
    operator[](size_t num)
{
    void *mem = m_locality.allocate(num * sizeof(TType), m_logger);
    new (mem) TType[num];
    return std::unique_ptr<TType, LocalizedArrayDeleter<TType> >(
            static_cast<TType *>(mem), LocalizedArrayDeleter<TType>(m_locality, num)
            );
}

template<typename TType>
LocalizedObjectDeleter<TType>::
    LocalizedObjectDeleter(MemoryLocalityAccess locality) :
        m_locality(locality)
{
}

template<typename TType>
LocalizedObjectDeleter<TType>::
    LocalizedObjectDeleter(const LocalizedObjectDeleter &other) :
        m_locality(other.m_locality)
{
}

template<typename TType>
LocalizedObjectDeleter<TType>::
    LocalizedObjectDeleter(LocalizedObjectDeleter &&other) :
        m_locality(std::move(other.m_locality))
{
}

template<typename TType>
void LocalizedObjectDeleter<TType>::
    operator()(void *mem)
{
    ((TType *)mem)->~TType();
    m_locality.deallocate(mem, sizeof(TType));
}

template<typename TType>
LocalizedArrayDeleter<TType>::
    LocalizedArrayDeleter(MemoryLocalityAccess locality, unsigned num) :
        m_locality(locality),
        m_array_size(num)
{
}

template<typename TType>
LocalizedArrayDeleter<TType>::
    LocalizedArrayDeleter(const LocalizedArrayDeleter &other) :
        m_locality(other.m_locality),
        m_array_size(other.m_array_size)
{
}

template<typename TType>
LocalizedArrayDeleter<TType>::
    LocalizedArrayDeleter(LocalizedArrayDeleter &&other) :
        m_locality(std::move(other.m_locality)),
        m_array_size(other.m_array_size)
{
}

template<typename TType>
void LocalizedArrayDeleter<TType>::
    operator()(void *mem)
{
    for(unsigned idx = 0; idx < m_array_size; idx += 1){
        ((TType *)mem)[idx].~TType();
    }
    m_locality.deallocate(mem, m_array_size * sizeof(TType));
}

}; //~namespace SDT 

#endif //H_SDT_LOCALITY_T_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
