////
///
//

#include <vector>
#include <stdexcept>
#include <iostream>
#include <thread>

#include "os_utilities.hpp"
#include "range.hpp"

#ifdef _POSIXLIKE_

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif //_GNU_SOURCE
#include <pthread.h>

#endif //_POSIXLIKE_

#ifdef _WIN32

#include <windef.h>
#include <windows.h>
#include <WinBase.h>

#endif //_WIN32

namespace JStream {

#ifdef _POSIXLIKE_
void set_affinity(const TAffinityVector &bitset, const pthread_t &thread);

void set_affinity(const TAffinityVector &bitset)
{
    set_affinity(bitset, pthread_self());
    return;
}

void set_affinity(const TAffinityVector &bitset, std::thread &affect)
{
    set_affinity(bitset, affect.native_handle());
    return;
}

void set_affinity(const TAffinityVector &bitset, const pthread_t &thread)
{
    int n_cpus = bitset.size();
    cpu_set_t *cpumask = CPU_ALLOC(n_cpus);
    CPU_ZERO_S(n_cpus, cpumask);
    for( auto idx : icount(bitset.size())){
        if(bitset[idx]){
            CPU_SET_S(idx, n_cpus, cpumask);
        }
    }
    auto retval = pthread_setaffinity_np(thread, n_cpus, cpumask);
    if(retval == -1){
    switch(errno){
        case EFAULT:
            std::cout << "AFFINITY ERROR: A supplied memory address was invalid." << std::endl;
            break;
        case EINVAL:
            std::cout << "AFFINITY ERROR: The affinity bit mask mask contains no processors that are "
            "currently physically on the system and permitted to  the  process according to any restrictions "
            "that may be imposed by the \"cpuset\" mechanism described in cpuset(7)." << std::endl;
            break;
       case EPERM:
            std::cout << "AFFINITY ERROR: (sched_setaffinity()) The calling process does not have appropriate privileges. "
                "The caller needs an  effective  user  ID equal  to the user ID or effective user ID of the process "
                "identified by pid, or it must possess the CAP_SYS_NICE capability." << std::endl;
            break;
        case ESRCH:
            std::cout << "AFFINITY ERROR: The process whose ID is pid could not be found." << std::endl;
            break;
        default:
            std::cout << "AFFINITY ERROR: unknown error" << std::endl;
            return;
       }
   }

    CPU_FREE(cpumask);
}
 
#endif //_POSIXLIKE_

#ifdef _WIN32

void set_affinity(const TAffinityVector &bitset,const HANDLE &thread)
{
    int n_cpus = bitset.size();
	uint64_t mask=0;
    for( auto idx : icount(bitset.size())){
        if(bitset[idx]){
		mask |= uint64_t(1) << idx;
        }
    }

    auto retval = SetThreadAffinityMask(thread, mask);
	char msg[300];
    if(retval == 0){
	auto err = GetLastError();
		FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM,
		0, err, 0, msg, sizeof(msg), 0 );
	    std::cout << "AFFINITY ERROR: "<< msg << std::endl;
	    return;
   }

}

void set_affinity(const TAffinityVector &bitset)
{
    set_affinity(bitset, GetCurrentThread());
}

/////
///  TODO: don't duplicate code
void set_affinity(const TAffinityVector &bitset, std::thread &thread)
{
    set_affinity(bitset, OpenThread(THREAD_ALL_ACCESS,false,thread.native_handle()));
}

#endif //_WIN32

}; //~namespace JStream
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

