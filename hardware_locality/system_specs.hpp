//////
///TODO: Some of this stuff could be specified by CMake
///

#ifndef H_JSTREAM_SYSTEM_SPECS_H
#define H_JSTREAM_SYSTEM_SPECS_H

namespace JStream {

static constexpr unsigned SIMD_ALIGNMENT = 16;

//////
///   Seems to be 4096 on systems that I have seen, but it could change
static constexpr unsigned PAGE_SIZE = 4096;

}; //~namespace JStream

#endif //H_JSTREAM_SYSTEM_SPECS_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

