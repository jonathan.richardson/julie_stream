////
///
///

#ifndef H_JSTREAM_SIMD_ALIGN_H
#define H_JSTREAM_SIMD_ALIGN_H

#include <cstdint>
#include <cstring>

#include "system_specs.hpp"

namespace JStream {

/////
///  This function returns a size which will ensure that the item following
///  it retains SIMD alignment requirements
inline uintptr_t simd_align_size(uintptr_t start_size)
{
    return start_size + SIMD_ALIGNMENT - (start_size % SIMD_ALIGNMENT);
}

template<typename TPtr>
TPtr simd_round_to_align(TPtr ptr){
    return (TPtr)(uintptr_t(ptr) + uintptr_t((void *)ptr) % SIMD_ALIGNMENT);
}

}; //~namespace JStream

#endif //H_JSTREAM_SIMD_ALIGN_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

