/////
///
///

#include "locality.hpp"

namespace SDT {


void DefaultThreadLocalityImpl::
    thread_locality_apply()
{
    return;
}

void *DefaultMemoryLocalityImpl::
    allocate(size_t size)
{
    return new char[size];
}

void *DefaultMemoryLocalityImpl::
    allocate(size_t size, LoggerAccess logger)
{
    return this->allocate(size);
}

void DefaultMemoryLocalityImpl::
    deallocate(void *mem, unsigned size)
{
    delete[] (char *)mem;
}

ThreadLocalityAccess thread_locality_default()
{
    static std::shared_ptr<IThreadLocalityImpl> thread_locality = std::make_shared<DefaultThreadLocalityImpl>();
    return ThreadLocalityAccess(thread_locality);
}

MemoryLocalityAccess memory_locality_default()
{
    static std::shared_ptr<IMemoryLocalityImpl> memory_locality = std::make_shared<DefaultMemoryLocalityImpl>();
    return MemoryLocalityAccess(memory_locality);
}

}; //~namespace SDT

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
