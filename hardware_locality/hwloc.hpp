////
///

#ifndef H_JSTREAM_HWLOC_H
#define H_JSTREAM_HWLOC_H

#include <memory>
#include <string>

#include <sdt/config.hpp>
#include <sdt/utilities/logger.hpp>

//////
///  hwloc needs the windows headers in windows...
#if SDT_OS_WINDOWS
#include <windef.h>
#include <windows.h>
#include <WinBase.h>
#endif //SDT_OS_WINDOWS

#include <hwloc.h>

#include "utilities/range.hpp"
//#include "utilities/logger.hpp"

#include "hardware_locality/SIMD_align.hpp"

#include "locality.hpp"

namespace JStream {
using namespace uLogger;
using std::shared_ptr;
using std::string;

class HwlocLocality;
class HwlocTopology;
typedef shared_ptr<HwlocTopology> HwlocTopologyPtr;
typedef shared_ptr<HwlocLocality> HwlocLocalityPtr;

class HwlocTopology : public
                      std::enable_shared_from_this<HwlocTopology>
{
    friend class HwlocLocality;
    hwloc_topology_t m_hwloc_topology;

public:
    static shared_ptr<HwlocTopology> detect_system_topology();

    ~HwlocTopology();

    unsigned locality_num();

    shared_ptr<HwlocLocality> locality(unsigned cpu_num);

protected:
    //////
    ///  Must hide the constructor because all topology objects should be constructed inside of 
    ///  shared_ptr, this is necessary because many of the sub-objects require pointers to this
    ///  topology.
    HwlocTopology();

};

class HwlocLocality : public SDT::IThreadLocalityImpl, public SDT::IMemoryLocalityImpl
{

    friend class HwlocTopology;

    shared_ptr<HwlocTopology> m_topology;
    hwloc_cpuset_t m_hwloc_cpuset;
    hwloc_nodeset_t m_hwloc_nodeset;

    static constexpr unsigned s_align_extra = 2*SIMD_ALIGNMENT;

public:
    ~HwlocLocality();

    void thread_locality_apply();

    string membind_area_repr(void *mem, size_t len);
    string repr();
    string repr_node();

    void *allocate(size_t size);
    void *allocate(size_t size, LoggerAccess logger);

    void deallocate(void *mem);
    void deallocate(void *mem, unsigned size);

protected:

    size_t allocated_size(void *mem);

    //////
    ///  assumes that the source was allocated and will need to be free'd later
    HwlocLocality(shared_ptr<HwlocTopology> topology, hwloc_cpuset_t source);
    //HwlocLocality(shared_ptr<HwlocTopology> topology, hwloc_nodeset_t source);
};

}; //~namespace JStream

#endif //H_JSTREAM_HWLOC_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
