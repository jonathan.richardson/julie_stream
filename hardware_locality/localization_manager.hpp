/////
///
///

#ifndef H_JSTREAM_LOCALIZATION_MANAGER_H
#define H_JSTREAM_LOCALIZATION_MANAGER_H

#include <unordered_map>
#include <string>

#include <sdt/utilities/logger.hpp>

#include "sdt/utilities/config_tree/config_tree.hpp"
#include "hwloc.hpp"
#include "locality.hpp"

namespace JStream {
using std::string;

class LocalizationManager;
typedef shared_ptr<LocalizationManager> LocalizationManagerPtr;

//////
///  global access to the LocalizationManager;
LocalizationManagerPtr localization_manager();

class LocalizationManager
{
    HwlocTopologyPtr m_topology;
    std::unordered_map<string, HwlocLocalityPtr> m_thread_mapping;
    HwlocLocalityPtr m_default_locality;

public:

    LocalizationManager();
    ~LocalizationManager();

	void configurations_import(ConfigTree::ConfigTreePtr config, LoggerAccess logger);

    HwlocTopologyPtr topology_get() const;

    HwlocLocalityPtr named_locality_get(const string &thread_name);
    bool named_locality_set(const string &thread_name, unsigned cpu_num);

    SDT::ThreadLocalityAccess thread_locality_get(const string &thread_name);
    SDT::ThreadLocalityAccess thread_locality_get(const SDT::Context &requested_context);
    SDT::MemoryLocalityAccess memory_locality_get(const string &resource_name);
    SDT::MemoryLocalityAccess memory_locality_get(const SDT::Context &requested_context);
};

}; //~namespace JStream

#endif //H_JSTREAM_LOCALIZATION_MANAGER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
