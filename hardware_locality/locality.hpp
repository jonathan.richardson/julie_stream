/////
///
///

#ifndef H_SDT_LOCALITY_H
#define H_SDT_LOCALITY_H

#include "jstream/config.hpp"
#include <memory>
#include <sdt/utilities/logger.hpp>

namespace SDT {

class IThreadLocalityImpl;
class IMemoryLocalityImpl;

template<typename TType>
class NewObjectFunctor;

template<typename TType>
class NewArrayFunctor;

template<typename TType>
class LocalizedObjectDeleter;

template<typename TType>
class LocalizedArrayDeleter;

class ThreadLocalityAccess
{
    std::shared_ptr<IThreadLocalityImpl> m_locality_impl;

public:
    inline ThreadLocalityAccess();
    inline ThreadLocalityAccess(std::shared_ptr<IThreadLocalityImpl>);
    inline ThreadLocalityAccess(const ThreadLocalityAccess &);
    inline ThreadLocalityAccess &operator=(const ThreadLocalityAccess &);
    inline ThreadLocalityAccess(ThreadLocalityAccess &&);

    inline void thread_locality_apply();
};

class MemoryLocalityAccess
{
    std::shared_ptr<IMemoryLocalityImpl> m_locality_impl;

public:
    inline MemoryLocalityAccess();
    inline MemoryLocalityAccess(std::shared_ptr<IMemoryLocalityImpl>);
    inline MemoryLocalityAccess(const MemoryLocalityAccess &);
    inline MemoryLocalityAccess &operator=(const MemoryLocalityAccess &);
    inline MemoryLocalityAccess(MemoryLocalityAccess &&);

    inline void *allocate(size_t size);
    inline void *allocate(size_t size, LoggerAccess logger);

    inline void deallocate(void *mem, unsigned size);

    template<typename TType>
    inline NewObjectFunctor<TType> new_object_logging(LoggerAccess logger);

    template<typename TType, typename ... TArgs>
    inline std::unique_ptr<TType, LocalizedObjectDeleter<TType> > new_object(TArgs &&... args);

    template<typename TType>
    inline NewArrayFunctor<TType> new_array_logging(LoggerAccess logger);

    template<typename TType>
    inline std::unique_ptr<TType, LocalizedArrayDeleter<TType> > new_array(unsigned num);
};

template<typename TType>
class NewObjectFunctor
{
    friend class MemoryLocalityAccess;

    MemoryLocalityAccess &m_locality;
    LoggerAccess m_logger;

    inline NewObjectFunctor(MemoryLocalityAccess &loc, LoggerAccess logger);
    inline NewObjectFunctor(const NewObjectFunctor&) = delete;
public:

    template<typename... TArgs>
    inline std::unique_ptr<TType, LocalizedObjectDeleter<TType> > operator()(TArgs... args);
};

template<typename TType>
class NewArrayFunctor
{
    friend class MemoryLocalityAccess;

    MemoryLocalityAccess &m_locality;
    LoggerAccess m_logger;

    inline NewArrayFunctor(MemoryLocalityAccess &loc, LoggerAccess logger);
    inline NewArrayFunctor(const NewArrayFunctor&) = delete;
public:

    inline std::unique_ptr<TType, LocalizedArrayDeleter<TType> > operator[](size_t num);
};

template<typename TType>
class LocalizedObjectDeleter
{
    friend class MemoryLocalityAccess;
    friend class NewObjectFunctor<TType>;

    MemoryLocalityAccess m_locality;

    inline LocalizedObjectDeleter(MemoryLocalityAccess locality);
    inline LocalizedObjectDeleter(const LocalizedObjectDeleter &other);
public:
    inline LocalizedObjectDeleter(LocalizedObjectDeleter &&other);
    inline void operator()(void *mem);
};

template<typename TType>
class LocalizedArrayDeleter
{
    friend class MemoryLocalityAccess;
    friend class NewArrayFunctor<TType>;

    MemoryLocalityAccess m_locality;
    unsigned m_array_size;

    inline LocalizedArrayDeleter(MemoryLocalityAccess locality, unsigned array_size);
public:
    inline LocalizedArrayDeleter(const LocalizedArrayDeleter &other);
    inline LocalizedArrayDeleter(LocalizedArrayDeleter &&other);

    inline void operator()(void *mem);
};

class IThreadLocalityImpl
{
    public:
    virtual void thread_locality_apply() = 0;
};

class IMemoryLocalityImpl
{
    public:

    virtual void *allocate(size_t size) = 0;
    virtual void *allocate(size_t size, LoggerAccess logger) = 0;

    virtual void deallocate(void *mem, unsigned size) = 0;
};

class DefaultThreadLocalityImpl : public 
                                  IThreadLocalityImpl
{
    virtual void thread_locality_apply();
};

class DefaultMemoryLocalityImpl : public
                                  IMemoryLocalityImpl
{
    virtual void *allocate(size_t size);
    virtual void *allocate(size_t size, LoggerAccess logger);

    virtual void deallocate(void *mem, unsigned size);
};

ThreadLocalityAccess thread_locality_default();
MemoryLocalityAccess memory_locality_default();

}; //~namespace SDT

#endif //H_SDT_LOCALITY_H
#include "locality_T.hpp"

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
