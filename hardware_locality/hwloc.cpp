////
///
///

#include <stdexcept>
#include <iostream>
#include <sstream>

#include "jstream/config.hpp"
#include "hwloc.hpp"

namespace JStream {
using std::string;

auto logger_hwloc = logger_root().child("hwloc");

string membind_policy_string(hwloc_membind_policy_t policy){
    switch(policy){
        case HWLOC_MEMBIND_DEFAULT:
            return "default";
        case HWLOC_MEMBIND_FIRSTTOUCH:
            return "firsttouch";
        case HWLOC_MEMBIND_BIND:
            return "bind";
        case HWLOC_MEMBIND_INTERLEAVE:
            return "interleave";
        case HWLOC_MEMBIND_REPLICATE:
            return "replicate";
        case HWLOC_MEMBIND_NEXTTOUCH:
            return "nexttouch";
        case HWLOC_MEMBIND_MIXED:
            return "mixed";
        default:
            return "unknown";
    }
}

HwlocTopology::
    HwlocTopology()
{
    auto retval = hwloc_topology_init(&m_hwloc_topology);
    if(retval != 0){
        throw(std::runtime_error("hwloc error"));
    }
    return;
}

shared_ptr<HwlocTopology> HwlocTopology::
    detect_system_topology()
{
    shared_ptr<HwlocTopology> topo(new HwlocTopology());
    auto retval = hwloc_topology_load(topo->m_hwloc_topology);
    if(retval != 0){
        throw(std::runtime_error("hwloc error"));
    }
    return topo;
}

HwlocTopology::
    ~HwlocTopology()
{
    hwloc_topology_destroy(m_hwloc_topology);
}

unsigned HwlocTopology::
    locality_num()
{
    hwloc_const_cpuset_t full_cpuset = hwloc_topology_get_complete_cpuset(m_hwloc_topology);
    if(full_cpuset == NULL){
        throw("Hwloc error");
    }
    return hwloc_bitmap_weight(full_cpuset);
}

shared_ptr<HwlocLocality> HwlocTopology::
    locality(unsigned cpu_num)
{
    assert(cpu_num < this->locality_num());
    hwloc_cpuset_t cpuset = hwloc_bitmap_alloc();
    hwloc_bitmap_only(cpuset, cpu_num);
    return shared_ptr<HwlocLocality>(new HwlocLocality(this->shared_from_this(), cpuset));
}

HwlocLocality::
    HwlocLocality(
        shared_ptr<HwlocTopology> topology, 
        hwloc_cpuset_t source
    ) :
        m_topology(topology)
{
    m_hwloc_cpuset = source;
    m_hwloc_nodeset = hwloc_bitmap_alloc();
    hwloc_cpuset_to_nodeset(m_topology->m_hwloc_topology, m_hwloc_cpuset, m_hwloc_nodeset);
}

HwlocLocality::
    ~HwlocLocality()
{
    hwloc_bitmap_free(m_hwloc_cpuset);
    hwloc_bitmap_free(m_hwloc_nodeset);
}

void HwlocLocality::
    thread_locality_apply()
{
    auto retval = hwloc_set_cpubind(
        m_topology->m_hwloc_topology, 
        m_hwloc_cpuset, 
        HWLOC_CPUBIND_THREAD | HWLOC_CPUBIND_STRICT
    );
    if(retval == -1){
        switch(errno){
            case ENOSYS:
                throw(std::runtime_error("CPU bind not supported by hwloc"));
                break;
            case EXDEV:
                throw(std::runtime_error("CPU bind not guaranteed by hwloc"));
                break;
            default:
                throw(std::runtime_error("unknown Hwloc cpubind error"));
                break;
        }
    }
}

string HwlocLocality::membind_area_repr(void *mem, size_t len)
{
    hwloc_cpuset_t cpuset = hwloc_bitmap_alloc();
    hwloc_membind_policy_t policy;
    auto retval = hwloc_get_area_membind(
            m_topology->m_hwloc_topology, 
            mem, len,
            cpuset,
	        &policy,
            HWLOC_MEMBIND_THREAD
	);

    /////
    ///  This can occur on a non-numa machine
    if(hwloc_bitmap_iszero(cpuset)){
        hwloc_bitmap_copy(cpuset, hwloc_topology_get_complete_cpuset(m_topology->m_hwloc_topology));
    }

    char bitmap_str[256];
    auto written = hwloc_bitmap_list_snprintf(bitmap_str, sizeof(bitmap_str), cpuset);
    std::stringstream SS;
    SS << "<" << string(bitmap_str, written)   << " : "
              << membind_policy_string(policy) << ">";
    hwloc_bitmap_free(cpuset);
    return SS.str();
}

string HwlocLocality::repr()
{
    char bitmap_str[256];
    auto written = hwloc_bitmap_list_snprintf(bitmap_str, sizeof(bitmap_str), m_hwloc_cpuset);
    return string(bitmap_str, written);
}

string HwlocLocality::repr_node()
{
    char bitmap_str[256];
    auto written = hwloc_bitmap_list_snprintf(bitmap_str, sizeof(bitmap_str), m_hwloc_nodeset);
    return string(bitmap_str, written);
}

void *HwlocLocality::
    allocate(size_t size)
{
    return this->allocate(size, logger_hwloc);
}

void *HwlocLocality::
    allocate(size_t size, LoggerAccess logger_mem)
{
    /////
    ///  round to the page size while returning a SIMD aligned size
    size_t truesize = PAGE_SIZE*(1 + ((size + s_align_extra)-1)/PAGE_SIZE);

    void* block_ptr = hwloc_alloc_membind_policy_nodeset(
        m_topology->m_hwloc_topology, 
        truesize,
        m_hwloc_nodeset,
        HWLOC_MEMBIND_BIND,
        HWLOC_MEMBIND_THREAD | HWLOC_MEMBIND_STRICT
    );
    if(block_ptr == NULL){
        logger_mem.warning("WARNING: strict mem binding failed");
        block_ptr = hwloc_alloc_membind_policy_nodeset(
            m_topology->m_hwloc_topology, 
            truesize,
            m_hwloc_nodeset,
            HWLOC_MEMBIND_BIND,
            HWLOC_MEMBIND_THREAD
        );
    }
    if(block_ptr == NULL){
        switch(errno){
            case ENOSYS:
		//*((int *)nullptr) = 911;
                throw(std::runtime_error("MEM bind not supported by hwloc"));
                break;
            case EXDEV:
                throw(std::runtime_error("MEM bind not guaranteed by hwloc"));
                break;
            default:
                throw(std::runtime_error("unknown Hwloc membind error"));
                break;
        }
    }

    logger_mem.detail("Alloc on cpus: [",this->repr(), "] given to: ", this->membind_area_repr(block_ptr, truesize));

    *((size_t *)block_ptr) = size;
	if(block_ptr != simd_round_to_align(block_ptr)){
        logger_mem.warning("memory not aligned!");
	}
	//logger_mem.warning("memory alignment: ", (uintptr_t(mem)) % SIMD_ALIGNMENT);

    return (void *)((char *)block_ptr + s_align_extra);
}

void HwlocLocality::
    deallocate(void *data_ptr)
{
    size_t truesize = PAGE_SIZE*(1 + ((this->allocated_size(data_ptr) + s_align_extra)-1)/PAGE_SIZE);
	auto retval = hwloc_free(m_topology->m_hwloc_topology, (void *)((char *)data_ptr - s_align_extra), truesize);
}

void HwlocLocality::
    deallocate(void *data_ptr, unsigned size)
{
    unsigned allocated_size = this->allocated_size(data_ptr);
    assert(size == allocated_size);
    size_t truesize = PAGE_SIZE*(1 + ((allocated_size + s_align_extra)-1)/PAGE_SIZE);
	auto retval = hwloc_free(m_topology->m_hwloc_topology, (void *)((char *)data_ptr - s_align_extra), truesize);
}

size_t HwlocLocality::
	allocated_size(void *data_ptr)
{
    return *(size_t *)((char *)(data_ptr) - s_align_extra);
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
