////
///
#ifndef H_OS_UTILITIES_H
#define H_OS_UTILITIES_H

#include <vector>
#include <thread>

namespace JStream {

typedef std::vector<bool> TAffinityVector;

void set_affinity(const TAffinityVector &bitset);
void set_affinity(const TAffinityVector &bitset, std::thread &affect);

}; //~namespace JStream

#endif //H_OS_UTILITIES_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

