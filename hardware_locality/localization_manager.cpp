/////
///
///

#include "localization_manager.hpp"

namespace JStream {
using namespace uLogger;
auto logger_localization = logger_root().child("localization_manager");

LocalizationManager::
    LocalizationManager()
{
    m_topology = HwlocTopology::detect_system_topology();
    m_default_locality = m_topology->locality(0);
}

LocalizationManager::
    ~LocalizationManager()
{
}

HwlocTopologyPtr LocalizationManager::
    topology_get() const
{
    return m_topology;
}

HwlocLocalityPtr LocalizationManager::
    named_locality_get(const string &thread_name)
{
    auto iter = m_thread_mapping.find(thread_name);
    if(iter == m_thread_mapping.end()){
        return nullptr;
    }
    return iter->second;
}

bool LocalizationManager::
    named_locality_set(const string &thread_name, unsigned cpu_num)
{
    HwlocLocalityPtr locality = m_topology->locality(cpu_num);
    auto ret_pair = m_thread_mapping.emplace(thread_name, locality);
    return ret_pair.second;
}

LocalizationManagerPtr localization_manager()
{
    static LocalizationManagerPtr permanent_localization = LocalizationManagerPtr(new LocalizationManager);
    return permanent_localization;
}

/////
///  Returns the hardware locality assignment for the supplied thread name
SDT::ThreadLocalityAccess LocalizationManager::
    thread_locality_get(const string &thread_name)
{
	HwlocLocalityPtr hwloc_locality = this->named_locality_get(thread_name);
	if(hwloc_locality){
		return SDT::ThreadLocalityAccess(hwloc_locality);
	}else{
        logger_localization.warning( "Localization not found for thread \"", thread_name, "\"");
		return SDT::thread_locality_default();
	}
}

/////
///  Returns the hardware locality assignment for the supplied context
SDT::ThreadLocalityAccess LocalizationManager::
    thread_locality_get(const SDT::Context &requested_context)
{
    auto context = requested_context;
	while(not context.is_root()){
		HwlocLocalityPtr hwloc_locality = this->named_locality_get(context.full_name_dotted());
		if(hwloc_locality){
			return SDT::ThreadLocalityAccess(hwloc_locality);
		}
        logger_localization.warning("Localization not found for thread \"", requested_context.full_name_dotted(), "\"");
		context = context.up();
	}
	return SDT::thread_locality_default();
}

SDT::MemoryLocalityAccess LocalizationManager::
    memory_locality_get(const string &resource_name)
{
	HwlocLocalityPtr hwloc_locality = this->named_locality_get(resource_name);
	if(hwloc_locality){
		return SDT::MemoryLocalityAccess(hwloc_locality);
	}else{
        logger_localization.warning("Localization not found for thread \"", resource_name, "\"");
		return SDT::memory_locality_default();
	}
}

SDT::MemoryLocalityAccess LocalizationManager::
    memory_locality_get(const SDT::Context &requested_context)
{
    auto context = requested_context;
	while(not context.is_root()){
		HwlocLocalityPtr hwloc_locality = this->named_locality_get(context.full_name_dotted());
		if(hwloc_locality){
			return SDT::MemoryLocalityAccess(hwloc_locality);
		}
        logger_localization.warning( "Localization not found for memory \"", requested_context.full_name_dotted(), "\"");
		context = context.up();
	}
    return SDT::memory_locality_default();
}

void LocalizationManager::
	configurations_import(ConfigTree::ConfigTreePtr config, LoggerAccess logger)
{
	if(not config->is_array()) {
		throw(std::logic_error("thread localizations list must be a json array"));
	}
	for(auto idx : icount(config->array_size())) {
		auto channel_config = (*config)[idx];
		int core_idx  = 0;
		string thread_name = "(name_missing)";
		(*channel_config)["thread_name"]->get(thread_name);
		(*channel_config)["core_idx"]->get(core_idx);
		if((thread_name == "(name_missing)")) {
			logger.warning("thread name missing from localization config");
		}else if((core_idx < 0) or (core_idx >= this->topology_get()->locality_num())) {
			logger.warning("Bad channel address: ", core_idx, " for thread \"", thread_name, "\"");
		} else {
			this->named_locality_set(thread_name, core_idx);
		}
	}
	return;
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
