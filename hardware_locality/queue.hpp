////
///
///

#ifndef H_JSTREAM_HWLOCAL_QUEUE_H
#define H_JSTREAM_HWLOCAL_QUEUE_H

#include "hardware_locality/system_specs.hpp"
#include "queues/queue.hpp"

namespace JStream {

template<typename TObject>
using WaitingQueuePage = WaitingQueue<TObject>;

}; //~namespace JStream

#endif //H_JSTREAM_HWLOCAL_QUEUE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

