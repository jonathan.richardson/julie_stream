#include <iostream>

#include "queues/csd_accumulation_queue.hpp"

namespace JStream {

CSDAccumulationQueue::
    CSDAccumulationQueue() 
{
}

void CSDAccumulationQueue::
    push(TCarrier* csdData) 
{
    nbq.push(csdData);
}

CSDAccumulationQueue::TCarrier* CSDAccumulationQueue::
    get() 
{
    TCarrier* csdData;
    bool retval = nbq.pull(csdData); 
    return csdData;
}

void CSDAccumulationQueue::
    spush(TCarrier* csdData) 
{
    std::shared_ptr<TCarrier> sCsdData(csdData);
    snbq.push(sCsdData);
}

CSDAccumulationQueue::TCarrier* CSDAccumulationQueue::
    sget() 
{
    int i;
    std::shared_ptr<TCarrier> sCsdData;
    bool retval = snbq.pull(sCsdData); 
    return sCsdData.get();
}

size_t CSDAccumulationQueue::
    length() 
{
    return nbq.length();
}

size_t CSDAccumulationQueue::
    circulating_num() 
{
    return nbq.circulating_num();
}

}
