////
///
///
///
///

#ifndef H_JSTREAM_NONBLOCKING_QUEUE_H
#define H_JSTREAM_NONBLOCKING_QUEUE_H
#include <sdt/queues/queue_nonblocking.hpp>

namespace JStream {

template<typename TObject>
using NonBlockingQueue = SDT::NonBlockingQueue<TObject>;

};

#endif //H_JSTREAM_NONBLOCKING_QUEUE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
