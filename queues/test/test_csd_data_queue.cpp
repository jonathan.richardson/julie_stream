////
///

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <iostream>

#include <thread>
#include <chrono>
#include <functional>

#include "queues/csd_accumulation_queue.hpp"


using namespace JStream;

BOOST_AUTO_TEST_CASE( initial_test )
{
    CSDAccumulationQueue* cdq = new CSDAccumulationQueue();
    int n = cdq->length();

    unsigned n_channels = 4;
    unsigned length = 65536;
    double sample_freq = 1.e8;
    double overlap_frac = 0.5;
    int nToPush = 20;
    for (int i=0; i<nToPush; i++) {
        CSDCarrierDouble* csdd = new CSDCarrierDouble(
                                            n_channels, 
                                            length,
                                            sample_freq,
                                            overlap_frac
                                        );
        csdd->sequence_number_set(i+100);
        int j = csdd->sequence_number_get();
        std::cout << "before push  i=" << i << "  j=" << j << std::endl;
        cdq->push(csdd);
    }
    std::cout << "length=" << cdq->length() << std::endl;
    BOOST_CHECK_EQUAL(cdq->length(), nToPush);
    for (int i=0; i<nToPush; i++) {
        CSDCarrierDouble* csdd =  cdq->get();
        std::cout << "after pull: i="<<i<<"  frameId="<<csdd->sequence_number_get() <<std::endl;
        BOOST_CHECK_EQUAL(i+100,csdd->sequence_number_get());
    }
}


//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

