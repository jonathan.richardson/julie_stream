////
///
///
///
///
///


#ifndef H_JSTREAM_WAITING_QUEUE_H
#define H_JSTREAM_WAITING_QUEUE_H
#include <sdt/queues/wait_deny_queue.hpp>

namespace JStream {

template<typename TObject>
using WaitingQueue = SDT::WaitDenyQueue<TObject>;

};

#endif //H_JSTREAM_WAITING_QUEUE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
