///
///
///

#ifndef H_CSD_DATA_QUEUE_H
#define H_CSD_DATA_QUEUE_H

#include <atomic>
#include <cstdint>
#include <cassert>

#include <thread>
#include <chrono>
#include <functional>

#include "queues/queue_nonblocking.hpp"

#include "analysis/csd_carrier_double.hpp"

namespace JStream {

/////
///  Queue class used by the TCP client
class CSDAccumulationQueue 
{
    /// Convenience typedef
    typedef CSDCarrierDouble  TCarrier;

    NonBlockingQueue< std::shared_ptr<TCarrier> > snbq;
    NonBlockingQueue< TCarrier* >                 nbq;
    
public:
    CSDAccumulationQueue();

    void push(TCarrier* csdData);
    TCarrier* get();

    void spush(TCarrier* csdData);
    TCarrier* sget();

    size_t length();
    size_t circulating_num();
};

}; // namespace JStream

#endif //H_CSD_DATA_QUEUE_H
