////
///

#include "csd_worker.hpp"

namespace JStream {

using namespace uLogger;

typedef float TInternalOps; 
typedef void(*TBinaryOp)(TInternalOps&, const complex<TInternalOps> &);
typedef void(*TTernaryOp)(complex<TInternalOps> &, const complex<TInternalOps> &, const complex<TInternalOps>&);


/////
///  Load the next batch of timestream data for processing
///  Applies the window function & calibration constants
template<unsigned div_by_for_unroll>
    inline void data_fill_Nchannel(
            FFTMulti &fft_bank,
            const float *apodization,
            T5122Frame &time_buffer,
            unsigned batch_idx,
            unsigned channels_num
    )
{
    /// Copy over the next N samples with proper calibration & apodization
    for(auto chn_idx : icount(channels_num)){
        /// Get the ptrs to all the data arrays
        const int16_t *time_frame_ptr  = (const int16_t *) time_buffer.frame_range(chn_idx, batch_idx).begin();
        const float   *apodization_ptr = (const float *) apodization;
        float         *fft_input_ptr   = (float *) fft_bank.input_data(chn_idx).begin();

		/// Get the ADC calibration constants for the channel
		/// (for virtual sources, gain=1 and offset=0)
		auto gain   = time_buffer.gain(chn_idx);
		auto offset = time_buffer.offset(chn_idx);

        /// Load the data into the FFT bank for processing
        for(auto el_idx : icount(fft_bank.input_data(chn_idx).size()/div_by_for_unroll)){
            for(unsigned inner_idx = 0; inner_idx < div_by_for_unroll; inner_idx +=1 ){
				fft_input_ptr[inner_idx] = apodization_ptr[inner_idx] * (time_frame_ptr[inner_idx] * gain + offset); 
            }
            fft_input_ptr   += div_by_for_unroll;
            apodization_ptr += div_by_for_unroll;
            time_frame_ptr  += div_by_for_unroll;
        }
    }
}

/////
///  Add the power spectrum of the current batch to the target value
inline void CoAddInnerProduct(TInternalOps &target, const complex<TInternalOps> &L) 
{
	target += L.real()*L.real() + L.imag()*L.imag();
}

/////
///  Set the target value to the power spectrum of the current batch
inline void SetToInnerProduct(TInternalOps &target, const complex<TInternalOps> &L) 
{
	target  = L.real()*L.real() + L.imag()*L.imag();
}

/////
///  Add the cross-spectrum of the current batch to the target value
inline void CoAddInnerProduct(complex<TInternalOps> &target, const complex<TInternalOps> &L, const complex<TInternalOps>&R) 
{ 
    target += complex<TInternalOps>(L) * std::conj(complex<TInternalOps>(R)); 
}

/////
///  Set the target value to the cross-spectrum of the current batch
inline void SetToInnerProduct(complex<TInternalOps> &target, const complex<TInternalOps> &L, const complex<TInternalOps>&R) 
{ 
    target = complex<TInternalOps>(L) * std::conj(complex<TInternalOps>(R)); 
}

/////
///  Compute the PSDs/CSDs
template<
    unsigned div_by_for_unroll, 
    TBinaryOp  binary_op_psd,
    TTernaryOp ternary_op_csd
    >
inline void CSD_fill_Nchannel(
        FFTMulti &fft_bank,
        TCSDCarrier &csd_collection,
        unsigned channels_num
        )
{
    /////
    ///  PSDs
    for(auto idx : icount(0, channels_num)){
        const complex<TCSDCarrier::TNum> * __restrict chn_ptr      = (const complex<TCSDCarrier::TNum> *) __builtin_assume_aligned(fft_bank.fft_data(idx).begin(), 16);
        TCSDCarrier::TNum 				 * __restrict psd_data_ptr = (TCSDCarrier::TNum *) __builtin_assume_aligned(csd_collection.psd_get(idx).begin(), 16);
        for(auto el_idx : icount(csd_collection.psd_get(idx).size()/div_by_for_unroll)){
            for(unsigned inner_idx = 0; inner_idx < div_by_for_unroll; inner_idx+=1){
                binary_op_psd(
                    psd_data_ptr[inner_idx], 
                    chn_ptr[inner_idx]
                );
            }
            chn_ptr  += div_by_for_unroll;
            psd_data_ptr += div_by_for_unroll;
        }
    }
	
    /////
    ///  CSDs
    for(auto row_idx : icount(1, channels_num)){
        for(auto col_idx : icount(0, row_idx)){
            auto row_chn = fft_bank.fft_data(row_idx);
            auto col_chn = fft_bank.fft_data(col_idx);
            auto csd_data = csd_collection.csd_get(row_idx, col_idx);
            const complex<float> * __restrict row_chn_ptr  = (const complex<float> *) __builtin_assume_aligned(row_chn.begin(), 16);
            const complex<float> * __restrict col_chn_ptr  = (const complex<float> *) __builtin_assume_aligned(col_chn.begin(), 16);
            complex<float>       * __restrict csd_data_ptr = (complex<float> *) __builtin_assume_aligned(csd_data.begin(), 16);
            for(auto el_idx : icount(csd_data.size()/div_by_for_unroll)){
                for(unsigned inner_idx = 0; inner_idx < div_by_for_unroll; inner_idx+=1){
                    ternary_op_csd(
                        csd_data_ptr[inner_idx],
                        row_chn_ptr[inner_idx],
                        col_chn_ptr[inner_idx]
                    );
                }
                row_chn_ptr  += div_by_for_unroll;
                col_chn_ptr  += div_by_for_unroll;
                csd_data_ptr += div_by_for_unroll;
            }
        }
    }
}

/////
///  Compute worker constructor
CSDComputeWorker::
    CSDComputeWorker(
            unsigned worker_n,
            unsigned n_channels,
            double sample_freq,
            double overlap_frac,
            unsigned time_frame_len,
            shared_ptr<TApodization> apodization,
            LoggerAccess logger
    ) :
        TFrameSuperclass(logger.context()),
        TCSDSuperclass(logger.context()),
        BackgroundTask(logger.context()),
        m_worker_idx(worker_n),
        m_channels_num(n_channels),
        m_sample_freq(sample_freq),
        m_overlap_frac(overlap_frac),
        m_time_frame_len(time_frame_len),
        m_apodization(apodization),
        m_fft_size(apodization->size()),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
}

/////
///  Contains work loop
void CSDComputeWorker::
compute_loop()
{
    auto locality = this->locality_get();
    
	/// Get the power spectrum normalization factor, ENBW, and int-scaled apodization
	auto 	rescaling 		 = 1;
    double 	normalization	 = m_apodization->power_spectrum_scaling_get() / pow(rescaling, 2);
    double 	ENBW			 = m_apodization->ENBW_get();
    std::shared_ptr<float> apodization_buff = locality.new_array<float>(m_fft_size);
    for(auto idx : icount(m_fft_size)){
        apodization_buff.get()[idx] = float(rescaling) * m_apodization->apodization_data()[idx];
    }

	/// Ptrs to the current time frame, CSD frame
    shared_ptr<T5122Frame>  time_buffer;
    shared_ptr<TCSDCarrier> csd_collection;

    /// Convenience access for the queues since we have to resolve between our two superclass
    /// DataFlowTask types
    TCSDSuperclass::TCirculationQueue   &CSDCarrier_Q_in  = *this->TCSDSuperclass::queue_input_get();
    TCSDSuperclass::TCirculationQueue   &CSDCarrier_Q_out = *this->TCSDSuperclass::queue_output_get();
    TFrameSuperclass::TCirculationQueue &timestream_Q_in  = *this->TFrameSuperclass::queue_input_get();
    TFrameSuperclass::TCirculationQueue &timestream_Q_out = *this->TFrameSuperclass::queue_output_get();

	/// Locally-allocated CSDCarrier for computing CSDs
    TCSDCarrier csd_local(
        m_channels_num,
        m_fft_size / 2,
        m_sample_freq,
        m_overlap_frac,
        locality
    );

	/// Object holding the input & output data for the multi-FFT
    FFTMulti fft_bank(
        m_channels_num, 
        m_fft_size, 
        locality
    );

    while(not this->should_quit()){
		/// Pull in new CSD & time frames
        /// (Pull in the CSD unit first so that we are ready to go once we get a timestream)
        CSDCarrier_Q_in.pull_wait(csd_collection);
        timestream_Q_in.pull_wait(time_buffer);
        if(this->should_quit()){
            break; 
        }
        assert(csd_collection);
        assert(time_buffer);

		/// Compute the CSDs batch-by-batch and add them to the running average
		/// (*Note: "Frame" here refers to one single FFT batch*)
        constexpr unsigned div_by_for_unroll = 1024;
        for(auto batch_idx : time_buffer->frame_counter()){
		    /// Load the next batch of timestream data
            data_fill_Nchannel<div_by_for_unroll>(
                fft_bank,
                apodization_buff.get(),
                *time_buffer,
                batch_idx,
                m_channels_num
            ); 

			/// Compute the DFTs
            fft_bank.compute_ffts();
			
			/// Compute the CSDs and add them to the running totals (averages)
            if(batch_idx == 0){
                CSD_fill_Nchannel<div_by_for_unroll, SetToInnerProduct, SetToInnerProduct>(
                    fft_bank, 
                    csd_local, 
                    m_channels_num
                ); 
            }else{
                CSD_fill_Nchannel<div_by_for_unroll, CoAddInnerProduct, CoAddInnerProduct>(
                    fft_bank, 
                    csd_local, 
                    m_channels_num
                );
            }
        }

		/// Set the CSDCarrier frame metadata
        csd_local.sequence_number_set(time_buffer->sequence_number());
        csd_local.accumulation_set(time_buffer->num_frames());
		csd_local.timestamps_set(time_buffer->timestamp_begin(), time_buffer->timestamp_end());
		csd_local.ENBW_set(ENBW);
		csd_local.normalization_to_ps_set(normalization); //Note: Does NOT include accumulation (reported separately)
        csd_local.error_flag_set(time_buffer->error_flag());
        for(auto chn_idx : icount(m_channels_num)){
            csd_local.channel_voltage_set(
                chn_idx, 
                time_buffer->voltage_min(chn_idx),
                time_buffer->voltage_max(chn_idx)
            );
        }

		/// Copy the local CSD frame into the CSD queue element
        *csd_collection = csd_local;

		/// Push out the CSD, time frames
        timestream_Q_out.push(std::move(time_buffer));
        CSDCarrier_Q_out.push(std::move(csd_collection));
        csd_collection = shared_ptr<TCSDCarrier>(nullptr);
    }
}

}; //~namespace JStream
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
