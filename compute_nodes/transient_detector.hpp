////
///

#ifndef H_TRANSIENT_DETECTOR_H
#define H_TRANSIENT_DETECTOR_H

#include <sdt/utilities/logger.hpp>

#include "data_flow/data_flow_task.hpp"

#include "jstream_types.hpp"

namespace JStream {


/////
///  Transient detector class
///  (Is a node in the CSD superflow)
class TransientDetector : 
    public DataFlowTask< std::shared_ptr<TCSDCarrier> >
{
    /// Convenience typedefs
    typedef DataFlowTask< std::shared_ptr<TCSDCarrier> >          
        TCSDSuperclass;

    /// Logger
    LoggerAccess                    m_logger;

    /// Detector settings
    double                          m_trigger_threshold;    //Trigger threshold for CSD power excursions
    std::vector<unsigned>           m_aux_chs;              //External channel list
    std::vector<unsigned>           m_main_chs;             //Primary channel list
    std::shared_ptr<TCSDCarrier>    m_baseline;             //Baseline CSD frame
    unsigned                        m_baseline_size;        //Number of frames to average into baseline
    unsigned                        m_baseline_state = 0;   //Number of frames CURRENTLY averaged into baseline
    unsigned                        m_veto_thres;           //Error level at which to generate warnings 
    
public:
    /// Constructor
    TransientDetector(
        double trigger_threshold,
        unsigned baseline_size,
        std::vector<unsigned> &aux_chs,
        unsigned veto_thres,
        SDT::LoggerAccess logger
	);

protected:
    void compute_loop();
    
    /// Initializes the baseline CSD reference
    void baseline_init(TCSDCarrier &);   
    
    /// Performs a transient scan of the supplied frame
    unsigned scan_frame(TCSDCarrier &);
    
    /// Handler for a power excursion in a primary channel
    unsigned excursion_handler(
        unsigned chn_idx,
        unsigned freq_idx,
        TCSDCarrier &csd_frame
    );
    
    /// Returns TRUE if the supplied channel is auxiliary (externally sourced)
    bool auxiliary(unsigned); 
        
};

}; //~namespace JStream

#endif //H_TRANSIENT_DETECTOR_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
