////
///

#include "csd_writer.hpp"

namespace JStream {


/////
///  Constructor
CSDWriter::
    CSDWriter(
		std::vector<IDataSource::DataSourceSettings> source_metadata,
        DatabaseSingleRunPtr database_run,
        double file_write_time_s,
        double csd_accum_time_s,
		LoggerAccess logger
	) :
		TCSDSuperclass(logger.context()),
		BackgroundTask(logger.context()),
        m_source_metadata(source_metadata),
        m_database_run(database_run),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
    
    /// Set the number of CSD frames to write per HDF5 file
    m_file_frame_limit = std::max(
        unsigned(file_write_time_s / csd_accum_time_s), 
        unsigned(1)
    );
    m_logger.config("CSD file write time:  ", file_write_time_s, " s");
}

/////
///  Work loop
void CSDWriter::
    compute_loop()
{
	uint64_t    write_ct            = 0;
    uint64_t    run_time_beg        = 0; 
    uint64_t    run_time_end        = 0; 
    unsigned    file_frame_counter  = 0;
	std::string last_h5_fname;

    /// Convenience access for the queues since we have to resolve between our two superclass
    /// DataFlowTask types
    TCSDSuperclass::TCirculationQueue &CSDCarrier_Q_in  = *this->TCSDSuperclass::queue_input_get();
    TCSDSuperclass::TCirculationQueue &CSDCarrier_Q_out = *this->TCSDSuperclass::queue_output_get();

    /// Ptr to the current CSD frame 
    std::shared_ptr<TCSDCarrier>    csd_collection; 

    /// Ptr to the HDF5 writer
    std::shared_ptr< HDF5CSDWriter<TCSDCarrier> >  hdf5_writer;

    while(not this->should_quit()){
        /// Pull in the CSD unit
        CSDCarrier_Q_in.pull_wait(csd_collection);
        if(this->should_quit()){
            break;
        }
        assert(csd_collection);

        /// Write the CSDs to the HDF5 database
        m_logger.detail("Writing CSD superframe: ", csd_collection->sequence_number_get());
        if(not hdf5_writer){
            /// Initialize the HDF5 writer and create a new database entry
            hdf5_writer = std::make_shared< HDF5CSDWriter<TCSDCarrier> >(
                m_source_metadata,
                *csd_collection,
                m_database_run->fpath_get(),
                m_logger.child("HDF5")
            );
            run_time_beg = csd_collection->timestamp_begin() / 1E9;
            last_h5_fname = hdf5_writer->new_file(csd_collection->timestamp_begin());
        }else if(file_frame_counter == m_file_frame_limit){
            /// Update the run entry in the HDF5 database (creates a new data file)
            m_database_run->update_run(
                run_time_beg,
                csd_collection->timestamp_end() / 1E9,
                last_h5_fname
            );
            last_h5_fname = hdf5_writer->new_file(csd_collection->timestamp_begin());
            file_frame_counter = 0;
        }
        hdf5_writer->write_csd(*csd_collection);
        run_time_end = csd_collection->timestamp_end() / 1E9;      
        file_frame_counter += 1;
        
        /// Push out the CSD unit         
        CSDCarrier_Q_out.push(std::move(csd_collection));
        csd_collection = shared_ptr<TCSDCarrier>(nullptr);
        write_ct += 1;         
    }
    m_logger.digest("Wrote a total of ", write_ct, " superframes");

    m_database_run->update_run(
        run_time_beg,
        run_time_end,
        last_h5_fname
    );

	/// (HDF5 shuts down automagically via its shared_ptr)
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
