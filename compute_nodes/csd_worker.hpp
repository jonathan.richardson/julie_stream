////
///
///
#ifndef H_JSTREAM_CSD_WORKER_H
#define H_JSTREAM_CSD_WORKER_H

#include <sdt/utilities/logger.hpp>

#include "analysis/spectrum_tools.hpp"
#include "analysis/apodization.hpp"

#include "data_flow/data_flow_task.hpp"

#include "jstream_types.hpp"

namespace JStream {

//////
/// @brief Worker task which performs FFTs on SlidingFrameTimestream.
///
class CSDComputeWorker : public DataFlowTask<shared_ptr<TCSDCarrier> >,
                         public DataFlowTask<shared_ptr<T5122Frame> >
{
    /////
    ///  These are hugely convenient for when we need to access the queues
  public:
    typedef DataFlowTask<shared_ptr<TCSDCarrier> > 
        TCSDSuperclass;
    typedef DataFlowTask<shared_ptr<T5122Frame>  > 
        TFrameSuperclass;
    typedef Apodization<float>                     
        TApodization;

  private:
    /// Logger
    LoggerAccess                m_logger;
  
    /// Run configuration parameters
    unsigned 					m_worker_idx;
    unsigned 					m_channels_num;
    unsigned                    m_time_frame_len;
    unsigned                    m_fft_size;
    double                      m_sample_freq;
    double 		                m_overlap_frac;
    
    /// Ptr to the window function values
    shared_ptr<TApodization> 	m_apodization;

  public:
    CSDComputeWorker(
		unsigned worker_n,
		unsigned n_channels,
        double sample_freq,
        double overlap_frac,
        unsigned time_frame_len,
		shared_ptr<TApodization> apodization,
		LoggerAccess logger
    );

  protected:
    void compute_loop();

    /////
    ///  Since we are inheriting from two separate DataFlowTask types, our should_quit
    ///  should reflect the status of either of the flow controllers
    inline bool should_quit()
    {
        return (TCSDSuperclass::should_quit() or 
                TFrameSuperclass::should_quit() or 
                this->join_waiting()
                );
    }
};

}; //~namespace JStream

#endif //H_JSTREAM_CSD_WORKER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

