////
///

#include "csd_accumulator.hpp"

namespace JStream {

using namespace uLogger;


/////
///  Constructor
CSDAccumulator::
    CSDAccumulator(
        size_t super_accumulation,
        LoggerAccess logger
    ) :
		TCSDSuperclass(logger.context()),
		BackgroundTask(logger.context()),
        m_super_accumulation(super_accumulation), 
		m_csd_super_flow(),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
}

CSDAccumulator::
    ~CSDAccumulator()
{
}

/////
///  Creates the internal CSD superflow queue
void CSDAccumulator::
	internal_flow_setup(const std::vector<std::shared_ptr<TCSDFlowNode>> &flows)
{
	m_csd_super_flow = std::make_shared<TCSDFlow>(flows, false /*make circular flow*/);
}

/////
///  Adds the supplied CSD frame to the internal CSD superflow queue
void CSDAccumulator::
	prefill_csd_add(shared_ptr<TCSDCarrier> csd)
{
	m_csd_super_flow->end_queue_get()->push(csd);
}

/////
///  Contains work loop
void CSDAccumulator::
    compute_loop()
{
	uint64_t frame_count 		= 0,
			 superframe_count   = 0,
			 num_batches 		= 0,
             no_drop_duration   = 0,
			 range   			= m_super_accumulation;

	/// Ptrs to current CSD frame & superframe
    shared_ptr<TCSDCarrier>	frame;
    shared_ptr<TCSDCarrier>	superframe;

	/// Priority queue to order the CSD frames as they arrive
	std::priority_queue<
        shared_ptr<TCSDCarrier>, 
        std::vector<shared_ptr<TCSDCarrier>>, 
        CompareFrameID 
    > frame_buffer;

    /// Convenience access for the queues since we have to resolve between our two superclass types
    TCSDSuperclass::TCirculationQueue &CSDCarrier_Q_in  = *this->TCSDSuperclass::queue_input_get();
    TCSDSuperclass::TCirculationQueue &CSDCarrier_Q_out = *this->TCSDSuperclass::queue_output_get();

	/// Start the internal avg_csd_queue
    m_csd_super_flow->flow_tasks_start();

	while(not this->should_quit()){
        /////
		///  Pull in a new superframe for filling
		m_csd_super_flow->end_queue_get()->pull_wait(superframe);
        assert(superframe);

		/// Flush the priority queue
		/// (Writes all in-range CSD frames first)
		while(not frame_buffer.empty()){
			if( frame_buffer.top()->sequence_number_get() < range ){
				/// Add the CSDs to the running average in the superframe
                /// (Also sets the metadata & checks for CSD frame-dropping)
                if(frame_count == 0){
                    *superframe = *frame_buffer.top();
                }else{
                    superframe->csd_frame_add(*frame_buffer.top());
                }

				/// Push out the CSD frame
				CSDCarrier_Q_out.push(std::move(frame_buffer.top()));
				frame_buffer.pop();
				frame_count++;
			}else{
                break; 
            }
		}
			
		/// Pull new CSD frames until the superframe is filled
		while(frame_count < m_super_accumulation){
			/// Pull in the next csd frame	
			CSDCarrier_Q_in.pull_wait(frame);
			if(this->should_quit()){
                break; 
            }
            assert(frame);
			num_batches++;
            
            /// On the first CSD frame, set the time interval spanned by one continuous superframe
            if(not superframe_count and not frame_count){
                uint64_t overlap_offset = (frame->overlap_frac_get() * frame->length() * 2) 
                                                * (1.e9 / frame->sample_freq_get());
                no_drop_duration = (frame->timestamp_end() - frame->timestamp_begin()) * m_super_accumulation
                                                - overlap_offset * (m_super_accumulation - 1);
            }
            
			if(frame->sequence_number_get() < range){
				/// Add the CSDs to the running average in the superframe
                /// (Also sets the metadata & checks for CSD frame-dropping)
                if(frame_count == 0){
                    *superframe = *frame;
                }else{
                    superframe->csd_frame_add(*frame);
                }

				/// Push out the CSD frame
				CSDCarrier_Q_out.push(std::move(frame));
				frame_count++;
			}else{
				/// Push the csd frame onto the priority queue
				frame_buffer.push(std::move(frame));
			}
            frame = shared_ptr<TCSDCarrier>(nullptr);
		}
        if(this->should_quit()){
            break; 
        }
        
        /// Set the superframe error code
        unsigned error = 0;
        {    /// Check whether frame-dropping has occurred
            auto duration = superframe->timestamp_end() - superframe->timestamp_begin();
            if(duration > no_drop_duration){
                error = 10;
            }
        }
        if(error > superframe->error_flag_get()){
            superframe->error_flag_set(error);
        }
        
        /// Set the superframe ID number
        superframe->sequence_number_set(superframe_count);
        
		/// Push out the filled superframe        
		m_csd_super_flow->start_queue_get()->push(std::move(superframe));
        superframe = shared_ptr<TCSDCarrier>(nullptr);
		superframe_count += 1;
		frame_count 	  = 0;
		range 			 += m_super_accumulation;	
    }
    m_logger.digest("Accumulated a total of ", num_batches, " frames into ", superframe_count, " superframes");

	/// Stop the internal csd_super_flow queue
    m_csd_super_flow->flow_tasks_end();
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
