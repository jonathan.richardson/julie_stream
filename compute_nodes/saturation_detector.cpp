////
///

#include "saturation_detector.hpp"

namespace JStream {


/////
///  Constructor
SaturationDetector::
    SaturationDetector(
        double level,
        unsigned num_channels,
        unsigned veto_thres,
        SDT::LoggerAccess logger
    ) :
        TFrameSuperclass(logger.context()),
        BackgroundTask(logger.context()),
        m_level(level),
        m_channels_n(num_channels),
        m_veto_thres(veto_thres),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
    
    /// Set the saturation tolerance level
    /// (Fractional dynamic range of 14-bit integer)
    assert(m_level <= 1.0 & m_level > 0.0);
    m_max_range = m_level * std::numeric_limits<int16_t>::max();
    m_min_range = m_level * std::numeric_limits<int16_t>::min();
}

/////
///  Returns a custom warning message for the event
void SaturationDetector::
    warning_msg(
        std::vector<bool> &chn_state,
        unsigned timestamp
    )
{
    std::stringstream SS;
    
    /// Affected channel(s)
    SS << "Clipping detected in Ch(s): ";
    auto chn_idx = 0;
    for(bool ok : chn_state){
        if(not ok){
            SS << "  " << chn_idx;
        }
        chn_idx += 1;
    }
    
    /// Date/time of occurrence
    auto time = std::chrono::seconds(timestamp);
    auto time_pt = std::chrono::system_clock::time_point(time);
    auto date = std::chrono::system_clock::to_time_t(time_pt);
    std::string time_str = ctime(&date);
    SS << "\n\t" << time_str;
    
    /// Print the warning to the logger
    if(m_veto_thres <= 40){
        m_logger.warning(SS.str());
    }else{
        m_logger.detail(SS.str());
    }
}

/////
///  Contains work loop
void SaturationDetector::
compute_loop()
{
	/// Ptr to the current time frame
    std::shared_ptr<T5122Frame> time_frame;

   /// Contains saturation scan result for each channel
    std::vector<bool> state(m_channels_n);    
    
    /// Convenience access for the time frame queues
    TFrameSuperclass::TCirculationQueue &Q_in = *this->TFrameSuperclass::queue_input_get();
    TFrameSuperclass::TCirculationQueue &Q_out = *this->TFrameSuperclass::queue_output_get();

    while(not this->should_quit()){
		/// Pull in a new time frame
        Q_in.pull_wait(time_frame);
        if(this->should_quit()){
            break;
        }
        assert(time_frame);    
        
        /// Perform the scan on all channels
        unsigned error = 0;
        for(auto chn_idx : time_frame->channel_counter()){
            /// Get the maximum and minimum signal values
            auto buf = time_frame->channel_timebuffer(chn_idx);        
            auto min = buf[0];
            auto max = buf[0];
            for(auto val: buf){
                if(val < min){
                    min = val;
                }
                if(val > max){
                    max = val;
                }
            }
            
            /// Set the maximum, mimimum voltages in this time frame
            auto gain   = time_frame->gain(chn_idx);
            auto offset = time_frame->offset(chn_idx);            
            auto V_max  = max * gain + offset;
            auto V_min  = min * gain + offset;
            time_frame->channel_voltage_set(chn_idx, V_min, V_max);
            
            /// Check for saturation
            if( (max > m_max_range) or 
                (min < m_min_range)
            ){
                /// Clipping detected
                error = 40;
                state[chn_idx] = false;
            }else{
                /// Data are OK
                state[chn_idx] = true;
            }
        }
        time_frame->error_flag_set(error);        
        if(error){
            /// RATE LIMITER: Only generate one warning per second
            unsigned timestamp = time_frame->timestamp_begin() / 1E9;
            if((timestamp - m_time_last_warn) > 1){
                this->warning_msg(state, timestamp);
                m_time_last_warn = timestamp;
            }
        }

		/// Push out the time frame
        Q_out.push(std::move(time_frame));
        time_frame = shared_ptr<T5122Frame>(nullptr);
    }
}

}; //~namespace JStream
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
