////
///

#include "timeseries_writer.hpp"

namespace JStream {

void TimeseriesWriteNodeConfig::
    parse_config_basic(ConfigTreeAccess config)
{
    config["use_hdf5_ts_writer" ].get(m_use_hdf5_ts_writer);
    config["write_on_clipping"  ].get(m_write_on_clipping);

    config["write_interval_s"   ].get(m_write_interval_s);
    config["rate_limit_s"       ].get(m_rate_limit_s);
    config["new_file_interval_s"].get(m_new_file_interval_s);

    config["rate_limit_num"     ].get(m_rate_limit_num);
}


/////
///  Constructor
TimeseriesWriteNode::
    TimeseriesWriteNode(
        const TimeseriesWriteNodeConfig &config,
        TWriterPtr writer,
        SDT::LoggerAccess logger
    ) :
        TFrameSuperclass(logger.context()),
        BackgroundTask(logger.context()),
        m_config(config),
        m_hdf5_ts_writer(writer),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
}

/////
///  Contains work loop
void TimeseriesWriteNode::
compute_loop()
{
	/// Ptr to the current time frame
    std::shared_ptr<T5122Frame> time_frame;

    /// Convenience access for the time frame queues
    TFrameSuperclass::TCirculationQueue &Q_in = *this->TFrameSuperclass::queue_input_get();
    TFrameSuperclass::TCirculationQueue &Q_out = *this->TFrameSuperclass::queue_output_get();
    uint64_t time_last_autowrite_s = 0;

    //this deque implements the clipping rate limiter which can enable consecutive clipping views
    std::deque<uint64_t> time_last_clipping_ns_deque(m_config.m_rate_limit_num, 0);
    uint64_t time_last_clipping_warn_ns = 0;

    std::thread write_loop([this](){this->write_loop();});

    while(not this->should_quit()){
		/// Pull in a new time frame
        Q_in.pull_wait(time_frame);
        if(this->should_quit()){
            break; 
        }
        assert(time_frame);
        bool do_clipping_write = false;
        bool do_auto_write = false;

        //m_logger.debug("Time to next TS write: ", (time_frame->timestamp_begin() - time_last_autowrite_s) / 1e9);
        if(time_frame->timestamp_begin() - time_last_autowrite_s  > m_config.m_write_interval_s * 1e9){
            time_last_autowrite_s = time_frame->timestamp_begin();
            do_auto_write = true;
        }

        //TODO enum these flags
        if(time_frame->error_flag() == 40){
            if(time_frame->timestamp_begin() - time_last_clipping_ns_deque.front() > m_config.m_rate_limit_s * 1e9){
                do_clipping_write = true;
                time_last_clipping_ns_deque.pop_front();
                time_last_clipping_ns_deque.push_back(time_frame->timestamp_begin());
            }else{
                if(time_frame->timestamp_begin() - time_last_clipping_warn_ns >= 1e9){
                    m_logger.detail("TS clipping writing is being rate-limited");
                    time_last_clipping_warn_ns = time_frame->timestamp_begin();
                }
            }
        }

        if(do_auto_write){
            //allow the queue to be 1 longer so that the autowrite will usually succeed even when clipping is bad
            if(m_inner_circulation.length() < m_config.m_rate_limit_num + 1){
                /// Push out the time frame
                m_inner_circulation.push(std::move(time_frame));
                time_frame = shared_ptr<T5122Frame>(nullptr);
            }else{
                m_logger.warning("TS auto writing can't keep up!");
            }
        }else if(do_clipping_write){
            if(m_inner_circulation.length() < m_config.m_rate_limit_num){
                /// Push out the time frame
                m_inner_circulation.push(std::move(time_frame));
                time_frame = shared_ptr<T5122Frame>(nullptr);
            }else{
                m_logger.warning("TS clipping writing can't keep up!");
            }
        }

        if(time_frame){
            /// Push out the time frame
            Q_out.push(std::move(time_frame));
            time_frame = shared_ptr<T5122Frame>(nullptr);
        }
    }

    m_inner_circulation.deny_wait_set();
    write_loop.join();
    m_inner_circulation.deny_wait_clear();
}

/////
///  Contains work loop
void TimeseriesWriteNode::
write_loop()
{
	uint64_t    write_ct        = 0,
                time_limit_ns   = 0,
                run_end_time    = 0;                
	std::string last_h5_fname;

	/// Ptr to the current time frame
    std::shared_ptr<T5122Frame> time_frame;

    /// Convenience access for the time frame queues
    TFrameSuperclass::TCirculationQueue &Q_in = m_inner_circulation;
    TFrameSuperclass::TCirculationQueue &Q_out = *this->TFrameSuperclass::queue_output_get();

    while(not this->should_quit()){
		/// Pull in a new time frame
        Q_in.pull_wait(time_frame);
        if(this->should_quit()){
            break; 
        }
        assert(time_frame);

        /// Write the Timeseries to the HDF5 database
        m_logger.detail("Writing TS superframe: ", time_frame->sequence_number());

        m_hdf5_ts_writer->write(*time_frame, m_config.m_new_file_interval_s);
        run_end_time = time_frame->timestamp_end() / 1E9;           

        Q_out.push(std::move(time_frame));
        time_frame = shared_ptr<T5122Frame>(nullptr);
        write_ct += 1;         
    }

    m_logger.digest("Wrote a total of ", write_ct, " time series");
}



}; //~namespace JStream
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
