////
///

#include "transient_detector.hpp"

namespace JStream {


/////
///  Constructor
TransientDetector::
    TransientDetector(
        double trigger_threshold,
        unsigned baseline_size,
        std::vector<unsigned> &aux_chs,
        unsigned veto_thres,
        SDT::LoggerAccess logger
	) :
		TCSDSuperclass(logger.context()),
		BackgroundTask(logger.context()),
        m_baseline_size(baseline_size),
        m_trigger_threshold(trigger_threshold / baseline_size),
        m_aux_chs(aux_chs),
        m_veto_thres(veto_thres),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
}

/////
///  Contains work loop
void TransientDetector::
    compute_loop()
{
    uint64_t	                    num_frames = 0;
    std::shared_ptr<TCSDCarrier>    csd_frame;
    unsigned                        error_flag;

    /// Convenience access for the superflow queues
    TCSDSuperclass::TCirculationQueue &CSDCarrier_Q_in  = *this->TCSDSuperclass::queue_input_get();
    TCSDSuperclass::TCirculationQueue &CSDCarrier_Q_out = *this->TCSDSuperclass::queue_output_get();  

	while(not this->should_quit()){
        /// Pull in the next CSD superframe 			
        CSDCarrier_Q_in.pull_wait(csd_frame);
        if(this->should_quit()){
            break; 
        }
        assert(csd_frame);
        m_logger.detail("Scanning CSD Superframe #", csd_frame->sequence_number_get());

        if(m_baseline_state < m_baseline_size){
            /// Initialize the baseline
            this->baseline_init(*csd_frame);
        }else{
            /// Scan the frame for transient activity
            error_flag = this->scan_frame(*csd_frame);
            if(error_flag > csd_frame->error_flag_get()){
                csd_frame->error_flag_set(error_flag);
            }
        }
        
        /// Push out the CSD superframe         
        CSDCarrier_Q_out.push(std::move(csd_frame));
        csd_frame = std::shared_ptr<TCSDCarrier>(nullptr);
		num_frames += 1;
    }
    m_logger.digest("Scanned a total of ", num_frames, " superframes");
}

/////
///  Sets the reference CSD
///  (Detector will scan for power excursions relative to this baseline)
void TransientDetector::
    baseline_init(TCSDCarrier &source)
{
    if(m_baseline_state == m_baseline_size){
        /// Already intialized
        return;
    }
    
    /// Add the supplied frame to the baseline
    if(not m_baseline){
        /// Allocate the CSD container
        m_baseline.reset(new TCSDCarrier(source));
    }else{
        /// Accumulate the supplied frame into the baseline average
        m_baseline->csd_frame_add(source);
    }
    m_baseline_state += 1;
}

/////
///  Scans for power excursions relative to the baseline reference
unsigned TransientDetector::
    scan_frame(TCSDCarrier &csd_frame)
{
    unsigned    error_code  = 0;
    auto        num_chs     = csd_frame.channels_num();
    auto        length      = csd_frame.length();

    /// Scan each channel in turn
    for(auto chn_idx : icount(num_chs)){
        unsigned error = 0;
        auto p1 = csd_frame.psd_get(chn_idx).begin();
        auto p0 = m_baseline->psd_get(chn_idx).begin();
        for(unsigned freq_idx : icount(length)){
            /// Check each bin for a power excursion above threshold
            /// (Breaks on the first excursion encountered)
            auto dev = p1[freq_idx] / p0[freq_idx];
            if(dev > m_trigger_threshold){
                /// Handle the excursion
                error = this->excursion_handler(
                    chn_idx, 
                    freq_idx,
                    csd_frame
                );
                break;      
            }
        }
        if(error > error_code){
            /// Set the highest error code of any channel for the entire frame
            error_code = error;
        }
    }
    return error_code;
}     

/////
///  Handler for a power excursion in a primary channel
unsigned TransientDetector::
    excursion_handler(
        unsigned chn_idx,
        unsigned freq_idx,
        TCSDCarrier &csd_frame
    )
{
    /// Get date/time of occurrence
    unsigned timestamp = csd_frame.timestamp_begin() / 1E9;
    auto time = std::chrono::seconds(timestamp);
    auto time_pt = std::chrono::system_clock::time_point(time);
    auto date = std::chrono::system_clock::to_time_t(time_pt);
    std::string time_str = ctime(&date);

    /// Determine the source
    if(this->auxiliary(chn_idx)){
        /// Transient detected in an auxiliary channel
        std::stringstream SS;
        SS << "RF transient detected:\tCh" << chn_idx << " (aux)";
        SS << "\n\t" << time_str;
        if(m_veto_thres <= 20){
            m_logger.warning(SS.str());
        }else{
            m_logger.detail(SS.str());
        }
        return 20;
    }else{
        /// Transient detected in a primary channel
        /// Attempt to identify it using the auxiliary channels
        for(auto aux_idx : m_aux_chs){
            auto p1 = csd_frame.psd_get(aux_idx).begin();
            auto p0 = m_baseline->psd_get(aux_idx).begin();                            
            auto dev = p1[freq_idx] / p0[freq_idx];
            if(dev > m_trigger_threshold){
                /// Positive ID as external contamination
                std::stringstream SS;
                SS << "RF transient detected:\tCh" << chn_idx;
                SS << "\n\t" << time_str;
                if(m_veto_thres <= 31){
                    m_logger.warning(SS.str());
                }else{
                    m_logger.detail(SS.str());
                }
                return 31;
            }
        }

        /// Transient was not identified in the external channels
        std::stringstream SS;
        SS << "Unidentified transient detected:\tCh" << chn_idx;
        SS << "\n\t" << time_str;
        if(m_veto_thres <= 30){
            m_logger.warning(SS.str());
        }else{
            m_logger.detail(SS.str());
        }
        return 30;
    }
}

/////        
///  Returns TRUE if the supplied channel is auxiliary (externally sourced)
bool TransientDetector::
    auxiliary(unsigned chn_idx)
{
    auto auxiliary = false;
    for(auto aux_idx : m_aux_chs){
        if(chn_idx == aux_idx){
            auxiliary = true;
            break;
        }
    }
    return auxiliary;
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :