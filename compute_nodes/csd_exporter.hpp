////
///

#ifndef H_JSTREAM_CSD_EXPORTER_H
#define H_JSTREAM_CSD_EXPORTER_H

#include <iostream>
#include <fstream>
#include <cstdio>
#include <complex>

#include <boost/asio.hpp>

#include <sdt/utilities/logger.hpp>

#include "utilities/stdtime.hpp"
#include "utilities/config_tree/config_tree_json.hpp"

#include "data_flow/data_flow_task.hpp"

#include "jstream_types.hpp"

#include "analysis/csd_carrier_double.hpp"

//#include "data_management/hdf5_writer.hpp"

namespace JStream {
using namespace ConfigTree;


/////
/// TCP NETWORK MODULE CLASS
class CSDExporter : public DataFlowTask< shared_ptr<TCSDCarrier> >
{
    /// Convenience typedefs
    typedef DataFlowTask<shared_ptr<TCSDCarrier> >          TCSDSuperclass;
	typedef std::function<void (const CSDCarrierDouble &)>  TAccumulationCallback;
    
    /// Logger
    LoggerAccess    m_logger;

    /// Wrapper for the function to be called after accumulating a new CSD frame
	TAccumulationCallback m_accumulation_callback;    
    
	/// TCP settings
    bool		m_use_tcp_link 			= false;
	float		m_send_interval_s  		= 10.0;   //Time interval between data sends (s)
	unsigned	m_port 					= 2000;   //TCP server port    

	/// CSD properties
	unsigned	m_num_chs				= 0;	  //Number of data channels
	unsigned	m_csd_length			= 0;	  //Length of each CSD/PSD
	double		m_sample_freq           = -1.0;	  //Sample frequency (Hz)
    double      m_overlap_frac          = -1.0;   //Welch overlap fraction
	
	double		m_T0					= -1.0;	  //Reference start time (s)
	string		m_final_avg_output_file; 	 	  //Name of the file in which to write the final averages
    
    /// Ptrs to the 5122 settings
//    std::vector< shared_ptr<const T5122Config> > m_adc_settings;

    /// Error flag threshold at which to veto frames (exclude from running average)
    unsigned    m_veto_thres            = 100;

public:
    /// Constructor
    CSDExporter(
		unsigned num_chs,
		unsigned csd_length,
        double sample_freq,
        double overlap_frac,
        unsigned veto_thres,
		//std::vector< shared_ptr<const T5122Config> > &adc_settings,
        LoggerAccess logger
	);

    /// Sets the callback function for a new CSD accumulation
	void accumulation_callback_set(TAccumulationCallback callback);

    /// Reports the total system throughput
    void log_system_throughput(CSDCarrierDouble &current_accumulation);

protected:
    void compute_loop();
};

}; //~namespace JStream

#endif //H_JSTREAM_CSD_EXPORTER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
