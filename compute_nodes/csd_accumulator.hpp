////
///

#ifndef H_JSTREAM_CSD_ACCUMULATOR_H
#define H_JSTREAM_CSD_ACCUMULATOR_H

#include <iostream>
#include <cstdio>
#include <complex>
#include <queue>

#include <boost/asio.hpp>

#include <sdt/utilities/logger.hpp>

#include "utilities/config_tree/config_tree_json.hpp"

#include "data_flow/data_flow_task.hpp"

#include "jstream_types.hpp"

namespace JStream {
using namespace ConfigTree;


/////
///  Comparsion utility class for ordering the CSD frames in the priority queue
class CompareFrameID {
  public:
	bool operator()(std::shared_ptr<TCSDCarrier> frame1, std::shared_ptr<TCSDCarrier> frame2){
		if(frame2->sequence_number_get() < frame1->sequence_number_get()){
			return true; 
        }else{
			return false;
        }
	}
};

/////
///  Super-accumulator provides second layer of CSD averaging
class CSDAccumulator : public DataFlowTask<shared_ptr<TCSDCarrier> >
{
	/// Convenience access to the queues
	typedef DataFlowTask<shared_ptr<TCSDCarrier> >    TCSDSuperclass;
    
    /// Logger
    LoggerAccess                m_logger;

	/// Internal CSD superframe queue parameters
	std::shared_ptr< TCSDFlow > m_csd_super_flow;		//Ptr to the internal queue
	size_t	 					m_super_accumulation;	//The number of CSD frames per superframe

  public:
	CSDAccumulator(
        size_t super_accumulation,
        LoggerAccess logger
    );
    
    ~CSDAccumulator();

	void internal_flow_setup(const std::vector<std::shared_ptr<TCSDFlowNode> > &flows);
	void prefill_csd_add(shared_ptr<TCSDCarrier>);

  protected:
	void compute_loop();
};

}; //~namespace JStream

#endif //H_JSTREAM_CSD_ACCUMULATOR_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
