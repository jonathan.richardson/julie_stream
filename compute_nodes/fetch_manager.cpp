////
///

#include "fetch_manager.hpp"

namespace JStream {


/////
///  Constructor
FetchManager::
    FetchManager(
		TDataSourceCollection data_sources,
        bool monitor_ifo_state,
        unsigned veto_thres,
        LoggerAccess logger
    ) :
		TFrameSuperclass(logger.context()),
		BackgroundTask(logger.context()),
        m_data_sources(data_sources),
        m_monitor_ifo_state(monitor_ifo_state),
        m_veto_thres(veto_thres),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);

    /// Get the data source configuration
    for(auto idx : icount(m_data_sources.size())){
        auto &source = m_data_sources[idx];
		
		/// Enable frame dropping if "realtime" set for the data sources
		/// (If enabled for one source, will be enabled for all sources)
        if(source->realtime_is()){
            m_is_realtime = true;
        }
		
		/// Set the sampling frequency
        if(m_sample_freq == -1){
            m_sample_freq = source->sample_freq();
        }else{
            if(source->sample_freq() != m_sample_freq){
                throw std::logic_error("All data sources must run at the same sample frequency!"); 
            }
        }
		
		/// Total up the number of device channels
        m_channels_num += source->channels_num();
    }
}

/////
/// Get the sampling frequency (Hz)
double FetchManager::
    sample_freq()
{
    return m_sample_freq; 
}

/////
///  Get the total number of channels
unsigned FetchManager::
    channels_total_num()
{
    return m_channels_num; 
}

/////
///  Populate the time flow queue with time frames
void FetchManager::
    prefill_frame_setup(
        std::shared_ptr<T5122Frame> prototype, 
        unsigned num
    )
{
    TFrameSuperclass::TCirculationQueue &Q_in = *this->TFrameSuperclass::queue_input_get();
    for(auto idx : icount(num)){   //loop over the number of frames in the input queue
        auto fetch_buffer_inject = std::shared_ptr<T5122Frame>(new T5122Frame(*prototype));
        Q_in.push(fetch_buffer_inject);
    }
    m_framed_length = prototype->framed_buffer_len();
}

/////
///  Sets the handler function for a 5122 buffer overflow
void FetchManager::
	hw_exception_callback_set(TExceptionCallback callback)
{
	m_exception_callback = callback;
}

/////
///  Sets the IFO lock state
void FetchManager::
	ifo_lock_state_set(bool state)
{
    if(m_monitor_ifo_state){
        m_logger.action("IFO lock state: ", state ? "ACQUIRED" : "LOST");    
        m_ifo_lock_state = state;
    }
}

/////
///  Compute loop
void FetchManager::
    compute_loop()
{
    /// Initial reporting
	m_logger[6].action("Starting Fetch Manager loop");
    if(m_is_realtime){
        m_logger.detail("Will drop frames"); 
    }else{
        m_logger.detail("Will NOT drop frames");
    }

    unsigned    fetch_ct            = 0;
    unsigned    drop_ct             = 0;
    unsigned    lock_loss_ct        = 0;
	unsigned    prefill_length_last = 0;
	uint64_t	offset_5122		    = 0.0;
    uint64_t    time_begin 		    = 0.0;
    uint64_t    time_end   		    = 0.0;
    auto 	    num_sources		    = m_data_sources.size();
    auto        time_increment 		= m_data_sources[0]->time_increment();

	/// The current data ranges to be filled
	std::vector<TFillRange> fill_buffers(m_channels_num);
	std::vector<TTimeRange> fill_times(m_channels_num);
	std::vector<TCalRange>  fill_gain(m_channels_num);
	std::vector<TCalRange>  fill_offset(m_channels_num);

	/// The current batch of timestamps (one value per channel)
	std::vector<uint64_t>   timestamps(m_channels_num);
    
	/// Input and output queues
    TFrameSuperclass::TCirculationQueue &Q_in = *this->TFrameSuperclass::queue_input_get();
    TFrameSuperclass::TCirculationQueue &Q_out = *this->TFrameSuperclass::queue_output_get();

	/// Pull in the first frame to fill
    /// (Can fail, but it is protected by the while loop's condition)
    std::shared_ptr<T5122Frame> fetch_time_current(nullptr);    
    Q_in.pull_wait(fetch_time_current);
    if(fetch_time_current){
        fetch_time_current->sequence_number_set(fetch_ct); 
    }

	/// Start fetching data from all data sources
    for(auto &source : m_data_sources){
        source->fetching_start(); 
    }

    while(not this->should_quit()){
        m_logger.detail("Fetch timebuffer #: ", fetch_ct);

		/// SET THE RANGE OF THE FRAME BUFFERS TO FILL WITH NEW DATA
        for(auto chn_idx : icount(m_channels_num)){ //loop over the total number of channels
			/// Set the channel range to be filled with new data
			/// (Shifts the begin_ptr by the number of points to be prefilled)
            TFillRange curr_buff = fetch_time_current->channel_timebuffer(chn_idx); 
            curr_buff.begin_shift(prefill_length_last);
            fill_buffers[chn_idx] = curr_buff;

			/// Set the range for holding the ADC calibration constants
			fill_gain[chn_idx]   = fetch_time_current->gain_buffer(chn_idx);
			fill_offset[chn_idx] = fetch_time_current->offset_buffer(chn_idx);
			
			/// Set the timestamp range
			fill_times[chn_idx] = TTimeRange(&timestamps[chn_idx], 1);
        }

        try{
            /// FETCH DATA UNTIL THE FRAME BUFFERS ARE FILLED
            /// (Each channel buffer holds one accumulation time worth of data)
            uint32_t chn_bitset = 0;
            uint32_t channels_full_bitset = (1 << m_channels_num) - 1;
            while(chn_bitset != channels_full_bitset){
                unsigned time_chn_idx = 0;
                for(auto ds_idx : icount(m_data_sources.size())){ //loop over the data sources
                    auto &source = m_data_sources[ds_idx];
                    for(auto ds_chn_idx : icount(source->channels_num())){ //loop over the data source channels
                        /// Fetch a batch of data (automatically increments the ranges)
                        /// (For >1 real 5122s, fetches from the ThreadedReaders; o/w
                        ///  fetches from the data source(s) directly)
                        TFillRange &chn_buffer    = fill_buffers[time_chn_idx];
                        TTimeRange &chn_timestamp = fill_times[time_chn_idx];
                        TCalRange  &chn_gain      = fill_gain[time_chn_idx];
                        TCalRange  &chn_offset    = fill_offset[time_chn_idx];
                        source->fetch_data(
                            ds_chn_idx, 
                            chn_buffer, 
                            chn_timestamp,
                            chn_gain,
                            chn_offset
                        );
                        time_chn_idx += 1;
                        if(chn_buffer.empty()){
                            /// This channel buffer is full
                            chn_bitset |= 1 << (time_chn_idx - 1);
                        }
                    }//~device channel loop
                }//~device loop
            }//~buffer fill loop
        }catch(std::runtime_error& error){
            /// Call the hardware exception handler asynchronously, then quit      
            m_logger.fatal(error.what());
            auto err_handler = [this](){return m_exception_callback();};
            boost::thread async(err_handler);
            break;
        }

		/// SET THE FRAME TIMESTAMPS
        /// (Timestamps of the first sample in the first FFT batch and
        ///  the last sample in the last FFT batch)
        for(auto chn_idx : icount(m_channels_num - 1)){
            /// Enforce that the sample counts are consistent for each channel
            assert(timestamps[chn_idx] == timestamps[chn_idx + 1]);
        }
		time_begin = timestamps[0] - (prefill_length_last * time_increment);
        time_end = time_begin + (m_framed_length * time_increment);
        fetch_time_current->timestamps_set(time_begin, time_end);
        
		/// If STOP has been signaled, just push this CSD frame out and shut down now
        if(this->should_quit()){
            if( (not m_monitor_ifo_state) or
                (m_ifo_lock_state) 
            ){
                Q_out.push(std::move(fetch_time_current));
                fetch_ct += 1;
            }
            break;
        }
        
        /// Get the next 5122 frame
        std::shared_ptr<T5122Frame> fetch_time_next(nullptr);
        bool did_pull_frame = false;
        if( (m_monitor_ifo_state) and 
            (not m_ifo_lock_state)
        ){
            /// Overwrite the current frame (lock loss)
            fetch_time_next = fetch_time_current;
            lock_loss_ct += 1;
            m_logger[6].action("Vetoed lock-loss Frame #", fetch_time_current->sequence_number());            
        }else{
            if(m_is_realtime){
                /// Attempt to grab a new frame
                did_pull_frame = Q_in.pull(fetch_time_next);
                if(not did_pull_frame){
                    /// Overwrite the current frame
                    fetch_time_next = fetch_time_current;
                    drop_ct += 1;
                    
                    /// Generate a logger warning
                    std::stringstream SS;
                    SS << "Dropped Frame #" << fetch_time_current->sequence_number();                    
                    if(m_veto_thres <= 10){
                        m_logger.warning(SS.str());
                    }else{
                        m_logger.detail(SS.str());
                    }
                }else{
                    fetch_ct += 1;
                }
            }else{
                /// Wait for a new frame
                did_pull_frame = Q_in.pull_wait(fetch_time_next);
                if(not did_pull_frame){
                    break;
                }else{
                    fetch_ct += 1;
                }
            }
        }
        fetch_time_next->sequence_number_set(fetch_ct);
        
        /// Copy the unframed + overlap points from the end of the current frame to the 
        /// beginning of the new frame
        prefill_length_last = fetch_time_next->prefill_overlap_size(*fetch_time_current);
        for(auto chn_idx : icount(m_channels_num)){
            Range<TFill*> next_buff = fetch_time_next->channel_timebuffer(chn_idx);
            Range<TFill*> curr_buff = fetch_time_current->channel_timebuffer(chn_idx);
            memcpy(next_buff.begin(), curr_buff.end() - prefill_length_last, sizeof(TFill) * prefill_length_last);
        }

		/// PUSH OUT THE CURRENT FRAME
        if(did_pull_frame){
            Q_out.push(std::move(fetch_time_current));
        }
        fetch_time_current = std::move(fetch_time_next);
	}//~work loop

	/// End the DAQ on each data source 
    for(auto &source : m_data_sources){
        source->fetching_finish();
    }

    /// Final reporting
    {
        auto rate = 100 * double(fetch_ct) / (fetch_ct + drop_ct + lock_loss_ct);
        m_logger.digest("Processed a total of ", fetch_ct, " frames (", rate, "%)");
    }
    if(m_is_realtime){
        auto rate = 100 * double(drop_ct) / (fetch_ct + drop_ct + lock_loss_ct);
        m_logger.digest("Dropped a total of ", drop_ct, " frames (", rate, "%)");
    }
    if(m_monitor_ifo_state){
        auto rate = 100 * double(lock_loss_ct) / (fetch_ct + drop_ct + lock_loss_ct);
        m_logger.digest("Vetoed a total of ", lock_loss_ct, " lock-loss frames (", rate, "%)");
    }
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
