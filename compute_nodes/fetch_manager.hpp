////
///
///

#ifndef H_FETCH_MANAGER_H
#define H_FETCH_MANAGER_H

#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>

#include <boost/thread.hpp>

#include <sdt/utilities/logger.hpp>

#include "utilities/stdtime.hpp"

#include "data_sources/data_source_base.hpp"

#include "data_flow/data_flow_task.hpp"
#include "data_flow/background_task.hpp"

#include "jstream_types.hpp"

namespace JStream {


/////
///  Fetch Manager
class FetchManager : 
    public DataFlowTask<std::shared_ptr<T5122Frame> >
{
    /// Convenience typedefs
	typedef std::function<void ()>   
        TExceptionCallback;
    typedef WaitingQueuePage<std::shared_ptr<T5122Frame> >  
        TFramePendingQueue;
    typedef DataFlowTask<std::shared_ptr<T5122Frame> >  	
        TFrameSuperclass;
    typedef std::vector<std::shared_ptr<IDataSource> >  	
        TDataSourceCollection;
    typedef int16_t 			
        TFill;
    typedef Range<TFill *>		
        TFillRange;
	typedef Range<uint64_t *>   
        TTimeRange;
	typedef Range<double *> 	
        TCalRange;
        
    /// Logger
    LoggerAccess            m_logger;

    /// Data sources
    TDataSourceCollection   m_data_sources;
    
    /// Run properties
    bool                    m_is_realtime       = false;
    bool                    m_monitor_ifo_state = false;
    bool                    m_ifo_lock_state    = false;
    double                  m_sample_freq       = -1.0;
    unsigned                m_channels_num      = 0;
    unsigned                m_framed_length     = 0;
    unsigned                m_veto_thres;
    
    /// Wrapper for the function to be called after a hardware exception
	TExceptionCallback      m_exception_callback;

public:
    /// Constructor
    FetchManager(
		std::vector< shared_ptr<IDataSource> > data_sources,
        bool monitor_ifo_state,
        unsigned veto_thres,
        LoggerAccess logger
    );

    /// Return total number of channels
    unsigned channels_total_num();
    
    /// Return sample frequency (Hz)
    double sample_freq();

    /// Creates the time carrier frames
    void prefill_frame_setup(
		shared_ptr<T5122Frame> prototype, 
		unsigned num
    );

    ///  Sets the IFO lock state
    void ifo_lock_state_set(bool);    
    
    /// Sets the handler function to call after a hardware failure
    void hw_exception_callback_set(TExceptionCallback);        
        
protected:
    void compute_loop();	
};

}; //~namespace JStream

#endif //H_FETCH_MANAGER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! 
