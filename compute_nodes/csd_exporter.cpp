////
///

#include "csd_exporter.hpp"

namespace JStream {

using namespace uLogger;


/////
///  Constructor
CSDExporter::
    CSDExporter(
		unsigned num_chs,
		unsigned csd_length,
        double sample_freq,
        double overlap_frac,
        unsigned veto_thres,
        //std::vector< std::shared_ptr<const DataSourceConfig> > &adc_settings,
        LoggerAccess logger
	) :
		TCSDSuperclass(logger.context()),
		BackgroundTask(logger.context()),
        m_logger(logger),
		m_num_chs(num_chs),
		m_csd_length(csd_length),
        m_sample_freq(sample_freq),
        m_overlap_frac(overlap_frac),
        m_veto_thres(veto_thres)
		//m_adc_settings(adc_settings)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
}

/////
///  Sets the callback function for a new CSD frame accumulation
void CSDExporter::
	accumulation_callback_set(TAccumulationCallback callback)
{
	m_accumulation_callback = callback;
}

/////
///  Contains work loop
void CSDExporter::
    compute_loop()
{
    uint64_t	num_frames = 0;
    
    std::shared_ptr<TCSDCarrier> csd_collection;

    /// Locally-allocated container for the running CSD accumulation
    CSDCarrierDouble local_accumulation(
        m_num_chs, 
        m_csd_length, 
        m_sample_freq,
        m_overlap_frac,
        localization_manager()->memory_locality_get(m_logger.context())
    );

    /// Convenience access for the superflow queues
    TCSDSuperclass::TCirculationQueue &CSDCarrier_Q_in  = *this->TCSDSuperclass::queue_input_get();
    TCSDSuperclass::TCirculationQueue &CSDCarrier_Q_out = *this->TCSDSuperclass::queue_output_get();

	while(not this->should_quit()){
        /// Pull in the CSD superframe 			
        CSDCarrier_Q_in.pull_wait(csd_collection);
        if(this->should_quit()){
            break; 
        }
        assert(csd_collection);
        m_logger.detail("Averaging CSD Superframe #", csd_collection->sequence_number_get());

        if(csd_collection->error_flag_get() < m_veto_thres){
            /// Coadd the new frame into the running accumulation
            local_accumulation.csd_frame_add(*csd_collection);
            if(m_accumulation_callback){
                /// Call the accumulation server callback function
                m_accumulation_callback(local_accumulation);
            }
        }else{
            m_logger.action("Vetoing frame #", csd_collection->sequence_number_get());
        }

        /// Push out the CSD superframe         
        CSDCarrier_Q_out.push(std::move(csd_collection));
        csd_collection = std::shared_ptr<TCSDCarrier>(nullptr);
		num_frames++;
    }
    m_logger.digest("Accumulated a total of ", num_frames, " superframes");

    /// Compute & log the system throughput
    this->log_system_throughput(local_accumulation);

    /*
	/////
	///  Write the final CSD averages to an HDF file
    if(m_final_avg_output_file == "") {
        return; }
    else {
        std::cout << "COMM: Writing the final CSD averages log to \"" << m_final_avg_output_file << "\"....\n"; }

    auto frame_ptr = std::make_shared<CSDCarrier>(m_csd_averages);
    int  status    = -1.0;
    
#if JSTREAM_HAS_HDF5
    /// Initialize an HDF5 writer
    HDF5CSDWriter hdf5_csd_writer(m_adc_settings, frame_ptr);

    /// Create a new HDF5 file
    status = hdf5_csd_writer.new_file(m_final_avg_output_file);			
    if(status != 0) {
        m_logger.error("Could not create file for final CSD averages!\n");
        return;
    }
                
    /// Write the new CSD entry to file
    status = hdf5_csd_writer.write_csd(frame_ptr);
    if(status != 0) {
        m_logger.error("Final CSD averages write failed!"); 
        return;
    }
#endif //~JSTREAM_HAS_HDF5
    
    std::cout << "Done.\n";
    /// HDF5 shuts down + closes the file automatically in its destructor
    */
}

/////
///  Compute & log the system throughput
void CSDExporter::
    log_system_throughput(CSDCarrierDouble  &current_accumulation)
{
    double    duration_s        = (current_accumulation.timestamp_end() - current_accumulation.timestamp_begin()) / 1.e9;
    uint64_t  num_samples       = (current_accumulation.length() * 2) * current_accumulation.channels_num() 
                                    * current_accumulation.accumulation_get();                                    
    double    total_data        = num_samples * sizeof(int16_t);
    double    sample_throughput = num_samples / duration_s;
    double    data_throughput   = total_data / duration_s;
    
    m_logger.digest("System throughput:\t", sample_throughput/1e6, " MS/s\t[", data_throughput/1e9, " GB/s]\n\t",
                    "Total integration time:\t", duration_s, " s\n\t",
                    "Total data processed:\t", total_data/1e12, " TB"  
                   );
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
