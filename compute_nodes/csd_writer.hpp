////
///

#ifndef H_JSTREAM_CSD_WRITER_H
#define H_JSTREAM_CSD_WRITER_H

#include "jstream/config.hpp"
#if JSTREAM_HAS_HDF5

#include <iostream>
#include <cstdio>
#include <complex>
#include <algorithm>

#include <sdt/utilities/logger.hpp>

#include "utilities/config_tree/config_tree_json.hpp"

#include "data_sources/data_source_base.hpp"

#include "data_flow/data_flow_task.hpp"

#include "data_management/hdf5_writer.hpp"
#include "data_management/database_manager.hpp"

#include "jstream_types.hpp"

namespace JStream {
using namespace ConfigTree;


/////
///  CSDWriter class
class CSDWriter : public DataFlowTask<std::shared_ptr<TCSDCarrier> >
{
    /// Convenience typedefs
    typedef DataFlowTask< shared_ptr<TCSDCarrier> >     
        TCSDSuperclass;

    /// Logger
    LoggerAccess    m_logger;

    /// Writer settings
	unsigned		m_file_frame_limit;             //Number of CSD frames to write to a single HDF5 file
    std::string     m_filepath;                     //Root HDF5 archive path

	/// Stores the data source metadata
    std::vector<IDataSource::DataSourceSettings> 
                    m_source_metadata;

    DatabaseSingleRunPtr m_database_run;

public:
    /// Constructor
    CSDWriter(
        std::vector<IDataSource::DataSourceSettings> source_metadata,
        DatabaseSingleRunPtr m_database_run,
        double file_write_time_s,
        double csd_accum_time_s,
		LoggerAccess logger
    );
    
protected:
    void compute_loop();

};

}; //~namespace JStream

#endif //~JSTREAM_HAS_HDF5

#endif //H_JSTREAM_CSD_WRITER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

