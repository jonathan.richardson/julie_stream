////
///

#ifndef H_SATURATION_SCANNER_H
#define H_SATURATION_SCANNER_H

#include <algorithm>

#include <sdt/utilities/logger.hpp>

#include "utilities/range.hpp"

#include "analysis/apodization.hpp"

#include "data_flow/data_flow_task.hpp"

#include "jstream_types.hpp"

namespace JStream {


//////
///  Class for scanning the raw 5122 data for saturation (clipping)
class SaturationDetector : 
    public DataFlowTask<shared_ptr<T5122Frame> >
{
    /// Convenient for when we need to access the queues
  public:
    typedef DataFlowTask<shared_ptr<T5122Frame> >
        TFrameSuperclass;

  private:
    /// Logger
    SDT::LoggerAccess   m_logger;

    /// Number of channels
    unsigned            m_channels_n;
    
    /// Saturation tolerance level
    double              m_level;
    int16_t             m_max_range;
    int16_t             m_min_range;
    
    /// Timestamp of last warning
    unsigned            m_time_last_warn = 0;

    /// Error level at which to generate warnings
    unsigned            m_veto_thres;        
    
  public:
    SaturationDetector(
        double level,
        unsigned num_channels,
        unsigned veto_thres,
        SDT::LoggerAccess logger
    );

  protected:
    void compute_loop();

    /// Scans the specified channel timestream for bit saturation
    bool saturation_scan(
        unsigned chn_idx,
        T5122Frame &time_frame
    );
    
    /// Prints a custom warning message to the logger
    void warning_msg(
        std::vector<bool> &chn_state,
        unsigned timestamp
    );    
};

}; //~namespace JStream

#endif //H_SATURATION_SCANNER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

