////
///

#ifndef H_TIMESERIES_WRITER_H
#define H_TIMESERIES_WRITER_H

#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/config_tree/config_tree.hpp>

#include "utilities/range.hpp"

#include "analysis/apodization.hpp"

#include "data_flow/data_flow_task.hpp"
#include "data_management/database_manager.hpp"
#include "data_management/hdf5_ts_writer.hpp"

#include "jstream_types.hpp"

namespace JStream {


struct TimeseriesWriteNodeConfig
{
    bool     m_use_hdf5_ts_writer  = false;
    bool     m_write_on_clipping   = true;

    double   m_write_interval_s    = 60;
    double   m_rate_limit_s        = 60;
    double   m_new_file_interval_s = 600;

    uint32_t m_rate_limit_num      = 3;

    void parse_config_basic(ConfigTreeAccess);
};

//////
///  Class for scanning the raw 5122 data for saturation (clipping)
class TimeseriesWriteNode : 
    public DataFlowTask<shared_ptr<T5122Frame> >
{
    /// Convenient for when we need to access the queues
  public:
    typedef DataFlowTask<shared_ptr<T5122Frame> >
        TFrameSuperclass;

    typedef  HDF5TSWriter<T5122Frame> TWriter;
    typedef  std::shared_ptr<TWriter> TWriterPtr;

  private:
    //make const to prevent any worries about thread-safety between the 
    //detection thread and the writing thread
    const TimeseriesWriteNodeConfig m_config;

    /// Logger
    SDT::LoggerAccess         m_logger;

    TFrameSuperclass::TCirculationQueue m_inner_circulation;

    TWriterPtr                          m_hdf5_ts_writer;

  public:
    TimeseriesWriteNode(
        const TimeseriesWriteNodeConfig &config,
        TWriterPtr writer,
        SDT::LoggerAccess logger
    );

  protected:
    void compute_loop();

    //actually writes the data, uses m_inner_circulation to asynchronously get and write selected timeseries
    void write_loop();
};

}; //~namespace JStream

#endif //H_TIMESERIES_WRITER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

