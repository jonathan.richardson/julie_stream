////
///
///
///

#define BOOST_TEST_DYN_LINK
//#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <iostream>

#include <thread>
#include <chrono>
#include <functional>

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>

#include <boost/program_options.hpp>

#include "sdt/utilities/logger.hpp"

#include "queues/csd_accumulation_queue.hpp"


using namespace JStream;

namespace argparse = boost::program_options;
//BOOST_AUTO_TEST_CASE( initial_test )
//{
//}

namespace{
    auto logger = uLogger::logger_root().child("julie_stream");
}

int main(int argc, const char* argv[]) 
{	
    std::cout << "<<< JulieStream v" << JSTREAM_VERSION << " >>>" << std::endl ;

    /////
	///  First get the command-line options
	//TLogLevels 	verbosity_level;		//Verbosity level of the code output messages
    double 		time_to_run;			//Integration time
    string 		write_fname;			//Output filename for config settings used
	bool		calibrate = false;		//Switch to perform 5122 self-calibration
	size_t		hw_concurrency;			//Number of cores enabled
				
    try {
		/////
		///  The command-line options
        argparse::options_description desc("Allowed options");
        desc.add_options()
            ("help,h", 
                  "produce help message"
            )
            ("verbose,v", boost::program_options::value<int>()->default_value(5)->implicit_value(8),
                  "enable verbosity"      "\n"
                  " -10: fatal"           "\n"
                  " -5: error"            "\n"
                  " -2: warnings"         "\n"
                  "  0: digest"           "\n"
                  "  2: configuration"    "\n"
                  "  5: runtime"          "\n"
                  "  10: programming"     "\n"
                  "  10: debugging"
            )
            ("time,t",    argparse::value<double>()->default_value(10),
                  "Time (in seconds) to collect"                            "\n"
                  "  0: Only read configs (doesn't start queues)"           "\n"
                  " -1: collect indefinitely"
            )
            ("basic_config,b",  argparse::value< string >()->default_value("<MISSING>"), 
                  "Configuration file: Basic settings override"
            )
            ("advanced_config,a",  argparse::value< string >()->default_value("<MISSING>"), 
                "Configuration file: Advanced settings override"
            )
            ("effective-config,e",  argparse::value< string >()->implicit_value("-"),
                  "Write config file with defaults filled in (\"-\" writes to stdout)"
            )
            ("calibrate_5122,c",  argparse::value(&calibrate)->zero_tokens(),
                  "Perform a 5122 self-calibration at startup"
            )
        ;

        argparse::variables_map option_variables;
        argparse::store(argparse::command_line_parser(argc, argv).options(desc).run(), option_variables);
        argparse::notify(option_variables);
   
        if (option_variables.count("help")) {
            std::cout << desc;
            return 0;
        }
		
        uLogger::TLoggingLevels verbosity_level = (option_variables["verbose"].as<TLoggingLevels>());
        logger.detail("Verbosity level: ", int(verbosity_level));

		/////
		///  Set the integration time
        time_to_run = option_variables["time"].as<double>();
		if(time_to_run > 0) { std::cout << "Integration time: " << time_to_run << " s\n"; }
		if(time_to_run == -1) { std::cout << "Integration time: FOREVER\n"; }
        if( (time_to_run < 0) and (time_to_run != -1) ) {
            logger.fatal("Are you kidding me? You can't run for negative time!\n"
                               "Use -1 to run indefinitely"
                              );
            return -3;
        }

        if (option_variables.count("effective-config")) {
            logger.detail("effective-config: ", option_variables["effective-config"].as<std::string>());
            return 0;
        }

    }
    catch(std::exception& e) {
        logger.fatal(e.what());
        return -1;
    }
}


//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

