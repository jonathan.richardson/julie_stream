////
///
///
///

#define BOOST_TEST_DYN_LINK
//#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <iostream>

#include <thread>
#include <chrono>
#include <functional>

#include <boost/asio.hpp> //needs to be first for winsock defs
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>

#include <boost/program_options.hpp>

#include "sdt/utilities/logger.hpp"

#include "queues/csd_accumulation_queue.hpp"
#include "jstream_bridge/jstream_client.hpp"

using namespace JStream;

namespace argparse = boost::program_options;
//BOOST_AUTO_TEST_CASE( initial_test )
//{
//}

namespace{
    auto logger = uLogger::logger_root().child("julie_stream");
}

int main(int argc, const char* argv[]) 
{	
    std::cout << "<<< JulieStream v" << JSTREAM_VERSION << " >>>" << std::endl ;
    JStreamBridgeClient* jsbc = new JStreamBridgeClient("tcp://localhost:1234");
    std::cout<<"call start"<<std::endl;
    jsbc->start();
    std::cout<<"call stop"<<std::endl;
    jsbc->stop();
    std::cout<<"done"<<std::endl;
}


//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

