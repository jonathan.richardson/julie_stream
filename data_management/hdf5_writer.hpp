////
///

#ifndef H_JSTREAM_HDF5_WRITER_H
#define H_JSTREAM_HDF5_WRITER_H

#include "jstream/config.hpp"
#if JSTREAM_HAS_HDF5

#include <memory>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

#include <hdf5.h>
#include <H5Cpp.h>

#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>

#include "utilities/stdtime.hpp"

#include "hdf5_utilities.hpp"
#include "analysis/csd_carrier_float.hpp"
#include "analysis/csd_carrier_double.hpp"

namespace JStream {


/////
///  HDF5 writer class
template <typename TCSDCarrier>
class HDF5CSDWriter
{
    /// Logger
    SDT::LoggerAccess       m_logger;
    
    /// HDF5 archive path
    std::string             m_filepath;

    /// HDF5 handles
    H5::H5File              m_file;
    H5::DataSpace           m_psd_dataspace;
    H5::DataSpace           m_csd_dataspace;
    H5::DataSpace           m_time_dataspace;
    H5::DataSpace           m_voltage_dataspace;
    H5::DataSpace           m_config_dataspace;
    H5::DataSpace           m_5122_dataspace;
    H5::DataSpace           m_time_memspace;
    H5::DataSpace           m_voltage_min_memspace;
    H5::DataSpace           m_voltage_max_memspace;
    H5::CompType            m_time_tid;
    H5::CompType            m_config_tid;
    H5::CompType            m_5122_tid;
    H5::DSetCreatPropList   m_time_create;
    H5::DSetCreatPropList   m_voltage_create;
    H5::DataSet             m_time_dataset;
    H5::DataSet             m_voltage_min_dataset;
    H5::DataSet             m_voltage_max_dataset;
    H5::Group               m_data_entry;
    H5::DataType            m_datatype;
 
    /// Data source & run metadata
    std::vector<IDataSource::DataSourceSettings>
                            m_source_metadata;
    _detail_h5::RunSettings        
                            m_run_metadata;

    /// Frame counter (for timestamp dataset extension)
    unsigned                m_num_frames;

public:
    /// Constructor
    HDF5CSDWriter(
        std::vector<IDataSource::DataSourceSettings> source_metadata,
        const TCSDCarrier &csd,
        const std::string &filepath,
        SDT::LoggerAccess logger
    );
    ~HDF5CSDWriter();

    /// Create a new h5 file
    std::string new_file(uint64_t timestamp);

    /// Write a CSD frame
    void write_csd(const TCSDCarrier &csd);
    
protected:
    /// Creates the custom HDF5 datatypes and containers
    void _hdf5_setup();
    
};

extern template class HDF5CSDWriter<CSDCarrierDouble>;
extern template class HDF5CSDWriter<CSDCarrierFloat>;
}; //~namespace JStream

#include "hdf5_writer_T.hpp"
#endif //~JSTREAM_HAS_HDF5

#endif // H_JSTREAM_HDF5_WRITER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
