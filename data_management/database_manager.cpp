/////
///

#include "database_manager.hpp"

namespace JStream {


/////
///  Database manager constructor
DatabaseManager::
	DatabaseManager(
        std::string	database_path,
        ConfigTreeJsonRoot adv_config,
        SDT::LoggerAccess logger
    ) :
        m_database_path(database_path),
        m_adv_config(adv_config),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
}

/////
///  Creates the JSON run log
DatabaseSingleRunPtr DatabaseManager::
    register_run(
        const std::string oper_name,
        const std::string comments,
        const std::vector<std::string> sources,
        ConfigTreeJsonRoot run_config        
    )
{
    std::string run_id;
    std::string run_path;
    std::string run_log_path;

    {   /// Assign the first available run ID
        unsigned run_id_n = 1;
        while(true){
            std::stringstream id;
            id << std::setw(5) << std::setfill('0') << run_id_n; 
            run_id = id.str();
            run_path = m_database_path + "/" + run_id;        
            if(not boost::filesystem::exists(run_path)){
                break;
            }
            run_id_n += 1;
        }
        m_logger.action("Run ID: ", run_id); 
    }
    
    {   /// Create a new run subdirectory
        auto subdir = boost::filesystem::path(run_path);
        boost::filesystem::create_directory(subdir);     
    }
      
    {    /// Create the run log
        ConfigTreeJsonRoot root("run", m_logger);     
        auto run = root.access();    
        run["run_id"].set(run_id);
        run["operator"].set(oper_name);
        run["comments"].set(comments);    
        ////Dont set the times yet, as the run hasn't been updated even once
        //run["time_beg"].set();
        //run["time_end"].set(intmax_t(start_time_s)); 
        for(auto src_idx : icount(sources.size())){
            run["sources"][src_idx].set(sources[src_idx]);
        }

        /// Write the run log to the subdirectory
        run_log_path = run_path + "/run_log.json";    
        root.effective_config_write(run_log_path);        
    }
    
    {   /// Write the advanced DAQ configuration to the subdirectory
        auto config_path = run_path + "/adv_config_settings.json";    
        m_adv_config.effective_config_write(config_path);
    }
    
    {   /// Write the run configuration to the subdirectory
        auto config_path = run_path + "/run_config_settings.json";    
        run_config.effective_config_write(config_path);
    }

    return std::make_shared<DatabaseSingleRun>(
        run_id,
        run_path,
        run_log_path,
        m_logger.child("run")
    );
}

/////
///  Processes a database search request
void DatabaseManager::
    search(
        const std::string &query,
        SearchHitTransmitter transmitter,
        TransmissionTerminator terminator
    )
{
    int num_hits = -1;
    
    try{  
        /// Parse the remotely-supplied JSON search query      
        ConfigTreeJsonRoot root("search", m_logger);
        auto parsed = root.parse_string(query);
        if(not parsed){
            throw std::runtime_error("JSON run data string parse failed!");
        }       
        
        /// Assign a search method based on the supplied search parameters
        auto search_params = root.access();           
        auto search_engine = this->search_engine_selector(search_params);
        switch(search_engine){
            case 1:
                {   /// Search by run ID
                    std::string run_id;
                    search_params["run_id"].get(run_id);                   
                    m_logger.detail("Search request received for run #", run_id);                
                    num_hits = this->_search_by_run_id(
                        run_id,
                        transmitter
                    );
                    break;
                }

            case 2:
                {   /// Search by operator name
                    std::string op_name;
                    search_params["operator"].get(op_name);
                    m_logger.detail("Search request received for operator: ", op_name);                
                    num_hits = this->_search_by_operator(
                        op_name,
                        transmitter
                    );
                    break;
                }

            case 3:
                {   /// Search by time range
                    uint64_t t_beg, t_end;
                    search_params["time_beg"].get(t_beg, 0, 2E9);
                    search_params["time_end"].get(t_end, t_beg, 2E9);
                    m_logger.detail("Search request received for time range:\n\t", t_beg, " - ", t_end);
                    num_hits = this->_search_by_time_range(
                        t_beg, 
                        t_end,
                        transmitter
                    );
                    break;
                }
                
            default:
                throw std::logic_error("SHOULD NEVER REACH");
        }
        m_logger.detail("Database search complete. Total hits:\t", num_hits);   
    }catch(std::runtime_error &error){
        m_logger.error(error.what());
    }
    terminator(num_hits);    
}

/////
///  Returns the appropriate search method code based on the supplied search parameters
unsigned DatabaseManager::
    search_engine_selector(ConfigTreeAccess &search_params)
{
    unsigned        search_engine;   
    std::string     run_id,
                    op_name;
    uint64_t        t_beg, 
                    t_end;
  
    /// Load the search parameter values
    search_params["run_id"].get(run_id, true);  
    search_params["operator"].get(op_name, true);
    search_params["time_beg"].get(t_beg, 0, 2E9, true);
    search_params["time_end"].get(t_end, t_beg, 2E9, true);    
    
    /// Set the correct search code 
    if(not run_id.empty()){
        search_engine = 1;
    }else if(not op_name.empty()){
        search_engine = 2;
    }else if(t_beg >= 0 and t_end > t_beg){
        search_engine = 3;
    }else{
        throw std::runtime_error("Invalid search parameter(s)");
    }
    return search_engine;
}

/////
///  Processes a database search by run ID
unsigned DatabaseManager::
    _search_by_run_id(
        const std::string &run_id,
        SearchHitTransmitter transmitter
    )
{
    /// Get the log for the specified run
    auto run_log = this->run_lookup(run_id);
    if(run_log.empty()){
        /// Run not found
        return 0;
    }

    /// Transmit the search hit to the remote client
    auto sent = transmitter(run_log);
    if(not sent){
        throw std::runtime_error("Search result transfer failed!");
    }
    return 1;
}

/////
///  Processes a database search by operator name
unsigned DatabaseManager::
    _search_by_operator(
        const std::string &operator_name,
        SearchHitTransmitter transmitter
    )
{
    unsigned num_hits = 0;            
    for(auto run_id = 1; ; run_id += 1){
        /// Get the log for this run
        auto run_log = this->run_lookup(run_id);
        if(run_log.empty()){
            break;
        }

        /// Parse the log
        ConfigTreeJsonRoot root("search", m_logger);
        auto parsed = root.parse_string(run_log);
        if(not parsed){
            throw std::runtime_error("JSON run data string parse failed!");
        }
        
        /// Check whether the run is owned by the specified operator  
        std::string op_name;        
        auto data = root.access();        
        data["operator"].get(op_name);
        if(op_name.compare(operator_name) != 0){
            continue;
        }
        
        /// Transmit the search hit to the remote client
        auto sent = transmitter(run_log);
        if(not sent){
            throw std::runtime_error("Search result transfer failed!");
        }
        num_hits += 1;        
    }
    return num_hits;
}

/////
///  Processes a database search by date range
unsigned DatabaseManager::
    _search_by_time_range(
        uint64_t time_beg,
        uint64_t time_end,
        SearchHitTransmitter transmitter
    )
{    
    unsigned num_hits = 0;            
    for(auto run_id = 1; ; run_id += 1){
        /// Get the log for this run
        auto run_log = this->run_lookup(run_id);
        if(run_log.empty()){
            break;
        }
        
        /// Parse the log
        ConfigTreeJsonRoot root("search", m_logger);
        auto parsed = root.parse_string(run_log);
        if(not parsed){
            throw std::runtime_error("JSON run data string parse failed!");
        }        
        
        /// Check whether the run is within the specified time window   
        uint64_t t_beg, t_end;
        auto data = root.access();
        data["time_beg"].get(t_beg, 0, 2E9);
        data["time_end"].get(t_end, t_beg, 2E9); 
        if(t_end < time_beg){
            continue;
        }else if(t_beg > time_end){
            break;
        }
        
        /// Transmit the search hit to the remote client
        auto sent = transmitter(run_log);
        if(not sent){
            throw std::runtime_error("Search result transfer failed!");
        }
        num_hits += 1;        
    }
    return num_hits;
}

/////
///  Checks whether the specified run exists and returns its log
std::string DatabaseManager::
    run_lookup(unsigned run_id)
{
    std::ostringstream SS;
    SS << std::setw(5) << std::setfill('0') << run_id;
    return this->run_lookup(SS.str()); 
}

/////
///  Checks whether the specified run exists and returns its log data
std::string DatabaseManager::
    run_lookup(const std::string &run_id)
{
    std::string run_log;

    /// Check whether the run exists
    std::ostringstream SS;
    SS << m_database_path << "/" << run_id << "/" << "run_log.json";
    auto run_log_path = SS.str(); 
    if(boost::filesystem::exists(run_log_path)){
        /// Get the size of the run log file
        std::ifstream fstream(run_log_path);
        fstream.seekg(0, fstream.end);
        int fsize = fstream.tellg();
        fstream.seekg(0, fstream.beg);

        /// Read the run log into the JSON string 
        char *buffer = new char[fsize];
        fstream.read(buffer, fsize);
        run_log = std::string(buffer, fsize);         
    }
    return run_log;
}

/////
///  Send each frame in the requested range to the client
void DatabaseManager::
    sequential_transfer(
        const std::string &query,
        MetadataTransmitter meta_transmitter,
        CSDFloatTransmitter csd_transmitter,
        TransmissionTerminator terminator
    )
{
    int64_t send_ct = 0;

    try{
        /// Get the transfer request parameters
        std::string run_id;
        intmax_t    t_beg, t_end;      
        {
            /// Parse the transfer request
            ConfigTreeJsonRoot root("transfer", m_logger);
            auto parsed = root.parse_string(query);
            if(not parsed){
                throw std::runtime_error("JSON run data string parse failed!");
            }
            
            /// Load the run parameters
            root.access()["run_id"].get(run_id);
            root.access()["time_beg"].get(t_beg, 0, 2E9);
            root.access()["time_end"].get(t_end, t_beg, 2E9);
        }
        
        /// Get the list of h5 filenames for the requested run
        std::vector<std::string> h5_file_list;
        {
            /// Retrieve the log for this run
            auto run_log = this->run_lookup(run_id);
            if(run_log.empty()){
                throw std::runtime_error("Cannot find the specified run");
            }
        
            /// Parse the run log
            ConfigTreeJsonRoot root("transfer", m_logger);
            auto parsed = root.parse_string(run_log);
            if(not parsed){
                throw std::runtime_error("JSON run data string parse failed!");
            }
        
            /// Read in the h5 filenames
            auto h5_conf = root.access()["h5_files"];
            auto num_files = h5_conf.array_size();
            h5_file_list.resize(num_files);
            for(auto idx : icount(num_files)){
                std::string fname;
                h5_conf[idx].get(fname);
                h5_file_list[idx] = m_database_path + "/" + run_id + "/" + fname;
            }
        }
        
        /// Read the HDF5 files
        { 
            uint64_t        frame_id        = 0,
                            start_time_ns   = 0;  
            HDF5CSDReader<CSDCarrierFloat> hdf5_reader(m_logger);
            for(auto h5_fpath : h5_file_list){
                /// Open the next h5 file read-only
                hdf5_reader.open_file(h5_fpath);
                if(frame_id == 0){
                    /// Set the run start time
                    start_time_ns = hdf5_reader.timestamps()[0].t_begin;         

                    /// Send the hardware metadata to the client            
                    for(auto source_settings : hdf5_reader.hw_settings()){
                        auto sent = meta_transmitter(source_settings);
                        if(not sent){
                            throw std::runtime_error("Metadata transfer failed!");
                        }
                    }
                }
              
                /// Read in the CSD data
                auto timestamps = hdf5_reader.timestamps();
                auto num_frames = timestamps.size();
                for(auto frame_idx : icount(num_frames)){
                    /// Check whether the frame is within the specified time window   
                    auto frame_t_beg = (timestamps[frame_idx].t_begin - start_time_ns) / 1E9;
                    auto frame_t_end = (timestamps[frame_idx].t_end - start_time_ns) / 1E9;  
                    if(frame_t_end < t_beg){
                        frame_id += 1;
                        continue;
                    }else if(frame_t_beg > t_end){
                        break;
                    }

                    /// Read in the CSD frame
                    auto csd_frame = hdf5_reader.read_csd(
                        frame_id,
                        frame_idx
                    );
                    
                    /// Send the frame to the remote client
                    auto sent = csd_transmitter(csd_frame);
                    if(not sent){
                        throw std::runtime_error("CSD transfer failed!");
                    }
                    send_ct  += 1;
                    frame_id += 1;
                }//~frame loop             
            }//~h5 file loop 
        }
        m_logger.detail("Transfer request complete. Total frames sent:\t", send_ct);  
    }catch(std::runtime_error &error){
        m_logger.error(error.what());
        send_ct = -1;
    }
    terminator(send_ct);
}

/////
///  Send only the average over the requested range to the client
void DatabaseManager::
    preaccum_transfer(
        const std::string &query,
        MetadataTransmitter meta_transmitter,
        CSDDoubleTransmitter csd_transmitter,
        TransmissionTerminator terminator
    )
{
    int64_t send_ct = 0;

    try{
        /// Get the transfer request parameters
        std::string run_id;
        intmax_t    t_beg, t_end;      
        {
            /// Parse the transfer request
            ConfigTreeJsonRoot root("transfer", m_logger);
            auto parsed = root.parse_string(query);
            if(not parsed){
                throw std::runtime_error("JSON run data string parse failed!");
            }
            
            /// Load the run parameters
            root.access()["run_id"].get(run_id);
            root.access()["time_beg"].get(t_beg, 0, 2E9);
            root.access()["time_end"].get(t_end, t_beg, 2E9);
        }
        
        /// Get the list of h5 filenames for the requested run
        std::vector<std::string> h5_file_list;
        {
            /// Retrieve the log for this run
            auto run_log = this->run_lookup(run_id);
            if(run_log.empty()){
                throw std::runtime_error("Cannot find the specified run");
            }
        
            /// Parse the run log
            ConfigTreeJsonRoot root("transfer", m_logger);
            auto parsed = root.parse_string(run_log);
            if(not parsed){
                throw std::runtime_error("JSON run data string parse failed!");
            }
        
            /// Read in the h5 filenames
            auto h5_conf = root.access()["h5_files"];
            auto num_files = h5_conf.array_size();
            h5_file_list.resize(num_files);
            for(auto idx : icount(num_files)){
                std::string fname;
                h5_conf[idx].get(fname);
                h5_file_list[idx] = m_database_path + "/" + run_id + "/" + fname;
            }
        }
        
        /// Read the HDF5 files
        { 
            uint64_t        frame_id        = 0,
                            accum_ct        = 0,
                            start_time_ns   = 0;  
            HDF5CSDReader<CSDCarrierFloat> hdf5_reader(m_logger);
            std::shared_ptr<CSDCarrierDouble> csd_accum;
            for(auto h5_fpath : h5_file_list){
                /// Open the next h5 file read-only
                hdf5_reader.open_file(h5_fpath);
                if(frame_id == 0){
                    /// Set the run start time
                    start_time_ns = hdf5_reader.timestamps()[0].t_begin;    

                    /// Send the hardware metadata to the client            
                    for(auto source_settings : hdf5_reader.hw_settings()){
                        auto sent = meta_transmitter(source_settings);
                        if(not sent){
                            throw std::runtime_error("Metadata transfer failed!");
                        }
                    }                    
                }
              
                /// Read in the CSD data
                auto timestamps = hdf5_reader.timestamps();
                auto num_frames = timestamps.size();
                for(auto frame_idx : icount(num_frames)){
                    /// Check whether the frame is within the specified time window   
                    auto frame_t_beg = (timestamps[frame_idx].t_begin - start_time_ns) / 1E9;
                    auto frame_t_end = (timestamps[frame_idx].t_end - start_time_ns) / 1E9;  
                    if(frame_t_end < t_beg){
                        frame_id += 1;
                        continue;
                    }else if(frame_t_beg > t_end){
                        break;
                    }

                    /// Read in the CSD frame
                    auto csd = hdf5_reader.read_csd(
                        frame_id,
                        frame_idx
                    );
                    
                    /// Accumulate the frame into the local container
                    if(not csd_accum){
                        csd_accum = std::make_shared<CSDCarrierDouble>(
                            csd.channels_num(),
                            csd.length(),
                            csd.sample_freq_get(),
                            csd.overlap_frac_get()
                        );
                    }
                    csd_accum->csd_frame_add(csd);
                    accum_ct += 1;
                    frame_id += 1;
                }//~frame loop
            }//~h5 file loop
            if(accum_ct > 0){
                /// Send the final, accumulated frame to the remote client
                auto sent = csd_transmitter(*csd_accum);
                if(not sent){
                    throw std::runtime_error("CSD transfer failed!");
                }
                send_ct = 1;
            }
        }
        m_logger.detail("Transfer request complete. Total frames sent:\t", send_ct);  
    }catch(std::runtime_error &error){
        m_logger.error(error.what());
        send_ct = -1;
    }
    terminator(send_ct);
}

/////
///  Database run entry constructor
DatabaseSingleRun::
    DatabaseSingleRun(
        std::string run_id,
        std::string run_path,
        std::string run_log_path,
        SDT::LoggerAccess logger
    ) : 
        m_logger(logger),
        m_run_id(run_id),
        m_run_path(run_path),
        m_run_log_path(run_log_path)
{}

/////
///  Update the database run log
void DatabaseSingleRun::
    update_run(
        uint64_t beg_time_s,
        uint64_t end_time_s,
        std::string hdf5_fname
    )
{
    std::string         type_str;
    intmax_t            type_int;
    ConfigTreeJsonRoot  root("run", m_logger);   
    
    /// Load the current log file
    if(not root.parse_file(m_run_log_path)){
        m_logger.error("Must register this run in the database first");
        return;
    }
    auto run = root.access();
    
    /// Copy over the immutable data
    run["run_id"].get(type_str);
    run["run_id"].set(type_str);
    run["operator"].get(type_str);
    run["operator"].set(type_str);
    run["comments"].get(type_str);
    run["comments"].set(type_str);
    if(run["time_beg"].is_null()){
        type_int = intmax_t(beg_time_s);
    }else{
        run["time_beg"].get(type_int);
    }
    run["time_beg"].set(type_int);    
    for(auto src_idx : icount(run["sources"].array_size())){
        run["sources"][src_idx].get(type_str);
        run["sources"][src_idx].set(type_str);
    }                
    
    /// Update the run end time
    run["time_end"].set(intmax_t(end_time_s));
    
    /// Update the h5 file list
    auto num_files = run["h5_files"].array_size();
    for(auto file_idx : icount(num_files)){
        run["h5_files"][file_idx].get(type_str);
        run["h5_files"][file_idx].set(type_str);
    }
    run["h5_files"][num_files].set(hdf5_fname);
    
    /// Write the updated run log
    root.effective_config_write(m_run_log_path);        
}

std::string DatabaseSingleRun::
    fpath_get() const 
{
    return m_run_path;
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
