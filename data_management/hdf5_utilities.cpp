////
///

#include "hdf5_utilities.hpp"

namespace JStream {
using std::string;


/////
///  CREATE THE FILENAME LABEL
std::string get_file_label(uint64_t timestamp)
{
    /// Convert the timestamp to local calendar time
    time_t sec_since_epoch = static_cast<time_t>(timestamp / unsigned(1e9));
    const struct tm *local_time = localtime(&sec_since_epoch);

    /// Create the formatted filename string
    std::stringstream ss;
    ss << std::setfill('0')
       << std::setw(4) << (local_time->tm_year + 1900) << "-"
       << std::setw(2) << (local_time->tm_mon + 1) << "-"
       << std::setw(2) << local_time->tm_mday << "_"
       << std::setw(2) << local_time->tm_hour << "."
       << std::setw(2) << local_time->tm_min << "."
       << std::setw(2) << local_time->tm_sec << ".h5";
    return ss.str();
}

/////
///  CREATE THE GROUP NAME LABEL
std::string get_frame_label(uint64_t sequence_number)
{ 
    std::stringstream ss;
    ss << std::setfill('0') << std::setw(10) << sequence_number;
    return ss.str();
}

/////
///  CREATE THE CSD LABEL
std::string get_csd_label(
        unsigned row_idx,
        unsigned col_idx
    )
{
    std::stringstream ss;
    ss << "<" << row_idx << col_idx << ">";
    return ss.str();
}    

/////
///  Checks supplied metadata for consistency
///  (Throws std::runtime_error on failure)
void HDF5MetadataChecker::
    consistency_check(
        _detail_h5::RunSettings run_settings,
        std::vector<IDataSource::DataSourceSettings> hw_settings
    )
{
    if(m_hw.size() == 0){
        /// Set the initial metadata
        m_run   = run_settings;
        m_hw    = hw_settings;
        return;
    }
    
    /// Check run settings
    if(
        run_settings.num_chs                != m_run.num_chs                or         
        run_settings.num_freq_bins          != m_run.num_freq_bins          or    
        run_settings.sample_freq            != m_run.sample_freq            or       
        run_settings.ENBW                   != m_run.ENBW                   or               
        run_settings.normalization_to_ps    != m_run.normalization_to_ps    or  
        run_settings.frame_accumulation     != m_run.frame_accumulation     or	  
        run_settings.overlap_frac           != m_run.overlap_frac         
    ){
        throw std::runtime_error("Inconsistent run settings detected!");
    }
    
    /// Check hardware settings for each data source
    if(hw_settings.size() != m_hw.size()){
        throw std::runtime_error("Inconsistent number of data sources detected!");
    }    
    for(auto idx : icount(m_hw.size())){
        if(
            std::strcmp(hw_settings[idx].adc_name, m_hw[idx].adc_name)     != 0     or
            std::strcmp(hw_settings[idx].adc_type, m_hw[idx].adc_type)     != 0     or
            std::strcmp(hw_settings[idx].pll_status, m_hw[idx].pll_status) != 0     or            
            hw_settings[idx].sample_freq    != m_hw[idx].sample_freq                or
            hw_settings[idx].input_range	!= m_hw[idx].input_range                or
            hw_settings[idx].dc_offset      != m_hw[idx].dc_offset                  or
            hw_settings[idx].scaling        != m_hw[idx].scaling                    or
            hw_settings[idx].bandwidth      != m_hw[idx].bandwidth                  or
            hw_settings[idx].impedance      != m_hw[idx].impedance                  or
            hw_settings[idx].coupling  	    != m_hw[idx].coupling                              
        ){
            throw std::runtime_error("Inconsistent hardware settings detected!");
        }
    }
}

}; //~namespace JStream
