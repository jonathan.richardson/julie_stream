////
///

#ifndef H_DATABASE_MANAGER_H
#define H_DATABASE_MANAGER_H

#include "jstream/config.hpp"
#if JSTREAM_HAS_HDF5

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstring>

#include <boost/filesystem.hpp>

#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>
#include <sdt/utilities/config_tree/config_tree_json.hpp>

#include "utilities/range.hpp"

#include "data_sources/data_source_base.hpp"

#include "analysis/csd_carrier_float.hpp"
#include "analysis/csd_carrier_double.hpp"

#include "hdf5_reader.hpp"

namespace JStream {

using ConfigTree::ConfigTreeAccess;
using ConfigTree::ConfigTreeJsonRoot;
class DatabaseSingleRun;
typedef std::shared_ptr<DatabaseSingleRun> DatabaseSingleRunPtr;


/////
///  Database manager class
class DatabaseManager
{
public:
    /// Required functor types for remote data transmission
    /// (The appropriate transmitter must be supplied by the calling server)
	typedef std::function<bool(const std::string &)>
        SearchHitTransmitter;          
	typedef std::function<bool(const IDataSource::DataSourceSettings &)>   
        MetadataTransmitter;
	typedef std::function<bool(const CSDCarrierFloat &)> 
        CSDFloatTransmitter;
	typedef std::function<bool(const CSDCarrierDouble &)> 
        CSDDoubleTransmitter;        
    typedef std::function<bool(int)>
        TransmissionTerminator;

private:              
    /// Logger
    SDT::LoggerAccess           m_logger;
    
    /// Path to central HDF5 database
    std::string	                m_database_path; 
    
    /// Advanced DAQ configuration
    ConfigTreeJsonRoot          m_adv_config;    
    
public:
    /// Constructor
    DatabaseManager(
        std::string	database_path,
        ConfigTreeJsonRoot adv_config,        
        SDT::LoggerAccess logger
    );
    
    /////////////////////////////
    ///  Database write interface
    /////////////////////////////
        
    /// Registers a new run in the HDF5 database
    DatabaseSingleRunPtr register_run(
        const std::string oper_name,
        const std::string comments,
        const std::vector<std::string> sources,
        ConfigTreeJsonRoot run_config        
    );
    
    /////////////////////////////
    ///  Database read interface
    /////////////////////////////
    
    /// Database search
    void search(
        const std::string &query,
        SearchHitTransmitter transmitter,
        TransmissionTerminator terminator
    );    
    
    /// Database transfer of all in-range frames
    void sequential_transfer(
        const std::string &query,
        MetadataTransmitter meta_transmitter,
        CSDFloatTransmitter csd_transmitter,
        TransmissionTerminator terminator
    );
    
    /// Database transfer of pre-accumulated average over range
    void preaccum_transfer(
        const std::string &query,
        MetadataTransmitter meta_transmitter,
        CSDDoubleTransmitter csd_transmitter,
        TransmissionTerminator terminator   
    );
    
protected:
    /// Inner database search methods
    unsigned _search_by_run_id(
        const std::string &run_id,
        SearchHitTransmitter transmitter
    );
    unsigned _search_by_operator(
        const std::string &run_id,
        SearchHitTransmitter transmitter
    );    
    unsigned _search_by_time_range(
        uint64_t time_beg,
        uint64_t time_end,
        SearchHitTransmitter transmitter
    );
    
    ///  Returns the appropriate search method code based on the supplied search parameters
    unsigned search_engine_selector(ConfigTreeAccess &search_params);
  
    /// Checks whether the specified run exists and returns its log data
    std::string run_lookup(unsigned);
    std::string run_lookup(const std::string &);
};

class DatabaseSingleRun
{
    SDT::LoggerAccess           m_logger;
    /// Paths/ID for the current run
    std::string                 m_run_path;
    std::string                 m_run_log_path;
    std::string                 m_run_id;

public:
    DatabaseSingleRun(
        std::string run_id,
        std::string run_path,
        std::string run_log_path,
        SDT::LoggerAccess logger
    );

    /// Updates the log file of an existing run
    void update_run(
        uint64_t beg_time_s,
        uint64_t end_time_s,
        std::string hdf5_fname
    );    

    std::string fpath_get() const;
};

}; //~namespace JStream

#endif //~JSTREAM_HAS_HDF5

#endif //H_DATABASE_MANAGER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
