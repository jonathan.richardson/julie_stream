////
///

#include "hdf5_ts_writer.hpp"

namespace JStream {


/////
///  Constructor
template<typename T5122Frame>
HDF5TSWriter<T5122Frame>::
    HDF5TSWriter(
        std::vector<IDataSource::DataSourceSettings> source_metadata,
        const std::string &filepath,
        SDT::LoggerAccess logger
    ) :
        m_source_metadata(source_metadata),
        m_filepath(filepath),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
    
    /// Configure HDF5
    this->_hdf5_setup();
}

template<typename T5122Frame>
HDF5TSWriter<T5122Frame>::
    ~HDF5TSWriter()
{
    m_logger.action("Finalizing time series file");
}


/////
///  Creates the custom HDF5 datatypes and containers
template<typename T5122Frame>
void HDF5TSWriter<T5122Frame>::
    _hdf5_setup()
{
    try{
        /// Suppress automatic error printing
        H5::Exception::dontPrint();
        
        /// Create the dataspaces
        
        { /// Timestamps (extendible)
            hsize_t size[1]     = {1};
            hsize_t max_size[1] = {H5S_UNLIMITED};
            m_time_dataspace    = H5::DataSpace(1, size, max_size);

            /// Must enable chunking for an extendible dataspace
            hsize_t chunk_size[1] = {1};    
            m_time_create.setChunk(1, chunk_size);
        }
        
        { /// 5122 device settings
            hsize_t size[1]  = {m_source_metadata.size()};
            m_5122_dataspace = H5::DataSpace(1, size);
        }
         
        /// Create the compound data types

        /// Data source hardware settings  
        m_5122_tid = H5::CompType(sizeof(IDataSource::DataSourceSettings));
        m_5122_tid.insertMember(
            "adc_name", 
            HOFFSET(IDataSource::DataSourceSettings, adc_name), 
            H5::StrType(H5::PredType::C_S1, STR_LEN)
        );
        m_5122_tid.insertMember(
            "adc_type", 
            HOFFSET(IDataSource::DataSourceSettings, adc_type), 
            H5::StrType(H5::PredType::C_S1, STR_LEN)
        );  
        m_5122_tid.insertMember(
            "pll_status", 
            HOFFSET(IDataSource::DataSourceSettings, pll_status), 
            H5::StrType(H5::PredType::C_S1, STR_LEN)
        );  
        m_5122_tid.insertMember(
            "sample_freq", 
            HOFFSET(IDataSource::DataSourceSettings, sample_freq), 
            H5::PredType::NATIVE_DOUBLE
        );
        m_5122_tid.insertMember(
            "input_range", 
            HOFFSET(IDataSource::DataSourceSettings, input_range), 
            H5::PredType::NATIVE_DOUBLE
        );
        m_5122_tid.insertMember(
            "dc_offset", 
            HOFFSET(IDataSource::DataSourceSettings, dc_offset), 
            H5::PredType::NATIVE_DOUBLE
        );
        m_5122_tid.insertMember(
            "scaling", 
            HOFFSET(IDataSource::DataSourceSettings, scaling), 
            H5::PredType::NATIVE_DOUBLE
        );
        m_5122_tid.insertMember(
            "bandwidth", 
            HOFFSET(IDataSource::DataSourceSettings, bandwidth), 
            H5::PredType::NATIVE_DOUBLE
        );
        m_5122_tid.insertMember(
            "impedance", 
            HOFFSET(IDataSource::DataSourceSettings, impedance), 
            H5::PredType::NATIVE_DOUBLE
        );
        m_5122_tid.insertMember(
            "coupling", 
            HOFFSET(IDataSource::DataSourceSettings, coupling), 
            H5::PredType::NATIVE_INT
        );
        
        /// Timestamps      
        m_time_tid = H5::CompType(sizeof(_detail_h5::Timestamps));
        m_time_tid.insertMember(
            "t_begin", 
            HOFFSET(_detail_h5::Timestamps, t_begin), 
            H5::PredType::NATIVE_UINT64
        );
        m_time_tid.insertMember(
            "t_end", 
            HOFFSET(_detail_h5::Timestamps, t_end), 
            H5::PredType::NATIVE_UINT64
        );
        m_time_tid.insertMember(
            "flag", 
            HOFFSET(_detail_h5::Timestamps, error_flag), 
            H5::PredType::NATIVE_UINT32
        );

    }
    catch(H5::Exception error){
        std::stringstream SS;
        SS << error.getFuncName() << ": " << error.getDetailMsg();
        m_logger.error(SS.str());
        throw std::runtime_error("HDF5 initialization failure!");        
    }
}

/////
///  START A NEW HDF5 FILE
template<typename T5122Frame>
std::string HDF5TSWriter<T5122Frame>::
    new_file(const T5122Frame &frame)
{
    std::string h5_fname;

    
    try{
        /// Create the new HDF5 file
        h5_fname = get_file_label(frame.timestamp_begin());
        auto h5_path = m_filepath + "/" + "ts_" + h5_fname;
        m_logger.action("Writing to time series file:\n\t", h5_path);       
        m_file = H5::H5File(
            h5_path, 
            H5F_ACC_TRUNC
        );

        /// Structure the file and write the metadata

        /// Create the group for TS data
        m_timeseries_entry = H5::Group(m_file.createGroup("Timeseries"));

        /// Create the group for metadata
        H5::Group metadata_entry(m_file.createGroup("Metadata"));

        /// Create the extendible timestamp dataset & get its memory space
        m_time_dataset  = metadata_entry.createDataSet(
            "Timestamps", 
            m_time_tid, 
            m_time_dataspace, 
            m_time_create
        );  
        m_time_memspace = m_time_dataset.getSpace();
        m_num_frames = 0;

        if(m_source_metadata.size() > 0){ 
            /// Write the 5122 settings for each device
            auto dataset = metadata_entry.createDataSet(
                "Devices", 
                m_5122_tid, 
                m_5122_dataspace
            );
            dataset.write(
                m_source_metadata.data(), 
                m_5122_tid
            );
        }
        hsize_t size[1]  = {frame.num_channels()};
        std::vector<double> adj_data(frame.num_channels());
        {
            for(auto chn_idx : frame.channel_counter()){
                adj_data[chn_idx] = frame.gain(chn_idx);
            }
            /// Write the 5122 settings for each device
            auto dataset = metadata_entry.createDataSet(
                "Gains", 
                H5::PredType::NATIVE_DOUBLE,
                H5::DataSpace(1, size)
            );
            dataset.write(
                adj_data.data(), 
                H5::PredType::NATIVE_DOUBLE
            );
        }
        {
            for(auto chn_idx : frame.channel_counter()){
                adj_data[chn_idx] = frame.offset(chn_idx);
            }
            auto dataset = metadata_entry.createDataSet(
                "Offsets", 
                H5::PredType::NATIVE_DOUBLE,
                H5::DataSpace(1, size)
            );
            dataset.write(
                adj_data.data(), 
                H5::PredType::NATIVE_DOUBLE
            );
        }
    }
    catch(H5::Exception error){
        std::stringstream SS;
        SS << error.getFuncName() << ": " << error.getDetailMsg();
        m_logger.error(SS.str());
        throw std::runtime_error("HDF5 file creation failed!");        
    }
    m_timestamp_last_file_ns = frame.timestamp_begin();
    return h5_fname;
}

/////
///  WRITE A TS BATCH TO FILE
template<typename T5122Frame>
void HDF5TSWriter<T5122Frame>::
    write(const T5122Frame &frame, double new_file_rate_s)
{
    if(
        (
             (new_file_rate_s >= 0) and 
             ((frame.timestamp_begin() - m_timestamp_last_file_ns) / 1e9 > new_file_rate_s)
         ) or 
        (m_num_frames < 0)  // check the file needs first initialization
    ){
        this->new_file(frame);
    }
    try{
        /// Create a new TS frame entry
        auto frame_label = get_frame_label(frame.sequence_number());
        H5::Group entry(m_timeseries_entry.createGroup(frame_label)); 

        /// Write the PSD/TS data to the new entry
        for(auto chn_idx : frame.channel_counter()){
            /// Timeseries (real)
            auto timebuffer = frame.channel_timebuffer(chn_idx);

            /// Create the PSD/TS container & write data to it
            std::stringstream ss;
            ss << "<" << chn_idx << ">";
            auto label = ss.str();

            hsize_t size[] = {timebuffer.size()};
            auto dataset = entry.createDataSet(
                label, 
                hdf_static_dtype<typename T5122Frame::TNum>(), 
                H5::DataSpace(1, size)
            );
            dataset.write(
                (typename T5122Frame::TNum *) timebuffer.begin(), 
                hdf_static_dtype<typename T5122Frame::TNum>()
            );
        }

        /// Write the frame timestamps

        /// Append the timestamps to the timestamp dataset
        _detail_h5::Timestamps timestamps;
        timestamps.t_begin = frame.timestamp_begin();
        timestamps.t_end = frame.timestamp_end();
        timestamps.error_flag = frame.error_flag();
        
        /// Extend the timestamp dataset (assures that dataset is at least the specified size)
        hsize_t new_size[1] = {m_num_frames + 1};
        m_time_dataset.extend(new_size);    

        /// Select the dataset region for writing (hyperslab)
        hsize_t offset[1] = {m_num_frames};
        hsize_t size[1]   = {1};
        auto time_slab = m_time_dataset.getSpace();
        time_slab.selectHyperslab(
            H5S_SELECT_SET, 
            size, 
            offset
        );    
        m_time_dataset.write(
            &timestamps, 
            m_time_tid, 
            m_time_memspace,
            time_slab
        );
        
        auto timestamp_dataset = entry.createDataSet(
            "Timestamps", 
            m_time_tid, 
            H5::DataSpace(0, NULL)
        );  
        timestamp_dataset.write(
            &timestamps,
            m_time_tid
        );
        m_num_frames += 1;
    }
    catch(H5::Exception error){
        std::stringstream SS;
        SS << error.getFuncName() << ": " << error.getDetailMsg();
        m_logger.error(SS.str());
        throw std::runtime_error("Time series frame write failed!");         
    }
}

}; //~namespace JStream
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
