/////
///
///
#include "hdf5_ts_writer.hpp"
#include "hdf5_writer.hpp"

namespace JStream {

template class HDF5TSWriter<T5122Frame>;
template class HDF5CSDWriter<CSDCarrierFloat>;
template class HDF5CSDWriter<CSDCarrierDouble>;

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
