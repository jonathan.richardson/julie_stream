////
///

#ifndef H_HDF5_UTILITIES_H
#define H_HDF5_UTILITIES_H

#include <chrono>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <stdexcept>
#include <cstring>

#include <H5Cpp.h>

#include "data_sources/data_source_base.hpp"

namespace JStream {

namespace _detail_h5 {
    template<typename T> struct _Dummy{};

    /// HDF5 definitions
    #define FILE_EXT            ".h5"
    
    /// Run metadata container type
    typedef struct {
        unsigned    num_chs;              //Total number of channels
        unsigned    num_freq_bins;        //PSD/CSD size
        double      sample_freq;          //Sample frequency (Hz)
        double      ENBW;                 //Effective noise bandwith [Hz]  
        double      normalization_to_ps;  //Normalization for a *one* FFT batch (i.e., 2/S1^2)
        unsigned    frame_accumulation;	  //Number of FFT batches per CSD frame
        double      overlap_frac;         //FFT frame overlap fraction
    } RunSettings;

    /// Timestamp metadata container type
    typedef struct {
        uint64_t    t_begin             = -1.0;     //Begin timestamp
        uint64_t    t_end               = -1.0;     //End timestamp
        unsigned    error_flag          = 0;        //Error code (see CSDCarrierBase definition for codes)
    } Timestamps;
    
}; //~namespace _detail_h5


/////
///  Label generator utilities

/// Returns HDF5 filename based on supplied timestamp
std::string get_file_label(uint64_t sequence_number);

/// Returns HDF5 CSD group name based on supplied frame ID
std::string get_frame_label(uint64_t sequence_number);
    
/// Returns HDF5 dataset name for an individual CSD
std::string get_csd_label(
    unsigned row_idx, 
    unsigned col_idx
);

/////
///  Metadata consistency-checking utility class
class HDF5MetadataChecker
{
    /// Metadata containers
    _detail_h5::RunSettings m_run;
    std::vector<IDataSource::DataSourceSettings> m_hw;

public:
    HDF5MetadataChecker(){}

    /// Checks supplied metadata for consistency
    void consistency_check(
        _detail_h5::RunSettings run_settings,
        std::vector<IDataSource::DataSourceSettings> hw_settings
    );    
};

template<typename TNum>
inline H5::PredType hdf_static_dtype(){
    static_assert(_detail_h5::_Dummy<TNum>::value, "Unknown data type");
    return;
}

template<>
inline H5::PredType hdf_static_dtype<int16_t>()
{
    return H5::PredType::NATIVE_INT16;
}

template<>
inline H5::PredType hdf_static_dtype<float>()
{
    return H5::PredType::NATIVE_FLOAT;
}

template<>
inline H5::PredType hdf_static_dtype<double>()
{
    return H5::PredType::NATIVE_DOUBLE;
}

}; //~namespace JStream

#endif //H_HDF5_UTILITIES_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
