////
///

#ifndef H_JSTREAM_HDF5_READER_H
#define H_JSTREAM_HDF5_READER_H

#include "jstream/config.hpp"
#if JSTREAM_HAS_HDF5

#include <memory>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

#include <hdf5.h>
#include <H5Cpp.h>

#include "sdt/utilities/logger.hpp"
#include "sdt/utilities/logger_impl.hpp"

#include "jstream/config.hpp"

#include "hardware_locality/localization_manager.hpp"

#include "utilities/stdtime.hpp"

#include "analysis/csd_carrier_float.hpp"

#include "hdf5_utilities.hpp"

namespace JStream {


/////
///  HDF5 reader class
template <typename TCSDCarrier>
class HDF5CSDReader
{
    /// Logger
    SDT::LoggerAccess       m_logger;
    
    /// HDF5 handles
    H5::H5File              m_file;    
    
    /// Metadata containers
    _detail_h5::RunSettings m_run;
    std::vector<IDataSource::DataSourceSettings> 
                            m_hw;
    std::vector<_detail_h5::Timestamps> 
                            m_timestamps;
                        
    /// CSD container
    TCSDCarrier             m_csd;    
    
    /// Metadata consistency checker
    HDF5MetadataChecker     m_meta_checker;

public:
    /// Constructor
    HDF5CSDReader(SDT::LoggerAccess logger);
    
    /// Opens specified h5 file & reads metadata
    bool open_file(std::string &filepath);  
    
    /// Reads specified CSD frame
    const TCSDCarrier& read_csd(
        uint64_t frame_id,
        uint64_t frame_file_idx
    ); 
    
    /// Return references to the metadata
    const std::vector<IDataSource::DataSourceSettings>& hw_settings();
    const _detail_h5::RunSettings&                      run_settings();     
    const std::vector<_detail_h5::Timestamps>&          timestamps();    
    
protected:
    /// Initializes HDF5
    void _hdf5_setup();
    
};

}; //~namespace JStream
#include "hdf5_reader_T.hpp"
#endif //JSTREAM_HAS_HDF5

#endif // H_JSTREAM_HDF5_READER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
