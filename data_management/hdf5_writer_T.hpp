////
///

#include "hdf5_writer.hpp"

namespace JStream {


/////
///  Constructor
template<typename TCSDCarrier>
HDF5CSDWriter<TCSDCarrier>::
    HDF5CSDWriter(
        std::vector<IDataSource::DataSourceSettings> source_metadata,
        const TCSDCarrier &csd,
        const std::string &filepath,
        SDT::LoggerAccess logger
    ) :
        m_source_metadata(source_metadata),
        m_filepath(filepath),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
    
    /// Set the run metadata
    m_run_metadata.sample_freq           = csd.sample_freq_get();
    m_run_metadata.num_chs               = csd.channels_num();
    m_run_metadata.num_freq_bins         = csd.length();
    m_run_metadata.ENBW                  = csd.ENBW_get();
    m_run_metadata.normalization_to_ps   = csd.normalization_to_ps_get();
	m_run_metadata.frame_accumulation    = csd.accumulation_get(); 
    m_run_metadata.overlap_frac          = csd.overlap_frac_get();
       
    /// Configure HDF5
    this->_hdf5_setup();
}

template<typename TCSDCarrier>
HDF5CSDWriter<TCSDCarrier>::
    ~HDF5CSDWriter()
{
    m_logger.action("Finalizing CSD file");
}

/////
///  Creates the custom HDF5 datatypes and containers
template<typename TCSDCarrier>
void HDF5CSDWriter<TCSDCarrier>::
    _hdf5_setup()
{
    try{
        /// Suppress automatic error printing
        H5::Exception::dontPrint();
        
        /// Get the data precision
        m_datatype = hdf_static_dtype<typename TCSDCarrier::TNum>();
        
        /// DATASPACES
        
        {   /// PSD (real)
            hsize_t size[1] = {m_run_metadata.num_freq_bins};
            m_psd_dataspace = H5::DataSpace(1, size);
        }
        
        {   /// CSD (complex)
            hsize_t size[2] = {m_run_metadata.num_freq_bins, 2};
            m_csd_dataspace = H5::DataSpace(2, size);
        }

        {   /// Timestamps (extendible)
            hsize_t size[1]     = {1};
            hsize_t max_size[1] = {H5S_UNLIMITED};
            m_time_dataspace    = H5::DataSpace(1, size, max_size);

            /// Must enable chunking for an extendible dataspace
            hsize_t chunk_size[1] = {1};    
            m_time_create.setChunk(1, chunk_size);
        }
        
        {   /// Voltages (extendible)
            hsize_t size[2]     = {1, m_run_metadata.num_chs};
            hsize_t max_size[2] = {H5S_UNLIMITED, m_run_metadata.num_chs};
            m_voltage_dataspace = H5::DataSpace(2, size, max_size);

            /// Must enable chunking for an extendible dataspace
            hsize_t chunk_size[2] = {1, m_run_metadata.num_chs};    
            m_voltage_create.setChunk(2, chunk_size);
        }
           
        {   /// Run configuration data
            hsize_t size[1]    = {1};
            m_config_dataspace = H5::DataSpace(1, size);
        }
        
        {   /// 5122 device settings
            hsize_t size[1]  = {m_source_metadata.size()};
            m_5122_dataspace = H5::DataSpace(1, size);
        }
         
        /// COMPOUND DATA TYPES

        {   /// Run configuration settings
            m_config_tid = H5::CompType(sizeof(_detail_h5::RunSettings));
            m_config_tid.insertMember(
                "num_chs", 
                HOFFSET(_detail_h5::RunSettings, num_chs), 
                H5::PredType::NATIVE_UINT
            );
            m_config_tid.insertMember(
                "num_freq_bins", 
                HOFFSET(_detail_h5::RunSettings, num_freq_bins), 
                H5::PredType::NATIVE_UINT);
            m_config_tid.insertMember(
                "sample_freq", 
                HOFFSET(_detail_h5::RunSettings, sample_freq), 
                H5::PredType::NATIVE_DOUBLE
            );
            m_config_tid.insertMember(
                "ENBW", 
                HOFFSET(_detail_h5::RunSettings, ENBW), 
                H5::PredType::NATIVE_DOUBLE
            );
            m_config_tid.insertMember(
                "normalization_to_ps", 
                HOFFSET(_detail_h5::RunSettings, normalization_to_ps), 
                H5::PredType::NATIVE_DOUBLE
            );    
            m_config_tid.insertMember(
                "frame_accumulation", 
                HOFFSET(_detail_h5::RunSettings, frame_accumulation), 
                H5::PredType::NATIVE_UINT
            );    
            m_config_tid.insertMember(
                "overlap_frac", 
                HOFFSET(_detail_h5::RunSettings, overlap_frac), 
                H5::PredType::NATIVE_DOUBLE
            );
        }
          
        {   /// Data source hardware settings  
            m_5122_tid = H5::CompType(sizeof(IDataSource::DataSourceSettings));
            m_5122_tid.insertMember(
                "adc_name", 
                HOFFSET(IDataSource::DataSourceSettings, adc_name), 
                H5::StrType(H5::PredType::C_S1, STR_LEN)
            );
            m_5122_tid.insertMember(
                "adc_type", 
                HOFFSET(IDataSource::DataSourceSettings, adc_type), 
                H5::StrType(H5::PredType::C_S1, STR_LEN)
            );  
            m_5122_tid.insertMember(
                "pll_status", 
                HOFFSET(IDataSource::DataSourceSettings, pll_status), 
                H5::StrType(H5::PredType::C_S1, STR_LEN)
            );  
            m_5122_tid.insertMember(
                "sample_freq", 
                HOFFSET(IDataSource::DataSourceSettings, sample_freq), 
                H5::PredType::NATIVE_DOUBLE
            );
            m_5122_tid.insertMember(
                "input_range", 
                HOFFSET(IDataSource::DataSourceSettings, input_range), 
                H5::PredType::NATIVE_DOUBLE
            );
            m_5122_tid.insertMember(
                "dc_offset", 
                HOFFSET(IDataSource::DataSourceSettings, dc_offset), 
                H5::PredType::NATIVE_DOUBLE
            );
            m_5122_tid.insertMember(
                "scaling", 
                HOFFSET(IDataSource::DataSourceSettings, scaling), 
                H5::PredType::NATIVE_DOUBLE
            );
            m_5122_tid.insertMember(
                "bandwidth", 
                HOFFSET(IDataSource::DataSourceSettings, bandwidth), 
                H5::PredType::NATIVE_DOUBLE
            );
            m_5122_tid.insertMember(
                "impedance", 
                HOFFSET(IDataSource::DataSourceSettings, impedance), 
                H5::PredType::NATIVE_DOUBLE
            );
            m_5122_tid.insertMember(
                "coupling", 
                HOFFSET(IDataSource::DataSourceSettings, coupling), 
                H5::PredType::NATIVE_INT
            );
        }
        
        {   /// Timestamps      
            m_time_tid = H5::CompType(sizeof(_detail_h5::Timestamps));
            m_time_tid.insertMember(
                "t_begin", 
                HOFFSET(_detail_h5::Timestamps, t_begin), 
                H5::PredType::NATIVE_UINT64
            );
            m_time_tid.insertMember(
                "t_end", 
                HOFFSET(_detail_h5::Timestamps, t_end), 
                H5::PredType::NATIVE_UINT64
            );
            m_time_tid.insertMember(
                "flag", 
                HOFFSET(_detail_h5::Timestamps, error_flag), 
                H5::PredType::NATIVE_UINT32
            );
        }      
    }
    catch(H5::Exception error){
        std::stringstream SS;
        SS << error.getFuncName() << ": " << error.getDetailMsg();
        m_logger.error(SS.str());
        throw std::runtime_error("HDF5 initialization failure!");        
    }
}

/////
///  START A NEW HDF5 FILE
template<typename TCSDCarrier>
std::string HDF5CSDWriter<TCSDCarrier>::
    new_file(uint64_t timestamp)
{
    std::string h5_fname;
    m_num_frames = 0;
    
    try{
        /// Create the new HDF5 file
        h5_fname = get_file_label(timestamp);
        auto h5_path = m_filepath + "/" + h5_fname;
        m_logger.action("Writing to CSD file:\n\t", h5_path);       
        m_file = H5::H5File(
            h5_path, 
            H5F_ACC_TRUNC
        );

        /// Create the group for CSD data
        m_data_entry = H5::Group(m_file.createGroup("Data"));

        /// Create the group for metadata
        auto metadata_entry = H5::Group(m_file.createGroup("Metadata"));

        {   /// Create the extendible timestamp dataset & get its memory space
            m_time_dataset = metadata_entry.createDataSet(
                "Timestamps", 
                m_time_tid, 
                m_time_dataspace, 
                m_time_create
            );  
            m_time_memspace = m_time_dataset.getSpace();
        }
        
        {   /// Create the extendible minimum voltage dataset & get its memory space
            m_voltage_min_dataset = metadata_entry.createDataSet(
                "Minimum Voltages", 
                H5::PredType::NATIVE_DOUBLE, 
                m_voltage_dataspace, 
                m_voltage_create
            );  
            m_voltage_min_memspace = m_voltage_min_dataset.getSpace(); 
        }
        
        {   /// Create the extendible maximum voltage dataset & get its memory space
            m_voltage_max_dataset = metadata_entry.createDataSet(
                "Maximum Voltages", 
                H5::PredType::NATIVE_DOUBLE, 
                m_voltage_dataspace, 
                m_voltage_create
            );  
            m_voltage_max_memspace = m_voltage_max_dataset.getSpace(); 
        }        

        {   /// Write the run configuration data
            auto dataset = metadata_entry.createDataSet(
                "Configuration", 
                m_config_tid, 
                m_config_dataspace
            );
            dataset.write(&m_run_metadata, m_config_tid);
        }

        {   /// Write the 5122 settings for each device
            auto dataset = metadata_entry.createDataSet(
                "Devices", 
                m_5122_tid, 
                m_5122_dataspace
            );
            dataset.write(
                m_source_metadata.data(), 
                m_5122_tid
            );
        }
    }
    catch(H5::Exception error){
        std::stringstream SS;
        SS << error.getFuncName() << ": " << error.getDetailMsg();
        m_logger.error(SS.str());
        throw std::runtime_error("HDF5 file creation failed!");        
    }
    return h5_fname;
}

/////
///  WRITE A CSD BATCH TO FILE
template<typename TCSDCarrier>
void HDF5CSDWriter<TCSDCarrier>::
    write_csd(const TCSDCarrier &csd)
{
	/// Verify consistent run settings for each CSD
    if(
        csd.channels_num()              != m_run_metadata.num_chs               or
        csd.length()                    != m_run_metadata.num_freq_bins         or
        csd.accumulation_get()          != m_run_metadata.frame_accumulation    or
        csd.sample_freq_get()           != m_run_metadata.sample_freq           or
        csd.normalization_to_ps_get()   != m_run_metadata.normalization_to_ps   or
        csd.ENBW_get()                  != m_run_metadata.ENBW                  or
        csd.overlap_frac_get()          != m_run_metadata.overlap_frac
    ){
        throw std::runtime_error("Inconsistent CSD metadata detected!");         
    }

    try{
        /// Create a new CSD frame entry
        auto frame_label = get_frame_label(csd.sequence_number_get());
        H5::Group entry(m_data_entry.createGroup(frame_label)); 

        /// Write the PSD/CSD data to the new entry
        for(auto row_idx : icount(m_run_metadata.num_chs)){
            for(auto col_idx : icount(row_idx + 1)){
                /// Create the PSD/CSD container & write data to it
                auto csd_label = get_csd_label(row_idx, col_idx);
                if(row_idx == col_idx){
                    auto dataset = entry.createDataSet(
                        csd_label, 
                        m_datatype, 
                        m_psd_dataspace
                    );
                    dataset.write(
                        (typename TCSDCarrier::TNum *) csd.psd_get(row_idx).begin(), 
                        m_datatype
                    );
                }else{
                    auto dataset = entry.createDataSet(
                        csd_label, 
                        m_datatype, 
                        m_csd_dataspace
                    );
                    dataset.write(
                        (typename TCSDCarrier::TComplex *) csd.csd_get(row_idx, col_idx).begin(), 
                        m_datatype
                    );
                }
            }
        }

        /// Write the frame timestamps
        {   
            /// Extend the timestamp dataset (assures that dataset is at least the specified size)
            hsize_t new_size[1] = {m_num_frames + 1};
            m_time_dataset.extend(new_size);    

            /// Select the dataset region for writing
            hsize_t offset[1] = {m_num_frames};
            hsize_t size[1]   = {1};
            auto hyperslab = m_time_dataset.getSpace();
            hyperslab.selectHyperslab(
                H5S_SELECT_SET, 
                size, 
                offset
            );    

            /// Append the timestamps to the timestamp dataset
            _detail_h5::Timestamps timestamps;
            timestamps.t_begin = csd.timestamp_begin();
            timestamps.t_end = csd.timestamp_end();
            timestamps.error_flag = csd.error_flag_get();
            m_time_dataset.write(
                &timestamps, 
                m_time_tid, 
                m_time_memspace, 
                hyperslab
            );
        }
        
        /// Write the minimum channel voltages
        {   
            /// Extend the voltage dataset (assures that dataset is at least the specified size)
            hsize_t new_size[2] = {m_num_frames + 1, m_run_metadata.num_chs};
            m_voltage_min_dataset.extend(new_size);    

            /// Select the dataset region for writing (hyperslab)
            hsize_t offset[2] = {m_num_frames, 0};
            hsize_t size[2]   = {1, m_run_metadata.num_chs};
            auto hyperslab = m_voltage_min_dataset.getSpace();
            hyperslab.selectHyperslab(
                H5S_SELECT_SET, 
                size, 
                offset
            );    

            /// Append the channel voltages to the dataset
            m_voltage_min_dataset.write(
                csd.voltage_min_buffer().begin(), 
                H5::PredType::NATIVE_DOUBLE, 
                m_voltage_min_memspace, 
                hyperslab
            );
        }
        
        /// Write the maximum channel voltages
        {   
            /// Extend the voltage dataset (assures that dataset is at least the specified size)
            hsize_t new_size[2] = {m_num_frames + 1, m_run_metadata.num_chs};
            m_voltage_max_dataset.extend(new_size);    

            /// Select the dataset region for writing (hyperslab)
            hsize_t offset[2] = {m_num_frames, 0};
            hsize_t size[2]   = {1, m_run_metadata.num_chs};
            auto hyperslab = m_voltage_max_dataset.getSpace();
            hyperslab.selectHyperslab(
                H5S_SELECT_SET, 
                size, 
                offset
            );    

            /// Append the channel voltages to the dataset
            m_voltage_max_dataset.write(
                csd.voltage_max_buffer().begin(), 
                H5::PredType::NATIVE_DOUBLE, 
                m_voltage_max_memspace, 
                hyperslab
            );
        }                
        m_num_frames += 1;
    }
    catch(H5::Exception error){
        std::stringstream SS;
        SS << error.getFuncName() << ": " << error.getDetailMsg();
        m_logger.error(SS.str());
        throw std::runtime_error("CSD frame write failed!");         
    }
}

}; //~namespace JStream
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
