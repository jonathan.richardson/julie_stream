////
///

#include "hdf5_reader.hpp"

namespace JStream {


/////
///  Constructor
template<typename TCSDCarrier>
HDF5CSDReader<TCSDCarrier>::
    HDF5CSDReader(SDT::LoggerAccess logger) :
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);

    /// Configure HDF5
    this->_hdf5_setup();
}

/////
///  Creates the custom HDF5 datatypes and containers
template<typename TCSDCarrier>
void HDF5CSDReader<TCSDCarrier>::
    _hdf5_setup()
{
    try{
        /// Suppress automatic error printing
        H5::Exception::dontPrint();
    }
    catch(H5::Exception error){
        std::stringstream SS;
        SS << error.getFuncName() << ": " << error.getDetailMsg();
        m_logger.error(SS.str());
        throw std::runtime_error("HDF5 initialization failed!");        
    }
}

/////
///  Opens an HDF5 file and reads in its metadata
template<typename TCSDCarrier>
bool HDF5CSDReader<TCSDCarrier>::
    open_file(std::string &filepath)
{
    try{
        /// Open the next HDF5 file in read-only mode
        m_file = H5::H5File(
            filepath, 
            H5F_ACC_RDONLY
        );
        m_logger.detail("Reading file:\n\t\t", filepath);
        
        { /// Read in the timestamp metadata
            auto data = m_file.openDataSet("Metadata/Timestamps");      
            auto size = data.getSpace().getSimpleExtentNpoints();                
            m_timestamps.resize(size);
            data.read(
                m_timestamps.data(),
                data.getCompType()
            );
        }

        { /// Read in the run configuration metadata
            auto data = m_file.openDataSet("Metadata/Configuration");           
            data.read(
                &m_run,
                data.getCompType()
            );
        }
      
        { /// Read in the hardware metadata
            auto data = m_file.openDataSet("Metadata/Devices");
            auto size = data.getSpace().getSimpleExtentNpoints();
            m_hw.resize(size);
            data.read(
                m_hw.data(),
                data.getCompType()
            );                
        }    
    }catch(H5::Exception error){
        m_logger.error(error.getCDetailMsg());
        throw std::runtime_error("h5 file open failed!");
    }
    
    /// Allocate & intialize the CSD container
    m_csd = TCSDCarrier(
        m_run.num_chs, 
        m_run.num_freq_bins, 
        m_run.sample_freq,
        m_run.overlap_frac,
        localization_manager()->memory_locality_get(m_logger.context())
    );      
    m_csd.ENBW_set(m_run.ENBW);
    m_csd.normalization_to_ps_set(m_run.normalization_to_ps);
    
    /// Check the metadata for consistency
    m_meta_checker.consistency_check(m_run, m_hw);   
    return true;
}

/////
///  Returns a reference to the hardware settings
template<typename TCSDCarrier>
const std::vector<IDataSource::DataSourceSettings>& HDF5CSDReader<TCSDCarrier>::
    hw_settings()
{
    return m_hw;
}

/////
///  Returns a reference to the run settings
template<typename TCSDCarrier>
const _detail_h5::RunSettings& HDF5CSDReader<TCSDCarrier>::
    run_settings()
{
    return m_run;
}

/////
///  Returns a reference to the timestamps
template<typename TCSDCarrier>
const std::vector<_detail_h5::Timestamps>& HDF5CSDReader<TCSDCarrier>::
    timestamps()
{
    return m_timestamps;
}

/////
///  Reads a CSD frame into the persistent internal carrier
template<typename TCSDCarrier>
const TCSDCarrier& HDF5CSDReader<TCSDCarrier>::
    read_csd(
        uint64_t frame_id,
        uint64_t frame_file_idx
    )
{
    if(m_csd.channels_num() == 0){
        throw std::runtime_error("No h5 file open for reading");
    }

    try{
        /// Read the frame data into the CSDCarrier
        std::stringstream frame_name;
        frame_name << "Data/" << std::setw(10) << std::setfill('0') << frame_id;  
        m_logger.detail("Reading CSD group:\t", frame_name.str());
        for(auto row_idx : icount(m_run.num_chs)){
            for(auto col_idx : icount(row_idx + 1)){
                /// Read the PSD/CSD dataset 
                auto dataset_name = frame_name.str() + "/<" + std::to_string(row_idx) 
                                        + std::to_string(col_idx) + ">";
                auto dataset = m_file.openDataSet(dataset_name);
                if(row_idx == col_idx){
                    dataset.read(
                        m_csd.psd_get(row_idx).begin(),
                        dataset.getDataType()
                    );                              
                }else{
                    dataset.read(
                        m_csd.csd_get(row_idx, col_idx).begin(),
                        dataset.getCompType()
                    );
                }
            }//~col_idx loop
        }//~row_idx loop

        /// Update the CSDCarrier metadata
        m_csd.sequence_number_set(frame_id);
        m_csd.accumulation_set(m_run.frame_accumulation);        
        m_csd.timestamps_set(
            m_timestamps[frame_file_idx].t_begin, 
            m_timestamps[frame_file_idx].t_end
        );
        m_csd.error_flag_set(m_timestamps[frame_file_idx].error_flag);
    }catch(H5::Exception error){
        m_logger.error(error.getCDetailMsg());
        throw std::runtime_error("CSD read failed!");
    }
    return m_csd;
}        

}; //~namespace JStream
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :        