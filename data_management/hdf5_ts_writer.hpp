////
///
#ifndef H_JSTREAM_HDF_TIMESERIES_WRITER_H
#define H_JSTREAM_HDF_TIMESERIES_WRITER_H

#include "jstream/config.hpp"
#if JSTREAM_HAS_HDF5

#include <memory>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

#include <hdf5.h>
#include <H5Cpp.h>

#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>

#include "jstream_types.hpp"
#include "utilities/stdtime.hpp"

#include "hdf5_utilities.hpp"

namespace JStream {


/////
///  HDF5 writer class
template <typename T5122Frame>
class HDF5TSWriter
{
    /// Logger
    SDT::LoggerAccess       m_logger;
    
    /// HDF5 archive path
    std::string             m_filepath;

    /// HDF5 handles
    H5::H5File              m_file;

    H5::DataSpace           m_ts_dataspace;
    H5::DataSpace           m_time_dataspace;
    H5::DataSpace           m_time_memspace;
    H5::DataSpace           m_5122_dataspace;

    H5::CompType            m_time_tid;
    H5::CompType            m_5122_tid;

    H5::DSetCreatPropList   m_time_create;
    H5::DataSet             m_time_dataset;
    H5::Group               m_timeseries_entry;
    H5::DataType            m_datatype;
 
    /// Data source & run metadata
    std::vector<IDataSource::DataSourceSettings>
                            m_source_metadata;

    /// Frame counter (for timestamp dataset extension)
    /// negative indicates that the file isn't initialized
    signed                  m_num_frames = -1;
    uint64_t                m_timestamp_last_file_ns = 0;

public:
    /// Constructor
    HDF5TSWriter(
        std::vector<IDataSource::DataSourceSettings> source_metadata,
        const std::string &filepath,
        SDT::LoggerAccess logger
    );

    ~HDF5TSWriter();

    /// Write a TS frame
    void write(const T5122Frame &frame, double new_file_rate_s);
    
protected:
    /// Create a new h5 file
    std::string new_file(const T5122Frame &frame);

    /// Creates the custom HDF5 datatypes and containers
    void _hdf5_setup();
    
};

extern template class HDF5TSWriter<T5122Frame>;
}; //~namespace JStream

#include "hdf5_ts_writer_T.hpp"

#endif //~JSTREAM_HAS_HDF5

#endif // H_JSTREAM_HDF_TIMESERIES_WRITER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
