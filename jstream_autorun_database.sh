#!/usr/bin/sh

# Keep restarting the DAQ code until it exits with status code 0 
# (i.e., a controlled shutdown)
status=-1
while [ $status != 0 ]
do
    echo -e ">>> Database server is live\n"
    julie_stream.exe -a adv_settings_julie2_database.json
    status=$?
    echo -e "\n>>> Exit status: $status\n"
done