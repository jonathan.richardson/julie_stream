/////
///

#include <sdt/utilities/config_tree/config_tree_json.hpp>

#include "persistent_jstream_server.hpp"

namespace JStream {
using namespace ConfigTree;


/////
///  Constructor for the TCP communications layer
PersistentJStreamBridgeServer::
	PersistentJStreamBridgeServer(LoggerAccess logger) :
        m_logger(logger),
        m_acceptors()
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
    
    /// Create the SDT connections manager
    /// (Using boost::asio)
	auto asio_manager = std::make_shared<SDT::AsioTaskManager>();
	m_acceptors.endpoint_manager_set(asio_manager);

    /// Set the handler function to be called by SDT when a client connects
    /// (launches the data transmission loop)
    m_acceptors.transport_actor_set(
        SDT::TransportActorGenerator(
            std::bind(
                &PersistentJStreamBridgeServer::_connection_accepted_f,
                this,
                std::placeholders::_1
            )
        )
    );
}

/////
///  Destructor for the TCP communications module
PersistentJStreamBridgeServer::
	~PersistentJStreamBridgeServer() 
{
    /// Close any open connection
	this->connections_end();
}

/////
///  Sets the underlying data server
void PersistentJStreamBridgeServer::
	server_set(std::shared_ptr<JStreamBridgeServer> server)
{
	m_server_impl = server;
}

/////
///  Adds an address on which the server will listen for client connection requests
///  (Ignores addresses which have already been inserted)
void PersistentJStreamBridgeServer::
	accept_address_add(const string &address)
{
    m_acceptors.endpoint_add(address);
}

/////
///  Starts listening for client connection requests
void PersistentJStreamBridgeServer::
	connections_begin()
{
    m_acceptors.start();
}

/////
///  Ends the background task which is starting connections, as well as stops all of the 
///  connections themselves
void PersistentJStreamBridgeServer::
	connections_end()
{
    m_acceptors.stop();
    m_acceptors.join();
    if(m_server_impl){
        Guard guard_f(m_f_protect);
        m_server_impl->stop();
        m_server_impl->join();
    }
}

/////
///  Handler for a new client connection
void PersistentJStreamBridgeServer::
	_connection_accepted_f(SDT::TaskManagerPtr transport_manager)
{
    Guard guard_f(m_f_protect);

    /// If the server is already transmitting to another client, reject the connection
    /// (Server currently supports only one client at a time)
    if(m_server_impl->is_live()){
        m_logger.action("Incoming connection request rejected. Server is already in use!");
        return;
    }

    /// Make sure that any previous connection has been stopped and joined
    m_server_impl->stop();
    m_server_impl->join();

    LOGGER_DEBUG(m_logger, "Bridge Connection Started");
    
    /// Connect the new client and start the data transmission loop
    m_server_impl->connect(transport_manager);
    m_server_impl->start();
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :