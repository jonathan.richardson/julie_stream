////
/// Defines some classes for convenienty doing "good" FFTs, fft's from fftw or
/// other libraries with known performance
///

#ifndef H_IFFT_FFTW_ACCESS_H
#define H_IFFT_FFTW_ACCESS_H

#include <complex>
#include <fftw3.h>

#include "utilities/range.hpp"
namespace ifft {
using utilities::Range;
using utilities::icount;
using std::complex;

typedef complex<float> C_float;
typedef complex<double> C_double;

////
///Given a pointer to data, creates an output buffer and FFT plan
///
class FFT_FFTWf
{
    Range<C_float *>  m_input_data;
    Range<C_float *>  m_fft_data;
    Range<float   *>  m_psd_data;

    fftwf_plan m_fft_plan;

    public:
    FFT_FFTWf(size_t input_size, bool estimate = false);
    FFT_FFTWf(const FFT_FFTWf &) = delete;
    FFT_FFTWf &operator=(const FFT_FFTWf &) = delete;
    ~FFT_FFTWf();

    Range<C_float *> input_data()
    { return m_input_data;}
    Range<C_float *> fft_data()
    { return m_fft_data;}
    Range<float   *> psd_data()
    { return m_psd_data;}

    void             compute_fft();
    void             compute_psd();
};

////
///Given a pointer to data, creates an output buffer and FFT plan
///This LEAVES OUT the nyquist bin. ifft doesn't return it because it is not very useful
///
class FFTR_FFTWf
{
    Range<float *>    m_input_data;
    Range<C_float *>  m_fft_data;
    Range<float   *>  m_psd_data;

    fftwf_plan m_fft_plan;

    public:
    FFTR_FFTWf(size_t input_size, bool estimate = false);
    FFTR_FFTWf(const FFTR_FFTWf &) = delete;
    FFTR_FFTWf &operator=(const FFTR_FFTWf &) = delete;
    ~FFTR_FFTWf();

    Range<float *> input_data()
    { return m_input_data;}
    Range<C_float *> fft_data()
    { return m_fft_data;}
    Range<float   *> psd_data()
    { return m_psd_data;}

    void             compute_fft();
    void             compute_psd();
};

class FFT_FFTWd
{
    Range<C_double *>  m_input_data;
    Range<C_double *>  m_fft_data;
    Range<double   *>  m_psd_data;

    fftw_plan m_fft_plan;

    public:
    FFT_FFTWd(size_t input_size, bool estimate = false);
    FFT_FFTWd(const FFT_FFTWd &) = delete;
    FFT_FFTWd &operator=(const FFT_FFTWd &) = delete;
    ~FFT_FFTWd();

    Range<C_double *> input_data()
    { return m_input_data;}
    Range<C_double *> fft_data()
    { return m_fft_data;}
    Range<double   *> psd_data()
    { return m_psd_data;}

    void              compute_fft();
    void              compute_psd();
};

////
///Given a pointer to data, creates an output buffer and FFT plan
///This LEAVES OUT the nyquist bin. ifft doesn't return it because it is not very useful
///
class FFTR_FFTWd
{
    Range<double *>    m_input_data;
    Range<C_double *>  m_fft_data;
    Range<double   *>  m_psd_data;

    fftw_plan m_fft_plan;

    public:
    FFTR_FFTWd(size_t input_size, bool estimate = false);
    FFTR_FFTWd(const FFTR_FFTWd &) = delete;
    FFTR_FFTWd &operator=(const FFTR_FFTWd &) = delete;
    ~FFTR_FFTWd();

    Range<double *> input_data()
    { return m_input_data;}
    Range<C_double *> fft_data()
    { return m_fft_data;}
    Range<double   *> psd_data()
    { return m_psd_data;}

    void             compute_fft();
    void             compute_psd();
};

}; //~namespace ifft

#endif //H_IFFT_FFTW_ACCESS_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

