////
///
///

#include <stdexcept>
#include <iostream>

#include "utilities/runtime_collections.hpp"

#include "ifft_floats_single.hpp"

#include "algorithms/float/fft_accessor.hpp"
#include "algorithms/float/decimation_in_freq.hpp"
#include "algorithms/float/decimation_in_time.hpp"
#include "algorithms/float/decimation_in_freq_opt.hpp"
#include "algorithms/float/decimation_in_freq_unroll.hpp"
#include "algorithms/float/decimation_in_freq_fullstage.hpp"

namespace ifft {

unsigned constexpr generate_min_exp_size = 1;
unsigned constexpr generate_max_exp_size = 20;

const unsigned FFTPlanFloat::s_min_exp = generate_min_exp_size;
const unsigned FFTPlanFloat::s_max_exp = generate_max_exp_size;

template<unsigned E_FFT>
struct FloatFFTGenerator
{
    static constexpr FFTPlanFloat::TFloatFFTInPlace value = FFT<FFTPlanFloat::TFloat, E_FFT, 1, FreqAlgorithmsFullStage<10> >::compute;
};

const FFTPlanFloat::TFloatFFTInPlace * FFTPlanFloat::
    s_algorithm_set = GeneratorList<generate_min_exp_size, generate_max_exp_size, FloatFFTGenerator>::collection;

bool is_pwr_2(unsigned value)
{
    return bool(value && (value - 1));
}
unsigned pwr_2_log(unsigned value)
{
    int count = 0;
    value = value/2;
    while(value){
        value = value/2;
        count += 1;
    }
    return count;
}

FFTPlanFloat::
    FFTPlanFloat(unsigned size) : 
        m_size(size)
{
    if(not is_pwr_2(size)){
        throw std::logic_error("FFTs currently implemented only for powers of 2");
    }
    auto e_fft = pwr_2_log(size);
    if(e_fft < s_min_exp){
        throw std::logic_error("FFTs size too small (not implemented in library)");
    }
    if(e_fft >= s_max_exp){
        throw std::logic_error("FFT size too large (not implemented in library)");
    }
    m_planned_algorithm = s_algorithm_set[e_fft - s_min_exp];
}

FFTPlanFloat::
    FFTPlanFloat(const FFTPlanFloat &other)
{
    m_size = other.m_size;
    m_planned_algorithm = other.m_planned_algorithm;
    return;
}

void FFTPlanFloat::
    transform(Range<complex<TFloat> *> data)
{
    if(data.size() != m_size)
    {
        throw std::logic_error("FFT input not the correct size for this plan");
    }
    m_planned_algorithm(data.begin());
}

//////
///  Out Of Order transform
///

const unsigned FFTPlanFloatOOO::s_min_exp = generate_min_exp_size;
const unsigned FFTPlanFloatOOO::s_max_exp = generate_max_exp_size;

template<unsigned E_FFT>
struct FloatFFTOOOGenerator
{
    static constexpr FFTPlanFloatOOO::TFloatFFTInPlace value = FFT<FFTPlanFloatOOO::TFloat, E_FFT, 1, FreqAlgorithms>::compute_ooo;
};

const FFTPlanFloatOOO::TFloatFFTInPlace * FFTPlanFloatOOO::
    s_algorithm_set = GeneratorList<generate_min_exp_size, generate_max_exp_size, FloatFFTOOOGenerator>::collection;

template<unsigned E_FFT>
struct FloatReorderGenerator
{
    static constexpr FFTPlanFloatOOO::TFloatFFTInPlace value = FFT<FFTPlanFloatOOO::TFloat, E_FFT, 1, FreqAlgorithms>::post_reorder;
};

const FFTPlanFloatOOO::TFloatFFTInPlace * FFTPlanFloatOOO::
    s_reorder_set = GeneratorList<generate_min_exp_size, generate_max_exp_size, FloatReorderGenerator>::collection;

FFTPlanFloatOOO::
    FFTPlanFloatOOO(unsigned size) : 
        m_size(size)
{
    if(not is_pwr_2(size)){
        throw std::logic_error("FFTs currently implemented only for powers of 2");
    }
    auto e_fft = pwr_2_log(size);
    if(e_fft < s_min_exp){
        throw std::logic_error("FFTs size too small (not implemented in library)");
    }
    if(e_fft >= s_max_exp){
        throw std::logic_error("FFT size too large (not implemented in library)");
    }
    m_planned_algorithm = s_algorithm_set[e_fft - s_min_exp];
    m_planned_reorder   = s_reorder_set[e_fft - s_min_exp];
}

FFTPlanFloatOOO::
    FFTPlanFloatOOO(const FFTPlanFloatOOO &other)
{
    m_size = other.m_size;
    m_planned_algorithm = other.m_planned_algorithm;
    m_planned_reorder   = other.m_planned_reorder;
    return;
}

void FFTPlanFloatOOO::
    transform_ooo(Range<complex<TFloat> *> data)
{
    if(data.size() != m_size)
    {
        throw std::logic_error("FFT input not the correct size for this plan");
    }
    m_planned_algorithm(data.begin());
}

void FFTPlanFloatOOO::
    reorder_post(Range<complex<TFloat> *> data)
{
    if(data.size() != m_size)
    {
        throw std::logic_error("FFT input not the correct size for this plan");
    }
    m_planned_reorder(data.begin());
}

}; //~namespace ifft

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

