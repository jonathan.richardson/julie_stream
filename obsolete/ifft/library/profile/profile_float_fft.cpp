////
///
///

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <chrono>
#include <complex>
#include <cmath>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>

#include "ifft_floats.hpp"
#include "easy_fftw.hpp"

namespace ifft {

}; //~namespace ifft

using namespace ifft;
using std::complex;

void profile(int E_FFT)
{
	typedef float TData;
    typedef complex<TData> TCData;
	typedef int32_t TIData;
    typedef complex<int32_t> TCIData;

    auto N_FFT = unsigned(1)<<E_FFT;
    std::cout << "E_FFT: " << E_FFT << std::endl;

    FFT_FFTWf    fftw_plan(N_FFT);
    FFTPlanFloat ifft_plan(N_FFT);

    boost::mt19937 entropy_source;
    boost::random::normal_distribution<TData> noise_gen(0. /*mean*/, 1. /*std dev.*/);

    // sample data
    auto ifft_data      = Range<TCData*>(new TCData[N_FFT], N_FFT);
    auto ifft_idata     = Range<TCIData*>(new TCIData[N_FFT], N_FFT);
    auto ifft_data_copy = Range<TCData*>(new TCData[N_FFT], N_FFT);

    for (auto idx : ifft_data.idx_range()) {
        ifft_data[idx] = TCData(noise_gen(entropy_source), noise_gen(entropy_source));
        ifft_data_copy[idx] = ifft_data[idx];
        ifft_idata[idx] = ifft_data[idx];
        fftw_plan.input_data()[idx] = ifft_data[idx];
    }

    double time_to_measure_s = 1.;

    double total_time;

    double fftw_time_total;
    double fftw_time_sq_total;
    unsigned fftw_samples = 0;

    double ifft_time_total;
    double ifft_time_sq_total;
    unsigned ifft_samples = 0;
    while( total_time < time_to_measure_s ){
        {
            auto time_before = std::chrono::high_resolution_clock::now();
            fftw_plan.compute_fft();
            auto time_after  = std::chrono::high_resolution_clock::now();
            double duration_s = std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1> > >(time_after - time_before).count();
            fftw_time_total += duration_s;
            fftw_time_sq_total += duration_s*duration_s;
            fftw_samples += 1;

            total_time += duration_s;
        }
        {
            auto time_before = std::chrono::high_resolution_clock::now();
            ifft_plan.transform(ifft_data);
            auto time_after  = std::chrono::high_resolution_clock::now();
            double duration_s = std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1> > >(time_after - time_before).count();
            ifft_time_total += duration_s;
            ifft_time_sq_total += duration_s*duration_s;
            ifft_samples += 1;

            total_time += duration_s;
        }
    }

    double fftw_time_avg = fftw_time_total/fftw_samples;
    double fftw_time_stddev = std::sqrt(fftw_time_sq_total/fftw_samples - fftw_time_avg*fftw_time_avg);
    std::cout << "fftw avg: " << fftw_time_avg << std::endl;
    std::cout << "fftw std_dev: " << fftw_time_stddev << std::endl;

    double ifft_time_avg = ifft_time_total/ifft_samples;
    double ifft_time_stddev = std::sqrt(ifft_time_sq_total/ifft_samples - ifft_time_avg*ifft_time_avg);
    std::cout << "ifft avg: " << ifft_time_avg << std::endl;
    std::cout << "ifft std_dev: " << ifft_time_stddev << std::endl;

    std::cout << "ratio: " << ifft_time_avg / fftw_time_avg << std::endl;


}

BOOST_AUTO_TEST_CASE( profile_float_fft_10 )
{
    profile(13);
    profile(10);
}

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

