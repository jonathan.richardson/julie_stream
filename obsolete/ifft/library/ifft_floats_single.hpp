////
///
///

#ifndef H_IFFT_LIBRARY_FLOATS_SINGLE_H 
#define H_IFFT_LIBRARY_FLOATS_SINGLE_H

#include <complex>

#include "utilities/range.hpp"

namespace ifft {
using std::complex;
using utilities::Range;

class FFTPlanFloat
{
public:
    typedef float TFloat;
    typedef void (*TFloatFFTInPlace)(complex<TFloat> *);

private:
    /////
    ///  Generated array of floats to run from library collection
    ///  of generated algorithms
    static const TFloatFFTInPlace  *s_algorithm_set;
    static const unsigned           s_min_exp;
    static const unsigned           s_max_exp;

    /////
    ///  Runtime values
    unsigned int      m_size;
    TFloatFFTInPlace  m_planned_algorithm;

public:
    FFTPlanFloat(unsigned size);
    FFTPlanFloat(const FFTPlanFloat &);
    FFTPlanFloat &operator=(const FFTPlanFloat &);

    void transform(Range<complex<TFloat> *> data);
};

class FFTPlanFloatOOO
{
public:
    typedef float TFloat;
    typedef void (*TFloatFFTInPlace)(complex<TFloat> *);
    typedef void (*TFloatReorder)(complex<TFloat> *);

private:
    /////
    ///  Generated array of floats to run from library collection
    ///  of generated algorithms
    static const TFloatFFTInPlace  *s_algorithm_set;
    static const TFloatReorder     *s_reorder_set;
    static const unsigned           s_min_exp;
    static const unsigned           s_max_exp;

    /////
    ///  Runtime values
    unsigned int      m_size;
    TFloatFFTInPlace  m_planned_algorithm;
    TFloatReorder     m_planned_reorder;

public:
    FFTPlanFloatOOO(unsigned size);
    FFTPlanFloatOOO(const FFTPlanFloatOOO &);

    void transform_ooo(Range<complex<TFloat> *> data);
    void reorder_post(Range<complex<TFloat> *> data);
};

}; //~namespace ifft

#endif //H_IFFT_LIBRARY_FLOATS_SINGLE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

