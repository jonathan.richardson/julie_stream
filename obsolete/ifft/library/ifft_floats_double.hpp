////
///
///

#ifndef H_IFFT_LIBRARY_FLOATS_DOUBLE_H
#define H_IFFT_LIBRARY_FLOATS_DOUBLE_H

#include <complex>

#include "utilities/range.hpp"

namespace ifft {
using std::complex;
using utilities::Range;

class FFTPlanDouble
{
public:
    typedef double TFloat;
    typedef void (*TFloatFFTInPlace)(complex<TFloat> *);

private:
    /////
    ///  Generated array of doubles to run from library collection
    ///  of generated algorithms
    static const TFloatFFTInPlace *s_algorithm_set;
    static const unsigned          s_min_exp;
    static const unsigned          s_max_exp;

    /////
    ///  Runtime values
    unsigned int      m_size;
    TFloatFFTInPlace  m_planned_algorithm;

public:
    FFTPlanDouble(unsigned size);
    FFTPlanDouble(const FFTPlanDouble &);
    FFTPlanDouble &operator=(const FFTPlanDouble &);

    /////
    ///  Actual FFT calls
    void operator()(Range<complex<TFloat> *> data);

    void operator()(complex<TFloat> *data_start, complex<TFloat> *data_end)
    {
        return (*this)(Range<complex<TFloat> *>(data_start, data_end));
    }

    void operator()(complex<TFloat> *data_start, size_t length)
    {
        return (*this)(Range<complex<TFloat> *>(data_start, length));
    }

    void operator()(TFloat *data_start[2], size_t length)
    {
        return (*this)(Range<complex<TFloat> *>(reinterpret_cast<complex<TFloat> *>(data_start), length));
    }
};

}; //~namespace ifft

#endif //H_IFFT_LIBRARY_FLOATS_DOUBLE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

