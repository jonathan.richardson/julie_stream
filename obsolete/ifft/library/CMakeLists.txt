INCLUDE (TestBigEndian)
TEST_BIG_ENDIAN(SYSTEM_BIGENDIAN)
ADD_DEFINITIONS("-DSYSTEM_ISBIGENDIAN=${SYSTEM_BIGENDIAN}")

SET(CMAKE_CXX_FLAGS "-std=c++11")

SET(Boost_USE_MULTITHREADED)
FIND_PACKAGE(Boost 1.50 COMPONENTS
                        #unit_test_framework
                        #thread
                        #chrono
                        #filesystem
                        #system
                        REQUIRED
    )
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIR})

SET(OPT -O3)
SET(OPT "${OPT} -fomit-frame-pointer")
SET(OPT "${OPT} -march=native")
SET(OPT "${OPT} -mtune=native")
SET(OPT "${OPT} -fstrict-aliasing")
SET(OPT "${OPT} -fstrict-overflow")
SET(OPT "${OPT} -malign-double")
SET(OPT "${OPT} -fvariable-expansion-in-unroller")
SET(OPT "${OPT} -ffast-math")
SET(OPT "${OPT} -fassociative-math")
SET(OPT "${OPT} -freciprocal-math")
SET(OPT "${OPT} -fno-signed-zeros")
SET(OPT "${OPT} -fsingle-precision-constant")
SET(OPT "${OPT} -ftracer")
#SET(OPT "${OPT} -msse")
#SET(OPT "${OPT} -msse2")
#SET(OPT "${OPT} -msse3")
#SET(OPT "${OPT} -msse4")
SET(OPT "${OPT} -fselective-scheduling")
SET(OPT "${OPT} -fsel-sched-pipelining")
SET(OPT "${OPT} -fsel-sched-pipelining-outer-loops")
SET(OPT "${OPT} -fipa-pta")
SET(OPT "${OPT} -fno-schedule-insns")
SET(OPT "${OPT} -fschedule-insns")
SET(OPT "${OPT} -floop-block")
SET(OPT "${OPT} -fschedule-insns2")
SET(OPT "${OPT} -fsched-stalled-insns=2")
SET(OPT "${OPT} -fsched-stalled-insns-dep=2")
SET(OPT "${OPT} -fsched2-use-superblocks")
SET(OPT "${OPT} --param inline-unit-growth=200")
SET(OPT "${OPT} --param max-inline-insns-single=1000")
SET(OPT "${OPT} --param large-function-insns=5000")
SET(OPT "${OPT} --param large-unit-insns=500000")
SET(OPT "${OPT} --param l2-cache-size=256")
SET(OPT "${OPT} --param l1-cache-size=64")
SET(OPT "${OPT} -frename-registers")
#SET(OPT "${OPT} -ftree-vectorizer-verbose=5")
#SET(OPT "${OPT} -fsched-verbose=5")
#SET(OPT "${OPT} -fsel-sched-pipelining-verbose=5")
SET_SOURCE_FILES_PROPERTIES(fft_double_collect.cpp COMPILE_FLAGS "-O3 -march=native -mtune=native")
SET_SOURCE_FILES_PROPERTIES(fft_single_collect.cpp COMPILE_FLAGS ${OPT})


ADD_LIBRARY(ifft STATIC
    easy_fftw.cpp
    fft_double_collect.cpp
    fft_single_collect.cpp
    )

TARGET_LINK_LIBRARIES(ifft
    fftw3
    fftw3f
    ${Boost_LIBRARIES}
    )

ADD_SUBDIRECTORY(profile)

