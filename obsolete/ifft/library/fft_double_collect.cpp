////
///
///

#include "utilities/runtime_collections.hpp"

#include "ifft_floats_double.hpp"

#include "algorithms/float/fft_accessor.hpp"
#include "algorithms/float/decimation_in_freq.hpp"
#include "algorithms/float/decimation_in_freq_opt.hpp"
#include "algorithms/float/decimation_in_freq_unroll.hpp"

namespace ifft {

unsigned constexpr generate_min_exp_size = 0;
unsigned constexpr generate_max_exp_size = 25;

template<unsigned E_FFT>
struct DoubleFFTGenerator
{
    static constexpr FFTPlanDouble::TFloatFFTInPlace value = FFT<FFTPlanDouble::TFloat, E_FFT, 1, FreqAlgorithms>::compute;
};

const unsigned FFTPlanDouble::s_min_exp = generate_min_exp_size;
const unsigned FFTPlanDouble::s_max_exp = generate_max_exp_size;

const FFTPlanDouble::TFloatFFTInPlace * FFTPlanDouble::
    s_algorithm_set = GeneratorList<generate_min_exp_size, generate_max_exp_size, DoubleFFTGenerator>::collection;

}; //~namespace ifft

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

