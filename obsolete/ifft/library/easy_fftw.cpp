////
///
///

#include <iostream>

#include "easy_fftw.hpp"

namespace ifft
{

FFT_FFTWf::
    FFT_FFTWf(size_t input_size, bool estimate)
{
    size_t fft_size = input_size;
    
    m_input_data = Range<C_float *>(new C_float[input_size], input_size);
    m_fft_data   = Range<C_float *>(new C_float[fft_size], fft_size);
    m_psd_data   = Range<float   *>(new float[fft_size], fft_size);

    m_fft_plan = fftwf_plan_dft_1d(
                        input_size,
                        reinterpret_cast<float(*)[2]>(m_input_data.begin()),
                        reinterpret_cast<float(*)[2]>(m_fft_data.begin()),
						-1,
                        estimate ? FFTW_UNALIGNED | FFTW_ESTIMATE : (FFTW_DESTROY_INPUT | FFTW_MEASURE)
    );
    fftwf_print_plan(m_fft_plan);
    std::cout << std::endl;
}

FFT_FFTWf::
    ~FFT_FFTWf()
{
    fftwf_destroy_plan(m_fft_plan);

	delete[] m_fft_data.begin();
	delete[] m_input_data.begin();
	delete[] m_psd_data.begin();
}

void FFT_FFTWf::
    compute_fft()
{
    fftwf_execute(m_fft_plan);
}

void FFT_FFTWf::
    compute_psd()
{
	for(auto idx : m_fft_data.idx_range())
	{
		m_psd_data[idx] = std::abs(m_fft_data[idx]);
	}
}

FFTR_FFTWf::
    FFTR_FFTWf(size_t input_size, bool estimate)
{
    size_t fft_size = (input_size)/2 + 1 - 1;//removes the nyquist bin!
    
    m_input_data = Range<float *>(new float[input_size], input_size);
    //add space for the nyquist bin
    m_fft_data   = Range<C_float *>(new C_float[fft_size+1], fft_size);
    m_psd_data   = Range<float   *>(new float[fft_size], fft_size);

    m_fft_plan = fftwf_plan_dft_r2c_1d(
                        input_size,
                        m_input_data.begin(),
                        reinterpret_cast<float(*)[2]>(m_fft_data.begin()),
                        estimate ? FFTW_ESTIMATE : (FFTW_DESTROY_INPUT | FFTW_MEASURE)
    );
    //fftwf_print_plan(m_fft_plan);
}

FFTR_FFTWf::
    ~FFTR_FFTWf()
{
    fftwf_destroy_plan(m_fft_plan);

	delete[] m_fft_data.begin();
	delete[] m_input_data.begin();
	delete[] m_psd_data.begin();
}

void FFTR_FFTWf::
    compute_fft()
{
    fftwf_execute(m_fft_plan);
}

void FFTR_FFTWf::
    compute_psd()
{
	for(auto idx : m_fft_data.idx_range())
	{
		m_psd_data[idx] = std::abs(m_fft_data[idx]);
	}
}

FFT_FFTWd::
    FFT_FFTWd(size_t input_size, bool estimate)
{
    size_t fft_size = input_size;
    
    m_input_data = Range<C_double *>(new C_double[input_size], input_size);
    m_fft_data   = Range<C_double *>(new C_double[fft_size], fft_size);
    m_psd_data   = Range<double   *>(new double[fft_size], fft_size);

    m_fft_plan = fftw_plan_dft_1d(
                        input_size,
                        reinterpret_cast<double(*)[2]>(m_input_data.begin()),
                        reinterpret_cast<double(*)[2]>(m_fft_data.begin()),
						-1,
                        estimate ? FFTW_ESTIMATE : (FFTW_DESTROY_INPUT | FFTW_MEASURE)
    );
    //fftw_print_plan(m_fft_plan);
}

FFT_FFTWd::
    ~FFT_FFTWd()
{
    fftw_destroy_plan(m_fft_plan);

	delete[] m_fft_data.begin();
	delete[] m_input_data.begin();
	delete[] m_psd_data.begin();
}

void FFT_FFTWd::
    compute_fft()
{
    fftw_execute(m_fft_plan);
}

void FFT_FFTWd::
    compute_psd()
{
	for(auto idx : m_fft_data.idx_range())
	{
		m_psd_data[idx] = std::abs(m_fft_data[idx]);
	}
}

FFTR_FFTWd::
    FFTR_FFTWd(size_t input_size, bool estimate)
{
    size_t fft_size = (input_size)/2 + 1 - 1;//removes the nyquist bin!
    
    m_input_data = Range<double *>(new double[input_size], input_size);
    //add space for the nyquist bin
    m_fft_data   = Range<C_double *>(new C_double[fft_size+1], fft_size);
    m_psd_data   = Range<double   *>(new double[fft_size], fft_size);

    m_fft_plan = fftw_plan_dft_r2c_1d(
                        input_size, //fftw needs that nyquist bin
                        m_input_data.begin(),
                        reinterpret_cast<double(*)[2]>(m_fft_data.begin()),
                        estimate ? FFTW_ESTIMATE : (FFTW_DESTROY_INPUT | FFTW_MEASURE)
    );
    //fftwf_print_plan(m_fft_plan);
}

FFTR_FFTWd::
    ~FFTR_FFTWd()
{
    fftw_destroy_plan(m_fft_plan);

	delete[] m_fft_data.begin();
	delete[] m_input_data.begin();
	delete[] m_psd_data.begin();
}

void FFTR_FFTWd::
    compute_fft()
{
    fftw_execute(m_fft_plan);
}

void FFTR_FFTWd::
    compute_psd()
{
	for(auto idx : m_fft_data.idx_range())
	{
		m_psd_data[idx] = std::abs(m_fft_data[idx]);
	}
}


}; //~namespace ifft

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

