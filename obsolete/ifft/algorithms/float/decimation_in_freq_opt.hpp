////
///
///

#ifndef H_IFFT_FLOAT_DECIMATION_IN_FREQ_OPT_H
#define H_IFFT_FLOAT_DECIMATION_IN_FREQ_OPT_H

#include "decimation_in_freq.hpp"

namespace ifft {

struct FreqAlgorithmsOpt : public FreqAlgorithms
{
    template<typename TNum, size_t _E_Stage, int Direction, typename Algorithms>
    struct Butterfly2x
    {
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in)
        {
            data_in = (complex<TNum> *)__builtin_assume_aligned(data_in, 16);
            complex<TNum> RU_twiddle0(1.,0.);
            complex<TNum> RU_twiddle1(CTCos(2*C_PI/N_Stage), -Direction*CTSin(2*C_PI/N_Stage));
            complex<TNum> RU_rotator(CTCos(2*C_PI*2/N_Stage), -Direction*CTSin(2*C_PI*2/N_Stage));

            for (unsigned long idx=0; idx<N_Stage_2; idx+=2) {
                complex<TNum> temp0 = data_in[idx]-data_in[idx+N_Stage_2];
                complex<TNum> temp1 = data_in[idx+1]-data_in[idx+1+N_Stage_2];
                data_in[idx] += data_in[idx+N_Stage_2];
                data_in[idx+1] += data_in[idx+1+N_Stage_2];
                data_in[idx+N_Stage_2] = RU_twiddle0 * temp0;
                data_in[idx+1+N_Stage_2] = RU_twiddle1 * temp1;
                RU_twiddle0 = RU_twiddle0*RU_rotator;
                RU_twiddle1 = RU_twiddle1*RU_rotator;
            }
        }
    };

    template<typename TNum, int Direction, typename Algorithms>
    struct FFT_OOO_MICRO_1
    {
        static constexpr size_t E_Stage = 1;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in)
        {
            complex<TNum> temp = data_in[0]-data_in[1];
            data_in[0] += data_in[1];
            data_in[1] = temp;
        }
    };

    template<typename TNum, int Direction, typename Algorithms>
    struct FFT_OOO_MICRO_2
    {
        static constexpr size_t E_Stage = 2;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in)
        {
            data_in = (complex<TNum> *)__builtin_assume_aligned(data_in, 16);
            complex<TNum> temp;
            temp = data_in[0]-data_in[2];
            data_in[0] += data_in[2];
            data_in[2] = temp;

            temp = data_in[1]-data_in[3];
            data_in[1] += data_in[3];
            data_in[3] = complex<TNum>(
                            Direction * temp.imag(),
                           -Direction * temp.real()
                                      );

            temp = data_in[0]-data_in[1];
            data_in[0] += data_in[1];
            data_in[1] = temp;

            temp = data_in[2]-data_in[3];
            data_in[2] += data_in[3];
            data_in[3] = temp;
        }
    };

    template<typename TNum, int Direction, typename Algorithms>
    struct FFT_OOO_MICRO_2_USEVAR
    {
        static constexpr size_t E_Stage = 2;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in)
        {
            data_in = (complex<TNum> *)__builtin_assume_aligned(data_in, 16);
            complex<TNum> A1 = data_in[0];
            complex<TNum> B1 = data_in[1];
            complex<TNum> C1 = data_in[2];
            complex<TNum> D1 = data_in[3];

            complex<TNum> A2 = A1+C1;
            complex<TNum> B2 = B1+D1;
            complex<TNum> C2 = A1-C1;
            complex<TNum> D2 = complex<TNum>(
                                 Direction * (B1.imag()-D1.imag()), 
                                -Direction * (B1.real()-D1.real())
                                            );

            data_in[0] = A2+B2;
            data_in[1] = A2-B2;

            data_in[2] = C2+D2;
            data_in[3] = C2-D2;
        }
    };

    template<typename TNum, int Direction, typename Algorithms>
    struct Butterfly_MICRO_3
    {
        static constexpr size_t E_Stage = 3;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in)
        {
            data_in = (complex<TNum> *)__builtin_assume_aligned(data_in, 16);
            constexpr TNum invroot2 = CTCos(2*C_PI/N_Stage);

            complex<TNum> temp;

            //RU_twiddle = (1, 0);
            temp = data_in[0]-data_in[4];
            data_in[0] += data_in[4];
            data_in[4] = temp;

            //RU_twiddle = (invroot2, -Direction*invroot2);
            temp = data_in[1]-data_in[5];
            data_in[1] += data_in[5];
            data_in[5] = complex<TNum>(
                            invroot2 * (temp.real() + Direction * temp.imag()), 
                            invroot2 * (temp.imag() - Direction * temp.real())
                                      );

            //RU_twiddle = (0, -Direction);
            temp = data_in[2]-data_in[6];
            data_in[2] += data_in[6];
            data_in[6] = complex<TNum>(Direction * temp.imag(), -Direction * temp.real());

            //RU_twiddle = (-invroot2, -Direction*invroot2);
            temp = data_in[3]-data_in[7];
            data_in[3] += data_in[7];
            data_in[7] = complex<TNum>(
                            invroot2 * (-temp.real() + Direction * temp.imag()), 
                            invroot2 * (-temp.imag() - Direction * temp.real())
                                      );
        }
    };

    template<typename TNum, int Direction, typename Algorithms>
    struct FFT_OOO_MICRO_3
    {
        static constexpr size_t E_Stage = 3;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template Butterfly_MICRO_3<TNum, Direction, Algorithms>::apply(data_in);
            Algorithms::template FFT_OOO_MICRO_2_USEVAR<TNum, Direction, Algorithms>::apply(data_in);
            Algorithms::template FFT_OOO_MICRO_2_USEVAR<TNum, Direction, Algorithms>::apply(data_in+N_Stage/2);
        }
    };

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int Direction, typename Algorithms>
    struct CooleyTukey{
        static constexpr size_t E_FFT = _E_FFT;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template Butterfly2x<TNum, E_Stage, Direction, Algorithms>::apply(data_in);
            Algorithms::template CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in);
            Algorithms::template CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in + N_Stage/2);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct CooleyTukey<TNum, _E_FFT, 0, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in) { }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct CooleyTukey<TNum, _E_FFT, 1, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template FFT_OOO_MICRO_1<TNum, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct CooleyTukey<TNum, _E_FFT, 2, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in)
        {
            //Algorithms::template FFT_OOO_MICRO_2_USEVAR<TNum, Direction, Algorithms>::apply(data_in);
            Algorithms::template FFT_OOO_MICRO_2<TNum, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct CooleyTukey<TNum, _E_FFT, 3, Direction, Algorithms>{
        static constexpr size_t E_FFT = _E_FFT;
        static constexpr size_t E_Stage = 3;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template FFT_OOO_MICRO_3<TNum, Direction, Algorithms>::apply(data_in);
        }
    };
};

}; //~namespace ifft

#endif //H_IFFT_FLOAT_DECIMATION_IN_FREQ_OPT_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

