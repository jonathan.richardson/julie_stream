////
///
///

#ifndef H_IFFT_FLOAT_DECIMATION_IN_FREQ_FULLSTAGE_H
#define H_IFFT_FLOAT_DECIMATION_IN_FREQ_FULLSTAGE_H

#include "decimation_in_freq.hpp"
#include "decimation_in_freq_opt.hpp"

namespace ifft {

template<int _UNROLL_LEVEL=6>
struct FreqAlgorithmsFullStage : public FreqAlgorithmsOpt
{
    static constexpr int UNROLL_LEVEL = _UNROLL_LEVEL;

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int UpDown, int Direction, typename Algorithms>
    struct ButterflyFullStage
    {
        static constexpr size_t E_FFT   = _E_FFT;
        static constexpr size_t N_FFT   = 1<<E_FFT;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;
        static constexpr size_t E_Blocks = E_FFT - E_Stage;
        static constexpr size_t N_Blocks = 1<<E_Blocks;

        static inline void apply(complex<TNum> *data_in)
        {
            data_in = (complex<TNum> *)__builtin_assume_aligned(data_in, 16);
            complex<TNum> RU_rotator(CTCos(2*C_PI/N_Stage), -(UpDown*Direction)*CTSin(2*C_PI/N_Stage));
            if(UpDown > 0){
                for (unsigned long idx_block=0; idx_block < N_Blocks; idx_block+=1) {
                    complex<TNum> RU_twiddle(1.,0.);
                    for (unsigned long idx_data=0; idx_data<N_Stage_2; idx_data += 1) {

                        complex<TNum> A = data_in[idx_data];
                        complex<TNum> B = data_in[idx_data+N_Stage_2];
                        complex<TNum> TW_temp = RU_twiddle;
                        data_in[idx_data] += B;
                        data_in[idx_data+N_Stage_2] = TW_temp * (A-B);
                        RU_twiddle = RU_twiddle*RU_rotator;
                    }
                    data_in += N_Stage;
                }
            }else{
                data_in += N_FFT;
                for (unsigned long idx_block=0; idx_block < N_Blocks; idx_block+=1) {
                    data_in -= N_Stage;
                    complex<TNum> RU_twiddle(1.,0.);
                    for (unsigned long idx_data=N_Stage_2; idx_data>0; idx_data -= 1) {
                        complex<TNum> A = data_in[idx_data-1];
                        complex<TNum> B = data_in[idx_data-1+N_Stage_2];
                        complex<TNum> TW_temp = RU_twiddle;
                        data_in[idx_data-1] += B;
                        data_in[idx_data-1+N_Stage_2] = TW_temp * (A-B);
                        RU_twiddle = RU_twiddle*RU_rotator;
                    }
                }
            }
        }
    };

    template<size_t _IDX_point, typename TNum, size_t _E_Stage, int Direction, typename Algorithms>
    struct ButterflySingleA
    {
        /////
        ///  _IDX_point runs from 0 to N_Stage/2
        static constexpr size_t IDX_point = _IDX_point;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in) __attribute__((always_inline))
        {
            constexpr complex<TNum> RU_twiddle(CTCos(2*C_PI*IDX_point/N_Stage), -Direction*CTSin(2*C_PI*IDX_point/N_Stage));
            complex<TNum> A = data_in[IDX_point];
            complex<TNum> B = data_in[IDX_point+N_Stage_2];
            data_in[IDX_point] += B;
            data_in[IDX_point+N_Stage_2] = RU_twiddle * (A-B);
        }
    };

    template<size_t _IDX_point, typename TNum, size_t _E_Stage, int Direction, typename Algorithms>
    struct ButterflySingleB
    {
        /////
        ///  _IDX_point runs from 0 to N_Stage/2
        static constexpr size_t IDX_point = _IDX_point;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in) __attribute__((always_inline))
        {
            constexpr complex<TNum> RU_twiddle(CTCos(2*C_PI*(IDX_point-N_Stage_2)/N_Stage), 
                    -Direction*CTSin(2*C_PI*(IDX_point-N_Stage_2)/N_Stage));
            complex<TNum> A = data_in[IDX_point];
            complex<TNum> B = data_in[IDX_point+N_Stage_2];
            data_in[IDX_point] += B;
            data_in[IDX_point+N_Stage_2] = RU_twiddle * (B-A);
        }
    };

    template<size_t _IDX_point, typename TNum, size_t _E_Stage, int Direction, typename Algorithms>
    struct ButterflySingle : public std::conditional<
                                       ( _IDX_point < 1<<(_E_Stage-1)),
                                       ButterflySingleA<_IDX_point, TNum, _E_Stage, Direction, Algorithms>,
                                       ButterflySingleB<_IDX_point, TNum, _E_Stage, Direction, Algorithms>
                                                 >::type
    {};

    template<typename TNum, size_t _E_Stage, int Direction, typename Algorithms>
    struct ButterflyUnroll
    {
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in) __attribute__((always_inline))
        {
            APPLY_BISECT<0, N_Stage_2-1, TNum>::apply(data_in);
        }

        template<size_t LO, size_t HI, typename _P>
        struct APPLY_BISECT
        {
            static inline void apply(complex<TNum> *data_in) __attribute__((always_inline))
            {
                APPLY_BISECT<LO, LO+((HI-LO)/2), _P>::apply(data_in);
                APPLY_BISECT<1+LO+((HI-LO)/2), HI, _P>::apply(data_in);
            }
        };

        template<size_t LO, typename _P>
        struct APPLY_BISECT<LO, LO, _P>
        {
            static inline void apply(complex<TNum> *data_in) __attribute__((always_inline))
            {
                Algorithms::template ButterflySingle<LO, TNum, E_Stage, Direction, Algorithms>::apply(data_in);
            }
        };
    };

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int UpDown, int Direction, typename Algorithms>
    struct ButterflyFullStageUnroll
    {
        static constexpr size_t E_FFT   = _E_FFT;
        static constexpr size_t N_FFT   = 1<<E_FFT;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;
        static constexpr size_t E_Blocks = E_FFT - E_Stage;
        static constexpr size_t N_Blocks = 1<<E_Blocks;

        static inline void apply(complex<TNum> *data_in)
        {
            if(UpDown > 0){
                for (unsigned long idx_block=0; idx_block < N_Blocks; idx_block+=1) {
                    ButterflyUnroll<TNum, E_Stage, Direction, Algorithms>::apply(data_in);
                    data_in += N_Stage;
                }
            }else{
                data_in += N_FFT;
                for (unsigned long idx_block=0; idx_block < N_Blocks; idx_block+=1) {
                    data_in -= N_Stage;
                    ButterflyUnroll<TNum, E_Stage, Direction, Algorithms>::apply(data_in);
                }
            }
        }
    };

    template<typename TNum, int _E_FFT, int UpDown, int Direction, typename Algorithms>
    struct FFT_OOO_MICRO_3_Fullstage
    {
        static constexpr size_t E_FFT   = _E_FFT;
        static constexpr size_t N_FFT   = 1<<E_FFT;
        static constexpr size_t E_Stage = 3;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;
        static constexpr size_t E_Blocks = E_FFT - E_Stage;
        static constexpr size_t N_Blocks = 1<<E_Blocks;

        static inline void apply(complex<TNum> *data_in)
        {
            if(UpDown > 0){
                for (unsigned long idx_block=0; idx_block < N_Blocks; idx_block+=1) {
                    Algorithms::template FFT_OOO_MICRO_3<TNum, Direction, Algorithms>::apply(data_in);
                    data_in += N_Stage;
                }
            }else{
                data_in += N_FFT;
                for (unsigned long idx_block=0; idx_block < N_Blocks; idx_block+=1) {
                    data_in -= N_Stage;
                    Algorithms::template FFT_OOO_MICRO_3<TNum, Direction, Algorithms>::apply(data_in);
                }
            }
        }
    };

    template<typename TNum, int _E_FFT, int UpDown, int Direction, typename Algorithms>
    struct FFT_OOO_UNROLL_3_Fullstage
    {
        static constexpr size_t E_FFT   = _E_FFT;
        static constexpr size_t N_FFT   = 1<<E_FFT;
        static constexpr size_t E_Stage = 3;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;
        static constexpr size_t E_Blocks = E_FFT - E_Stage;
        static constexpr size_t N_Blocks = 1<<E_Blocks;

        static inline void apply(complex<TNum> *data_in)
        {
            for (unsigned long idx_block=0; idx_block < N_FFT; idx_block+=N_Stage * UpDown) {
                Algorithms::template FFT_OOO_UNROLL_3<TNum, Direction, Algorithms>::apply(data_in+idx_block);
            }
        }
    };

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int Direction, typename Algorithms>
    struct __CooleyTukey{
        static constexpr size_t E_FFT = _E_FFT;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template ButterflyFullStage< 
                TNum, E_FFT, E_Stage, (2*int(E_Stage%2) - 1) /*alternate*/,Direction, Algorithms
                >::apply(data_in);
            Algorithms::template CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukey<TNum, _E_FFT, 0, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in) { }
    };

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int Direction, typename Algorithms>
    struct __CooleyTukeyUnroller{
        static constexpr size_t E_FFT = _E_FFT;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template ButterflyFullStageUnroll< 
                TNum, E_FFT, E_Stage, (2*int(E_Stage%2) - 1) /*alternate*/,Direction, Algorithms
                >::apply(data_in);
            Algorithms::template CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukeyUnroller<TNum, _E_FFT, 0, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in) { }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukeyUnroller<TNum, _E_FFT, 3, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template FFT_OOO_MICRO_3_Fullstage<TNum, _E_FFT, 1, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int Direction, typename Algorithms>
    struct CooleyTukey : public std::conditional<
                                       ( _E_Stage <= UNROLL_LEVEL ),
                                       __CooleyTukeyUnroller<TNum, _E_FFT, _E_Stage, Direction, Algorithms>,
                                       __CooleyTukey<TNum, _E_FFT, _E_Stage, Direction, Algorithms>
                                                 >::type
    { };
};

}; //~namespace ifft

#endif //H_IFFT_FLOAT_DECIMATION_IN_FREQ_FULLSTAGE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

