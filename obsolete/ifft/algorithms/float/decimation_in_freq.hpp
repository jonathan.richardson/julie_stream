////
///
///

#ifndef H_IFFT_FLOAT_DECIMATION_IN_FREQ_H
#define H_IFFT_FLOAT_DECIMATION_IN_FREQ_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include <complex>

#include "algorithms/static_math.hpp"
#include "algorithms/algorithms_base.hpp"

namespace ifft {
using std::complex;

struct FreqAlgorithms : public AlgorithmsBase
{
    template<typename TNum, size_t _E_Stage, int Direction, typename Algorithms>
    struct Butterfly
    {
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in)
        {
            data_in = (complex<TNum> *)__builtin_assume_aligned(data_in, 16);
            complex<TNum> RU_twiddle(1.,0.);
            complex<TNum> RU_rotator(CTCos(2*C_PI/N_Stage), -Direction*CTSin(2*C_PI/N_Stage));

            for (unsigned long idx=0; idx<N_Stage_2; ++idx) {
                complex<TNum> temp = data_in[idx]-data_in[idx+N_Stage_2];
                data_in[idx] += data_in[idx+N_Stage_2];
                data_in[idx+N_Stage_2] = RU_twiddle * temp;
                RU_twiddle = RU_twiddle*RU_rotator;
            }
        }
    };

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int Direction, typename Algorithms>
    struct CooleyTukey{
        static constexpr size_t E_FFT = _E_FFT;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template Butterfly<TNum, E_Stage, Direction, Algorithms>::apply(data_in);
            Algorithms::template CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in);
            Algorithms::template CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in + N_Stage/2);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct CooleyTukey<TNum, _E_FFT, 0, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in) { }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct FFT_OOO
    {
        static void apply(complex<TNum> *data_in)
        {
            Algorithms::template CooleyTukey<TNum, _E_FFT, _E_FFT, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct FFT
    {
        static void apply(complex<TNum> *data_in)
        {
            Algorithms::template FFT_OOO<TNum, _E_FFT, Direction, Algorithms>::apply(data_in);
            Algorithms::template FFTPostOrder<TNum, _E_FFT, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, typename Algorithms>
    struct FFTPostOrder : public LocationBitSwap<TNum, _E_FFT, Algorithms>
    { };

    //////
    ///  DOES NOT COMPUTE THE NYQUIST BIN
    template<typename TNum, size_t _E_RFFT, typename Algorithms>
    struct FFTReal
    {
        static constexpr int Direction = 1;
        static void apply(TNum *data_in)
        {
            auto data_in_cplx = reinterpret_cast<complex<TNum> *>(data_in);
            /////
            ///  Compute the FFT as usual on half the number of points, with the time interlaced
            ///  between real and complex parts
            Algorithms::template FFT_OOO<TNum, _E_RFFT-1, Direction, Algorithms>::apply(data_in_cplx);
            Algorithms::template FFTPostOrder<TNum, _E_RFFT-1, Algorithms>::apply(data_in_cplx);
            Algorithms::template R2CEndButterflyTime<TNum, _E_RFFT, Algorithms>::apply(data_in_cplx);
        }
    };
    //////
    ///  DOES NOT COMPUTE THE NYQUIST BIN
    template<typename TNum, size_t _E_RFFT, typename Algorithms>
    struct FFTReal_OOO
    {
        static constexpr int Direction = 1;
        static void apply(TNum *data_in)
        {
            auto data_in_cplx = reinterpret_cast<complex<TNum> *>(data_in);
            /////
            ///  Compute the FFT as usual on half the number of points, with the time interlaced
            ///  between real and complex parts
            Algorithms::template FFT_OOO<TNum, _E_RFFT-1, Direction, Algorithms>::apply(data_in_cplx);
            Algorithms::template R2CEndButterflyTime<TNum, _E_RFFT, Algorithms>::apply(data_in_cplx);
        }
    };

    template<typename TNum, size_t _E_RFFT, typename Algorithms>
    struct R2CEndButterflyTime
    {
        static constexpr size_t E_RFFT = _E_RFFT;
        static constexpr size_t N_RFFT = 1<<E_RFFT;
        static constexpr size_t N_RFFT_2 = N_RFFT/2;
        static constexpr size_t N_RFFT_4 = N_RFFT/4;
        static constexpr int Direction = 1;

        static inline void apply(complex<TNum> *data_in)
        {
            /////
            ///  There are only N_RFFT/2 data points

            complex<TNum> RU_rotator(CTCos(2*C_PI/N_RFFT), -Direction*CTSin(2*C_PI/N_RFFT));

            /////
            ///  This special butterfly's twiddle factor starts at 1 rotation past i, the 1 rotation 
            ///  because the for loop skips idx 0, and the i because the separation equations need 
            ///  it.
            complex<TNum> RU_twiddle = complex<TNum>(0, 1) * RU_rotator;//complex<TNum>(Direction*CTSin(2*C_PI/N_RFFT), CTCos(2*C_PI/N_RFFT));
            complex<TNum> RU_twiddle2 = complex<TNum>(-1, 0) / RU_rotator;//complex<TNum>(Direction*CTSin(2*C_PI/N_RFFT), CTCos(2*C_PI/N_RFFT));

            for (unsigned long idx=1; idx<N_RFFT_4; ++idx) {
                unsigned long idx_refl = N_RFFT_2-idx;

                complex<TNum> Y_idx = data_in[idx];
                complex<TNum> Y_rev = data_in[idx_refl];
                TNum OneSubTwiddle_r = 1-RU_twiddle.real();
                TNum OnePlsTwiddle_r = 1+RU_twiddle.real();

                data_in[idx] = ((1.-RU_twiddle) * Y_idx + (1.+RU_twiddle) * std::conj(Y_rev))/2.;
                data_in[idx_refl] = ((1.-RU_twiddle) * Y_rev + (1.+RU_twiddle2) * std::conj(Y_idx))/2.;
                /*
                data_in[idx] = std::complex<TNum>(
                                        (OneSubTwiddle_r * Y_idx.real() - RU_twiddle.imag() * Y_idx.imag() +
                                         OnePlsTwiddle_r * Y_rev.real() + RU_twiddle.imag() * Y_rev.imag())/2
                                        ,
                                       -(OnePlsTwiddle_r * Y_idx.real() - RU_twiddle.imag() * Y_idx.imag() +
                                         OneSubTwiddle_r * Y_rev.real() + RU_twiddle.imag() * Y_rev.imag())/2
                                                  );

                data_in[idx_refl] =  std::complex<TNum>(
                                       (-OneSubTwiddle_r * Y_idx.imag() - RU_twiddle.imag() * Y_idx.real() +
                                        OnePlsTwiddle_r * Y_rev.imag() - RU_twiddle.imag() * Y_rev.real())/2
                                        ,
                                       -(OnePlsTwiddle_r * Y_idx.imag() + RU_twiddle.imag() * Y_idx.real() +
                                        -OneSubTwiddle_r * Y_rev.imag() + RU_twiddle.imag() * Y_rev.real())/2
                                                  );
                */

                RU_twiddle = RU_twiddle * RU_rotator;
                RU_twiddle2 = RU_twiddle2 / RU_rotator;
            }
            //////
            ///  Now fill the mean value bin (here just by erasing the nyquist bin data)
            complex<TNum> T_0 = data_in[0];
            data_in[0] = complex<TNum>(T_0.real()+T_0.imag(),0);
        }
    };

    template<typename TNum, size_t _E_Stage, typename Algorithms>
    struct R2CButterflyInFreq
    {
        ///////
        ///  NOT WORKING (YET)
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;
        static constexpr int Direction = 1;

        static inline void apply(TNum *data_in)
        {
            complex<TNum> RU_twiddle(1.,0.);
            complex<TNum> RU_rotator(CTCos(2*C_PI/N_Stage), -Direction*CTSin(2*C_PI/N_Stage));

            for (unsigned long idx=0; idx<N_Stage; idx+=2) {
                TNum temp = data_in[idx]-data_in[idx+N_Stage_2];
                data_in[idx] += data_in[idx+N_Stage_2];
                data_in[idx+N_Stage_2] = RU_twiddle * temp;
                RU_twiddle = RU_twiddle*RU_rotator;
            }
        }
    };
};

}; //namespace ifft

#endif //H_IFFT_FLOAT_DECIMATION_IN_FREQ_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

