////
///
///

#ifndef H_IFFT_FLOAT_DECIMATION_IN_FREQ_UNROLL_H
#define H_IFFT_FLOAT_DECIMATION_IN_FREQ_UNROLL_H

#include "decimation_in_freq.hpp"
#include "decimation_in_freq_opt.hpp"

namespace ifft {

template<int _UNROLL_LEVEL=6>
struct FreqAlgorithmsUnroll : public FreqAlgorithmsOpt
{
    static constexpr int UNROLL_LEVEL = _UNROLL_LEVEL;

    template<size_t _IDX_point, typename TNum, size_t _E_Stage, int Direction, typename Algorithms>
    struct ButterflySingle
    {
        /////
        ///  _IDX_point runs from 0 to N_Stage/2
        static constexpr size_t IDX_point = _IDX_point;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in) __attribute__((always_inline))
        {
            complex<TNum> RU_twiddle(CTCos(2*C_PI*IDX_point/N_Stage), -Direction*CTSin(2*C_PI*IDX_point/N_Stage));
            complex<TNum> temp = data_in[IDX_point]-data_in[IDX_point+N_Stage_2];
            data_in[IDX_point] += data_in[IDX_point+N_Stage_2];
            data_in[IDX_point+N_Stage_2] = RU_twiddle * temp;
        }
    };

    template<typename TNum, size_t _E_Stage, int Direction, typename Algorithms>
    struct ButterflyUnroll
    {
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in) __attribute__((always_inline))
        {
            APPLY_BISECT<0, N_Stage_2-1, TNum>::apply(data_in);
        }

        template<size_t LO, size_t HI, typename _P>
        struct APPLY_BISECT
        {
            static inline void apply(complex<TNum> *data_in) __attribute__((always_inline))
            {
                APPLY_BISECT<LO, LO+((HI-LO)/2), _P>::apply(data_in);
                APPLY_BISECT<1+LO+((HI-LO)/2), HI, _P>::apply(data_in);
            }
        };

        template<size_t LO, typename _P>
        struct APPLY_BISECT<LO, LO, _P>
        {
            static inline void apply(complex<TNum> *data_in) __attribute__((always_inline))
            {
                Algorithms::template ButterflySingle<LO, TNum, E_Stage, Direction, Algorithms>::apply(data_in);
            }
        };
    };

    template<typename TNum, int Direction, typename Algorithms>
    struct FFT_OOO_UNROLL_3
    {
        static constexpr size_t E_Stage = 3;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template ButterflyUnroll<TNum, 3, Direction, Algorithms>::apply(data_in);
            Algorithms::template ButterflyUnroll<TNum, 2, Direction, Algorithms>::apply(data_in);
            Algorithms::template ButterflyUnroll<TNum, 2, Direction, Algorithms>::apply(data_in+N_Stage/2);
            Algorithms::template ButterflyUnroll<TNum, 1, Direction, Algorithms>::apply(data_in);
            Algorithms::template ButterflyUnroll<TNum, 1, Direction, Algorithms>::apply(data_in+N_Stage/4);
            Algorithms::template ButterflyUnroll<TNum, 1, Direction, Algorithms>::apply(data_in+N_Stage/2);
            Algorithms::template ButterflyUnroll<TNum, 1, Direction, Algorithms>::apply(data_in+N_Stage/2 + N_Stage/4);
        }
    };

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int Direction, typename Algorithms>
    struct __CooleyTukey{
        static constexpr size_t E_FFT = _E_FFT;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template Butterfly<TNum, E_Stage, Direction, Algorithms>::apply(data_in);
            Algorithms::template __CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in);
            Algorithms::template __CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in + N_Stage/2);
        }
    };


    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukey<TNum, _E_FFT, 0, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in) { }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukey<TNum, _E_FFT, 1, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template FFT_OOO_MICRO_1<TNum, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukey<TNum, _E_FFT, 2, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template FFT_OOO_MICRO_2<TNum, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukey<TNum, _E_FFT, 3, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template FFT_OOO_MICRO_3<TNum, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukey<TNum, _E_FFT, 4, Direction, Algorithms>{
        static constexpr size_t E_Stage = 4;
        static inline void apply(complex<TNum> *data_in)
        { Algorithms::template __CooleyTukeyUnroller<TNum, _E_FFT, E_Stage, Direction, Algorithms>::apply(data_in); }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukey<TNum, _E_FFT, 5, Direction, Algorithms>{
        static constexpr size_t E_Stage = 5;
        static inline void apply(complex<TNum> *data_in)
        { Algorithms::template __CooleyTukeyUnroller<TNum, _E_FFT, E_Stage, Direction, Algorithms>::apply(data_in); }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukey<TNum, _E_FFT, 6, Direction, Algorithms>{
        static constexpr size_t E_Stage = 6;
        static inline void apply(complex<TNum> *data_in)
        { Algorithms::template __CooleyTukeyUnroller<TNum, _E_FFT, E_Stage, Direction, Algorithms>::apply(data_in); }
    };

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int Direction, typename Algorithms>
    struct __CooleyTukeyUnroller{
        static constexpr size_t E_FFT = _E_FFT;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template ButterflyUnroll<TNum, E_Stage, Direction, Algorithms>::apply(data_in);
            Algorithms::template __CooleyTukeyUnroller<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in);
            Algorithms::template __CooleyTukeyUnroller<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in + N_Stage/2);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukeyUnroller<TNum, _E_FFT, 0, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in) { }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukeyUnroller<TNum, _E_FFT, 1, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template FFT_OOO_MICRO_1<TNum, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukeyUnroller<TNum, _E_FFT, 2, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template FFT_OOO_MICRO_2<TNum, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct __CooleyTukeyUnroller<TNum, _E_FFT, 3, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template FFT_OOO_MICRO_3<TNum, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int Direction, typename Algorithms>
    struct CooleyTukey : public std::conditional<
                                       ( _E_Stage <= UNROLL_LEVEL ),
                                       __CooleyTukeyUnroller<TNum, _E_FFT, _E_Stage, Direction, Algorithms>,
                                       __CooleyTukey<TNum, _E_FFT, _E_Stage, Direction, Algorithms>
                                                 >::type
    { };
};

}; //~namespace ifft

#endif //H_IFFT_FLOAT_DECIMATION_IN_FREQ_UNROLL_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

