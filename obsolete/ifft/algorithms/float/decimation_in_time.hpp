////
///
///

#ifndef H_IFFT_FLOAT_DECIMATION_IN_TIME_H
#define H_IFFT_FLOAT_DECIMATION_IN_TIME_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include <complex>

#include "algorithms/static_math.hpp"
#include "algorithms/algorithms_base.hpp"

namespace ifft {
using std::complex;

struct TimeAlgorithms : public AlgorithmsBase
{
    template<typename TNum, size_t _E_Stage, int Direction, typename Algorithms>
    struct Butterfly
    {
        static constexpr size_t E_Stage   = _E_Stage;
        static constexpr size_t N_Stage   = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        static inline void apply(complex<TNum> *data_in)
        {
            complex<TNum> RU_twiddle(1.,0.);
            complex<TNum> RU_rotator(CTCos(2*C_PI/N_Stage), -Direction*CTSin(2*C_PI/N_Stage));

            for (unsigned long idx=0; idx<N_Stage_2; ++idx) {
                complex<TNum> temp = data_in[idx+N_Stage_2] * RU_twiddle;
                data_in[idx+N_Stage_2] = data_in[idx] - temp;
                data_in[idx] += temp;
                RU_twiddle = RU_twiddle*RU_rotator;
            }
        }
    };

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int Direction, typename Algorithms>
    struct CooleyTukey{
        static constexpr size_t E_FFT = _E_FFT;
        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in);
            Algorithms::template CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in + N_Stage/2);
            Algorithms::template Butterfly<TNum, E_Stage, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct CooleyTukey<TNum, _E_FFT, 0, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in) { }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct FFT
    {
        static void apply(complex<TNum> *data_in)
        {
            Algorithms::template FFTPreOrder<TNum, _E_FFT, Algorithms>::apply(data_in);
            Algorithms::template CooleyTukey<TNum, _E_FFT, _E_FFT, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct FFT_OOO
    {
        static void apply(complex<TNum> *data_in)
        {
            Algorithms::template CooleyTukey<TNum, _E_FFT, _E_FFT, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, typename Algorithms>
    struct FFTPreOrder : public LocationBitSwap<TNum, _E_FFT, Algorithms>
    { };

};

}; //namespace ifft

#endif //H_IFFT_FLOAT_DECIMATION_IN_TIME_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

