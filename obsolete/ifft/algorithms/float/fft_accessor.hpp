////
///
///

#ifndef H_IFFT_FLOAT_FFT_ACCESSOR_H
#define H_IFFT_FLOAT_FFT_ACCESSOR_H

#include <complex>
namespace ifft {
using std::complex;

/////
///  Access type to view into the bags-of-algorithms and compute with them
template<typename TNum, size_t E_FFT, int Direction, typename Algorithms>
struct FFT
{
public:
    static void compute(complex<TNum> *data_in)
    {
        Algorithms::template FFT<TNum, E_FFT, Direction, Algorithms>::apply(data_in);
    }

    static void compute_ooo(complex<TNum> *data_in)
    {
        Algorithms::template FFT_OOO<TNum, E_FFT, Direction, Algorithms>::apply(data_in);
    }

    static void post_reorder(complex<TNum> *data_in)
    {
        Algorithms::template FFTPostOrder<TNum, E_FFT, Algorithms>::apply(data_in);
    }

    static void pre_reorder(complex<TNum> *data_in)
    {
        Algorithms::template FFTPreOrder<TNum, E_FFT, Algorithms>::apply(data_in);
    }
};

/////
///  Access type to view into the bags-of-algorithms and compute with them
template<typename TNum, size_t E_FFT, typename Algorithms>
struct FFTReal
{
    static constexpr int Direction = 1;
public:
    static void compute(TNum *data_in)
    {
        Algorithms::template FFTReal<TNum, E_FFT, Algorithms>::apply(data_in);
    }

    static void compute_ooo(TNum *data_in)
    {
        Algorithms::template FFTReal_OOO<TNum, E_FFT, Algorithms>::apply(data_in);
    }

    static void post_reorder(complex<TNum> *data_in)
    {
        Algorithms::template FFTPostOrder<TNum, E_FFT, Algorithms>::apply(data_in);
    }

    static void pre_reorder(TNum *data_in)
    {
        Algorithms::template FFTPreOrder<TNum, E_FFT, Algorithms>::apply(data_in);
    }
};


}; //~namespace ifft

#endif //H_IFFT_FLOAT_FFT_ACCESSOR_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

