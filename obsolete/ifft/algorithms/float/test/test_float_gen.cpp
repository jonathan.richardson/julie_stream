////
///
///
///
///

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <thread>
#include <chrono>
#include <functional>

#include "utilities/range.hpp"

#include "algorithms/float/fft_accessor.hpp"
#include "algorithms/float/decimation_in_time.hpp"
#include "algorithms/float/decimation_in_freq.hpp"
#include "algorithms/float/decimation_in_freq_opt.hpp"
#include "algorithms/float/decimation_in_freq_unroll.hpp"

#include "easy_fftw.hpp"

namespace ifft {
}; //~namespace ifft

using namespace std;
using namespace ifft;

BOOST_AUTO_TEST_CASE( test_float_fft )
{
	typedef double TData;
	enum {size_p = 3, size_N = 1 << size_p, IBS=16};

    FFT_FFTWd data_fftw(size_N, true /*estimate*/);

    // sample data
    auto data_in  = new complex<TData> [size_N];
    auto data_act = new complex<TData> [size_N];
    for (int idx=0; idx < size_N; ++idx) {
        data_in[idx] = std::complex<TData>(idx * 2, idx*2+1);
        data_act[idx] = data_in[idx];
        data_fftw.input_data()[idx] = data_in[idx];
    }

    data_fftw.compute_fft();

    FFT<TData, size_p, 1, FreqAlgorithmsUnroll<6> >::compute(data_act);

    // print the results
    cout<<"Size 8 FFT----------------------" << endl;
    for (auto idx : icount(size_N))
    {
        cout << setw(10) 
            << setprecision(5) 
            << "Error: " << (1 - std::abs((data_act[idx])/(data_fftw.fft_data()[idx]))) 
            << "\tIn:"    << data_in[idx]
            << "\tValue:" << data_act[idx]
            << endl;
    }
    cout<<"--------------------------------" << endl;
}

BOOST_AUTO_TEST_CASE( test_real_float_fft )
{
	typedef double TData;
	enum {size_p = 3, size_N = 1 << size_p, IBS=16};

    FFTR_FFTWd data_fftw(size_N, true /*estimate*/);

    // sample data
    auto data_in  = new TData[size_N];
    auto data_act = new TData[size_N];
    auto data_fft = reinterpret_cast<complex<TData> *>(data_act);
    for (int idx=0; idx < size_N; ++idx) {
        data_in[idx] = idx;
        data_act[idx] = data_in[idx];
        data_fftw.input_data()[idx] = data_in[idx];
    }

    data_fftw.compute_fft();

    FFTReal<TData, size_p, FreqAlgorithms >::compute(data_act);

    // print the results
    cout<<"--------------------------------" << endl;
    for (auto idx : icount(size_N/2))
    {
        cout << setw(10) 
            << setprecision(5) 
            << "Error: " << (1 - std::abs((data_fft[idx])/(data_fftw.fft_data()[idx]))) 
            << "\tIn:"    << data_in[idx*2] << ", " << data_in[idx*2+1]
            << "\tValue:" << data_fft[idx]
            << "\tFFTW Value:" << data_fftw.fft_data()[idx]
            << endl;
    }
    cout<<"--------------------------------" << endl;
}

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

