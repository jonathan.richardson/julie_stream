////
///
///

#ifndef H_ALGORITHMS_BASE_H
#define H_ALGORITHMS_BASE_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include <complex>

#include "static_math.hpp"

namespace ifft {
using std::complex;

struct AlgorithmsBase
{
    template<typename TNum, unsigned long _E_Frame, typename Algorithms>
    class LocationBitSwap
    {
        static constexpr size_t E_Frame = _E_Frame;
        static constexpr size_t N_Frame = 1<<E_Frame;
        static constexpr size_t N_Frame_2 = N_Frame/2;
    public:
        static void apply(complex<TNum> *data) 
        {
            unsigned long m,j=0;
            for (unsigned long i=0; i<N_Frame; ++i) {
                if (j>i) {
                    std::swap(data[j], data[i]);
                }
                m = N_Frame_2;
                while (m>=1 && j>=m) {
                    j -= m;
                    m >>= 1;
                }
                j += m;
            }
       }
    };
};

}; //namespace ifft

#endif //H_ALGORITHMS_BASE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

