////
///
#ifndef H_FFT_STATIC_MATH_H
#define H_FFT_STATIC_MATH_H


namespace ifft {

const double C_PI = 3.141592653589793238462643383279502884197169399375105820974944;

////// template class SinCosSeries
// common series to compile-time calculation
// of sine and cosine functions

constexpr long double IExpTaylor(long double X, unsigned E)
{
    return (E<=1) ? 
        (E ? X : 1) : 
        (X*X / (E * (E-1)) * -IExpTaylor(X, E - 2)) ;
}

constexpr long double ISinTaylor(long double X, unsigned E)
{
    return (E <= 0) ? 
        0 : 
        (X*X / (E * (E-1)) * -ISinTaylor(X, E - 2)) ;
}

constexpr long double ICosTaylor(long double X, unsigned E)
{
    return (E<=1) ? 
        (E ? 0 : 1) : 
        (X*X / (E * (E-1)) * -ICosTaylor(X, E - 2)) ;
}

constexpr long double ISinTaylorSum(long double X, unsigned N_terms)
{
    return (N_terms == 0) ? 0 : IExpTaylor(X, (N_terms-1) * 2 + 1) + ISinTaylorSum(X, N_terms - 1) ;
}

constexpr long double ICosTaylorSum(long double X, unsigned N_terms)
{
    return (N_terms == 0) ? 0 : IExpTaylor(X, (N_terms-1) * 2) + ICosTaylorSum(X, N_terms - 1) ;
}

constexpr long double CTAbs(long double X)
{return X < 0 ? -X : X;}

constexpr long double ISinTaylorSumAuto(long double X, unsigned N_terms, long double epsilon)
{
    return (CTAbs(IExpTaylor(X, (N_terms - 1) * 2 + 1) - IExpTaylor(X, (N_terms - 2) * 2 + 1)) < epsilon) ? 
        ISinTaylorSum(X, N_terms) : 
        ISinTaylorSumAuto(X, N_terms + 1, epsilon);
}

constexpr long double ICosTaylorSumAuto(long double X, unsigned N_terms, long double epsilon)
{
    return (CTAbs(IExpTaylor(X, (N_terms - 1) * 2) - IExpTaylor(X, (N_terms) * 2)) < epsilon) ? 
        ICosTaylorSum(X, N_terms) : 
        ICosTaylorSumAuto(X, N_terms + 1, epsilon);
}

constexpr long double CTSin(long double X, long double epsilon = 1e-20)
{ return ISinTaylorSumAuto(X, 5, epsilon);}

constexpr long double CTCos(long double X, long double epsilon = 1e-20)
{ return ICosTaylorSumAuto(X, 5, epsilon);}

template<typename T>
constexpr T CTMin(T L, T R)
{ return L<R ? L : R;}

template<typename T>
constexpr T CTMax(T L, T R)
{ return L>R ? L : R;}

}; //~namespace ifft


#endif //H_FFT_STATIC_MATH_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

