////
///
///

#ifndef H_IFFT_BITSAFE_OPERATIONS_H
#define H_IFFT_BITSAFE_OPERATIONS_H

namespace ifft {
    typedef int TBitsize;

    template<unsigned B>
    struct _BScaleUp
    {
        template<typename T>
        static T apply(T value)
        {
            return value * (T(1)<<B);
        }
    };
    template<unsigned B>
    struct _BScaleDown
    {
        template<typename T>
        static T apply(T value)
        {
            return value / (T(1)<<B);
        }
    };
    template<unsigned B>
    struct _BScaleDown_round
    {
        template<typename T>
        static T apply(T value)
        {
            return (value + ((T(1)<<(B/2))-1)) / (T(1)<<B);
        }
    };


    template<TBitsize B, bool Round=false>
    struct BScale : public 
                    std::conditional<
                        ( B >= 0 ), 
                        _BScaleUp<B>, 
                        typename std::conditional<Round, _BScaleDown_round<-B> , _BScaleDown<-B> >::type
                    >::type
    {};

    template<typename OType, typename IType, TBitsize B, bool Round=false>
    struct _BScaleThenCast
    {
        static OType apply(IType value)
        {
            return OType(BScale<B, Round>::apply(value));
        }
    };
    template<typename OType, typename IType, TBitsize B, bool Round=false>
    struct _CastThenBScale
    {
        static OType apply(IType value)
        {
            return BScale<B, Round>::apply(OType(value));
        }
    };

    template<typename OType, typename IType, TBitsize B, bool Round=false>
    struct BScaleCast : public 
                        std::conditional< 
                            ( (std::numeric_limits<OType>::digits < std::numeric_limits<IType>::digits) xor 
                              ( B < 0 ) //do the opposite if B < 0 so that we do the shift/division on a smaller type
                              ), 
                            _BScaleThenCast<OType, IType, B, Round>, 
                            _CastThenBScale<OType, IType, B, Round>
                        >::type
    {};


    /////
    ///  Does multiplication,
    ///  OT, OBits are the output type and dynamic range
    ///  LT, LBits are the left operand type and dynamic range
    ///  RT, RBits are the right operand type and dynamic range
    ///  MT is the inner-cast used to enhance the precision of the multiply
    ///  Round is whether the bit-shifts down should round
    template<typename OT, TBitsize OBits, 
             typename LT, TBitsize LBits, 
             typename RT, TBitsize RBits, 
             typename MT=OT, bool Round=false>
    struct BitsafeOps{
        static constexpr TBitsize MBitsMax = std::numeric_limits<MT>::digits;

        static OT MUL(LT L, RT R)
        {
            static constexpr TBitsize LWeight1 = MBitsMax/2 - LBits;
            static constexpr TBitsize RWeight1 = MBitsMax/2 - RBits;
            static_assert(MBitsMax >= RBits, "Inner cast should be bigger than operands");

            /////
            ///  BBalance fixes the problem where one side comes in with few bits and needs to shift 
            ///  larger to fill MBitsMax/2. This allows the other operand to have to lose FEWER bits by
            ///  taking the small operand's empty shift
            static constexpr TBitsize BBalance = CTMax(0, LWeight1) - CTMax(0, RWeight1);
            static constexpr TBitsize LWeight2 = LWeight1 - BBalance; 
            static constexpr TBitsize RWeight2 = RWeight1 + BBalance;

            static constexpr TBitsize OWeight = OBits - MBitsMax;

            /////
            ///  Here, the two operands are casted to the midcast type, then bitshifted to 
            ///  each half-saturate the midcast type bits. After multiplication, the 
            ///  midcast type is fully saturated. Then the bits are shifted down to the 
            ///  number requested in OBits.
            return BScaleCast<OT, MT, OWeight, Round>::apply(
                                            (BScaleCast<MT, LT, LWeight2, Round>::apply(L)) * 
                                            (BScaleCast<MT, RT, RWeight1, Round>::apply(R))
                                                );
        };

        static OT SUB(LT L, RT R)
        {
            static_assert(LBits == RBits, "Sub operations require the same fixed point characteristics");
            static_assert(MBitsMax >= RBits, "Inner cast cant be smaller than operands");
            static constexpr TBitsize ArithBits = LBits+1;
            static constexpr TBitsize OBitsMax  = std::numeric_limits<OT>::digits;
            typedef typename std::common_type<LT, RT>::type CT;

            //////
            ///  Static-time if should get pruned, otherwise will have to use templates for this if
            if(ArithBits <= OBitsMax){
                return BScaleCast<OT, CT, OBits-(ArithBits), Round>::apply(L-R);
            }else if(ArithBits <= MBitsMax){
                return BScaleCast<OT, MT, OBits-(ArithBits), Round>::apply(MT(L)-MT(R));
            }else if(Round){
                return BScaleCast<OT, CT, OBits-(ArithBits-2), Round>::apply(
                            BScale<LBits-1>::apply(L) - BScale<RBits-1>::apply((R+1)));
            }else{
                return BScaleCast<OT, CT, OBits-(ArithBits-2), Round>::apply(
                            BScale<LBits-1>::apply(L) - BScale<RBits-1>::apply(R));
            }
        };

        static OT ADD(LT L, RT R)
        {
            static_assert(LBits == RBits, "Add operations require the same fixed point characteristics");
            static_assert(MBitsMax >= RBits, "Inner cast cant be smaller than operands");
            static constexpr TBitsize ArithBits = LBits+1;
            static constexpr TBitsize OBitsMax  = std::numeric_limits<OT>::digits;
            typedef typename std::common_type<LT, RT>::type CT;

            //////
            ///  Static-time if should get pruned, otherwise will have to use templates for this if
            if(ArithBits <= OBitsMax){
                return BScaleCast<OT, CT, OBits-(ArithBits), Round>::apply(L+R);
            }else if(ArithBits <= MBitsMax){
                return BScaleCast<OT, MT, OBits-(ArithBits), Round>::apply(MT(L)+MT(R));
            }else if(Round){
                return BScaleCast<OT, CT, OBits-(ArithBits-2), Round>::apply(
                            BScale<LBits-1>::apply(L) + BScale<RBits-1>::apply((R+1)));
            }else{
                return BScaleCast<OT, CT, OBits-(ArithBits-2), Round>::apply(
                            BScale<LBits-1>::apply(L) + BScale<RBits-1>::apply(R));
            }
        };
    };

}; //~namespace ifft

#endif //H_IFFT_BITSAFE_OPERATIONS_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

