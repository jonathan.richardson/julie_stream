////
///
///

#ifndef H_INTEGER_ALGORITHMS_H
#define H_INTEGER_ALGORITHMS_H

#include <limits>

#include <iostream>
#include <iomanip>
#include <cmath>
#include <complex>

#include "algorithms/static_math.hpp"
#include "algorithms/algorithms_base.hpp"

#include "bitsafe_operations.hpp"

namespace ifft {
using std::complex;

/*  Possible Speedup to complex multiplication:

    R + jI = (a + jb)(c + jd) = (ac - bd) + j(bc + ad)
    k1 = a(c + d)
    k2 = d(a + b)
    k3 = c(b - a)
    R = k1 - k2 , and I = k1 + k3
*/

/*
template<typename TNum, size_t _E_Stage, int Direction, typename Algorithms>
struct Butterfly
{
    static constexpr size_t E_Stage = _E_Stage;
    static constexpr size_t N_Stage = 1<<E_Stage;
    static constexpr size_t N_Stage_2 = N_Stage/2;

    static inline void apply(complex<TNum> *data_in)
    {
        complex<TNum> w(1.,0.);
        complex<TNum> wp(CTCos(2*C_PI/N_Stage), -Direction*CTSin(2*C_PI/N_Stage));

        for (unsigned long i=0; i<N_Stage_2; ++i) {
            complex<TNum> temp = data_in[i]-data_in[i+N_Stage_2];
            data_in[i] += data_in[i+N_Stage_2];
            data_in[i+N_Stage_2] = w * temp;
            w = w*wp;
        }
    }
};
*/

namespace Integer
{
struct FreqAlgorithms : public AlgorithmsBase
{

    template< TBitsize _BitsOut, TBitsize _BitsIn, bool _Round=false >
    struct ButterflyBitPolicy
    {
        static constexpr TBitsize B_OUT = _BitsOut;
        static constexpr TBitsize B_IN  = _BitsIn;
        static constexpr bool     Round = _Round;
    };

    template<
        typename _TData, 
        typename _TRootUnity = _TData, 
        typename _TMidcast = _TData,
        typename _TRUMidcast = _TRootUnity
            >
    struct ButterflyCastPolicy
    {
		typedef _TData      TData;
		typedef _TMidcast   TMidcast;
		typedef _TRootUnity TRootUnity;
		typedef _TRUMidcast TRUMidcast;
    };

    template<
        typename _BitPolicy,  //Should be a ButterflyBitPolicy
        typename _CastPolicy, //Should be a ButterflyCastPolicy
        size_t _E_Stage, int Direction, typename Algorithms
            >
    struct IntButterfly
    {
		typedef typename _CastPolicy::TData      TData;
		typedef typename _CastPolicy::TMidcast   TMidcast;
		typedef typename _CastPolicy::TRootUnity TRootUnity;
		typedef typename _CastPolicy::TRUMidcast TRUMidcast;
        
        /////Bits range input
        static constexpr TBitsize   B_IN  = 30;
        static constexpr TBitsize   B_OUT = 30;
        static constexpr bool       Round = false;

        /////
        ///  Bits data field MAX,
        static constexpr TBitsize   B_DM  = std::numeric_limits<TData>::digits;
        /////
        ///  Bits in the midcast MAX
        static constexpr TBitsize   B_MCM  = std::numeric_limits<TMidcast>::digits;
        /////
        ///  Internal bitsize for the SUB operation
        static constexpr TBitsize   B_INT = CTMin(B_IN+1, B_MCM);

        /////
        ///  Bits used by the FFT root of unity
        static constexpr TBitsize   B_RU  = std::numeric_limits<TRootUnity>::digits;
        static constexpr TRootUnity BN_RU = (TRootUnity(1)<<(B_RU-1));

        static_assert(std::is_signed<TData>::value, "FFTs need signed integers");
        static_assert(std::is_signed<TMidcast>::value, "FFTs need signed integers");
        static_assert(std::is_signed<TRootUnity>::value, "FFTs need signed integers");
        static_assert(std::is_signed<TRUMidcast>::value, "FFTs need signed integers");

        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static constexpr size_t N_Stage_2 = N_Stage/2;

        /////
        ///  This number represents how small the SIN part of the FFT is, it allows for finer
        ///  precision during the RU rotation stage. This only happens on E_Stage > 4
        static constexpr TBitsize   B_RU_UNBAL = (E_Stage <= 4) ? 0 : (E_Stage - 3)/2;

        static inline void apply(complex<TData> *data_in)
        {
            complex<TRootUnity> RU_twiddle(1.*BN_RU, 0.*BN_RU);
            complex<TRootUnity> RU_rotator(BN_RU*CTCos(2*C_PI/N_Stage), -Direction* BN_RU * CTSin(2*C_PI/N_Stage));

            for (unsigned long i=0; i<N_Stage_2; ++i) {
                complex<TData> in_lo = data_in[i];
                complex<TData> in_hi = data_in[i+N_Stage_2];

                typedef BitsafeOps<TData, B_OUT, TData, B_IN, TData, B_IN, TMidcast, Round> Data_Data_Add;
                data_in[i] = complex<TData>(
                            Data_Data_Add::ADD(in_lo.real(), in_hi.real()) , 
                            Data_Data_Add::ADD(in_lo.imag(), in_hi.imag())
                        );

                typedef BitsafeOps<TMidcast, B_INT, TData, B_IN, TData, B_IN, TMidcast, Round> Data_Data_Sub;
                complex<TMidcast> in_sub = complex<TMidcast>(
                            Data_Data_Sub::SUB(in_lo.real(), in_hi.real()) , 
                            Data_Data_Sub::SUB(in_lo.imag(), in_hi.imag())
                        );

                /////
                ///  the B_IN+1 is because of in_sub picked up a bit worth of data in the 
                ///  subtraction
                typedef BitsafeOps<TData, B_OUT, TRootUnity, B_RU, TMidcast, B_INT, TMidcast, Round> RU_Data_Mul;
                data_in[i+N_Stage_2] = complex<TData>( 
                            RU_Data_Mul::MUL(RU_twiddle.real(), in_sub.real()) -
                            RU_Data_Mul::MUL(RU_twiddle.imag(), in_sub.imag())
                        ,
                            RU_Data_Mul::MUL(RU_twiddle.imag(), in_sub.real()) +
                            RU_Data_Mul::MUL(RU_twiddle.real(), in_sub.imag())
                        );

                typedef BitsafeOps<TRootUnity, B_RU, TRootUnity, B_RU, TRootUnity, B_RU, TRUMidcast, Round> RU_RU_Mul;
                RU_twiddle = complex<TRootUnity>( 
                            RU_RU_Mul::MUL(RU_twiddle.real(), RU_rotator.real()) -
                            RU_RU_Mul::MUL(RU_twiddle.imag(), RU_rotator.imag())
                        ,
                            RU_RU_Mul::MUL(RU_twiddle.imag(), RU_rotator.real()) +
                            RU_RU_Mul::MUL(RU_twiddle.real(), RU_rotator.imag())
                        );
            }
        }
    };

    template<typename TNum, size_t _E_FFT, size_t _E_Stage, int Direction, typename Algorithms>
    struct CooleyTukey{
        static constexpr size_t E_FFT = _E_FFT;
        static constexpr size_t N_FFT = 1<<E_FFT;

        static constexpr size_t E_Stage = _E_Stage;
        static constexpr size_t N_Stage = 1<<E_Stage;
        static inline void apply(complex<TNum> *data_in)
        {
            Algorithms::template Butterfly<TNum, E_Stage, Direction, Algorithms>::apply(data_in);
            Algorithms::template CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in);
            Algorithms::template CooleyTukey<TNum, E_FFT, E_Stage-1, Direction, Algorithms>::apply(data_in + N_Stage/2);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct CooleyTukey<TNum, _E_FFT, 0, Direction, Algorithms>{
        static inline void apply(complex<TNum> *data_in) { }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct FFT
    {
        static void apply(complex<TNum> *data_in)
        {
            Algorithms::template CooleyTukey<TNum, _E_FFT, _E_FFT, Direction, Algorithms>::apply(data_in);
            Algorithms::template FFTPostOrder<TNum, _E_FFT, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, int Direction, typename Algorithms>
    struct FFT_OOO
    {
        static void apply(complex<TNum> *data_in)
        {
            Algorithms::template CooleyTukey<TNum, _E_FFT, _E_FFT, Direction, Algorithms>::apply(data_in);
        }
    };

    template<typename TNum, size_t _E_FFT, typename Algorithms>
    struct FFTPostOrder : public LocationBitSwap<TNum, _E_FFT, Algorithms>
    { };

};


}; //namespace Integer
}; //namespace ifft

#endif //H_INTEGER_ALGORITHMS_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

