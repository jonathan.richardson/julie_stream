////
///
///
///
///

#include <thread>
#include <chrono>
#include <functional>

#include "algorithms/int/integer_algorithms.hpp"

namespace ifft {
}; //~namespace ifft

using namespace ifft;

int main(int argc, const char *argv[])
{
    // sample data
	typedef int32_t TData;
	enum {size_p = 3, size_N = 1 << size_p, IBS=16};
    auto data  = new complex<TData> [size_N];
    auto data2 = new complex<TData> [size_N];
    for (int idx=0; idx < size_N; ++idx) {
        data[idx] = std::complex<TData>(idx * 2, idx*2+1);
        data2[idx] = std::complex<TData>(idx * 2, idx*2+1);
    }

    typedef Integer::FreqAlgorithms::ButterflyBitPolicy<30, 16> BflyBbits;
    typedef Integer::FreqAlgorithms::ButterflyCastPolicy<TData> BflyCasts;
	Integer::FreqAlgorithms::IntButterfly<BflyBbits, BflyCasts, size_p, 1, Integer::FreqAlgorithms>::apply(data);

	return 0;
}

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

