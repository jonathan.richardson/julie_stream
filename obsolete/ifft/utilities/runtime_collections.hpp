////
///
///

#ifndef H_IFFT_RUNTIME_COLLECTIONS_H
#define H_IFFT_RUNTIME_COLLECTIONS_H

namespace ifft {

namespace _detail{
    template<int... S> struct MetaNumberList {
        template<template<int...> class TInto>
        using apply = TInto<S...>;
    };

    template<int L, int H, int ...S> struct MetaRange{
        typedef typename MetaRange<L, H-1, H-1, S...>::range range;
    };

    template<int L, int ...S> 
    struct MetaRange<L, L, S...>{ 
        typedef MetaNumberList<S...> range; 
    };
}; //namespace ~detail

/////
//  This is a piece of template wizardry which allows us to generate static arrays of objects, 
//  generated from template expressions.
template<unsigned N_lo, unsigned N_hi, template<unsigned N> class TGen>
struct GeneratorList
{
    template<int... N_counter>
    struct CollectionGen
    {
        static const decltype(TGen<N_lo>::value) collection[N_hi-N_lo];
    };

    static const decltype(TGen<N_lo>::value) *collection;
};

template<unsigned N_lo, unsigned N_hi, template<unsigned N> class TGen>                        //outer namespace(class) template
template<int... N_counter>                                                                     //inner namespace(class) template
const decltype(TGen<N_lo>::value)                                                              //type
    GeneratorList<N_lo, N_hi, TGen>::CollectionGen<N_counter...>::                             //Class namespace
collection[N_hi-N_lo] = {TGen<N_counter>::value ...};                                          //definition specifier

template<unsigned N_lo, unsigned N_hi, template<unsigned N> class TGen>                        //class template  
const decltype(TGen<N_lo>::value) *                                                            //type                 
        GeneratorList<N_lo,N_hi,TGen>::                                                        //namespace(class)
collection = _detail::MetaRange<N_lo, N_hi>::range::template apply<CollectionGen>::collection; //definition specifier 

}; //~namespace ifft

#endif //H_IFFT_RUNTIME_COLLECTIONS_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

