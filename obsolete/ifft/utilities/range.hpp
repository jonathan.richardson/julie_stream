////
///
#ifndef H_RANGE_H
#define H_RANGE_H

#include <boost/iterator/counting_iterator.hpp>

namespace utilities {

typedef boost::counting_iterator<size_t> TCountIter;

template<typename TIter>
class Range
{
    TIter m_begin;
    TIter m_end;

    typedef decltype(*m_begin) TPointee;

    public:
    Range(TIter begin, TIter end) :
        m_begin(begin), m_end(end)
    { }

    Range(TIter begin, size_t len) :
        m_begin(begin), m_end(begin + len)
    { }

    Range() :
        m_begin(), m_end()
    { }

    ////
    ///Default copy-constructors are good

    inline TIter begin() const
    {return m_begin;}

    inline TIter end() const
    {return m_end;}

    inline void begin_shift(size_t len){
        m_begin += len;
    }

    inline void end_shift(size_t len){
        m_end -= len;
    }

    inline void begin_inc(){
        ++m_begin;
    }

    inline void end_dec(){
        --m_end;
    }

    inline size_t size() const {
        return m_end - m_begin;
    }

    /////
    ///  Only works if TIter is random access
    inline TPointee &operator[](size_t idx) const{
        return m_begin[idx];
    }

    inline Range<TCountIter> idx_range(){
        return Range<TCountIter>(TCountIter(0), TCountIter(this->size()));
    }

    inline bool empty() const {
        return m_begin >= m_end;
    }

    ////
    ///Indicates that a range operation is over-extending the range
    inline bool broken() const {
        return m_begin > m_end;
    }
};

template<typename TIter>
inline Range<TIter> LeftLimitedRange(const Range<TIter> &fullrange, size_t length){
    if(length < fullrange.size()){
        TIter begin = fullrange.begin();
        return Range<TIter>(begin, begin + length);
    }else{
        return fullrange;
    }
}

template<typename TIter>
inline Range<TIter> RightLimitedRange(const Range<TIter> &fullrange, size_t length){
    if(length < fullrange.size()){
        TIter end = fullrange.end();
        return Range<TIter>(end - length, end);
    }else{
        return fullrange;
    }
}

/////
///  This is a very useful range which allows us to count indices with the new syntax
typedef Range<TCountIter> CountingRange;

inline CountingRange icount(size_t to_N){
    return CountingRange(TCountIter(0), TCountIter(to_N));
}

inline CountingRange icount(size_t from_N, size_t to_N){
    return CountingRange(TCountIter(from_N), TCountIter(to_N));
}

}; //~namespace utilities


#endif //H_RANGE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

