/////
///
///

#include "data_sources/virtual_5122_reader.hpp"
#include "data_sources/threaded_adaptor.hpp"
#include "data_sources/5122_reader.hpp"

#include "configuration_factories.hpp"

namespace JStream {
auto logger_factories = logger_root().child("factories");

shared_ptr<IDataSource> data_source_factory_dispatch(shared_ptr<IConfigTree> source_config)
{
    string source_type;
    (*source_config)["source_type"]->get(source_type, "");

    if(source_type == "5122")
    {
#ifdef JSTREAM_HAS_NISCOPE
        return std::make_shared<NIScope5122>(source_config);
#else  //JSTREAM_HAS_NISCOPE
        logger_main.error("NiScope 5122 not compiled in, using virtual source");
        return std::make_shared<Virtual5122>(source_config);
#endif //JSTREAM_HAS_NISCOPE
    }
    else if(source_type == "virtual")
    {
        data_sources.push_back(std::make_shared<Virtual5122>(source_config));
    }
    else if(source_type == "threaded_buffer")
    {
        auto inner_source = data_source_factory_dispatch((*source_config)["inner_source"]);
        return std::make_shared<ThreadedReader>(inner_source, source_config);
    }
    else if(source_type == "")
    {
        throw(std::logic_error("source_type field must be specified for each data source"));
    }
    else
    {
        throw(std::logic_error("Unknown data source type: " + source_type));
    }
}

std::vector< shared_ptr<IDataSource> > data_source_factory(shared_ptr<IConfigTree> list_of_sources)
{
    std::vector< shared_ptr<IDataSource> > data_sources;

    if(not list_of_sources->is_array()){
        throw(std::logic_error("Data source list must be an array of source-objects"));
    }
    if(icount(list_of_sources->array_size() == 0)){
        throw(std::logic_error("Data source list must have at least one source"));
    }

    for(auto idx : icount(list_of_sources->array_size())){
        source_config = (*list_of_sources)[idx];
        data_sources.push_back(data_source_factory_dispatch(source_config));
    }

    return data_sources;
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
