////
///
#ifndef H_JSTREAM_HEAPSORT_H
#define H_JSTREAM_HEAPSORT_H

#include "utilities/config_tree/config_tree_json.hpp"

#include "data_sources/data_source_config.hpp"

#include "data_flow/data_flow_task.hpp"

#include "jstream_types.hpp"

namespace JStream {

using namespace ConfigTree;

/////
///  Super-accumulator provides second layer of CSD averaging
class CSDHeapSort : public DataFlowTask<shared_ptr<TCSDCarrier> >
{
    /////
    ///  These are convenient for when we need to access the queues
    typedef DataFlowTask<shared_ptr<TCSDCarrier> >    TCSDSuperclass;

  public:
    CSDHeapSort(
		ConfigTreeJson& conf_data,
		string thread_name = "csd_heapsort"
	);
	
  protected:
    void compute_loop();
};

}; //~namespace JStream

#endif //H_JSTREAM_HEAPSORT_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

