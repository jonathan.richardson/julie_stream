////
///
///

#ifndef H_FETCH_WORKER_H
#define H_FETCH_WORKER_H

#include "data_sources/data_source_base.hpp"

#include "data_flow/data_flow_task.hpp"
#include "data_flow/background_task.hpp"

#include "jstream_types.hpp"

namespace JStream {

class InnerFetcher : public BackgroundTask
{
    typedef BackgroundTask TSuper;
public:
    typedef int16_t TFill;
    typedef Range<TFill *> TFillRange;

    struct ChnBuffers {
        std::vector<TFillRange> buffers;
        size_t                  prefill_amount;
        int                     fetch_num;
        bool                    data_good;
    };

private:
    typedef WaitingQueuePage< shared_ptr<ChnBuffers> > TBufferQueue;

    shared_ptr<IDataSource> m_source;
    shared_ptr<ChnBuffers>  m_chnbuff_public_send;
    shared_ptr<ChnBuffers>  m_chnbuff_public_return;
    TBufferQueue            m_queue_to;
    TBufferQueue            m_queue_from;
    TBufferQueue            m_queue_reserve;

public:
    InnerFetcher(shared_ptr<IDataSource> source);

    void        wait_done();
    void        push_buffer();

    unsigned    num_ready();

    const ChnBuffers &buffer_return_access() const;
    ChnBuffers &buffer_send_access();

    void computing_end();

protected:
    void compute_loop();

    /////
    ///  Makes an empty chnbuffer with the correct sizes
    shared_ptr<ChnBuffers> chnbuffer_make();
};

class FetchManager : public 
					 DataFlowTask< shared_ptr<T5122Frame> >
{
    typedef WaitingQueuePage< shared_ptr<T5122Frame> > TFramePendingQueue;
    typedef DataFlowTask<shared_ptr<T5122Frame> >  TFrameSuperclass;
    typedef std::vector< shared_ptr<IDataSource> >  TDataSourceCollection;
    typedef std::vector<shared_ptr<InnerFetcher>>   TInnerFetcherCollection;

    /////
    ///  Maybe put the timing data to return back here?
    TDataSourceCollection   m_data_sources;
    TInnerFetcherCollection m_inner_fetchers;
    bool                    m_is_realtime;
    unsigned                m_drop_eagerness;

    shared_ptr<T5122Frame>  m_frame_prototype;
    unsigned                m_frame_prototypes_num;

    public:
    FetchManager(
		std::vector< shared_ptr<IDataSource> > data_sources,
		unsigned drop_eagerness=4,
		string thread_name = "fetch_manager"
    );

    size_t channels_total_num();

    void    prefill_frame_setup(shared_ptr<T5122Frame> prototype, unsigned num);

    protected:

    void compute_loop();
    void timebuffer_to_inner_fetchers(T5122Frame &time_frame, size_t prefill_amount, size_t fetch_count);
};

}; //~namespace JStream

#endif //H_FETCH_WORKER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

