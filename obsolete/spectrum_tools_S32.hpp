////
///
#ifndef H_JULIE_SPECTRUM_TOOLS_S32_H
#define H_JULIE_SPECTRUM_TOOLS_S32_H

#include <memory>
#include <vector>

#include <cassert>
#include <cstddef>

#include <complex>

#include "utilities/cpp_utilities.hpp"
#include "utilities/range.hpp"

#include "kiss_fft/kiss_fftr_s32.h"

#include "time_framing.hpp"

namespace JStream {

using std::complex;
using std::shared_ptr;
typedef complex<double> C_double;

typedef complex<int32_t> C_int32;

class FFT_S32 : public 
            _detail::noncopyable
{
	static_assert(sizeof(C_int32) == sizeof(kiss_fft_s32_cpx), "integer complex types not memory cast compatible");

    Range<int32_t *>   m_input_data;
    Range<C_int32 *>   m_fft_data;
    bool               m_self_allocated;

    kiss_fftr_s32_cfg  m_fft_plan;

    public:
    FFT_S32(size_t input_size);
	//////
	///   plan_storage must be of at least length given by plan_mem_size_needed()
    FFT_S32(Range<int32_t *> input_array, Range<C_int32 *> output_array, Range<char *> plan_storage);
    ////TODO add ability to give memory for the plan as well

    ~FFT_S32();

	/////
	/// size needed by the plan to allocate
	static size_t plan_mem_size_needed(size_t input_size);

    Range<int32_t *>   input_data();
    Range<C_int32 *>   fft_data();

    void compute_fft();
};

class CSD_S32 : public _detail::noncopyable{
    typedef shared_ptr<FFT_S32> TFFTPtr;

    TFFTPtr m_fft1;
    TFFTPtr m_fft2;

    Range<C_double *> m_csd;

    int m_accumulated;

    bool m_self_allocated;

    public:

    CSD_S32(TFFTPtr fft1, TFFTPtr fft2);
    CSD_S32(TFFTPtr fft1, TFFTPtr fft2, Range<C_double *> storage);
    ~CSD_S32();

    void reset_csd();
    
    ///requires all of the FFTs to be calculated and ready to multiply
    void accumulate_csd();
};

class CSDMatrixUnit_S32 : public
                      _detail::noncopyable
{
    typedef Apodization<float> TApodization;
	typedef FFT_S32 TFFT;
	typedef CSD_S32 TCSD;
	typedef float	TMyApod;

    Range<shared_ptr<TFFT> *> m_channel_ffts;
    Range<shared_ptr<TCSD> *> m_csd_UT_matrix;
    Range<TMyApod *>          m_scaled_apodization;

    /////
    ///  Carries the memory allocated for the FFT data, CSD data, and the 
    ///  scaled apodization
    Range<char *>            m_core_block;

    public:
    CSDMatrixUnit_S32(size_t n_channels, const TApodization &apodization);
    ~CSDMatrixUnit_S32();

    size_t size_UD_matrix() const;

    ////
    //Taking arguments from 0 to the number of channels-1, 
    //this indexes into the matrix (which is represented flat)
    size_t index_UD_matrix(size_t row, size_t col) const;

    size_t channels_num() const;

    Range<const TMyApod *> apodization_data() const;
    shared_ptr<TFFT>	   fft_get(size_t idx);
    shared_ptr<TCSD>	   csd_get(size_t row_idx, size_t col_idx);
    
    void compute_ffts();
    void accumulate_csds();
    void reset_csds();
};

}; //~namespace JStream

#endif //H_JULIE_SPECTRUM_TOOLS_S32_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
