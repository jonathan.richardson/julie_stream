////
///

#ifndef H_JSTREAM_CSD_STREAM_H
#define H_JSTREAM_CSD_STREAM_H

#include <thread>
#include <cstdint>
#include <memory>

#include <boost/asio.hpp>

#include <sdt/transport/asio/task_asio.hpp>
#include <sdt/transport/task_connections.hpp>
#include <sdt/transport/multi_acceptor.hpp>
#include <sdt/queues/notify_queue.hpp>
#include <sdt/config.hpp>
#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>

#include "hardware_locality/localization_manager.hpp"

#include "data_sources/data_source_base.hpp"

#include "analysis/csd_carrier_base.hpp"

#include "jstream_inner_server.hpp"

namespace JStream {
using SDT::LoggerAccess;

//////
///  Compiler firewall as this guy has heavy machinery that shouldn't slow down compiles 
///  (internally holds all of the SDT stream classes)
namespace _detail {
    class _CSDServer;
}; //~namespace _detail


/////
///  Wraps the data server in a TCP communications layer 
class PersistentJStreamBridgeServer
{
    typedef std::mutex              Mutex;
    typedef std::unique_lock<Mutex> Guard;
    
    /// Logger
    LoggerAccess            m_logger;

	/// Protects the fpga_server state
    Mutex                   m_f_protect;

    /// The underlying data server
    std::shared_ptr<JStreamBridgeServer> m_server_impl;

    /// Contains the acceptor sockets for client connections
    SDT::TaskMultiAcceptor  m_acceptors;
    
public:
    PersistentJStreamBridgeServer(LoggerAccess logger);
    ~PersistentJStreamBridgeServer();
    
    /// Sets the underlying data server
    void server_set(std::shared_ptr<JStreamBridgeServer> server);

    /// Adds an address on which to listen for client connection requests
    void accept_address_add(const std::string &);

    /// Starts the background task to start the connections and maintain them
    void connections_begin();

    /// Ends the background task which is starting connections, as well as stops all of the 
    /// connections themselves
    void connections_end();

private:
    /// New connection handler
    void _connection_accepted_f(SDT::TaskManagerPtr transport_manager);
};

}; //~namespace JStream

#endif //H_JSTREAM_CSD_STREAM_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
