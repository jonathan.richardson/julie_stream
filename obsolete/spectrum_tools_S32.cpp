////
///
#include <memory>
#include <cstdlib>

#include <cassert>
#include <stdexcept>

#include "spectrum_tools_S32.hpp"

#define SIMD_ALIGNMENT 16

namespace JStream{

FFT_S32::
    FFT_S32(size_t input_size) :
        m_self_allocated(true),
        m_input_data(), 
        m_fft_data()
{

    size_t fft_size = (input_size) / 2 + 1;

    m_input_data = Range<int32_t *>(new int32_t[input_size], input_size);
    m_fft_data   = Range<C_int32 *>(new C_int32[fft_size], fft_size);

	
	m_fft_plan = kiss_fftr_s32_alloc(
			input_size,
			false, //not an ifft
			nullptr, //have it allocate everything
			nullptr	 //have it allocate everything
			);
	assert(m_fft_plan != nullptr);
}

size_t FFT_S32::
	plan_mem_size_needed(size_t input_size)
{
	size_t len_mem = 0;
	kiss_fft_s32_alloc(input_size, false, nullptr, &len_mem); 
	return len_mem;
}

FFT_S32::
    FFT_S32(Range<int32_t *> input_array, Range<C_int32 *> output_array, Range<char *> plan_storage) :
        m_self_allocated(false),
        m_input_data(input_array), 
        m_fft_data(output_array)
{
    size_t needed_fft_size = (m_input_data.size() + 1) / 2;
    assert(m_fft_data.size() == needed_fft_size);
	assert(plan_storage.size() >= plan_mem_size_needed(m_input_data.size()));

	assert(plan_storage.begin());
	size_t memsize = plan_storage.size();
	m_fft_plan = kiss_fftr_s32_alloc(
			m_input_data.size(),
			false, //not an ifft
			plan_storage.begin(),
			&memsize
			);
	assert(m_fft_plan != nullptr);
}

FFT_S32::
    ~FFT_S32()
{
    if(m_self_allocated){
		/////
		///  Turns out that m_fft_plan is actually an opaque pointer that we are expected to free
		///  if kiss_fftr_s32_alloc allocated it
		free(m_fft_plan);

        delete[] m_fft_data.begin();
        delete[] m_input_data.begin();
    }
}

void FFT_S32::
    compute_fft()
{
	kiss_fftr_s32(m_fft_plan, m_input_data.begin(), reinterpret_cast<kiss_fft_s32_cpx *>(m_fft_data.begin()));
}

Range<int32_t *> FFT_S32::
    input_data()
{
    return m_input_data;
}

Range<C_int32 *> FFT_S32::
    fft_data()
{
    return m_fft_data;
}

CSD_S32::
    CSD_S32(TFFTPtr fft1, TFFTPtr fft2) :
        m_self_allocated(true),
        m_fft1(fft1), 
        m_fft2(fft2)
{
    auto length = m_fft1->fft_data().size();
    assert(length == m_fft2->fft_data().size());

    m_csd = Range<C_double *>(new C_double[length], length);
    ////
    //This initializes to 0
    this->reset_csd();
}

CSD_S32::
    CSD_S32(TFFTPtr fft1, TFFTPtr fft2, Range<C_double *> storage) :
        m_self_allocated(false),
        m_fft1(fft1), 
        m_fft2(fft2),
        m_csd(storage)
{
    auto length = m_fft1->fft_data().size();
    assert(length == m_fft2->fft_data().size());
    assert(length == m_csd.size());

    ////
    //This initializes to 0
    this->reset_csd();
}

CSD_S32::
    ~CSD_S32()
{
    if(m_self_allocated){
        delete[] m_csd.begin();
    }
}

void CSD_S32::
    reset_csd()
{
    m_accumulated = 0;
    for(auto &csd_element : m_csd){
        csd_element = 0;
    }
}

void CSD_S32::
    accumulate_csd()
{
    auto fft1_data = m_fft1->fft_data();
    auto fft2_data = m_fft2->fft_data();
    for(auto idx : m_csd.idx_range()){
        m_csd[idx] += fft1_data[idx] * std::conj(fft2_data[idx]);
    }
    return;
}

inline size_t UDmatrix_num_elements(size_t matrix_size) 
{
    return (matrix_size*(matrix_size+1)/2);
}

size_t CSDMatrixUnit_S32::
    size_UD_matrix() const
{
    auto num_channels = m_channel_ffts.size();
    return UDmatrix_num_elements(num_channels);
}

size_t CSDMatrixUnit_S32::
    index_UD_matrix(size_t row, size_t col) const
{
    assert(row < m_channel_ffts.size());
    assert(col <= row);
    return ((row+1)*(row+2)/2) - col - 1;
}

CSDMatrixUnit_S32::
    CSDMatrixUnit_S32(size_t n_channels, const TApodization &apodization):
        m_core_block(),
        m_scaled_apodization()
{
    auto len_fft_inputs = apodization.size();

    /////
    ///  The outputs are in a fftwf_complex rather than floats
    auto len_fft_outputs = (len_fft_inputs + 1) / 2;

    ///Start with an align size extra, so that we can fix the alignment coming
    ///out of malloc
    size_t block_size = SIMD_ALIGNMENT;
    ///apodization window size
    size_t apodization_size_in_block = simd_align_size(sizeof(float) * len_fft_inputs);
    block_size += apodization_size_in_block;
    ///fft_size in all channels
    size_t fft_plan_size_in_block = simd_align_size(TFFT::plan_mem_size_needed(len_fft_inputs));
    size_t fft_in_size_in_block = simd_align_size(sizeof(int32_t) * len_fft_inputs);
    size_t fft_out_size_in_block = simd_align_size(sizeof(C_int32) * len_fft_outputs);
    block_size += n_channels * (fft_plan_size_in_block + fft_in_size_in_block + fft_out_size_in_block);
    ///CSD size in all channels;
    size_t csd_size_in_block = simd_align_size(sizeof(C_double) * len_fft_outputs);
    block_size += UDmatrix_num_elements(n_channels) * csd_size_in_block;

    /////
    ///  Make the block
    m_core_block = Range<char *>((char *)malloc(block_size), block_size);
    auto blk_start = m_core_block.begin();

    ///starting with this typecasting magic to determine and fix the given alignment
    size_t blk_cur_offset = uintptr_t((void *)m_core_block.begin()) % SIMD_ALIGNMENT;

    m_scaled_apodization = Range<float *>((float *)(blk_start + blk_cur_offset), len_fft_inputs);
    blk_cur_offset += apodization_size_in_block;
    ///go ahead and initialize it
    for( auto idx : m_scaled_apodization.idx_range() ){
        m_scaled_apodization[idx] = apodization.apodization_data()[idx];
    }

    auto channel_ffts = new shared_ptr<TFFT>[n_channels];
    m_channel_ffts = Range<shared_ptr<TFFT> *>(channel_ffts, channel_ffts + n_channels);
    for(auto &fftptr_el : m_channel_ffts){
        Range<char *> plan_storage((char *)(blk_start + blk_cur_offset), fft_plan_size_in_block);
        blk_cur_offset += fft_plan_size_in_block;
        Range<int32_t *> in_storage((int32_t *)(blk_start + blk_cur_offset), len_fft_inputs);
        blk_cur_offset += fft_in_size_in_block;
        Range<C_int32 *> out_storage((C_int32 *)(blk_start + blk_cur_offset), len_fft_outputs);
        blk_cur_offset += fft_out_size_in_block;

        //fftptr_el = shared_ptr<TFFT>(new TFFT(in_storage, out_storage, plan_storage));
        fftptr_el = shared_ptr<TFFT>(new TFFT(len_fft_inputs));
    }

    auto size_matrix_flattened = this->size_UD_matrix();
    auto csd_UT_matrix = new shared_ptr<TCSD>[size_matrix_flattened];
    m_csd_UT_matrix = Range<shared_ptr<TCSD> *>(csd_UT_matrix, csd_UT_matrix + size_matrix_flattened);

    for(auto row_idx : icount(n_channels)){
        for(auto col_idx : icount(row_idx + 1)){
            Range<C_double *> in_storage((C_double *)(blk_start + blk_cur_offset), len_fft_outputs);
            blk_cur_offset += csd_size_in_block;

            m_csd_UT_matrix[this->index_UD_matrix(row_idx, col_idx)] = 
                shared_ptr<TCSD>(new TCSD(
                            m_channel_ffts[row_idx], 
                            m_channel_ffts[col_idx],
                            in_storage
                        ));
        }
    }
}

CSDMatrixUnit_S32::
    ~CSDMatrixUnit_S32()
{
    delete[] m_csd_UT_matrix.begin();
    delete[] m_channel_ffts.begin();
    free(m_core_block.begin());
}

size_t CSDMatrixUnit_S32::
    channels_num() const 
{ return m_channel_ffts.size(); }

Range<const float *> CSDMatrixUnit_S32::
    apodization_data() const
{
    ////TODO find a more automatic way to do this cast
    return Range<const float *>(m_scaled_apodization.begin(), m_scaled_apodization.end());
}

shared_ptr<CSDMatrixUnit_S32::TFFT> CSDMatrixUnit_S32::
    fft_get(size_t idx)
{
    assert(idx < m_channel_ffts.size());
    return m_channel_ffts[idx];
}

shared_ptr<CSDMatrixUnit_S32::TCSD> CSDMatrixUnit_S32::
    csd_get(size_t row_idx, size_t col_idx)
{
    return m_csd_UT_matrix[this->index_UD_matrix(row_idx, col_idx)];
}

void CSDMatrixUnit_S32::
    compute_ffts()
{
    for(auto &fft_el : m_channel_ffts){
        fft_el->compute_fft();
    }
}

void CSDMatrixUnit_S32::
    accumulate_csds()
{
    for(auto &csd_el : m_csd_UT_matrix){
        csd_el->accumulate_csd();
    }
}

void CSDMatrixUnit_S32::
    reset_csds()
{
    for(auto &csd_el : m_csd_UT_matrix){
        csd_el->reset_csd();
    }
}


}//namespace ~JStream;
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
