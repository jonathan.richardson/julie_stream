%module csd_carrier

%include "std_shared_ptr.i";
%include "std_string.i"
%include "std_vector.i"
%include "std_complex.i"
%include "stdint.i"
 //%include <boost_shared_ptr.i>
%shared_ptr(CSDCarrier);
 //%include "jstream_types.hpp"
%include "analysis/csd_carrier.hpp"
 //%include "utilities/logger.hpp"
 //%include "hardware_locality/hwloc.hpp"



%{
#define SWIG_FILE_WITH_INIT
#include "Python.h"
#include "jstream_types.hpp"
#include "analysis/csd_carrier.hpp"
  //#include "utilities/logger.hpp"   /// this has Variadic templates which SWIG does not like
  //#include "hardware_locality/hwloc.hpp"  /// this has a fancy enum and constexpr that SWIG does not like
%}

// Get the NumPy typemaps
//must come after def of SWIG_FILE_WITH_INIT as the numpy def needs it. (swig parses in-order)
%include "numpy.i"

%init %{
  import_array();
  //if we need ufunc support from numpy
  //import_ufuncs();
%}

%{
#include <vector>
#include <complex>
#include <boost/asio.hpp>
using boost::asio::ip::tcp;

//#include "csd_connection.hpp"
// using namespace JStream;


PyObject *getTwelve()
{
  return Py_BuildValue("i",12);
}
PyObject *getMFrameId()
{

  return Py_BuildValue("l",123);
}
PyObject *numpy_cast(const std::vector<double> &v)
{
    npy_intp length[] = {npy_intp(v.size())};
    PyObject *array = PyArray_SimpleNew(1, length, PyArray_DOUBLE);
    memcpy(PyArray_DATA(array), v.data(), sizeof(double) * v.size());
    return array;
}

PyObject *numpy_cast(const std::vector<std::complex<double> > &v)
{
    npy_intp length[] = {npy_intp(v.size())};
    PyObject *array = PyArray_SimpleNew(1, length, PyArray_CDOUBLE);
    memcpy(PyArray_DATA(array), v.data(), sizeof(std::complex<double>) * v.size());
    return array;
}

%}



//%template(VectorFloat) std::vector<float>;
//%template(VectorDouble) std::vector<double>;
//%template(VectorCDouble) std::vector<std::complex<double> >;
//%shared_ptr(FPGAClient);
//%typemap(out) std::vector<double> {
//    std::cout << "test" << std::endl;
//    int length = $1.size();
//    $result = PyArray_FromDims(1, &length, PyArray_DOUBLE);
//    memcpy(PyArray_DATA($result), $1.data(), sizeof(double) * length);
//}
//std::vector<double> conv(const std::vector<double> &v);

%feature("autodoc", "2");

%nothread;
PyObject *getTwelve();
PyObject *getMFrameId();
PyObject *numpy_cast(const std::vector<double> &v);
PyObject *numpy_cast(const std::vector<std::complex<double> > &v);

/////
//all python GIL safe
%thread;
//%include "jstream_types.hpp"
//%include "connection.hpp"
//%include "csd_connection.hpp"

///@TODO make this throw a specific python exception, rather than RuntimeError
//%exception
//{
//    try{
//    $action
//    } 
//    catch(Disconnected &DE){
//        PyErr_SetString(PyExc_RuntimeError, "Disconnected");
//        return NULL;
//    }
//}

