////
/// Provides serialization primitives on top of a socket.
/**
 * Each message sent using this class consists of:
 * (1) An 8-byte header containing the length of the serialized data in hexadecimal.
 * (2) The serialized data.
**/
#ifndef H_JSTREAM_CONNECTION_H
#define H_JSTREAM_CONNECTION_H

#define VERSION			1.0
#define header_length	8

#include <iomanip>
#include <string>
#include <sstream>
#include <vector>
#include <complex>
#include <chrono>

#include <boost/thread/thread.hpp>
#include <boost/asio.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/bind.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/lexical_cast.hpp>

#include "csd_avg.hpp"
#include "utilities/stdtime.hpp"


namespace JStream {

using boost::asio::ip::tcp;

class Tconnection
{
public:
	/////
	///  SERVER CONSTRUCTOR OVERLOAD
	Tconnection(const unsigned short port)
	: acceptor(io_service, tcp::endpoint(tcp::v4(), port)),
	  socket(io_service)
	{
		/// Listening for incoming connection
		std::cout << "COMM: Listening for client...\n";
		acceptor.accept(socket, error);
		if(error) {
			std::cerr << "COMM: Connection error\n" << error.message() << std::endl;
			socket.close();
			return;
		}
		
		/// Check client/server version compatibility
		std::cout << "COMM: Checking client/server version compatibility...\n";
		
		float			version = 0.0;
		unsigned short	accepted = 0;

		sync_read(version);
		if(error) {
			std::cerr << "COMM: Connection error\n" << error.message() << std::endl;
			socket.close();
			return;
		}

		if(version != VERSION) {
			/// Versions incompatible. Signal the client and return.
			sync_write(accepted);
			if(error) {
				std::cerr << "COMM: Connection error\n" << error.message() << std::endl;
				socket.close();
				return;
			}
			std::cout << "COMM: Client/server version incompatility\n"
				<< "Client v" << version << ", Server v" << VERSION <<std::endl;
			socket.close();
			return;
		}
		else {
			/// Accept the connection.
			accepted = 1;
			sync_write(accepted);
			if(error) {
				std::cerr << "COMM: Connection error\n" << error.message() << std::endl;
				socket.close();
				return;
			}
			std::cout << "COMM: Client v" << version << " connected!\n";
		}
	}

	/////
	///  CLIENT CONSTRUCTOR OVERLOAD
	Tconnection(const std::string& host, const std::string& port)
	: acceptor(io_service, tcp::endpoint(tcp::v4(), 0)),
	  socket(io_service)
	{
		/// Resolve the host name into an IP address
		tcp::resolver			resolver(io_service);
		tcp::resolver::query 	query(host, port);
		tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
		tcp::endpoint 			endpoint = *endpoint_iterator;

		/// Connect to host
		socket.connect(endpoint, error);
		while(error) {
			/// Try the next endpoint on failure.
			endpoint_iterator++;
			if(endpoint_iterator != tcp::resolver::iterator()) {
				socket.close();
				endpoint = *endpoint_iterator;
				socket.connect(endpoint, error);
			}
			else {
				/// An error occurred. Log it and return.
				std::cerr << "COMM: Connection error\n" << error.message() << std::endl;
				return;
			}
		}
		
		/// Check client/server version compatibility
		std::cout << "COMM: Checking client/server version compatibility...\n";
		
		float			version = VERSION;
		unsigned short	accepted = 0;

		sync_write(version);
		if(error) {
			std::cerr << "COMM: Connection error\n" << error.message() << std::endl;
			socket.close();
			return;
		}
		
		sync_read(accepted);
		if(error) {
			std::cerr << "COMM: Connection error\n" << error.message() << std::endl;
			socket.close();
			return;
		}
		
		if(not accepted) {
			/// Versions incompatible. Log it and return.
			std::cout << "COMM: Server rejected connection due to version incompatility\n"
				<< "Try updating to the latest client release." <<std::endl;
			socket.close();
			return;
		}
		else {
			/// Inform the caller that a connection has been successfully established.
			std::cout << "COMM: Connected to server!\n";
		}
	}

	/////
	///  RETURN THE CONNECTION ERROR STATUS
	boost::system::error_code& get_error()
	{
		return error;
	}
	
	/////
	///  RETURN THE UNDERLYING SOCKET
	tcp::socket& get_socket()
	{
		return socket;
	}

	/////
	///  SYNCHORNOUSLY WRITE A DATA STRUCTURE TO THE NETWORK STREAM
	template <typename T>
	void sync_write(const T& data)
	{
		/// Serialize the data.
		std::ostringstream archive_stream;
		boost::archive::text_oarchive archive(archive_stream);
		archive << data;
		outbound_data = archive_stream.str();

		/// Format the header.
		std::ostringstream headerstream;
		headerstream << std::setw(header_length) << std::hex << outbound_data.size();
		if (!headerstream || headerstream.str().size() != header_length) {
			/// Header creation failed. Log it and return.
			boost::system::error_code error(boost::asio::error::invalid_argument);		
			std::cerr << "COMM: Error creating header\n";
			return;
		}
		outbound_header = headerstream.str();

		/// Buffer the serialized data and write it to the network stream.
		std::vector<boost::asio::const_buffer> buffers;
		buffers.push_back(boost::asio::buffer(outbound_header));
		buffers.push_back(boost::asio::buffer(outbound_data));
		boost::asio::write(socket, buffers, error);
		if(error) {
			boost::system::error_code error(boost::asio::error::invalid_argument);    
			std::cerr << "COMM: Write error\n" << error.message() << std::endl;
			return;
		}

		/// Print sent data size in MB
		float size = outbound_data.size()/1.0E6;
		printf("COMM: %2.6f MB sent\n", size);	
	}

	/////
	///  SYNCHRONOUSLY READ A DATA STRUCTURE FROM THE NETWORK STREAM
	template <typename T>
	void sync_read(T& data)
	{
		/// Read the packet header from the network stream.
		boost::asio::read(socket, boost::asio::buffer(inbound_header), error);
		if(error) {
			std::cerr << "COMM: Read error\n" << error.message() << std::endl;
			return;
		}

		/// Determine the length of the serialized data.
		std::istringstream	is(std::string(inbound_header, header_length));
		std::size_t			inbound_datasize = 0;
		if(!(is >> std::hex >> inbound_datasize)) {
			/// Header is invalid. Log it and return.
			boost::system::error_code	error(boost::asio::error::invalid_argument);
			std::cerr << "COMM: Read error\n" << error.message() << std::endl;
			return;
		}	

		/// Read the data from the network stream.
		inbound_data.resize(inbound_datasize);
		boost::asio::read(socket, boost::asio::buffer(inbound_data), error);
		if(error) {
			std::cerr << "COMM: Read error\n" << error.message() << std::endl;
			return;
		}

		/// Unpack the data structure from the data just received.
		try {
			std::string 			archive_data(&inbound_data[0], inbound_data.size());
			std::istringstream 	archive_stream(archive_data);
			boost::archive::text_iarchive	archive(archive_stream);
			archive >> data;
		}
		catch (std::exception& error) {
			/// Unable to decode the data. Log it and return.
			boost::system::error_code error(boost::asio::error::invalid_argument);
			std::cerr << "COMM: Read error\n" << error.message() << std::endl;
			return;
		}

		/// Print received data size in MB
		float size = inbound_datasize/1.0E6;
		printf("COMM: %2.6f MB received\n", size);
	}

private:
	/// TCP service object
	boost::asio::io_service 	io_service;
	
	/// Object for accepting connection requests
	tcp::acceptor 				acceptor;

	/// The underlying socket
	tcp::socket					socket;
	
	/// Connection error status
	boost::system::error_code	error;
	
	/// Outbound header
	std::string 				outbound_header;

	/// Outbound data
	std::string 				outbound_data;

	/// Inbound header
	char 						inbound_header[header_length];

	/// Inbound data
	std::vector<char>			inbound_data;	

}; //class Tconnection

} // namespace JStream

#endif //H_JSTREAM_CONNECTION_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
