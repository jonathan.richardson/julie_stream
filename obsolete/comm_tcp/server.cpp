////
///
#include "server.hpp"

namespace JStream {

using boost::asio::ip::tcp;


/////
///  CREATE SERVER LISTENING FOR CLIENT
Tserver::
	Tserver(const unsigned short port, TCSDavg& avg, boost::interprocess::interprocess_mutex& mutex)
{
	/// Launch the comm manager in a new thread.
	boost::thread thread(boost::bind(&Tserver::comm_manager, this, port, std::ref(avg), std::ref(mutex)));	
}


/////
///	 MANAGE CONNECTION
void Tserver::
	comm_manager(const unsigned short port, TCSDavg& avg, boost::interprocess::interprocess_mutex& mutex)
{
	/// Create a connector listening for the client.
	Tconnection	connection(port);

	/// Continuously send data to the client.
	if(connection.get_socket().is_open()) {transmission_loop(&connection, avg, std::ref(mutex));}

	/// Launch a new thread listening for another client.
	boost::thread thread(boost::bind(&Tserver::comm_manager, this, port, std::ref(avg), std::ref(mutex)));
	////TODO: Implement stop signal for a clean shutdown
}


/////
///	 CONTINUOUSLY TRANSMIT DATA TO CLIENT
void Tserver::
	transmission_loop(Tconnection *connection, TCSDavg& avg, boost::interprocess::interprocess_mutex& mutex)
{
	/// Inform the caller that transmission is starting.
	std::cout << "COMM: Starting data transmission...\n";

	/// Set reference timers
	auto start_time = std::chrono::high_resolution_clock::now();
	auto timer = start_time;

	/// Breaks when connection closes
	while(true)
	{
		timer = std::chrono::high_resolution_clock::now();
		if(duration_in_seconds(timer - start_time) >= INTERVAL) {
			/// Get a snapshot of the current CSD sums
			mutex.lock();
			TCSDavg snapshot = avg;
			mutex.unlock();
			
			/// Send the data to the client.
			connection->sync_write(snapshot);
			if(connection->get_error()) {
				std::cerr << "COMM: WARNING---Connection lost\n";
				return;
			}
			
			///Reset the timer.
			start_time = timer;			
		}
	}
}

} // namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
