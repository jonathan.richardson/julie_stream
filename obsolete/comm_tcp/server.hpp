////
///
#ifndef H_JSTREAM_SERVER_H
#define H_JSTREAM_SERVER_H

#include "connection.hpp"

/////
///  Port for TCP connections
#define PORT 		12345

/////
///  Interval between data sends in sec
#define INTERVAL 	1.0


namespace JStream {

using boost::asio::ip::tcp;

/////
///  SERVER OBJECT
class Tserver
{
public:
	///  Create a server object listening for a client
	Tserver(const unsigned short port, TCSDavg& avg, boost::interprocess::interprocess_mutex& mutex);

private:
	/// Manage connection
	void comm_manager(unsigned short port, TCSDavg& avg, boost::interprocess::interprocess_mutex& mutex);
	
	///	 Continuously transmit CSD data to client
	void transmission_loop(Tconnection *connection, TCSDavg& avg,
							boost::interprocess::interprocess_mutex& mutex);

}; // class Tserver

} // namespace JStream

#endif //H_JSTREAM_SERVER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
