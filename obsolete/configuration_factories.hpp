/////
///
///

#ifndef H_JSTREAM_CONFIGURATION_FACTORIES_H
#define H_JSTREAM_CONFIGURATION_FACTORIES_H

#include "utilities/config_tree/config_tree.hpp"

#include "data_sources/data_source_base.hpp"
namespace JStream {
using namespace ConfigTree;
using namespace uLogger;

std::vector< shared_ptr<IDataSource> > data_source_factory(shared_ptr<IConfigTree> list_of_sources);

}; //~namespace JStream

#endif //H_JSTREAM_CONFIGURATION_FACTORIES_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

