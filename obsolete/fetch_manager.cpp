////
///
#include <vector>

#include "utilities/os_utilities.hpp"
#include "utilities/logger.hpp"

#include "fetch_manager.hpp"

namespace JStream {
using namespace uLogger;

auto logger_fetch = logger_root().child("fetch_logger");

InnerFetcher::
    InnerFetcher(shared_ptr<IDataSource> source) :
		BackgroundTask("inner_fetcher"),
        m_source(source)
{
    for(auto idx : icount(10)){
        m_queue_reserve.push(chnbuffer_make());
    }
    m_chnbuff_public_send = chnbuffer_make();
    m_chnbuff_public_return = chnbuffer_make();
}

shared_ptr<InnerFetcher::ChnBuffers> InnerFetcher::
    chnbuffer_make()
{
    auto chnbuffer = std::make_shared<ChnBuffers>();
    chnbuffer->buffers.resize(m_source->channels_num());
    return chnbuffer;
}

InnerFetcher::ChnBuffers &InnerFetcher::
    buffer_send_access()
{
    return *m_chnbuff_public_send;
}

const InnerFetcher::ChnBuffers &InnerFetcher::
    buffer_return_access() const
{
    return *m_chnbuff_public_return;
}

unsigned InnerFetcher::num_ready()
{
    return m_queue_from.length();
}

void InnerFetcher::
    wait_done()
{
    m_queue_reserve.push(std::move(m_chnbuff_public_return));
    m_queue_from.pull_spinwait(m_chnbuff_public_return);
}

void InnerFetcher::
    push_buffer()
{
    m_queue_to.push(std::move(m_chnbuff_public_send));
    if(not m_queue_reserve.pull(m_chnbuff_public_send)){
        m_chnbuff_public_send = this->chnbuffer_make();
    }
    return;
}

void InnerFetcher::
    computing_end()
{
    m_queue_to.deny_wait_set();
    this->TSuper::computing_end();
    m_queue_to.deny_wait_clear();
}

void InnerFetcher::
    compute_loop()
{
    auto channels_num = m_source->channels_num();

    shared_ptr< ChnBuffers > buffer_set;

    if(not m_queue_to.pull_wait(buffer_set)){
        logger_fetch->fatal("Early exit from inner fetcher?");
        return;
    }

    std::vector<TFillRange> local_buffs;
	local_buffs.push_back(TFillRange(new TFill[buffer_set->buffers[0].size()], buffer_set->buffers[0].size()));
	local_buffs.push_back(TFillRange(new TFill[buffer_set->buffers[0].size()], buffer_set->buffers[0].size()));
LOGGER_DEBUG(logger_fetch, "buffer size: ", buffer_set->buffers[0].size());
    std::vector<TFillRange> copy_buffs;
    m_source->fetching_start();
    
    size_t fetch_count = 0;
    while(not this->join_waiting())
    {

        LOGGER_DEBUG(logger_fetch, "Inner Fetch, num waiting: ", m_queue_to.length());
        ////
        ///Acquire from both channels into the timestream data
        uint32_t chn_bitset = 0;
        uint32_t channels_full_bitset = (1 << channels_num) - 1;
        assert(buffer_set);
        //copy_buffs = buffer_set->buffers;
        copy_buffs = local_buffs;
        while(chn_bitset != channels_full_bitset){
            for(auto chn_idx : icount(channels_num) ){
                TFillRange &chn_buffer = copy_buffs[chn_idx];
				double time_stamp;
                ////
                ///if the channel is full, don't try to read into it
                if(chn_buffer.empty()){
                    chn_bitset |= 1 << chn_idx;
                    continue;
                }

                auto data_read_size = m_source->fetch_data(chn_idx, chn_buffer, time_stamp);
            }
        }

        shared_ptr< ChnBuffers > buffer_set_next;

        if(not m_queue_to.pull_spinwait(buffer_set_next)){
            //LOGGER_DEBUG(logger_fetch, "Inner fetcher has to wait!");
        }

        if(buffer_set_next){
            for(auto chn_idx : icount(channels_num))
            {
                auto prefill_range_to = LeftLimitedRange(buffer_set_next->buffers[chn_idx], buffer_set->prefill_amount);
                auto prefill_range_from = RightLimitedRange(buffer_set->buffers[chn_idx], prefill_range_to.size());
                for( auto el_idx : prefill_range_to.idx_range() )
                {
                    prefill_range_to[el_idx] = prefill_range_from[el_idx];
                }
            }
        }

        buffer_set->data_good = true;

        /////
        ///  inform that we finished
        m_queue_from.push(std::move(buffer_set));
        fetch_count += 1;

        if(this->join_waiting() or (not buffer_set_next)){break;}
        buffer_set = std::move(buffer_set_next);
    }
    m_source->fetching_finish();
    return;
}


FetchManager::
    FetchManager(TDataSourceCollection data_sources, unsigned drop_eagerness, string thread_name) :
		TFrameSuperclass(thread_name),
		BackgroundTask(thread_name),
        m_data_sources(data_sources),
        m_drop_eagerness(drop_eagerness)
{
    m_is_realtime = false;
    /////
    ///  Initialize queues and create worker threads
    size_t hw_concurrency = std::thread::hardware_concurrency();
    for(auto idx : icount(m_data_sources.size())){
        auto &data_source = m_data_sources[idx];

        if(data_source->realtime_is()){
            m_is_realtime = true;
        }

        m_inner_fetchers.push_back(std::make_shared<InnerFetcher>(data_source));
        //TAffinityVector affinity(hw_concurrency, false);
        //affinity[idx*2+2] = true;
        //m_inner_fetchers.back()->set_affinity(affinity);
    }
}

size_t FetchManager::
    channels_total_num()
{
    size_t n_count = 0;
    for(auto &ds_ptr : m_data_sources)
    {
        n_count += ds_ptr->channels_num();
    }
    return n_count;
}

void FetchManager::
    prefill_frame_setup(shared_ptr<T5122Frame> prototype, unsigned num)
{
    m_frame_prototype = prototype;
    m_frame_prototypes_num = num;
}

void FetchManager::
    timebuffer_to_inner_fetchers(T5122Frame &time_frame, size_t prefill_amount, size_t fetch_count)
{
    size_t chn_idx = 0;
    assert(&time_frame);
    for(auto ds_idx : icount(m_data_sources.size())){
        InnerFetcher &inner_fetcher = *m_inner_fetchers[ds_idx];
        for(auto ds_chn_idx : icount(m_data_sources[ds_idx]->channels_num())){
            auto chn_data = time_frame.channel_timebuffer(chn_idx);
            inner_fetcher.buffer_send_access().buffers[ds_chn_idx] = chn_data;
            chn_idx += 1;
        }
        inner_fetcher.buffer_send_access().prefill_amount = prefill_amount;
        inner_fetcher.buffer_send_access().data_good = false;
        inner_fetcher.buffer_send_access().fetch_num = fetch_count;
        inner_fetcher.push_buffer();
    }
}

void FetchManager::
    compute_loop()
{
    logger_fetch.action("Starting Fetch Manager loop");

    auto num_sources = m_data_sources.size();
    size_t dropped_count = 0;
    size_t fetch_count = 0;

    TFrameSuperclass::TCirculationQueue &Q_in  = *this->TFrameSuperclass::queue_input_get();
    TFrameSuperclass::TCirculationQueue &Q_out = *this->TFrameSuperclass::queue_output_get();

    {///set own affinity
    size_t hw_concurrency = std::thread::hardware_concurrency();
    TAffinityVector affinity(hw_concurrency, false);
    affinity[0] = true;
    JStream::set_affinity(affinity);
    }

    if(m_frame_prototype){
        for(auto t_idx : icount(m_frame_prototypes_num) ){
            auto fetch_buffer_inject = std::make_shared<T5122Frame>(*m_frame_prototype);
            Q_in.push(fetch_buffer_inject);
        }
    }

    if(m_is_realtime){
        logger_fetch.detail("Will drop frames");
    } else {
        logger_fetch.detail("Won't drop frames");
    }

    shared_ptr<T5122Frame> fetch_time_current(nullptr);
    TFramePendingQueue      frames_pending_queue;

    size_t prefill_amount = 0;

    ///This pull can fail, but it is protected by the while loop's condition
    Q_in.pull_wait(fetch_time_current);
    fetch_time_current->sequence_number_set(fetch_count);
    this->timebuffer_to_inner_fetchers(*fetch_time_current, prefill_amount, fetch_count);
    frames_pending_queue.push(fetch_time_current);
        
    for(auto &inner_fetcher: m_inner_fetchers){
        inner_fetcher->computing_start();
    }

    while(not this->should_quit()){
        logger_fetch.detail("Fetch timebuffer #:",fetch_count);

        shared_ptr<T5122Frame> fetch_time_next(nullptr);
        bool did_pull_frame = false;
        //////
        ///  This must be compared to length 1 rather than length 0 because the inner fetchers
        ///  double buffer
        bool will_wait_on_timebuffers = (frames_pending_queue.length() <= 1);
        //LOGGER_DEBUG(logger_fetch, "Pending: ", frames_pending_queue.length());

        if(m_is_realtime){
            if(will_wait_on_timebuffers){
                //did_pull_frame = not Q_in.pull_wait(fetch_time_next);
                did_pull_frame = Q_in.pull_spinwait(fetch_time_next);
            }else{
                did_pull_frame = Q_in.pull(fetch_time_next);
            }
        }else{
            if(not Q_in.pull(fetch_time_next)){
                if(will_wait_on_timebuffers){
                    logger_fetch.detail("FetchManager has to wait!");
                    if(not Q_in.pull_wait(fetch_time_next)){
                        if(this->should_quit()){
                            break;
                        }else{
                            throw(std::logic_error("Weird state"));
                        }
                    }
                    did_pull_frame = true;
                }
            }else{
                did_pull_frame = true;
            }
        }

        if(did_pull_frame){
            do{
                fetch_count += 1;
                fetch_time_next->sequence_number_set(fetch_count);
                prefill_amount = fetch_time_next->prefill_overlap_size(*fetch_time_current);
                this->timebuffer_to_inner_fetchers(*fetch_time_next, prefill_amount, fetch_count);
                frames_pending_queue.push(fetch_time_next);
                fetch_time_current = std::move(fetch_time_next);
            }while(Q_in.pull(fetch_time_next));
        }

        unsigned inner_fetch_frames_ready = unsigned(-1);
        for( auto &inner_fetcher : m_inner_fetchers){
            //LOGGER_DEBUG(logger_fetch, "Ready : ", inner_fetcher->num_ready());
            inner_fetch_frames_ready = std::min(inner_fetcher->num_ready(), inner_fetch_frames_ready);
        }

        if(did_pull_frame or (not (will_wait_on_timebuffers))){
            inner_fetch_frames_ready = std::max(inner_fetch_frames_ready, unsigned(1));
        }

        for(auto idx : icount(inner_fetch_frames_ready)){
            shared_ptr<T5122Frame> fetch_time_pending(nullptr);
            if(not frames_pending_queue.pull(fetch_time_pending)){
                throw(std::logic_error("should not be empty!"));
            }
            if(this->should_quit()){
                break;
            }
            /////
            ///  Wait for fetch_time_pending to finish filling
            for( auto &inner_fetcher : m_inner_fetchers){
                inner_fetcher->wait_done();
                assert(inner_fetcher->buffer_return_access().data_good);
                assert(inner_fetcher->buffer_return_access().fetch_num == fetch_time_pending->sequence_number_get());
            }

            if(frames_pending_queue.length() > m_drop_eagerness or (not m_is_realtime)){
                Q_out.push(std::move(fetch_time_pending));
            } else {
                fetch_count += 1;
                dropped_count += 1;
                logger_fetch.warning("Dropped Frame #", fetch_time_pending->sequence_number_get(), "!");
                fetch_time_next = std::move(fetch_time_pending);

                fetch_time_next->sequence_number_set(fetch_count);
                prefill_amount = fetch_time_next->prefill_overlap_size(*fetch_time_current);
                this->timebuffer_to_inner_fetchers(*fetch_time_next, prefill_amount, fetch_count);
                frames_pending_queue.push(fetch_time_next);
                fetch_time_current = std::move(fetch_time_next);
            }
        }
    }

    /////
    ///  Since we were cancelled, join all of the worker threads, as they got cancelled too
    for(auto &inner_fetcher: m_inner_fetchers){
        inner_fetcher->computing_end();
    }

    logger_fetch.digest("Fetched a total of ", fetch_count, " frames");
    if(m_is_realtime){
        logger_fetch.digest("Dropped a total of ", dropped_count, " frames");
    }
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
