////
///
///

#include "utilities/os_utilities.hpp"
#include "utilities/logger.hpp"
#include "analysis/spectrum_tools.hpp"
#include "csd_worker.hpp"

namespace JStream {
using namespace uLogger;
auto logger_csd = logger_root().child("csd_worker");

template<unsigned div_by_for_unroll>
    inline void data_fill_Nchannel(
            FFTMulti &fft_bank,
            const uint16_t * apodization,
            T5122Frame &time_buffer,
            unsigned sequence_numberx,
            unsigned channels_num
            )
{
    for(auto chn_idx : icount(channels_num)){
        const int16_t * time_frame_ptr = (const int16_t *)
		time_buffer.frame_range(chn_idx, sequence_numberx).begin();
            //__builtin_assume_aligned(time_buffer.frame_range(chn_idx, sequence_numberx).begin(), 16);
        const uint16_t * apodization_ptr = (const uint16_t *)
		apodization;
            //__builtin_assume_aligned(apodization, 16);
        float * fft_input_ptr = (float *)
		fft_bank.input_data(chn_idx).begin();
            //__builtin_assume_aligned(fft_bank.input_data(chn_idx).begin(), 16);
        for(auto el_idx : icount(fft_bank.input_data(chn_idx).size()/div_by_for_unroll))
        {
            for(unsigned inner_idx = 0; inner_idx < div_by_for_unroll; inner_idx+=1)
            {
                fft_input_ptr[inner_idx] = int32_t(apodization_ptr[inner_idx]) * int32_t(time_frame_ptr[inner_idx]);
                //fft_input_ptr[inner_idx] = apodization_ptr[inner_idx];
                //fft_input_ptr[inner_idx] = time_frame_ptr[inner_idx];
            }
            fft_input_ptr   += div_by_for_unroll;
            apodization_ptr += div_by_for_unroll;
            time_frame_ptr  += div_by_for_unroll;
        }
    }
}

typedef float TInternalOps; 
typedef void(*TBinaryOp)(TInternalOps&, const complex<TInternalOps> &);
inline void CoAddInnerProduct(TInternalOps &target, const complex<TInternalOps> &L) { target += L.real()*L.real() + L.imag()*L.imag(); }
inline void SetToInnerProduct(TInternalOps &target, const complex<TInternalOps> &L) { target  = L.real()*L.real() + L.imag()*L.imag(); }

typedef void(*TTernaryOp)(complex<TInternalOps> &, const complex<TInternalOps> &, const complex<TInternalOps>&);

inline void CoAddInnerProduct(complex<TInternalOps> &target, const complex<TInternalOps> &L, const complex<TInternalOps>&R) 
{ 
    target += complex<TInternalOps>(L) * std::conj(complex<TInternalOps>(R)); 
}

inline void CoAddInnerProduct2(complex<TInternalOps> &target, const complex<TInternalOps> &L, const complex<TInternalOps>&R) 
{ 
    TInternalOps k1 = TInternalOps(L.real()) * TInternalOps(R.real() - R.imag());
    TInternalOps k2 = TInternalOps(-R.imag()) * TInternalOps(L.real() + L.imag());
    TInternalOps k3 = TInternalOps(R.real()) * TInternalOps(L.imag() - L.real());
    target += complex<CSDCarrier::TNum>(k1 - k2, k1 + k3);
}

inline void SetToInnerProduct(complex<TInternalOps> &target, const complex<TInternalOps> &L, const complex<TInternalOps>&R) 
{ 
    target = complex<TInternalOps>(L) * std::conj(complex<TInternalOps>(R)); 
}

inline void SetToInnerProduct2(complex<TInternalOps> &target, const complex<TInternalOps> &L, const complex<TInternalOps>&R) 
{ 
    TInternalOps k1 = TInternalOps(L.real()) * TInternalOps(R.real() - R.imag());
    TInternalOps k2 = TInternalOps(-R.imag()) * TInternalOps(L.real() + L.imag());
    TInternalOps k3 = TInternalOps(R.real()) * TInternalOps(L.imag() - L.real());
    target = complex<CSDCarrier::TNum>(k1 - k2, k1 + k3);
}

template<
    unsigned div_by_for_unroll, 
    TBinaryOp  binary_op_psd,
    TTernaryOp ternary_op_csd
    >
inline void CSD_fill_Nchannel(
        FFTMulti &fft_bank,
        CSDCarrier &csd_collection,
        unsigned channels_num
        )
{
    ////
    ///PSDs first
    for(auto idx : icount(0, channels_num)){
        const complex<float> * __restrict chn_ptr = (const complex<float> *)
            __builtin_assume_aligned(fft_bank.fft_data(idx).begin(), 16);
        float * __restrict psd_data_ptr = (float *)
            __builtin_assume_aligned(csd_collection.psd_get(idx).begin(), 16);
        for(auto el_idx : icount(csd_collection.psd_get(idx).size()/div_by_for_unroll))
        {
            for(unsigned inner_idx = 0; inner_idx < div_by_for_unroll; inner_idx+=1)
            {
                binary_op_psd(psd_data_ptr[inner_idx], chn_ptr[inner_idx]);
            }
            chn_ptr  += div_by_for_unroll;
            psd_data_ptr += div_by_for_unroll;
        }
        //LOGGER_DEBUG(logger_csd, "PSD COMPUTE: ", idx);
    }
    for(auto row_idx : icount(1, channels_num)){
        for(auto col_idx : icount(0, row_idx)){
            auto row_chn = fft_bank.fft_data(row_idx);
            auto col_chn = fft_bank.fft_data(col_idx);
            auto csd_data = csd_collection.csd_get(row_idx, col_idx);

            const complex<float> * __restrict row_chn_ptr = (const complex<float> *)
                __builtin_assume_aligned(row_chn.begin(), 16);
            const complex<float> * __restrict col_chn_ptr = (const complex<float> *)
                __builtin_assume_aligned(col_chn.begin(), 16);
            complex<float> * __restrict csd_data_ptr = (complex<float> *)
                __builtin_assume_aligned(csd_data.begin(), 16);
            for(auto el_idx : icount(csd_data.size()/div_by_for_unroll))
            {
                for(unsigned inner_idx = 0; inner_idx < div_by_for_unroll; inner_idx+=1)
                {
                    ternary_op_csd(
                            csd_data_ptr[inner_idx],
                            row_chn_ptr[inner_idx],
                            col_chn_ptr[inner_idx]
                            );
                }
                row_chn_ptr  += div_by_for_unroll;
                col_chn_ptr  += div_by_for_unroll;
                csd_data_ptr += div_by_for_unroll;
            }
            //LOGGER_DEBUG(logger_csd, "CSD COMPUTE: ", row_idx, ", ", col_idx);
        }
    }
}

CSDComputeWorker::
    CSDComputeWorker(
            unsigned worker_n,
            unsigned n_channels,
            shared_ptr<TApodization> apodization,
            string thread_name
        ) :
        TFrameSuperclass(thread_name),
        TCSDSuperclass(thread_name),
        BackgroundTask(thread_name),
        m_worker_idx(worker_n),
        m_channels_num(n_channels),
        m_apodization(apodization)
{
}


void CSDComputeWorker::
compute_loop()
{
    size_t fft_count = 0;

    auto locality = this->locality_get();
    auto apodization_buff = locality->new_array<uint16_t>()[m_apodization->size()];
    for(auto idx : icount(m_apodization->size())) {
        apodization_buff[idx] = (std::numeric_limits<uint16_t>::max() * m_apodization->apodization_data()[idx]);
    }

    double normalization = m_apodization->power_density_scaling() * std::numeric_limits<uint16_t>::max();
    double NENBW = m_apodization->NENBW();

    shared_ptr<T5122Frame>  time_buffer;
    shared_ptr<TCSDCarrier> csd_collection;

    /////
    ///  Convenience access for the queues since we have to resolve between our two superclass
    ///  DataFlowTask types
    TCSDSuperclass::TCirculationQueue   &CSDCarrier_Q_in  = *this->TCSDSuperclass::queue_input_get();
    TCSDSuperclass::TCirculationQueue   &CSDCarrier_Q_out = *this->TCSDSuperclass::queue_output_get();
    TFrameSuperclass::TCirculationQueue &timestream_Q_in  = *this->TFrameSuperclass::queue_input_get();
    TFrameSuperclass::TCirculationQueue &timestream_Q_out = *this->TFrameSuperclass::queue_output_get();

    CSDCarrier csd_local(
					m_channels_num,
					m_apodization->size()/2,
					this->locality_get()
					);

    FFTMulti   fft_bank(
					m_channels_num, 
					m_apodization->size(), 
					this->locality_get()
					);

    while(true)
    {
        if(this->should_quit()){ break; }
        /////
        ///  Pull in the csd unit first so that we are ready to go once we
        ///  get that timestream
        CSDCarrier_Q_in.pull_wait(csd_collection);
        timestream_Q_in.pull_wait(time_buffer);

        /////
        ///  Didn't check the pulls to see if they succeeded, if they didn't then this is set.
        if(this->should_quit()){ break; }
        assert(csd_collection);
        assert(time_buffer);

        //LOGGER_DEBUG( logger_csd, "Worker ", m_worker_idx, " starting FFT : ", fft_count);

		/////
		///  Compute the FFTs-->CSDs batch-by-batch and add them to the running average
        constexpr unsigned div_by_for_unroll = 1024;
        for(auto sequence_numberx : icount(time_buffer->num_frames())){
            data_fill_Nchannel<div_by_for_unroll>(
                    fft_bank,
                    apodization_buff,
                    *time_buffer,
                    sequence_numberx,
                    m_channels_num
                    );

            fft_bank.compute_ffts();

            /*This is the conversion into an integer TInternalOps
              for(auto idx : icount(0, m_channels_num)){
              auto chn_ptr = fft_bank.fft_data(idx).begin();
              chn_ptr = (decltype(chn_ptr))__builtin_assume_aligned(chn_ptr, 16);
              for(auto el_idx : icount(csd_local->psd_get(idx).size()/div_by_for_unroll))
              {
              for(unsigned inner_idx = 0; inner_idx < div_by_for_unroll; inner_idx+=1)
              {
            //TODO:
            //Choose this conversion/shift factor perfectly given the signal size
            ((TInternalOps *)chn_ptr)[inner_idx] = TInternalOps(((float *)chn_ptr)[inner_idx] / (1 << ((32-16)/2 + 8/2)));
            }
            }
              chn_ptr  += div_by_for_unroll;
            }
             */

            /*
            if(time_buffer->sequence_number_get() == 5 and sequence_numberx == 0) {
                for(auto chn_idx : icount(m_channels_num)){
                    unsigned i = 0;
                    for(auto x : fft_bank.input_data(chn_idx)) {
                        LOGGER_DEBUG(logger_csd, "idx: ", i, " value: ", x);
                        i++;
                        if(i > 100){break;}
                    }
                    i = 0;
                    for(auto x : fft_bank.fft_data(chn_idx)) {
                        LOGGER_DEBUG(logger_csd, "idx: ", i, " value: ", x);
                        i++;
                        if(i > 100){break;}
                    }
                }
            }
            */

            if(sequence_numberx == 0){
                CSD_fill_Nchannel<div_by_for_unroll, SetToInnerProduct, SetToInnerProduct>( fft_bank, csd_local, m_channels_num);
            }else{
                CSD_fill_Nchannel<div_by_for_unroll, CoAddInnerProduct, CoAddInnerProduct>( fft_bank, csd_local, m_channels_num);
            }
        }

		/// Set the CSDCarrier frame metadata
        csd_local.sequence_number_set(time_buffer->sequence_number_get());
        csd_local.accumulation_set(time_buffer->num_frames());
		csd_local.timestamp_set(time_buffer->timestamp_get());

        //Benchmarks seem to indicate that this assignment (internally a
        //memcpy) is essentially free inside of NUMA.
        *csd_collection = csd_local;
        csd_collection->normalization_to_psd_set(normalization);
        csd_collection->NENBW_set(NENBW);

        timestream_Q_out.push(std::move(time_buffer));
        CSDCarrier_Q_out.push(std::move(csd_collection));
        csd_collection = shared_ptr<TCSDCarrier>(nullptr);
        fft_count += 1;
    }

    locality->delete_array(apodization_buff);
}

/* Found that this manual unroll is not faster, even if you assume the data is interlaced

   template<unsigned div_by_for_unroll>
   inline void data_fill_4channel(
   FFTMulti &fft_bank,
   CSDComputeWorker::TApodization &apodization,
   T5122Frame &time_buffer,
   unsigned sequence_numberx,
   unsigned channels_num
   )
   {
   auto apodization_ptr = apodization.apodization_data().begin();
   auto time_frame0_ptr = time_buffer.frame_range(0, sequence_numberx).begin();
   auto time_frame1_ptr = time_buffer.frame_range(1, sequence_numberx).begin();
   auto time_frame2_ptr = time_buffer.frame_range(2, sequence_numberx).begin();
   auto time_frame3_ptr = time_buffer.frame_range(3, sequence_numberx).begin();
   auto fft_input0_ptr  = fft_bank.input_data(0).begin();
   auto fft_input1_ptr  = fft_bank.input_data(1).begin();
   auto fft_input2_ptr  = fft_bank.input_data(2).begin();
   auto fft_input3_ptr  = fft_bank.input_data(3).begin();
   for(auto el_idx : icount(apodization.apodization_data().size()/div_by_for_unroll))
   {
   apodization_ptr += div_by_for_unroll;
   time_frame0_ptr += div_by_for_unroll;
   time_frame1_ptr += div_by_for_unroll;
   time_frame2_ptr += div_by_for_unroll;
   time_frame3_ptr += div_by_for_unroll;
   fft_input0_ptr  += div_by_for_unroll;
   fft_input1_ptr  += div_by_for_unroll;
   fft_input2_ptr  += div_by_for_unroll;
   fft_input3_ptr  += div_by_for_unroll;
   for(unsigned inner_idx = 0; inner_idx < div_by_for_unroll; inner_idx+=1)
   {
   fft_input0_ptr[inner_idx] = apodization_ptr[inner_idx] * time_frame0_ptr[inner_idx];
   fft_input1_ptr[inner_idx] = apodization_ptr[inner_idx] * time_frame1_ptr[inner_idx];
   fft_input2_ptr[inner_idx] = apodization_ptr[inner_idx] * time_frame2_ptr[inner_idx];
   fft_input3_ptr[inner_idx] = apodization_ptr[inner_idx] * time_frame3_ptr[inner_idx];
   }
   }
   }

   template<unsigned div_by_for_unroll, TTernaryOp ternary_op>
   inline void CSD_fill_4channel(
   FFTMulti &fft_bank,
   CSDCarrier &csd_collection,
   unsigned channels_num
   )
   {
   auto fft_chn0_ptr = fft_bank.fft_data(0).begin();
   auto fft_chn1_ptr = fft_bank.fft_data(1).begin();
   auto fft_chn2_ptr = fft_bank.fft_data(2).begin();
   auto fft_chn3_ptr = fft_bank.fft_data(3).begin();
   auto csd_0_0_ptr = csd_collection.csd_get(0, 0).begin();
   auto csd_1_0_ptr = csd_collection.csd_get(1, 0).begin();
   auto csd_1_1_ptr = csd_collection.csd_get(1, 1).begin();
   auto csd_2_0_ptr = csd_collection.csd_get(2, 0).begin();
   auto csd_2_1_ptr = csd_collection.csd_get(2, 1).begin();
   auto csd_2_2_ptr = csd_collection.csd_get(2, 2).begin();
   auto csd_3_0_ptr = csd_collection.csd_get(3, 0).begin();
   auto csd_3_1_ptr = csd_collection.csd_get(3, 1).begin();
   auto csd_3_2_ptr = csd_collection.csd_get(3, 2).begin();
   auto csd_3_3_ptr = csd_collection.csd_get(3, 3).begin();

   fft_chn0_ptr = (decltype(fft_chn0_ptr))__builtin_assume_aligned(fft_chn0_ptr, 16);
   fft_chn1_ptr = (decltype(fft_chn0_ptr))__builtin_assume_aligned(fft_chn0_ptr, 16);
   fft_chn2_ptr = (decltype(fft_chn0_ptr))__builtin_assume_aligned(fft_chn0_ptr, 16);
   fft_chn3_ptr = (decltype(fft_chn0_ptr))__builtin_assume_aligned(fft_chn0_ptr, 16);
   csd_0_0_ptr  = (decltype(csd_0_0_ptr) )__builtin_assume_aligned(csd_0_0_ptr,  16);
   csd_1_0_ptr  = (decltype(csd_1_0_ptr) )__builtin_assume_aligned(csd_1_0_ptr,  16);
   csd_1_1_ptr  = (decltype(csd_1_1_ptr) )__builtin_assume_aligned(csd_1_1_ptr,  16);
   csd_2_0_ptr  = (decltype(csd_2_0_ptr) )__builtin_assume_aligned(csd_2_0_ptr,  16);
csd_2_1_ptr  = (decltype(csd_2_1_ptr) )__builtin_assume_aligned(csd_2_1_ptr,  16);
csd_2_2_ptr  = (decltype(csd_2_2_ptr) )__builtin_assume_aligned(csd_2_2_ptr,  16);
csd_3_0_ptr  = (decltype(csd_3_0_ptr) )__builtin_assume_aligned(csd_3_0_ptr,  16);
csd_3_1_ptr  = (decltype(csd_3_1_ptr) )__builtin_assume_aligned(csd_3_1_ptr,  16);
csd_3_2_ptr  = (decltype(csd_3_2_ptr) )__builtin_assume_aligned(csd_3_2_ptr,  16);
csd_3_3_ptr  = (decltype(csd_3_3_ptr) )__builtin_assume_aligned(csd_3_3_ptr,  16);
for(auto el_idx : icount(csd_collection.csd_get(0,0).size()/div_by_for_unroll))
{
    fft_chn0_ptr += div_by_for_unroll;
    fft_chn1_ptr += div_by_for_unroll;
    fft_chn2_ptr += div_by_for_unroll;
    fft_chn3_ptr += div_by_for_unroll;
    csd_0_0_ptr  += div_by_for_unroll;
    csd_1_0_ptr  += div_by_for_unroll;
    csd_1_1_ptr  += div_by_for_unroll;
    csd_2_0_ptr  += div_by_for_unroll;
    csd_2_1_ptr  += div_by_for_unroll;
    csd_2_2_ptr  += div_by_for_unroll;
    csd_3_0_ptr  += div_by_for_unroll;
    csd_3_1_ptr  += div_by_for_unroll;
    csd_3_2_ptr  += div_by_for_unroll;
    csd_3_3_ptr  += div_by_for_unroll;
    for(unsigned inner_idx = 0; inner_idx < div_by_for_unroll; inner_idx+=1)
    {
        ternary_op(csd_0_0_ptr[inner_idx] , fft_chn0_ptr[inner_idx], std::conj(fft_chn0_ptr[inner_idx]);
                ternary_op(csd_1_0_ptr[inner_idx] , fft_chn1_ptr[inner_idx], std::conj(fft_chn0_ptr[inner_idx]);
                    ternary_op(csd_1_1_ptr[inner_idx] , fft_chn1_ptr[inner_idx], std::conj(fft_chn1_ptr[inner_idx]);
                        ternary_op(csd_2_0_ptr[inner_idx] , fft_chn2_ptr[inner_idx], std::conj(fft_chn0_ptr[inner_idx]);
                            ternary_op(csd_2_1_ptr[inner_idx] , fft_chn2_ptr[inner_idx], std::conj(fft_chn1_ptr[inner_idx]);
                                ternary_op(csd_2_2_ptr[inner_idx] , fft_chn2_ptr[inner_idx], std::conj(fft_chn2_ptr[inner_idx]);
                                    ternary_op(csd_3_0_ptr[inner_idx] , fft_chn3_ptr[inner_idx], std::conj(fft_chn0_ptr[inner_idx]);
                                        ternary_op(csd_3_1_ptr[inner_idx] , fft_chn3_ptr[inner_idx], std::conj(fft_chn1_ptr[inner_idx]);
                                            ternary_op(csd_3_2_ptr[inner_idx] , fft_chn3_ptr[inner_idx], std::conj(fft_chn2_ptr[inner_idx]);
                                                ternary_op(csd_3_3_ptr[inner_idx] , fft_chn3_ptr[inner_idx], std::conj(fft_chn3_ptr[inner_idx]);
                                                    }
                                                    }
}
*/

}; //~namespace JStream
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

