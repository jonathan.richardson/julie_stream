import os
import numpy as np
import functools
import json
import math
import textwrap
import sys

import pyqtgraph as pg    
from pyqtgraph.widgets.RemoteGraphicsView import RemoteGraphicsView

'''
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec

from matplotlib import rcParams
if sys.platform != 'win32':
    rcParams['text.usetex'] = True
rcParams['font.family']     = 'sans-serif'
rcParams['font.size']       = 14.0
#rcParams['xtick.direction'] = 'in'
#rcParams['ytick.direction'] = 'in'
'''
from ..utilities import plot_utils as utils


#####
###  Remote figure canvas class
###  (All rendering is done in a separate process)
class RemoteFigureCanvas():

    ###
    ## Constructor
    def __init__(
        self,
        num_chs,
        display_settings,
        fig_size_inches,
        dpi
    ):
        # Set the supplied display settings
        self.num_chs  = num_chs
        self.settings = display_settings

        # Create the MPL figure
        self.fig = Figure(
            figsize = fig_size_inches,
            dpi     = dpi
        )

        # Call the constructor of each inherited class
        super(FigureCanvas, self).__init__(self.fig)
                      
        # Create the list of plot keys
        self.keylist = []
        for row_idx in range(self.num_chs):
            for col_idx in range(row_idx + 1):
                if row_idx == col_idx:
                    # PSD on/off
                    key = 'a' + str(row_idx)
                    self.keylist.append(key)
                else:
                    # CSD maganitude on/off
                    key = 'c' + str(row_idx) + str(col_idx)
                    self.keylist.append(key)
                    
                    # CSD phase on/off
                    key = 'p' + str(row_idx) + str(col_idx)
                    self.keylist.append(key)
            #~for(col)
        #~for(row)
        return

        
    ###
    ## Create/update the frequency bins        
    def update_freq_bins(
        self,
        csd,
        smoothing_len = None
    ):
        if smoothing_len is None:
            smoothing_len = self.settings['smoothing_len']    
    
        # Set the smoothing length (must be power of two)   
        assert math.log(smoothing_len, 2) % 1 == 0
        self.settings['smoothing_len'] = smoothing_len      
        
        # Create the frequency bins
        length          = csd.length() / self.settings['smoothing_len']             
        nyquist_freq    = csd.sample_freq_get() / 2.            
        self.freqs      = np.linspace(0., nyquist_freq, length)

        # Set the frequency bin offset (hides the DC bins)
        self.offset  = int(2e-4 * length)
        if self.offset == 0:
            self.offset = 1  
                
        # Create the Hogan noise traces
        n_shot  = 1.5e-7                                                    # [V/rtHz]
        len_arm = 40.                                                       # [m]
        f_c = 2.39e7 / len_arm                                              # [Hz]
        norm = 2. * f_c**2 * (n_shot / 150.)                                # [V/Hz^3/2]
        f = self.freqs[self.offset:]                                        # [Hz]
        self.holo_asd = norm * np.divide(1 - np.cos(f / f_c), np.square(f)) # [V/rtHz]
        self.holo_coh = self.holo_asd / n_shot                              # [amplitude coherence]

        # Update the length of the animated lines
        placeholder = np.ones(self.freqs[self.offset:].size)
        for key in self.keylist:
            try:
                # Data curve
                self.lines[key].set_data(
                    self.freqs[self.offset:],   # new frequency bins
                    placeholder                 # y-values placeholder
                )
                
                # Background trace
                self.traces[key].set_data(
                    self.freqs[self.offset:],   # new frequency bins
                    placeholder                 # y-values placeholder
                )         
            except AttributeError:
                continue
            except KeyError:
                continue
        #~for(key)
        return
        
        
    ###
    ## Initialize the subplots
    def subplot_setup(
        self,
        csd
    ):
        # Clear the figure
        self.fig.clear()

        # Create the subplot(s) and animated line(s)
        self.ax          = {}
        self.lines       = {}
        self.poisson_lim = {}
        self.traces      = {}
        fs_key = self.settings['fullscreen']
        if not fs_key:
            # Create the full grid of PSD/CSD subplots
            grid = GridSpec(
                        3 * self.num_chs, 
                        self.num_chs, 
                        wspace = .10 * self.num_chs,#.45, 
                        hspace = .15 * self.num_chs,#.55, 
                        left   = .065, 
                        right  = .99, 
                        top    = .975, 
                        bottom = .05
                    )
                    
            for key in self.keylist:
                # Get the grid location
                ftype = key[0]
                ch1 = int(key[1])
                if ftype == 'a':
                    ch2 = ch1
                    grid_loc = grid[(3*ch1):(3*ch1 + 3), ch2]
                elif ftype == 'c':
                    ch2 = int(key[2])
                    grid_loc = grid[(3*ch1):(3*ch1 + 2), ch2]
                elif ftype == 'p':
                    ch2 = int(key[2])
                    grid_loc = grid[(3*ch1 + 2), ch2]
                else:
                    raise ValueError('Invalid subplot key (%s)' % key)
                    
                # Create the plot
                self._plot_create(
                    csd,
                    key, 
                    grid_loc
                )
            #~for(keys)
        else:
            # Create a single, fullscreen PSD/CSD plot
            grid = GridSpec(3, 1)
            ftype = fs_key[0]
            if ftype == 'a':
                # PSD plot
                grid_loc = grid[:, 0]
                self._plot_create(
                    csd, 
                    fs_key, 
                    grid_loc
                )
            elif ftype == 'c' or \
                 ftype == 'p':      
                # CSD mag/coherence plot
                key = 'c' + fs_key[1:]
                grid_loc = grid[:2, 0]        
                self._plot_create(
                    csd, 
                    key, 
                    grid_loc
                )

                # CSD phase plot
                key = 'p' + fs_key[1:]
                grid_loc = grid[2, 0]
                self._plot_create(
                    csd, 
                    key, 
                    grid_loc
                )
            else:
                raise ValueError('Invalid subplot key (%s)' % fs_key)
        #~if(fs_key)
        return
        
        
    ###
    ## Create a plot of the supplied type
    def _plot_create(
        self,
        csd,
        key,
        grid_loc
    ):
        # Get the appropriate plot attributes
        attr = self._attributes_get(
                    csd,
                    key
                )
            
        # Create the subplot
        self.ax[key] = ax = self.fig.add_subplot(grid_loc)
        
        # Create the plotted data line
        self.lines[key], = ax.plot(
                                self.freqs[self.offset:],               # x-values
                                np.ones(self.freqs[self.offset:].size), # y-values placeholder
                                color = attr['color'], 
                                animated = True
                            )
        if key[0] == 'c':
            # Create a coherence line for the Poisson statistical limit
            self.poisson_lim[key] = ax.axhline(
                                        1., 
                                        ls = '--', 
                                        color = 'k', 
                                        animated = True
                                    )
                                    
            # Create a holographic noise trace
            self.traces[key], = ax.plot(
                                    self.freqs[self.offset:],               # x-values
                                    np.ones(self.freqs[self.offset:].size), # y-values placeholder
                                    color = '.6'
                                )
            #~if(c)
        return
        
        
    ###
    ## Returns the appropriate subplot attributes
    def _attributes_get(
        self,
        csd,
        plot_key
    ):
        ftype = plot_key[0]
        
        attr = {}
        if ftype == 'a':
            # PSD general attributes
            attr['row_idx']        = int(plot_key[1])
            attr['col_idx']        = int(plot_key[1])
            attr['color']          = '#262F63'

            # PSD X-axis attributes
            if (attr['row_idx'] == self.num_chs -1) or \
               self.settings['fullscreen']:
                attr['xlabel']     = r'$\rm Frequency \; [Hz]$'
            else:
                attr['xlabel']     = ''
            attr['xlog']           = self.settings['xlog']
            if attr['xlog']:
                attr['xlog_str']   = 'log'
            else:
                attr['xlog_str']   = 'linear'
            attr['xrange']         = self.xrange_get()
            attr['xtick_vals']     = utils.tick_maker(attr['xrange'], attr['xlog'])
            attr['xtick_labs']     = utils.tick_labeler(attr['xtick_vals'], is_log = attr['xlog'])
            
            # PSD Y-axis attributes
            attr['ylabel']         = r'$\rm \sqrt{\langle' + str(attr['row_idx']) + str(attr['col_idx']) \
                                        + r'\rangle} \; \left[ V/\sqrt{Hz} \right]$'
            attr['text_size']      = 12
            attr['ylog']           = self.settings['ylog']
            if attr['ylog']:
                attr['ylog_str']   = 'log'
            else:
                attr['ylog_str']   = 'linear'
            attr['yrange']         = self.yrange_get(csd, plot_key)    
            attr['ytick_vals']     = utils.tick_maker(attr['yrange'], attr['ylog'])
            attr['ytick_labs']     = utils.tick_labeler(attr['ytick_vals'], is_log = attr['ylog'])         
        elif ftype == 'c':
            # CSD magnitude/coherence general attributes          
            attr['row_idx']        = int(plot_key[1])
            attr['col_idx']        = int(plot_key[2])
            attr['color']          = '#DE4D14'
            
            # CSD magnitude/coherence X-axis attributes 
            attr['xlabel']         = ''
            attr['xlog']           = self.settings['xlog']
            if attr['xlog']:
                attr['xlog_str']   = 'log'
            else:
                attr['xlog_str']   = 'linear'
            attr['xrange']         = self.xrange_get()
            attr['xtick_vals']     = utils.tick_maker(attr['xrange'], attr['xlog'])
            attr['xtick_labs']     = utils.tick_labeler(attr['xtick_vals'], is_log = attr['xlog']) 
            
            # CSD magnitude/coherence Y-axis attributes  
            if self.settings['coherence']:
                attr['ylabel']     = r'$\frac{\sqrt{|\langle' + str(attr['row_idx']) + str(attr['col_idx']) \
                                        + r'\rangle|}}{\sqrt[4]{\langle' + str(attr['col_idx']) \
                                        + str(attr['col_idx']) + r'\rangle \, \langle' \
                                        + str(attr['row_idx']) + str(attr['row_idx']) + r'\rangle}}$'
                attr['text_size']  = 18
            else:
                attr['ylabel']     = r'$\rm \sqrt{|\langle' + str(attr['row_idx']) + str(attr['col_idx']) \
                                        + r'\rangle|} \; \left[ V/\sqrt{Hz} \right]$'
                attr['text_size']  = 12
            attr['ylog']           = self.settings['ylog']    
            if attr['ylog']:
                attr['ylog_str']   = 'log'
            else:
                attr['ylog_str']   = 'linear'
            attr['yrange']         = self.yrange_get(csd, plot_key)
            attr['ytick_vals']     = utils.tick_maker(attr['yrange'], attr['ylog'])
            attr['ytick_labs']     = utils.tick_labeler(attr['ytick_vals'], is_log = attr['ylog'])                 
        elif ftype == 'p':   
            # CSD phase general attributes
            attr['row_idx']        = int(plot_key[1])
            attr['col_idx']        = int(plot_key[2])
            attr['color']          = '#6F7272'
            
            # CSD phase X-axis attributes
            if (attr['row_idx'] == self.num_chs - 1) or \
               self.settings['fullscreen']:
                attr['xlabel']     = r'$\rm Frequency \; [Hz]$'
            else:
                attr['xlabel']     = ''
            attr['xlog']           = self.settings['xlog']
            if attr['xlog']:
                attr['xlog_str']   = 'log'
            else:
                attr['xlog_str']   = 'linear'
            attr['xrange']         = self.xrange_get()
            attr['xtick_vals']     = utils.tick_maker(attr['xrange'], attr['xlog'])
            attr['xtick_labs']     = utils.tick_labeler(attr['xtick_vals'], is_log = attr['xlog'])
            
            # CSD phase Y-axis attributes
            attr['ylabel']         = r'$\phi_{' + str(attr['row_idx']) + str(attr['col_idx']) + r'}$'
            attr['text_size']      = 16
            attr['ylog']           = False    
            attr['ylog_str']       = 'linear'
            attr['yrange']         = self.yrange_get(csd, plot_key)
            attr['ytick_vals']     = [-180, 0, 180]
            attr['ytick_labs']     = [r'$-180^\circ$', r'$0^\circ$', r'$+180^\circ$']             
        else:
            raise ValueError('Invalid subplot key: %s' % plot_key)
        return attr                       

    
    ###
    ## Get the X-range for the supplied subplot
    def xrange_get(self):   
        if self.settings['freq_range']:
            x_range = self.settings['freq_range']
        else:
            x_range = [self.freqs[self.offset], self.freqs[-1]]
        assert x_range[0] < x_range[1]
        return x_range
    
    
    ###
    ## Get the Y-range for the supplied subplot
    def yrange_get(
        self,
        csd,
        plot_key
    ):
        # Get the range of bins to include       
        xmin, xmax = self.xrange_get()
        idx_min    = np.where(self.freqs <= xmin)[0][-1]
        idx_max    = np.where(self.freqs >= xmax)[0][0]

        # Compute the Y-range over the selected bins
        ftype = plot_key[0]
        if ftype == 'a' or \
           ftype == 'c':      
            # Power spectra and cross-spectra/coherences          
            if self.settings['yrange'] and \
               not self.settings['fullscreen']:
                # Get the global y-range of all plots of this type 
                for key in self.keylist:
                    if key[0] != ftype:
                        continue

                    # Get the Y-range of the current plot
                    spectrum     = self.csd_values_get(csd, key)[idx_min : idx_max]
                    ymin_current = 10**(math.floor(np.log10(np.min(spectrum))))
                    ymax_current = 10**(math.ceil(np.log10(np.max(spectrum))))
                    
                    # Take as the global range the furthest bounds of any plot
                    try:
                        if ymin_current < ymin:
                            ymin = ymin_current
                        if ymax_current > ymax:
                            ymax = ymax_current
                    except UnboundLocalError:
                        ymin = ymin_current
                        ymax = ymax_current
                #~for(key)
                try:
                    ymin
                except UnboundLocalError:
                    # Occurs if all subplots of this type are disabled
                    return None
            else:
                # Get the y-range of only this particular plot
                spectrum   = self.csd_values_get(csd, plot_key)[idx_min : idx_max]
                ymin     = 10**(math.floor(np.log10(np.min(spectrum))))
                ymax     = 10**(math.ceil(np.log10(np.max(spectrum))))   
                
            # Expand the y-range, if the min and max are almost the same
            if (ymax / ymin) - 1 < 0.1:
                if self.settings['ylog']:
                    ymin /= 10.
                    ymax *= 10.
                else:
                    ymin *= 0.9
                    ymax *= 1.1
        elif ftype == 'p':  
            # Phase y-range is always +/-180 deg
            ymin = -180.
            ymax =  180.    
        else:
            raise ValueError('Invalid subplot key: %s' % key)        

        return ymin, ymax          
        
        
    ###
    ## Returns the requested PSD/CSD values
    def csd_values_get(
        self, 
        csd,
        key
    ):
        _apply_smoothing = False
        if self.settings['smoothing_len'] > 1:
            _apply_smoothing = True            

        ftype = key[0]
        if ftype == 'a':        
            # Get PSD data from shared container
            chn            = int(key[1])
            norm           = csd.normalization_to_ps_get() / csd.accumulation_get()
            enbw           = csd.ENBW_get()
            spectrum       = csd.psd_getValues(chn)
            if _apply_smoothing:
                # Apply the frequency bin averaging
                spectrum   = self._apply_smoothing(spectrum)
            
            # Return the PSD in V/rtHz
            ps             = spectrum * norm                                    #[V^2]
            psd            = ps / enbw                                          #[V^2/Hz]
            asd            = np.sqrt(psd)                                       #[V/rtHz]
            return asd[self.offset:]
        elif ftype == 'c':
            # Get CSD magnitude data from shared container
            ch1            = int(key[1])
            ch2            = int(key[2])
            spectrum       = csd.csd_getValues(ch1, ch2)
            if _apply_smoothing:
                # Apply the frequency bin averaging
                spectrum   = self._apply_smoothing(spectrum)
            if self.settings['coherence']:
                ps_ch1     = csd.psd_getValues(ch1)
                ps_ch2     = csd.psd_getValues(ch2)
                if _apply_smoothing:
                    # Apply the frequency bin averaging
                    ps_ch1 = self._apply_smoothing(ps_ch1)
                    ps_ch2 = self._apply_smoothing(ps_ch2)
            else:
                norm       = csd.normalization_to_ps_get() / csd.accumulation_get()
                enbw       = csd.ENBW_get()            
            
            if self.settings['coherence']:
                # Return the amplitude cross-coherence
                coh        = np.sqrt(np.absolute(spectrum) / np.sqrt(ps_ch1 * ps_ch2)) #[dimensionless]
                return coh[self.offset:]
            else:
                # Return the CSD magnitude in V/rtHz
                cs         = spectrum * norm                                    #[V^2    complex]
                csd        = cs / enbw                                          #[V^2/Hz complex]                
                csd_mag    = np.absolute(csd)                                   #[V^2/Hz]
                asd_mag    = np.sqrt(csd_mag)                                   #[V/rtHz]
                return asd_mag[self.offset:]  
        elif ftype == 'p':
            # Get CSD phase data from shared container
            ch1            = int(key[1])
            ch2            = int(key[2])
            spectrum       = csd.csd_getValues(ch1, ch2)
            if _apply_smoothing:
                # Apply the frequency bin averaging
                spectrum   = self._apply_smoothing(spectrum)
                      
            # Return the phase in deg
            cs_phase       = np.angle(spectrum, deg = True)                     #[deg]
            return cs_phase[self.offset:]
        else:
            self.fetcher.lock.release()
            raise ValueError('Invalid subplot key: %s' % key)      
        
        
    ###
    ## Applies the global smoothing length to the supplied spectrum    
    def _apply_smoothing(
        self,
        spectrum
    ):
        length = len(self.freqs)
        dtype  = type(spectrum[0])
        smoothed_spectrum = np.zeros(length, dtype = dtype)
        for idx in range(length):
            beg_idx = idx * self.settings['smoothing_len']
            end_idx = beg_idx + self.settings['smoothing_len']
            smoothed_spectrum[idx] = np.average(spectrum[beg_idx:end_idx], axis=0)
        return smoothed_spectrum
        
        
    ###
    ## Redraw the entire figure
    def update_figure(
        self,
        csd
    ):
        # Update the subplots
        for key in self.keylist:
            try:
                ax = self.ax[key]
            except KeyError:
                continue

            # Get the appropriate subplot attributes
            attr = self._attributes_get(csd, key)

            # Apply the X-axis attributes         
            ax.set_xlabel(attr['xlabel'], fontsize=12)
            ax.set_xscale(attr['xlog_str'])
            ax.tick_params(axis='x', reset=True, which='both', length=3, width=1.25, labelsize=12)
            ax.set_xticks(attr['xtick_vals'])
            ax.set_xticklabels(attr['xtick_labs'])
            ax.set_xlim(attr['xrange'])
            
            # Apply the Y-axis attributes
            ax.set_ylabel(attr['ylabel'], fontsize=attr['text_size'])
            ax.set_yscale(attr['ylog_str'])
            ax.tick_params(axis='y', reset=True, which='both', length=3, width=1.25, labelsize=12)
            ax.set_yticks(attr['ytick_vals'])
            ax.set_yticklabels(attr['ytick_labs'])
            ax.set_ylim(attr['yrange'])
            
            # Apply the general attributes
            ax.grid(self.settings['grid'])
            
            if key[0] == 'c':
                # Set the holographic noise trace visibility
                self.traces[key].set_visible(self.settings['holo_trace'])  
                if self.settings['holo_trace']:
                    # Set the holographic noise trace values
                    if self.settings['coherence']:
                        signal = self.holo_coh
                    else:
                        signal = self.holo_asd
                    self.traces[key].set_ydata(signal)
        #~for(key)

        # Redraw the figure
        self.fig.canvas.draw()
        self.fig.canvas.blit(self.fig.bbox) #(FIX: This calls any mpl artists that still need to be updated)
        try:
            self.subplot_size
        except AttributeError:
            # Set the subplot size           
            key = self.ax.keys()[0]
            self.subplot_size = self.ax[key].bbox.width, self.ax[key].bbox.height          

        # Capture the new background of the figure        
        self.background = self.fig.canvas.copy_from_bbox(self.fig.bbox)
        return        
        
        
    ###
    ## Redraw the subplot data curves
    def update_lines(
        self,
        csd
    ):                  
        # Check whether a window resizing has occurred since the last update
        # (i.e., whether the figure must be redrawn)
        key = self.ax.keys()[0]
        current_size = self.ax[key].bbox.width, self.ax[key].bbox.height
        if current_size != self.subplot_size:
            self.subplot_size = current_size
            self.update_figure(csd)
     
        # Restore the previous figure background
        self.fig.canvas.restore_region(self.background)
        
        # Update the animated plot curves
        for key in self.keylist:
            try:
                ax = self.ax[key]   
            except KeyError:
                continue        

            # Update the data line
            spectrum = self.csd_values_get(csd, key)
            self.lines[key].set_ydata(spectrum)
            ax.draw_artist(self.lines[key])
            if key[0] == 'c' and \
               self.settings['coherence']:
                # Update the Poisson coherence level line
                level = self._poisson_coherence_get(csd)
                self.poisson_lim[key].set_ydata(level)
                ax.draw_artist(self.poisson_lim[key])
        #~for(key)

        # Call the updated line artists
        self.fig.canvas.blit(self.fig.bbox)
        return       
        
    '''
    ###
    ## Returns the current integration time (sec)
    def _integration_time_get(
        self,
        csd
    ): 
        int_time_s = (csd.timestamp_end() - csd.timestamp_begin()) / 1e9  
        return int_time_s        
    '''    
        
    ###
    ## Returns the current Poisson amplitude coherence level
    ## (Statistical limit if the two channel streams are random and uncorrelated)
    def _poisson_coherence_get(
        self,
        csd
    ):
        # Get the total CSD accumulation
        accumulation = csd.accumulation_get()
        if self.settings['smoothing_len'] > 1:
            # Account for the frequency bin averaging
            accumulation *= self.settings['smoothing_len']
            
        # Poisson coherence = 1/sqrt(N)
        min_coherence = 1. / accumulation**0.25
        return min_coherence
        
        
    ###
    ## Sets the subplot frequency range
    ## (Returns True if the requested range was set, False otherwise)
    def freq_range_set(
        self,
        csd,
        freq_range
    ):
        if freq_range is None:
            # Clear the frequency range
            if not self.settings['freq_range']:
                # Return if no range is currently set
                return True
            self.settings['freq_range'] = False
        else:
            # Get the supplied frequency range
            xmin = freq_range[0]
            xmax = freq_range[1]
            if xmin < 0 or \
               xmax < 0 or \
               xmin >= xmax:
                # Return an invalid range error
                return False

            # Adjust the frequency range, if necessary
            if xmin <= self.freqs[self.offset]:
                # Set the minimum displayable bin
                xmin = self.freqs[self.offset]
            else:
                # Set the closest bin <= xmin
                idx  = np.argmin(self.freqs < xmin) - 1
                xmin = self.freqs[idx]
            if xmax >= self.freqs[-1]:
                # Set the maximum displayable bin
                xmax = self.freqs[-1]
            else:
                # Set the closest bin >= xmax
                idx = np.argmax(self.freqs > xmax)
                xmax = self.freqs[idx]
                
            # Set the adjusted frequency range
            if self.settings['freq_range'] == [xmin, xmax]:
                # Return if the requested range is already set
                return True
            self.settings['freq_range'] = [xmin, xmax]
        #~if(freq_range)

        # Update the figure formatting
        self.update_figure(csd)
        
        # Restore the animated lines
        self.update_lines(csd)
        return True
        
        
    ###
    ## Saves the figure
    def save_figure(
        self, 
        csd,
        filename,
        remarks = 'None'
    ):
        # Overplot the data as non-animated lines
        # (FIX: Animated lines cannot be saved in this MPL backend)
        for key in self.keylist:
            try:
                ax = self.ax[key]   
            except KeyError:
                continue        
        
            # Get the appropriate subplot attributes
            attr = self._attributes_get(
                        csd,
                        key
                    )
            
            # Overplot a non-animated data line
            spectrum = self.csd_values_get(csd, key)
            ax.plot(
                self.freqs[self.offset:], 
                spectrum,
                color = attr['color']
            )
            if key[0] == 'c':
                if self.settings['coherence']:
                    # Overplot a non-animated Poisson coherence line           
                    poisson = self._poisson_coherence_get(csd)
                    ax.axhline(
                        poisson, 
                        ls = '--', 
                        color = 'k'
                    )
                    
                if self.settings['holo_trace']:
                    # Overplot a holographic noise trace
                    if self.settings['coherence']:
                        signal = self.holo_coh
                    else:
                        signal = self.holo_asd                 
                    ax.plot(
                        self.freqs[self.offset:],
                        signal,
                        color = '.6'
                    )
            #~if(c)                    
        #~for(key)
                
        # Add the metadata stamp
        self._add_metadata_label(csd, remarks)
                
        # Save the figure
        try:
            self.fig.savefig(
                filename#, 
                #dpi = utils.dpi(self.width, self.height)
            )
        except ValueError:
            print 'Error: Invalid file name or extension'

        # Clear/redraw the figure
        self.subplot_setup(csd)
        
        # Restore the figure formatting
        self.update_figure(csd)
        
        # Restore the animated lines
        self.update_lines(csd)
        return        
        
        
    ###
    ## Stamps the figure with a metadata label
    def _add_metadata_label(
            self,
            csd,
            remarks = 'None'
    ):
        x_pos       = 0.76
        y_pos       = 0.89
        delta_y     = 0.05  
        max_chars   = 38      

        # Integration time range 
        int_time_s = (csd.timestamp_end() - csd.timestamp_begin()) / 1e9 
        time_label = utils.sec_to_time_str(int_time_s)
        self.fig.text(
            x_pos, 
            y_pos, 
            'Integration time:\n' + time_label,
            va = 'top'
        )
        
        # Total accumulation (number of FFT batches)
        accumulation = '%.2e' % float(csd.accumulation_get())
        self.fig.text(
            x_pos, 
            y_pos - 1 * delta_y, 
            'Accumulation:\n' + accumulation,
            va = 'top'
        )

        # Sample frequency
        sample_freq_str = utils.sample_freq_str(csd.sample_freq_get())
        self.fig.text(
            x_pos, 
            y_pos - 2 * delta_y, 
            'Sample frequency:\n' + sample_freq_str,
            va = 'top'
        )

        # Frequency bin width
        freq_res_str = utils.freq_resolution_str(
                            csd.sample_freq_get(),
                            csd.length() * 2,
                            self.settings['smoothing_len']
                        )
        self.fig.text(
            x_pos, 
            y_pos - 3 * delta_y, 
            'Frequency bin width:\n' + freq_res_str,
            va = 'top'
        )

        # Supplied comments on the run
        if len(remarks) > max_chars:
            # Break the comment into multiple lines
            remarks = textwrap.fill(remarks, width = max_chars)
        self.fig.text(
            x_pos, 
            y_pos - 4 * delta_y, 
            'Remarks:\n' + remarks,
            va = 'top'
        )
        
        return        
        
#~FigureCanvas class
