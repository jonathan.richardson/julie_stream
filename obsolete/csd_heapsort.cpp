////
///
#include <iostream>
#include <cstdio>
#include <complex>
#include <queue>

#include <boost/asio.hpp>

#include "utilities/logger.hpp"
#include "csd_heapsort.hpp"

namespace JStream {

using namespace uLogger;

namespace{
    auto logger = logger_root().child("csd_accumulator");
};

/////
///  Comparsion utility class for ordering the CSD frames in the priority queue
class CompareFrameID {
  public:
	bool operator()(shared_ptr<TCSDCarrier> frame1, shared_ptr<TCSDCarrier> frame2) {
		if(frame2->sequence_number_get() < frame1->sequence_number_get()) {
            return true; }
		else {
            return false; }
	}
};

/////
///  Constructor
CSDHeapSort::
    CSDHeapSort(
		ConfigTreeJson& conf_data,
		string thread_name
	) :
		TCSDSuperclass(thread_name),
		BackgroundTask(thread_name)
{
}


/////
///  Contains work loop
void CSDHeapSort::
    compute_loop()
{
	uint64_t frame_count = 0;

	/////
	///  Priority queue to order the CSD frames as they arrive
	std::priority_queue<
				shared_ptr<TCSDCarrier>, 
				std::vector<shared_ptr<TCSDCarrier>>, 
				CompareFrameID 
				> frame_heap;

    /////
    ///  Convenience access for the queues since we have to resolve between our two superclass types
    TCSDSuperclass::TCirculationQueue &CSDCarrier_Q_in  = *this->TCSDSuperclass::queue_input_get();
    TCSDSuperclass::TCirculationQueue &CSDCarrier_Q_out = *this->TCSDSuperclass::queue_output_get();

	while( not this->should_quit() ) {
        /// Pull in the next csd frame	
        shared_ptr<TCSDCarrier>	current_carrier;
        CSDCarrier_Q_in.pull_wait(current_carrier);
        if(this->should_quit()){
            break; 
        }

        frame_heap.push(current_carrier);

		/////
		///  Flush the priority queue if no existing csd's would be added out-of-order
        while( (not frame_heap.empty()) and (frame_heap.top()->sequence_number_get() == frame_count) ) {
            CSDCarrier_Q_out.push(std::move(frame_heap.top()));
            frame_heap.pop();
            frame_count++;
        }
    }
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

