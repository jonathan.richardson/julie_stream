/////
///

#ifndef H_DATABASE_QUERY_SHUTTLE_H
#define H_DATABASE_QUERY_SHUTTLE_H

#include "jstream/config.hpp"
#if JSTREAM_HAS_HDF5

#include <sdt/serialization/json_schemas.hpp>
#include <sdt/serialization/json_helpers.hpp>

#include "data_management/database_manager.hpp"

namespace JStream {

/// Database query container type
// typedef struct {
    // std::string     run_id;
    // uint64_t        time_beg = 0;
    // uint64_t        time_end = 0;
    // std::string     oper;
    // std::string     comment;
// } DatabaseQuery;  


/////
///  Contains the serialization methods for the DatabaseQuery type
///  (Used internally by SDT)
struct DatabaseQueryShuttle
{
    typedef DatabaseManager::DatabaseQuery  TCarrier;

    SDT::StringShuttle<>    string_shuttle;
    
    /////
    ///  Constructors
	DatabaseQueryShuttle(){}
    
    /// Verifies that JSON headers are consistent between client and server
    DatabaseQueryShuttle(Json::Value &sink_grammar)
    {
        Json::Value source_grammar = json_schema();
        if(source_grammar != sink_grammar){
            std::cout << "Client/server version incompatibility!\n";
        }
        assert(source_grammar == sink_grammar);
    }

    /////
    ///  Returns the JSON header for the supported container type
    inline Json::Value json_schema()
    {
        Json::Value grammar;
        grammar["transmission_type"] = "DatabaseQuery";
		Json::Value &header = grammar["header"];
        json_object_field(header, "version", std::atof(JSTREAM_VERSION));
        return grammar;
    }

    /////
    ///  Serially writes the TCarrier object to the tranmission stream
    template<typename TSerialWriter>
    inline void stream_write(TSerialWriter &stream, const TCarrier &query)
    {
        /// Serially write the query to the stream    
        string_shuttle.stream_write<TSerialWriter>(stream, query.run_id);   //Run ID                                                    
        stream.serial_writeLE(uint64_t(query.time_beg));	                //Begin time (Unix sec)
        stream.serial_writeLE(uint64_t(query.time_end));	                //End time (Unix sec)
    }

    /////
    ///  Reassembles the CSDCarrierDouble  container from the binary network stream
    template<typename TSerialReader>
	inline TCarrier stream_read(TSerialReader &stream)
    {
        TCarrier    query;
            
        /// Serially read the metadata from the stream
        /// (NOTE: Data must be read from the binary stream in the same order that they were added)
        query.run_id = string_shuttle.stream_read<TSerialReader>(stream);   //Run ID                            
        stream.serial_readLE(query.time_beg);	                            //Begin time (Unix sec)
        stream.serial_readLE(query.time_end);	                            //End time (Unix sec)
        
		return query;
	}
};

}; //~namespace JStream

#endif //~JSTREAM_HAS_HDF5

#endif //H_DATABASE_QUERY_SHUTTLE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
