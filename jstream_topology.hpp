////
////
///

#ifndef H_JSTREAM_TOPOLOGY_H
#define H_JSTREAM_TOPOLOGY_H

#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <boost/thread.hpp>

#include <memory>
#include <cmath>
#include <functional>
#include <thread>
#include <chrono>
#include <iostream>
#include <sstream>
#include <vector>

#include "jstream/config.hpp"

#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>
#include <sdt/utilities/config_tree/config_tree_json.hpp>
#include <sdt/timeseries_server2/persistant_client.hpp>
#include <sdt/timeseries_registry/channel_multi_registry.hpp>
#include <sdt/timeseries_registry/channel_coadd_registry.hpp>
#include <sdt/transport/generic_multi_server.hpp>

#include "analysis/spectrum_tools.hpp"
#include "analysis/apodization.hpp"

#include "hardware_locality/localization_manager.hpp"

#include "utilities/stream_router.hpp"
#include "utilities/json_config_finders.hpp"

#include "compute_nodes/csd_accumulator.hpp"
#include "compute_nodes/csd_exporter.hpp"
#include "compute_nodes/csd_writer.hpp"
#include "compute_nodes/csd_worker.hpp"
#include "compute_nodes/fetch_manager.hpp"
#include "compute_nodes/saturation_detector.hpp"
#include "compute_nodes/transient_detector.hpp"
#include "compute_nodes/timeseries_writer.hpp"

#include "data_sources/threaded_adaptor.hpp"
#include "data_sources/virtual_5122_reader.hpp"
#include "data_sources/5122_utilities.hpp"

#include "data_sources/5122_reader.hpp"

#include "data_management/database_manager.hpp"

#include "jstream_bridge/realtime/multi_server_realtime.hpp"
#include "jstream_bridge/database/multi_server_database.hpp"

#include "jstream_types.hpp"

namespace JStream {


/////
///  Topology manager class
class JStreamTopologyController
{
    /// Convenience typedefs    
	typedef std::mutex              
        Mutex;
	typedef std::lock_guard<Mutex>  
        Guard;
    typedef std::unique_lock<Mutex> 
        Lock;
    typedef std::condition_variable 
        CondVar;     

	/// Controller state specifiers
	Mutex 	        m_protect;                                  //Protects state changes 
	bool	        m_run_state             = false;            //True if run in progress
	bool	        m_server_state          = false;            //True if networking enabled
    bool            m_system_state          = true;             //False if shutdown signaled
    
    CondVar         m_signal_abort;                             //Signals run aborted
    CondVar         m_signal_shutdown;                          //Signals system shutdown
    
    bool            m_is_key_handling       = false;            //True if key handling enabled

    /// Store the command line arguments
    int             m_argc;
    const char**    m_argv;
    
    /// JSON run configuration suppliers
    std::string     m_run_config_str;                               //JSON run configuration string
    std::string     m_run_config_fname;                             //Name of JSON run configuration file

    /// Advanced configuration settings
    size_t          m_5122_fetch_size       = 0;                    //The number of samples read in one ADC fetch
	unsigned  	    m_trigger_delay_s		= 0;	                //Time delay before the start trigger is sent (s)    
	unsigned        m_hw_concurrency        = 0;	                //Number of cores enabled
    int             m_fft_thread_num        = -1;                   //Number of CSD worker threads
	unsigned        m_fft_extra	            = 10;	                //Extra CSD frames for the queue
	int 		    m_timeframe_extra       = 10;                   //Extra time frames for the queue
    std::string     m_wisdom_fname;                                 //FFTW wisdom import filename
    bool		    m_enable_server_rt      = false;                //Enable/disable the realtime server
    bool		    m_enable_server_db      = false;                //Enable/disable the database server
	unsigned	    m_port_rt				= 8700;                 //Realtime server port
	unsigned	    m_port_db				= 8701;                 //Database server port       
    bool		    m_enable_saturation_detector    	
                                            = false;                //Enable/disable the saturation scanner
    double          m_saturation_threshold  = 0.9;                  //Fractional bit saturation tolerance level
    bool            m_enable_transient_detector       
                                            = false;                //Transient detector enable/disable switch      
    double          m_transient_threshold   = 10.0;                 //Relative power excursion triggering level
    double          m_baseline_size         = 10;                   //Size of transient detector reference accumulation
    std::vector<unsigned>
                    m_aux_chs;                                      //Auxilliary reference channels                    
	std::string	    m_database_path; 	                            //Path to the HDF5 database   
    
    /// Run configuration settings
    unsigned        m_num_chs               = 0;                    //Total number of channels
    unsigned        m_num_sources           = 0;                    //Number of data sources
    int     	    m_time_to_run_s         = -1;                   //Integration time (s)
    double          m_sample_freq           = 100e6;                //Sample frequency (Hz)
    unsigned        m_fft_size              = unsigned(1) << 16;    //FFT size
	double 		    m_overlap_frac          = .5;                   //Welch overlap fraction
    int 		    m_time_frame_len        = -1;                   //Number of samples per timeframe (per channel)
    int			    m_super_accumulation    = -1;                   //Number of CSD frames per superframe  
    double          m_accumulation_time_s   = 1.0;                  //CSD frame accumulation time (s) 
    bool            m_monitor_ifo_state     = false;                //Listens for IFO lock-state updates from control system
    long unsigned   m_start_time_s          = 0;                    //Absolute ADC trigger time
    unsigned        m_veto_thres            = 100;                  //Error level at which to veto frames    
    bool            m_enable_hdf5_writer    = false;                //HDF5 writer enable/disable switch
    std::string     m_operator_name;                                //IFO operator name
    std::string     m_run_comments;                                 //User-supplied comments
    std::vector<std::string>
                    m_source_labels;                                //Channel source descriptions
    unsigned        m_file_write_time_s     = 60;                   //CSD file write time (s)

    /// Time series writer configuration
    TimeseriesWriteNodeConfig 
                    m_ts_writer_config;
                    
    /////
    ///  Data flow components
    
    /// Logger
    LoggerAccess    m_logger;
    
    /// Message streams (routed to remote client)    
    std::ostream&   m_local_stream;
    StreamRouter    m_msg_stream;
    StreamRouter    m_err_stream;

    /// Hardware localization manager
    LocalizationManagerPtr              
                    m_loc_manager;

    /// Data sources
    std::vector<std::shared_ptr<IDataSource> > 
                    m_data_sources;

    /// Window function for FFTs
    std::shared_ptr<Apodization<float>> 
                    m_apodization;

    /// Data flow nodes
    std::shared_ptr<FetchManager>        
                    m_fetch_manager;
    std::shared_ptr<SaturationDetector>   
                    m_saturation_detector;
    std::shared_ptr<TCSDTaskVentilator>  
                    m_csd_csd_worker_pool;
    std::shared_ptr<TTimeTaskVentilator> 
                    m_time_csd_worker_pool;
    std::shared_ptr<CSDAccumulator>      
                    m_csd_accumulator;
    std::shared_ptr<TransientDetector>      
                    m_transient_detector;   
    #if JSTREAM_HAS_HDF5                     
    std::shared_ptr<CSDWriter>			 
                    m_csd_writer;
    std::shared_ptr<TimeseriesWriteNode> 
                    m_ts_writer;
    #endif
    std::shared_ptr<CSDExporter>		 
                    m_csd_exporter;

    /// Data flows
    std::shared_ptr<TCSDFlow>  			 
                    m_csd_flow;
    std::shared_ptr<TTimeFlow> 			 
                    m_time_flow;

    /// Network modules
    std::shared_ptr<RealtimeMultiServer>                  
                    m_server_rt;
    #if JSTREAM_HAS_HDF5                    
    std::shared_ptr<DatabaseMultiServer>
                    m_server_db;
    std::shared_ptr<DatabaseManager>
                    m_db_manager;                    
    #endif
    
public:
    /// Constructor
	JStreamTopologyController(
        int argc, 
        const char* argv[],
        LoggerAccess logger,
        std::ostream& local_stream
    );

    /// Change server state
	void networking_state_set(bool);
    
    ///  Sets the current IFO state
    void ifo_lock_state_set(bool);    
    
    /// Change the run state
	void run_state_set(
        bool on_off,
        bool run_config_preserve = false
    );
    
    /// Blocks main thread of execution, launches keyboard input handler
    void wait_for_shutdown(); 
    
    /// Returns total integration time (s)
	int time_to_run() const;
    
    /// Returns absolute trigger time (local s since UNIX epoch)
    unsigned trigger_time() const;
    
    /// Returns absolute (UNIX) timepoint of run ending
    std::chrono::system_clock::time_point end_timepoint() const;
    
protected:
    /// Load the advanced & run configuration settings
    ConfigTreeJsonRoot advanced_config_parse();
    ConfigTreeJsonRoot run_config_parse();    
    
    /// Inner run start/stop method
    void _run_start();
    void _run_stop(bool config_preserve);              
    
    /// Receives JSON config string from remote client
    void run_config_set(const std::string&);
    
    /// Set up network modules
    void realtime_server_create();
    #if JSTREAM_HAS_HDF5      
    void database_server_create();  
    #endif

    /// Create/destroy topology
    void topology_create(ConfigTreeJsonRoot run_config);
    void topology_destroy(bool run_config_preserve = false);

    /// Fill time/CSD queues with carrier frames
    void csd_flow_prefill();
    void time_flow_prefill();
    
    /// Timer loop for properly stopping timed runs (blocking)
    void run_timer();
    
    /// Hardware exception handler
    void hw_exception_handler();       
    
    /// Keyboard input handler (blocking)
    void key_handler();

};

}; //~namespace JStream

#endif //H_JSTREAM_TOPOLOGY_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
