////
///

#ifndef H_STREAM_ROUTER_H
#define H_STREAM_ROUTER_H

#include <iostream>
#include <memory>
#include <sstream>

namespace JStream {


/////
///  Stream buffer class for intercepting & rerouting messages
class StreamRouter : public std::streambuf
{
    /// Convenience typedefs
	typedef std::function<void (const std::string &)> \
                                    TMsgCallback;

    /// Streams & buffers
    std::streambuf                 *m_original_buffer;
    std::ostream&                   m_original_stream;
    std::shared_ptr<std::ostream>   m_new_stream;
    std::stringstream               m_char_buf;
    
    /// Wrapper for intercepted message handler
	TMsgCallback                    m_msg_callback;
    
public:
    /// Constructor
    StreamRouter(std::ostream& stream);
    
    /// Destructor
    ~StreamRouter();

    /// Set the server callback function for an intercepted message
    void msg_callback_set(TMsgCallback callback);

protected:    
    virtual int overflow(int c = traits_type::eof());
  
};

}; //~namespace JStream

#endif //H_STREAM_ROUTER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :