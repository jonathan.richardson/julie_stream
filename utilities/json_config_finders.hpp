////
///

#ifndef H_JSON_CONFIG_FINDERS_H
#define H_JSON_CONFIG_FINDERS_H

#include <boost/asio.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include <thread>

namespace JStream {

/////
///  Returns the filepath of the run settings JSON file
std::string run_config_find(std::string conf_basename);

/////
///  Returns the filepath of the advanced settings JSON file
std::string advanced_config_find(std::string conf_basename);

/////
///  Returns the filepath of the FPGA servers JSON file
std::string fpga_config_find();

}; //~namespace JStream

#endif //H_JSON_CONFIG_FINDERS_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
