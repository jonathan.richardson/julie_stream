////
///

#include "stream_router.hpp"

namespace JStream {

    /////
    ///  Constructor
    StreamRouter::
        StreamRouter(std::ostream& stream) :
            m_original_stream(stream),
            m_new_stream(NULL)
    {
        /// Disable internal buffering (overflow on every char)
        this->setp(0, 0);    
    
        /// Replace the buffer underlying the stream with this instance
        m_original_buffer = m_original_stream.rdbuf(this);
        
        /// Create a new stream underlied by the original buffer
        /// (For routing the intercepted messages to their original destination)
        m_new_stream.reset(new std::ostream(m_original_buffer));
    }
    
    /////
    ///  Destructor
    StreamRouter::
        ~StreamRouter()
    {
        /// Restore the original buffer of the supplied stream
        m_original_stream.rdbuf(m_original_buffer);
    }
    
    /////
    ///  Sets the callback function for an intercepted message
    ///  (Allows messages to be passed to the server for transmission)
    void StreamRouter::
        msg_callback_set(TMsgCallback callback)
    {
        m_msg_callback = callback;
    }

    /////
    ///  Callback function for intercepted message character 
    ///  (Reimplements the inherited std::streambuf method)
    int StreamRouter::overflow(int character)
    {
        char text = character;
        auto msg_str = std::string(&text, 1);
        
        /// Buffer the received characters, flushing on every newline
        m_char_buf << msg_str;
        if(msg_str == "\n"){
            /// Route the message to its original destination
            *m_new_stream << m_char_buf.str();
            if(m_msg_callback){
                /// Route the message to the server for transmission
                m_msg_callback(m_char_buf.str());
            }
            
            /// Reset the character buffer
            m_char_buf.str(std::string());
        }
        return character;
    }    

}; //~namespace JStream
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :