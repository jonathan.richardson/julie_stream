////
///
///

#include <sdt/utilities/range.hpp>

#ifndef H_RANGE_H
#define H_RANGE_H

namespace JStream {
    using SDT::Range;
    using SDT::icount;
    using SDT::CountingRange;
} //~namespace JStream
#endif //H_RANGE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
