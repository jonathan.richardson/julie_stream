////
///
///

#ifndef H_JSTREAM_TIME_UTILITIES_H
#define H_JSTREAM_TIME_UTILITIES_H

#include <chrono>
#include <cmath>

namespace JStream {

/////
///  Convert a chrono type time value to sec
template<typename TDNum, typename TDRatio>
double duration_in_seconds(std::chrono::duration<TDNum, TDRatio> std_duration)
{
    return std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1> > >(std_duration).count();
}

}; //~namespace JStream

#endif //H_JSTREAM_TIME_UTILITIES_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

