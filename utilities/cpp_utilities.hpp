////
///
#ifndef H_CPP_UTILITIES_H
#define H_CPP_UTILITIES_H


namespace JStream {

namespace _detail{
    /////
    ///  Much like the boost noncopyable
    struct noncopyable {
        noncopyable() = default;
        noncopyable(const noncopyable&) = delete;
        noncopyable & operator=(const noncopyable&) = delete;
    };
}; //namespace _detail

}; //~namespace JStream


#endif //H_CPP_UTILITIES_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

