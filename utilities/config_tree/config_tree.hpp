////
///
///

#include <sdt/utilities/config_tree/config_tree.hpp>
/*
#ifndef H_CONFIGURATION_TREE_IFACE_H
#define H_CONFIGURATION_TREE_IFACE_H

#include <memory>
#include <string>

#include "utilities/logger.hpp"

namespace ConfigTree {
using std::string;
using std::shared_ptr;

class IConfigTree
{
    public:
    virtual ~IConfigTree(){}

    virtual const string &name_get() const = 0;
    virtual bool is_string() const = 0;
    virtual bool is_number() const = 0;
    virtual bool is_float() const = 0;
    virtual bool is_int() const = 0;
    virtual bool is_bool() const = 0;

    virtual bool is_array() const = 0;
    virtual bool is_config() const = 0;

    virtual shared_ptr<IConfigTree> get_config_item(const string &element, bool quiet=false) = 0;
    shared_ptr<IConfigTree> operator[](const string &element)
    {
        return this->get_config_item(element);
    }

    virtual unsigned array_size() const = 0;
    virtual shared_ptr<IConfigTree> get_array_item(unsigned index, bool quiet=false) = 0;

    shared_ptr<IConfigTree> operator[](unsigned index)
    {
        return this->get_array_item(index);
    }

    virtual void get(string &value, bool quiet=false) = 0;
    virtual void set(const string &value, bool quiet=false) = 0;

    virtual void get(double &value, bool quiet=false) = 0;
    void get(float &value, bool quiet=false)
    {
        double val = value;
        this->get(val, quiet);
        value = val;
    }

    virtual void set(double  value, bool quiet=false) = 0;
    virtual void get(double &value, double L_bound, double U_bound, bool quiet=false) = 0;
    void get(float &value, float L_bound, float U_bound, bool quiet=false)
    {
        double val = value;
        this->get(val, L_bound, U_bound, quiet);
        value = val;
    }

    void get(int8_t &value, bool quiet=false){this->get_int_wrapper(value, quiet);}
    void get(int8_t &value, int8_t L_bound, int8_t U_bound, bool quiet=false){this->get_int_wrapper(value, L_bound, U_bound, quiet);}
    void get(int16_t &value, bool quiet=false){this->get_int_wrapper(value, quiet);}
    void get(int16_t &value, int16_t L_bound, int16_t U_bound, bool quiet=false){this->get_int_wrapper(value, L_bound, U_bound, quiet);}
    void get(int32_t &value, bool quiet=false){this->get_int_wrapper(value, quiet);}
    void get(int32_t &value, int32_t L_bound, int32_t U_bound, bool quiet=false){this->get_int_wrapper(value, L_bound, U_bound, quiet);}
    void get(int64_t &value, bool quiet=false){this->get_int_wrapper(value, quiet);}
    void get(int64_t &value, int64_t L_bound, int64_t U_bound, bool quiet=false){this->get_int_wrapper(value, L_bound, U_bound, quiet);}
    void get(uint8_t &value, bool quiet=false){this->get_int_wrapper(value, quiet);}
    void get(uint8_t &value, uint8_t L_bound, uint8_t U_bound, bool quiet=false){this->get_int_wrapper(value, L_bound, U_bound, quiet);}
    void get(uint16_t &value, bool quiet=false){this->get_int_wrapper(value, quiet);}
    void get(uint16_t &value, uint16_t L_bound, uint16_t U_bound, bool quiet=false){this->get_int_wrapper(value, L_bound, U_bound, quiet);}
    void get(uint32_t &value, bool quiet=false){this->get_int_wrapper(value, quiet);}
    void get(uint32_t &value, uint32_t L_bound, uint32_t U_bound, bool quiet=false){this->get_int_wrapper(value, L_bound, U_bound, quiet);}
    void get(uint64_t &value, bool quiet=false){this->get_int_wrapper(value, quiet);}
    void get(uint64_t &value, uint64_t L_bound, uint64_t U_bound, bool quiet=false){this->get_int_wrapper(value, L_bound, U_bound, quiet);}
    virtual void set(intmax_t     value, bool quiet=false) = 0;

    virtual void get(bool   &value, bool quiet=false) = 0;
    virtual void set(bool    value, bool quiet=false) = 0;

protected:
    virtual void get_int_generic(intmax_t    &value, intmax_t L_bound, intmax_t U_bound, bool quiet=false) = 0;

    template<typename TInt>
    void get_int_wrapper(TInt &value, TInt L_bound, TInt U_bound, bool quiet=false)
    {
        intmax_t val = value;
        this->get_int_generic(val, L_bound, U_bound, quiet);
        value = val;
    }

    template<typename TInt>
    void get_int_wrapper(TInt &value, bool quiet=false)
    {
        intmax_t val = value;
        this->get_int_generic(val, std::numeric_limits<TInt>::min(), std::numeric_limits<TInt>::max(), quiet);
        value = val;
    }


    //////
    ///  Logs warnings about unused/excess configs. Should be called after all configs have been 
    ///  pulled from an object
    //virtual void log_excess_configs();
};

}; //~namespace ConfigTree 

#endif //H_CONFIGURATION_TREE_IFACE_H
*/
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

