////
///
///
///
///
#include <sstream>

#include "config_tree_json.hpp"

namespace ConfigTree {

ConfigTreeJson::
    ConfigTreeJson(
        const string &source_name,
        LoggerPtr logger,
        Json::Value &json_from,
        Json::Value &json_to
                    ) : 
            m_name(source_name),
            m_json_from(json_from),
            m_json_to(json_to),
            m_logger(logger)
{
}

ConfigTreeJson::
    ~ConfigTreeJson()
{
}

const string &ConfigTreeJson::
    name_get() const
{
    return m_name;
}

bool ConfigTreeJson::is_array() const
{
    return m_json_from.isArray();
}

unsigned ConfigTreeJson::array_size() const
{
    if(not m_json_from.isArray()){
        throw(std::logic_error("Json config not an array, should always be checked first"));
    }
    return m_json_from.size();
}

bool ConfigTreeJson::is_config() const
{
    return m_json_from.isObject();
}

shared_ptr<IConfigTree> ConfigTreeJson::
    get_config_item(const string &element, bool quiet)
{
    Json::Value from_json;
    if(m_json_from.isObject()){
        if(m_json_from.isMember(element)){
            from_json = m_json_from[element];
        }else{
            if(not quiet){m_logger.config(m_name,".", element, " is missing");}
        }
    }else{
        if(not quiet){m_logger.config(m_name, ".", element, " missing (",m_name," not an object)");}
    }

    if(m_json_to.isNull()){
        m_json_to = Json::Value(Json::objectValue);
    }else if(not m_json_to.isObject()){
        m_logger.error(m_name, " config value changing types! (Multiple defaults accessed?)");
        m_json_to = Json::Value(Json::objectValue);
    }
    Json::Value &to_json = m_json_to[element];
    std::stringstream SS;
    SS << m_name << "." << element;
    return std::make_shared<ConfigTreeJson>(SS.str(), m_logger, from_json, to_json);
}

shared_ptr<IConfigTree> ConfigTreeJson::
    get_array_item(unsigned index, bool quiet)
{
    Json::Value from_json;
    if(m_json_from.isArray()){
        if(index < m_json_from.size()){
            from_json = m_json_from[index];
        }else{
            if(not quiet){m_logger.config(m_name,"[",index,"] is missing");}
        }
    }else{
        if(not quiet){m_logger.config(m_name, " not an array");}
    }

    if(m_json_to.isNull()){
        m_json_to = Json::Value(Json::arrayValue);
    }else if(not m_json_to.isArray()){
        m_logger.error(m_name, " config value changing types! (Multiple defaults accessed?)");
        m_json_to = Json::Value(Json::arrayValue);
    }
    Json::Value &to_json = m_json_to[index];
    std::stringstream SS;
    SS << m_name << "[" << index << "]";
    return std::make_shared<ConfigTreeJson>(SS.str(), m_logger, from_json, to_json);
}

bool ConfigTreeJson::is_string() const
{
    return m_json_from.isString();
}

bool ConfigTreeJson::is_number() const
{
    return m_json_from.isNumeric();
}

bool ConfigTreeJson::is_float() const
{
    return m_json_from.isNumeric();
}

bool ConfigTreeJson::is_int() const
{
    if(not m_json_from.isNumeric()){
        return false;
    }
    auto value = m_json_from.asDouble();
    return value == intmax_t(value);
}

bool ConfigTreeJson::is_bool() const
{
    return m_json_from.isBool();
}


void ConfigTreeJson::
    set(const string &value, bool quiet)
{
    if(m_json_to.isNull()){
        m_json_to = value;
    }else if(m_json_to.isString()){
        auto to_value = m_json_to.asString();
        if(to_value != value){
            m_logger.error(m_name, " config value changing! (Multiple defaults accessed?), from: \"", value, "\" to: \"", to_value, "\"");
        }
    }else{
        m_logger.error(m_name, " config value changing types! (Multiple defaults accessed?)");
    }
}

void ConfigTreeJson::
    get(string &value, bool quiet)
{
    if(m_json_from.isString()){
        value = m_json_from.asString();
        if(not quiet){m_logger.config(m_name, " = \"", value, "\"");}
    }else if(m_json_from.isNull()){
        if(not quiet){m_logger.config(m_name, " defaulting to \"", value, "\"");}
    }else{
        if(not quiet){m_logger.config(m_name, " wrong type, defaulting to \"", value, "\"");}
    }

    this->set(value, quiet);
    return;
}

void ConfigTreeJson::
    set(double value, bool quiet)
{
    if(m_json_to.isNull()){
        m_json_to = value;
    }else if(m_json_to.isNumeric()){
        auto to_value = m_json_to.asDouble();
        if(to_value != value){
            m_logger.error(m_name, " config value changing! (Multiple defaults accessed?), from: \"", value, "\" to: \"", to_value, "\"");
        }
    }else{
        m_logger.error(m_name, " config value changing types! (Multiple defaults accessed?)");
    }
}

void ConfigTreeJson::
    get(double &value, bool quiet)
{
    if(m_json_from.isNumeric()){
        value = m_json_from.asDouble();
        if(not quiet){m_logger.config(m_name, " = ", value);}
    }else if(m_json_from.isNull()){
        if(not quiet){m_logger.config(m_name, " defaulting to \"", value, "\"");}
    }else{
        if(not quiet){m_logger.config(m_name, " wrong type, defaulting to \"", value, "\"");}
    }

    this->set(value, quiet);
    return;
}

void ConfigTreeJson::
    get(double &value, double L_bound, double U_bound, bool quiet)
{
    if(m_json_from.isNumeric()){
        value = m_json_from.asDouble();
        if(not quiet){m_logger.config(m_name, " = ", value);}
    }else if(m_json_from.isNull()){
        if(not quiet){m_logger.config(m_name, " defaulting to \"", value, "\"");}
    }else{
        if(not quiet){m_logger.config(m_name, " wrong type, defaulting to \"", value, "\"");}
    }

    if(value < L_bound){
        if(not quiet){m_logger.warning(m_name, " = ",value,", must be >= ", L_bound,", coercing up");}
        value = L_bound;
    }else if(value > U_bound){
        if(not quiet){m_logger.warning(m_name, " = ",value,", must be <= ", U_bound,", coercing down");}
        value = U_bound;
    }

    this->set(value, quiet);
    return;
}

void ConfigTreeJson::
    set(intmax_t value, bool quiet)
{
    if(m_json_to.isNull()){
        m_json_to = Json::Value(Json::Int64(value));
    }else if(m_json_to.isNumeric()){
        auto to_value = m_json_to.asDouble();
        if(to_value != value){
            m_logger.error(m_name, " config value changing! (Multiple defaults accessed?), from: \"", value, "\" to: \"", to_value, "\"");
        }
    }else{
        m_logger.error(m_name, " config value changing types! (Multiple defaults accessed?)");
    }
}

void ConfigTreeJson::
    get_int_generic(intmax_t &value, intmax_t L_bound, intmax_t U_bound, bool quiet)
{
    double value_d;
    if(m_json_from.isNumeric()){
        value_d = m_json_from.asDouble();
        if(not quiet){m_logger.config(m_name, " = ", value);}
    }else if(m_json_from.isNull()){
        if(not quiet){m_logger.config(m_name, " defaulting to \"", value, "\"");}
        value_d = value;
    }else{
        if(not quiet){m_logger.config(m_name, " wrong type, defaulting to \"", value, "\"");}
        value_d = value;
    }
    if(value_d != intmax_t(value_d)){
        if(not quiet){m_logger.warning(m_name, " = ",value,", should be an integer, rounding down");}
    }
    value = intmax_t(value_d);

    if(value < L_bound){
        if(not quiet){m_logger.warning(m_name, " = ",value,", must be >= ", L_bound,", coercing up");}
        value = L_bound;
    }else if(value > U_bound){
        if(not quiet){m_logger.warning(m_name, " = ",value,", must be <= ", U_bound,", coercing down");}
        value = U_bound;
    }

    this->set(value, quiet);
    return;
}

void ConfigTreeJson::
    set(bool value, bool quiet)
{
    if(m_json_to.isNull()){
        m_json_to = value;
    }else if(m_json_to.isBool()){
        auto to_value = m_json_to.asBool();
        if(to_value != value){
            m_logger.error(m_name, " config value changing! (Multiple defaults accessed?), from: \"", 
                    value?"true":"false"   , "\" to: \"", 
                    to_value?"true":"false", "\""
                    );
        }
    }else{
        m_logger.error(m_name, " config value changing types! (Multiple defaults accessed?)");
    }
}

void ConfigTreeJson::
    get(bool &value, bool quiet)
{
    if(m_json_from.isBool()){
        value = m_json_from.asBool();
        if(not quiet){m_logger.config(m_name, " = ", value);}
    }else if(m_json_from.isNull()){
        if(not quiet){m_logger.config(m_name, " defaulting to false");}
    }else{
        if(not quiet){m_logger.config(m_name, " wrong type, coercing to ", (value?"true":"false"));}
    }

    this->set(value, quiet);
    return;
}

}; //~namespace ConfigTree
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :

