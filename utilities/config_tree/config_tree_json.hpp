////
/// 
///
/// 
///

#include <sdt/utilities/config_tree/config_tree_json.hpp>
/*
#ifndef H_CONFIGTREE_JSON_H
#define H_CONFIGTREE_JSON_H

#include <memory>
#include <jsoncpp/json.h>

#include "config_tree.hpp"

namespace ConfigTree {
using namespace uLogger;
using std::shared_ptr;
using std::string;

class ConfigTreeJson : public 
                         IConfigTree
{
    string m_name;
    LoggerPtr    m_logger;
    Json::Value m_json_from;
    Json::Value &m_json_to;

    public:
    ConfigTreeJson(
        const string &source_name,
        LoggerPtr logger,
        Json::Value &json_from,
        Json::Value &json_to
            );

    virtual ~ConfigTreeJson();

    virtual const string &name_get() const;

    virtual bool is_string() const;
    virtual bool is_number() const;
    virtual bool is_float() const;
    virtual bool is_int() const;
    virtual bool is_bool() const;

    virtual bool is_array() const;
    virtual bool is_config() const;

    virtual shared_ptr<IConfigTree> get_config_item(const string &element, bool quiet=false);
    //shared_ptr<IConfigTree> operator[](const string &element){return this->get_config_item(element);}

    virtual unsigned array_size() const;
    virtual shared_ptr<IConfigTree> get_array_item(unsigned index, bool quiet=false);
    //shared_ptr<IConfigTree> operator[](unsigned index){return this->get_array_item(index);}

    virtual void get(string &value, bool quiet=false);
    virtual void set(const string &value, bool quiet=false);

    virtual void get(double &value, bool quiet=false);
    virtual void set(double  value, bool quiet=false);
    virtual void get(double &value, double L_bound, double U_bound, bool quiet=false);

    virtual void set(intmax_t     value, bool quiet=false);

    virtual void get(bool   &value, bool quiet=false);
    virtual void set(bool    value, bool quiet=false);

protected:
    virtual void get_int_generic(intmax_t    &value, intmax_t L_bound, intmax_t U_bound, bool quiet=false);
    //////
    ///  Logs warnings about unused/excess configs. Should be called after all configs have been 
    ///  pulled from an object
    //virtual void log_excess_configs();
};

}; //~namespace ConfigTree

#endif //H_CONFIGTREE_JSON_H
*/
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
