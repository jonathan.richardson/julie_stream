////
///

#include "json_config_finders.hpp"

namespace JStream {
namespace FS = boost::filesystem;


/////
///  Returns the filepath of the run settings JSON file
///  (Utility function)
std::string run_config_find(std::string conf_basename)
{
    FS::path     conf_fpath;
    
    /// If not overriden from the command line, read the default file
    if(conf_basename.empty()){      
        conf_basename = "run_settings.json";
    }

    /// Look for the file in the current directory first
    conf_fpath = FS::path(conf_basename);
    if(FS::exists(conf_fpath)){
        return conf_fpath.string();
    }

    /// Look for the file in the /settings directory next
    conf_fpath = FS::path("settings") / FS::path(conf_basename);
    if(FS::exists(conf_fpath)){
        return conf_fpath.string();
    }

    { /// If still not found, set the global settings directory (if defined)
        const char *envstr = getenv("JSTREAM_PATH");
        if(envstr != nullptr){
            conf_fpath = FS::path(envstr) / FS::path("settings") / FS::path(conf_basename);
        }
    }
    return conf_fpath.string();
}

/////
///  Returns the filepath of the advanced settings JSON file
///  (Utility function)
std::string advanced_config_find(std::string conf_basename)
{
    FS::path     conf_fpath;
    auto         num_cores = std::thread::hardware_concurrency();
    
    /// If not overriden from the command line, read the default file
    /// based on the host name and number of cores enabled
    if(conf_basename.empty()){         
        /// Determine the host name
        std::string hostname = boost::asio::ip::host_name();

        /// Get the name of the appropriate file
        std::ostringstream ss;
        ss << "adv_settings_" << hostname << "_" << num_cores << "cores.json";     
        conf_basename = ss.str();
    }
    
    /// Look for the file in the current directory first
    conf_fpath = FS::path(conf_basename);
    if(FS::exists(conf_fpath)){
        return conf_fpath.string();
    }

    /// Look for the file in the (local) /settings directory next
    conf_fpath = FS::path("settings") / FS::path(conf_basename);
    if(FS::exists(conf_fpath)){
        return conf_fpath.string();
    }

    { /// Look for the file in the global settings directory next
        const char *envstr = getenv("JSTREAM_PATH");
        if(envstr != nullptr){
            conf_fpath = FS::path(envstr) / FS::path("settings") / FS::path(conf_basename);
            if(FS::exists(conf_fpath)){
                return conf_fpath.string();
            }
        }
    }

    { /// If not found, default to generic settings based only on the number of cores
        std::ostringstream ss;
        ss << "adv_settings_" << "default" << "_" << num_cores << "cores.json";        
        conf_basename = ss.str();    
        const char *envstr = getenv("JSTREAM_PATH");
        if(envstr != nullptr){
            conf_fpath = FS::path(envstr) / FS::path("settings") / FS::path(conf_basename);
            if(FS::exists(conf_fpath)){
                return conf_fpath.string();
            }
        }
    }        
    return conf_fpath.string();
}

/////
///  Returns the filepath of the FPGA servers JSON file
///  (Utility function)
std::string fpga_config_find()
{
    FS::path     conf_fpath;

    /// Look for the file in the global settings directory
    const char *envstr = getenv("JSTREAM_PATH");
    if(envstr != nullptr){
        conf_fpath = FS::path(envstr) / FS::path("settings") / FS::path("fpga_servers.json");
    }
    return conf_fpath.string();
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
