//////
///  @file julie_stream.cpp
///  @date
///  @breif 
///  
///  @author Jonathan Richardson
///
///  @copyright @ref LICENSE
///
///  @ingroup ALL
///  
///  @todo 
///  @bug

///  UTILITY PROGRAM FOR INTERFACING BETWEEN DAQ SERVER, FPGA SERVERS, AND DAQ CLIENTS


#include <iostream>

#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>

#include "jstream_bridge/repeater/jstream_repeater.hpp"

using namespace JStream;

namespace{
    auto logger = uLogger::logger_root().child("jstream");
}

int main(int argc, const char* argv[])
{
    uLogger::logrouter_root()->settings(5);

    /// Create the topology manager
    /// (Loads the advanced JSON configuration, which is machine-dependent)
    JStreamNetworkRepeater repeater(logger);

    /// Start the network repeater
    repeater.comm_state_set(true);

    /// Block the main thread until "shutdown" is keyed
    repeater.wait_for_shutdown();

    /// Stop the network repeater
    repeater.comm_state_set(false);
    return 0;
}

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :