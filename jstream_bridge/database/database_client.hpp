////
///

#ifndef H_DATABASE_CLIENT_H
#define H_DATABASE_CLIENT_H

#include "jstream/config.hpp"
#if JSTREAM_HAS_HDF5

#include <boost/asio.hpp>

#include <sdt/transport/asio/task_asio.hpp>
#include <sdt/transport/task_connections.hpp>
#include <sdt/streams/exceptions.hpp>
#include <sdt/queues/notify_queue.hpp>
#include <sdt/threading/notify/multi_wait.hpp>
#include <sdt/config.hpp>
#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>

#include "data_management/hdf5_writer.hpp"

#include "inner_client_database.hpp"

namespace JStream {


/////
///  Database client class
class DatabaseClient
{
    /// Convenience typedefs
    typedef std::mutex                              
        Mutex;
    typedef std::unique_lock<Mutex>                 
        Guard;

    /// Protects the connection state
    Mutex                                   m_p_protect;
    SDT::SwitchNotify                       m_is_connecting;
    SDT::SwitchNotify                       m_is_connected;

    /// Used by the SDT internal transceiver
    std::shared_ptr<_detail::_DatabaseClient>    
                                            m_inner;
    SDT::TaskTransportPtr                   m_transport_sending;
    SDT::TaskTransportPtr                   m_transport_receiving;
    SDT::TaskConnectPtr                     m_connections_task;

    /// Protects data transfer state
    Mutex                                   m_protect;
    SDT::SwitchNotify                       m_is_transferring;
    uint64_t                                m_rcv_ct;

    /// HDF5 writer    
    std::string                             m_local_path;    
#if JSTREAM_HAS_HDF5
    std::shared_ptr< HDF5CSDWriter<CSDCarrierFloat> >          
                                            m_hdf5_writer_flt;
    std::shared_ptr< HDF5CSDWriter<CSDCarrierDouble> >          
                                            m_hdf5_writer_dbl;   
#endif //~JSTREAM_HAS_HDF5                                            

    
    /// Database search results
    std::vector<std::string>                m_search_results;                                            
    
    /// Source metadata
    std::vector<IDataSource::DataSourceSettings>                 
                                            m_source_metadata;
                    
    /// Frame accumulation
    std::shared_ptr<CSDCarrierDouble>       m_local_csd;                                    

public:
    DatabaseClient(const std::string &connection_addr);

    ~DatabaseClient();
   
#ifndef SWIG
    /// Makes this class noncopyable
    DatabaseClient(const DatabaseClient&) = delete;
#endif
      
    /// Client controls
    bool wait_for_connection();
    bool is_connected();
	void start();
    bool stop();
    void join();
    bool is_live();
    
    /////////////////////////////
    /// Database access interface
    /////////////////////////////
    
    /// Database search 
    /// (Blocks until search completes, then returns results)
    std::vector<std::string> search_send(const std::string &);       
    
    /// Transfer data average over the supplied range
    /// (Blocks until transfer completes)
    std::shared_ptr<CSDCarrierDouble> transfer_averages_send(const std::string &query);
      
    /// Transfer data frames or data average to local disk
    /// (Blocks until transfer completes)      
    void transfer_to_disk_send(
        const std::string &query,
        const std::string &local_path,
        bool write_all
    ); 

protected:
    /// Lifetime management for the connected side of this object
	void _inner_start_p();
    bool _inner_stop();
    bool _inner_stop_p();
    void _inner_join_p();

    /// Event callbacks
    void connection_established(SDT::TaskManagerPtr);   
    void search_hit_rcv(const std::string);
    void metadata_rcv(const IDataSource::DataSourceSettings);
    void csd_float_rcv(const CSDCarrierFloat);
    void csd_double_rcv(const CSDCarrierDouble);    
    void transfer_complete_rcv(int);
    
    /// Wrapper functions for interfacing with the HDF5 writers
    void csd_write(const CSDCarrierFloat);
    void csd_write(const CSDCarrierDouble);

};

}; //~namespace JStream

#endif //~JSTREAM_HAS_HDF5

#endif //H_DATABASE_CLIENT_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :