////
///

#include "database_client.hpp"

namespace {
	uLogger::LoggerAccess logger = uLogger::logger_root().child("client");
}

namespace JStream {


/////
///  Database client constructor
DatabaseClient::
    DatabaseClient(const std::string &connection_addr) :
        m_is_transferring(false),
        m_is_connecting(false),
        m_is_connected(false)
{
    /// Associate this class type with the logger context
    logger.this_typename_set(this);
    
    /// Create the SDT connections task manager (uses boost::asio)
    /// and set the callback for a newly-established connection
	auto asio_manager = std::make_shared<SDT::AsioTaskManager>();
    SDT::TaskConnectFactory connect_factory;
	asio_manager->task_factory_access(connect_factory);
    m_connections_task = connect_factory(
        connection_addr, 
        SDT::TransportActorGenerator(
                std::bind(
                    &DatabaseClient::connection_established,
                    this,
                    std::placeholders::_1
                )
        )
    );
     
    /// Create the inner SDT transceiver & set the forwarding callbacks
	m_inner = std::make_shared<_detail::_DatabaseClient>();
    m_inner->m_rcvr_search_hit.handler_set(
        [this](const std::string hit){
            return this->search_hit_rcv(hit);
        }
    );
    m_inner->m_rcvr_metadata.handler_set(
        [this](const IDataSource::DataSourceSettings settings){
            return this->metadata_rcv(settings);
        }
    );    
    m_inner->m_rcvr_csd_float.binding_set(
        [this](const CSDCarrierFloat csd){
            return this->csd_float_rcv(csd);
        }
    );          
    m_inner->m_rcvr_csd_double.binding_set(
        [this](const CSDCarrierDouble csd){
            return this->csd_double_rcv(csd);
        }
    );            
    m_inner->m_rcvr_transfer_complete.handler_set(  
        [this](int signal){
            return this->transfer_complete_rcv(signal);
        }
    );         
}

/////
///  Client destructor
DatabaseClient::
    ~DatabaseClient()
{
	this->stop();
	this->join();
}

/////
///  Initiate a connection
void DatabaseClient::
    start()
{
	Guard p_guard(m_p_protect);
    if(not m_is_connecting){
        m_connections_task->start();
        m_is_connecting.set();
    }
}

/////
///  Close the connection
bool DatabaseClient::
    stop()
{
    bool retval = false;
    Guard p_guard(m_p_protect);
    if(m_is_connecting){
        m_connections_task->stop();
        m_is_connecting.clear();
        retval = true;
    }
    this->_inner_stop_p();
    return retval;
}

/////
///  Enforces a full stop on the task (blocks until it has fully stopped)  
void DatabaseClient::
    join()
{
    Guard p_guard(m_p_protect);
    m_connections_task->join();
    this->_inner_join_p();
}

/////
///  Returns TRUE if currently connected/trying to connect
bool DatabaseClient::
    is_live()
{
    return bool(m_is_connecting);
}

/////
///  Blocks until either a connection is established, or the connection task is stopped.
///  (Returns true for a good connection, false if the task is stopped)
bool DatabaseClient::
    wait_for_connection()
{
    SDT::MultiWait multi_wait;
    switch(
        multi_wait.block_many(
            m_is_connected.set_wait(), 
            m_is_connecting.clear_wait()
        )
    ){
        case 0:
            return true;
            break;

        case 1:
            return false;
            break;

        default:
            throw std::logic_error("SHOULD NEVER REACH");
            return false;
    }
}

/////
///  Returns client connection status
bool DatabaseClient::
    is_connected()
{
    return bool(m_is_connected);
}

/////
///  Callback function for a newly established connection
void DatabaseClient::
	connection_established(SDT::TaskManagerPtr transport_man)
{
	Guard p_guard(m_p_protect);

	if(not m_is_connected){
        /// Make sure any previously stopped connection is fully done
        this->_inner_join_p();

        /// Configure the SDT multiplexer
        SDT::DuplexDispatchFactory duplex;
		transport_man->task_factory_access(duplex);
		auto transport_duplex = duplex(
			m_inner->m_mplex_sndr.sender_dispatch(),
			m_inner->m_mplex_rcvr.receiver_dispatch()
		);
		m_transport_sending = transport_duplex.sender_task;
		m_transport_receiving = transport_duplex.receiver_task;
    
        /// Start receiving data from the server
        this->_inner_start_p();
	}else{
		throw SDT::fatal_error("Must stop and join previous connection");
	}
}

////
///  Starts receiving data from the server
void DatabaseClient::
	_inner_start_p()
{
	if(m_transport_sending){
        //SDT::ReceiverCompletionCallbackExt completion_ext;
        //m_transport_receiving->extension(completion_ext);
        //completion_ext(
        //    std::bind(
        //        &DatabaseClient::_inner_stop_p,
        //        this
        //    )
        //);
        m_transport_receiving->join_ready_event_handler_set(
                [this](){this->_inner_stop_p();}
        );
		m_transport_sending->start();
		m_transport_receiving->start();

        SDT::ReceiverDispatchWaitForReady ext;
        m_transport_receiving->extension(ext);
        m_is_connected.set();
	}else{
		throw SDT::fatal_error("Must connect first!");
	}
}

/////
///  
bool DatabaseClient::
    _inner_stop()
{
	Guard p_guard(m_p_protect);
    return this->_inner_stop_p();
}

/////
///
bool DatabaseClient::
    _inner_stop_p()
{
	if(m_is_connected){
		m_transport_sending->finalize();
		m_transport_receiving->finalize();
        m_is_connected.clear();
        // restart the connection attempts (they pause on a successful connection)
        if(m_is_connecting){
            m_connections_task->unpause();
        }
		return true;
	}
	return false;
}

/////
///
void DatabaseClient::
    _inner_join_p()
{
	if(m_transport_sending){
		m_transport_receiving->join();
		//The close here is necessary because
		//the receiver join only stops one half of the Duplex connection.
		//If the receiver is not actively sending data, it cannot tell if
		//the connection is done. This forces it closed.
		m_transport_sending->close();
		m_transport_sending->join();
        m_transport_sending = nullptr;
        m_transport_receiving = nullptr;
	}
}

/////
///  Sends database search request
std::vector<std::string> DatabaseClient::
    search_send(const std::string &query)
{
    m_rcv_ct = 0;
    m_search_results.clear();

    /// Send the search request to the server
    m_is_transferring.set();
    try{
        m_inner->m_sndr_data_search.async_call(query);
    }catch(const SDT::Disconnected &){
        this->_inner_stop();
        throw;
    }

    /// Block until the search completes, or a disconnect occurs
    /// (Asynchronous handlers will receive/compile the transmitted results)
    SDT::MultiWait multi_wait;
    switch(
        multi_wait.block_many(
            m_is_transferring.clear_wait(),
            m_is_connected.clear_wait()
        )
    ){
        case 0:
            logger.detail("Search complete. Total hits received:\t", m_rcv_ct);
            break;
            
        case 1:
            logger.error("Transfer failed! Connection was lost.");
            break;
            
        default:
            throw std::logic_error("SHOULD NEVER REACH");
    }
    return m_search_results;
}

/////
///  Initiates a data transfer of average over range
///  (Blocks until the transfer completes or a disconnect occurs)
std::shared_ptr<CSDCarrierDouble> DatabaseClient::
    transfer_averages_send(const std::string &query)
{
    m_rcv_ct = 0;
    m_local_csd.reset();    

    /// Send the data transfer request to the server
    m_is_transferring.set();
    try{
        m_inner->m_sndr_preaccum_data_transfer.async_call(query);
    }catch(const SDT::Disconnected &){
        this->_inner_stop();
        throw;
    }
    
    /// Block until the transfer completes, or a disconnect occurs
    /// (Asynchronous handlers will receive the transmitted frames)
    SDT::MultiWait multi_wait;
    switch(
        multi_wait.block_many(
            m_is_transferring.clear_wait(),
            m_is_connected.clear_wait()
        )
    ){
        case 0:
            logger.detail("Transfer complete. Total frames received:\t", m_rcv_ct);
            break;

        case 1:
            logger.error("Transfer failed! Connection was lost.");
            break;

        default:
            throw std::logic_error("SHOULD NEVER REACH");
    }
    return m_local_csd;
}


/////
///  Initiates a data transfer of all in-range frames
///  (Blocks until the transfer completes or a disconnect occurs)
void DatabaseClient::
    transfer_to_disk_send(
        const std::string &query,
        const std::string &local_path,
        bool write_all
    )    
{
#if JSTREAM_HAS_HDF5   
    m_rcv_ct = 0;
    m_local_path = local_path;

    /// Send the data transfer request to the server
    m_is_transferring.set();
    try{
        if(write_all){
            /// Request all in-range frames (received in single-precision)
            m_inner->m_sndr_sequential_data_transfer.async_call(query);
        }else{
            /// Request average frame over range (received in double-precision)
            m_inner->m_sndr_preaccum_data_transfer.async_call(query);
        }
    }catch(const SDT::Disconnected &){
        this->_inner_stop();
        throw;
    }
    
    /// Block until the transfer completes, or a disconnect occurs
    /// (Asynchronous handlers will receive the transmitted frames)
    SDT::MultiWait multi_wait;
    switch(
        multi_wait.block_many(
            m_is_transferring.clear_wait(),
            m_is_connected.clear_wait()
        )
    ){
        case 0:
            logger.detail("Transfer complete. Total frames received:\t", m_rcv_ct);
            break;

        case 1:
            logger.error("Transfer failed! Connection was lost.");
            break;

        default:
            throw std::logic_error("SHOULD NEVER REACH");
    }
    
    /// Close new HDF5 file and reset state
    m_hdf5_writer_flt.reset();
    m_hdf5_writer_dbl.reset();
    m_source_metadata.clear();
    m_local_path.clear();
#else
    logger.error("HDF5 writer is unavailable on this system");
#endif //~JSTREAM_HAS_HDF5
}

/////
///  Receives & stores transferred database search hit
void DatabaseClient::
    search_hit_rcv(const std::string hit)
{
    Guard p_guard(m_protect);    
    m_search_results.push_back(hit);
    m_rcv_ct += 1;
}

/////
///  Receives & stores transferred data source metadata
void DatabaseClient::
    metadata_rcv(const IDataSource::DataSourceSettings settings)
{
    Guard p_guard(m_protect);
    m_source_metadata.push_back(settings);
}

/////
///  Receives a transferred CSD frame from the HDF5 archive (single-precision)
///  (Writes to local disk)
void DatabaseClient::
    csd_float_rcv(const CSDCarrierFloat csd)
{
    Guard p_guard(m_protect);
    if(not m_local_path.empty()){
        this->csd_write(csd);
    }
    m_rcv_ct += 1;    
}

/////
///  Receives a transferred CSD frame from the HDF5 archive (double-precision)
///  (Copies to internal container and writes to local disk)
void DatabaseClient::
    csd_double_rcv(const CSDCarrierDouble csd)
{
    Guard p_guard(m_protect);
    m_local_csd = std::make_shared<CSDCarrierDouble>(csd);
    if(not m_local_path.empty()){
        this->csd_write(csd);
    }
    m_rcv_ct = 1;
}

/////
///  Writes a received CSD frame to local disk (single-precision)
void DatabaseClient::
    csd_write(const CSDCarrierFloat csd)
{
#if JSTREAM_HAS_HDF5
    if(not m_hdf5_writer_flt){
        /// Initialize the HDF5 writer
        m_hdf5_writer_flt = std::make_shared< HDF5CSDWriter<CSDCarrierFloat> >(
            m_source_metadata,
            csd,
            m_local_path,
            logger
        );
        
        /// Create the local HDF5 file
        m_hdf5_writer_flt->new_file(csd.timestamp_begin());
    }
    
    /// Write the CSD frame to disk    
    m_hdf5_writer_flt->write_csd(csd);
#endif //~JSTREAM_HAS_HDF5    
}

/////
///  Writes a received CSD frame to local disk (double-precision)
void DatabaseClient::
    csd_write(const CSDCarrierDouble csd)
{
#if JSTREAM_HAS_HDF5
    if(not m_hdf5_writer_dbl){
        /// Initialize the HDF5 writer
        m_hdf5_writer_dbl = std::make_shared< HDF5CSDWriter<CSDCarrierDouble> >(
            m_source_metadata,
            csd,
            m_local_path,
            logger
        );
        
        /// Create the local HDF5 file
        m_hdf5_writer_dbl->new_file(csd.timestamp_begin());
    }
    
    /// Write the CSD frame to disk    
    m_hdf5_writer_dbl->write_csd(csd);
#endif //~JSTREAM_HAS_HDF5    
}

/////
///  Receives the "transfer complete" signal from the server
void DatabaseClient::
    transfer_complete_rcv(int signal)
{
    m_is_transferring.clear();
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
