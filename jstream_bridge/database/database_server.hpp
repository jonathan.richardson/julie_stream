////
///

#ifndef H_JSTREAM_DATABASE_SERVER_H
#define H_JSTREAM_DATABASE_SERVER_H

#include "jstream/config.hpp"
#if JSTREAM_HAS_HDF5

#include <thread>
#include <cstdint>
#include <memory>

#include <boost/asio.hpp>

#include <sdt/transport/asio/task_asio.hpp>
#include <sdt/transport/task_connections.hpp>
#include <sdt/transport/multi_acceptor.hpp>
#include <sdt/queues/notify_queue.hpp>
#include <sdt/config.hpp>
#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>

#include "hardware_locality/localization_manager.hpp"

#include "data_sources/data_source_base.hpp"

#include "analysis/csd_carrier_base.hpp"

#include "inner_server_database.hpp"

namespace JStream {
using SDT::LoggerAccess;

//////
///  Compiler firewall as this guy has heavy machinery that shouldn't slow down compiles 
///  (internally holds all of the SDT stream classes)
namespace _detail {
    class _DatabaseServer;
}; //~namespace _detail


/////
///  The database server class
class DatabaseServer
{
    friend class DatabaseClient;

public:
    /// Receiver callback typedefs
    typedef std::function<void(
        const std::string &, 
        DatabaseManager::SearchHitTransmitter,
        DatabaseManager::TransmissionTerminator
    )>  DBSearchHandler;    
    
    typedef std::function<void(
        const std::string &, 
        DatabaseManager::MetadataTransmitter,
        DatabaseManager::CSDFloatTransmitter,
        DatabaseManager::TransmissionTerminator
    )>  DBSequentialTransferHandler;
    
    typedef std::function<void(
        const std::string &,
        DatabaseManager::MetadataTransmitter,
        DatabaseManager::CSDDoubleTransmitter,
        DatabaseManager::TransmissionTerminator
    )>  DBPreaccumTransferHandler;    
                            
protected:
    /// Convenience typedefs (for internal use)
    typedef std::mutex   
        Mutex;
    typedef std::unique_lock<Mutex>
        Guard;
    typedef std::function<void(const std::string &)>           
        DatabaseQueryHandler;           

    /// Protects the server state
    Mutex                                   m_p_protect;

    /// Logger
    SDT::LoggerAccess                       m_logger;

    /// Used by SDT internal transceiver
    std::shared_ptr<_detail::_DatabaseServer>    
                                            m_inner;               //internal data transmitter
    SDT::TaskTransportPtr                   m_transport_sending;   //internal send task list
    SDT::TaskTransportPtr                   m_transport_receiving; //internal receive task list

    /// Receiver callback function wrappers
    DatabaseQueryHandler                    m_callback_search;
    DatabaseQueryHandler                    m_callback_sequential_transfer;
    DatabaseQueryHandler                    m_callback_preaccum_transfer;

public:
    DatabaseServer(SDT::LoggerAccess logger);

    /// Server controls
    void connect(SDT::TaskManagerPtr manager);
	void start();
    bool stop();
    void join();
    bool is_live();

    /// Set the receive callbacks
    void callback_search_set(DBSearchHandler);
    void callback_sequential_transfer_set(DBSequentialTransferHandler);
    void callback_preaccum_transfer_set(DBPreaccumTransferHandler);

    /// Transmit data to remote client
    bool search_hit_send(const std::string &hit);
    bool metadata_send(const IDataSource::DataSourceSettings &);
    bool csd_float_send(const CSDCarrierFloat &);   
    bool csd_double_send(const CSDCarrierDouble &);     
    bool complete_send(int);    

protected:
    /// Event handlers
    void _search_request_rcv(const std::string &);     
    void _sequential_transfer_request_rcv(const std::string &);   
    void _preaccum_transfer_request_rcv(const std::string &);     

};

}; //~namespace JStream

#endif //~JSTREAM_HAS_HDF5

#endif //H_JSTREAM_DATABASE_SERVER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
