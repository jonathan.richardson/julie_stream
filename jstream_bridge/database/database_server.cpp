/////
///

#include <sdt/utilities/config_tree/config_tree_json.hpp>

#include "database_server.hpp"

namespace JStream {
using namespace ConfigTree;


/////
///  Data server constructor
DatabaseServer::
	DatabaseServer(SDT::LoggerAccess logger) :
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);

    /// Set up the inner receiver forwarding
    m_inner = std::make_shared<_detail::_DatabaseServer>();       
    m_inner->m_rcvr_data_search.handler_set(  
        [this](const std::string &query){
            return this->_search_request_rcv(query);
        }
    );    
    m_inner->m_rcvr_sequential_data_transfer.handler_set(
        [this](const std::string &query){
            return this->_sequential_transfer_request_rcv(query);
        }
    );
    m_inner->m_rcvr_preaccum_data_transfer.handler_set(
        [this](const std::string &query){
            return this->_preaccum_transfer_request_rcv(query);
        }
    );    
}

/////
///  Connect the data server to the client
void DatabaseServer::
	connect(SDT::TaskManagerPtr transport_man)
{
	Guard p_guard(m_p_protect);

	if(not m_transport_sending){
        /// Set up the send and receive task lists for the server
        /// (Does not start the tasks yet)
        SDT::DuplexDispatchFactory duplex;
		transport_man->task_factory_access(duplex);
		auto transport_duplex = duplex(
			m_inner->m_mplex_sndr.sender_dispatch(),
			m_inner->m_mplex_rcvr.receiver_dispatch()
		);
		m_transport_sending = transport_duplex.sender_task;
		m_transport_receiving = transport_duplex.receiver_task;
	}else{
		throw SDT::fatal_error("Must stop and join the previous connection!");
	}
}

/////
///  Starts the data transmission to a client
void DatabaseServer::
	start()
{
	Guard p_guard(m_p_protect);

	if(not m_transport_receiving->is_live()){
        /// Start the send and receive tasks
		m_transport_sending->start();
		m_transport_receiving->start();
        
        /// Force a wait to make sure that it is fully connected before continuing
        SDT::ReceiverDispatchWaitForReady ext;
        m_transport_receiving->extension(ext);
	}else{
		throw SDT::fatal_error("Must connect first!");
	}
}

/////
///  Returns TRUE if the server is currently running (connected to a client)
bool DatabaseServer::
    is_live()
{
	Guard p_guard(m_p_protect);
    
	if(m_transport_receiving){
		return m_transport_receiving->is_live();
	}
	return false;
}

/////
///  Shut down the server operations
///  (Returns TRUE if successful, FALSE if server was already shut down)
bool DatabaseServer::
    stop()
{
	Guard p_guard(m_p_protect);
    
	if(m_transport_receiving->is_live()){
        /// Stop sending and receiving data
		m_transport_sending->finalize();
		m_transport_receiving->finalize();
		return true;
	}
	return false;
}

/////
///  Terminates the connection (ends the transmission thread)
void DatabaseServer::
    join()
{
    SDT::TaskTransportPtr sender;
    SDT::TaskTransportPtr receiver;
	{
		Guard p_guard(m_p_protect);
		sender = m_transport_sending;
		receiver = m_transport_receiving;
	}

	if(sender){
		receiver->join();
		/// The close here is necessary because the receiver join only stops one half 
        /// of the Duplex connection. If the receiver is not actively sending data, 
        /// it cannot tell if the connection is done. This forces it closed.
		sender->close();
		sender->join();
		{
			Guard p_guard(m_p_protect);
			m_transport_sending   = nullptr;
			m_transport_receiving = nullptr;
		}
	}
}

/////
///  Sets the function the server will call when a database search request is received
void DatabaseServer::
    callback_search_set(DBSearchHandler callback)
{
    /// Transmission functors to pass to the database manager
    auto transmitter = 
        [this](const std::string &hit){
            return this->search_hit_send(hit);
        };
    auto terminator = 
        [this](int signal_code){
            return this->complete_send(signal_code);
        };      

    /// Bind the functors into a simplified internal callback
    m_callback_search = 
        [callback, transmitter, terminator](
            const std::string &query
        ){
            return callback(
                query, 
                transmitter, 
                terminator
            );
        };  
}

/////
///  Forwards a database search request to the supplied callback
void DatabaseServer::
    _search_request_rcv(const std::string &query)
{
    if(m_callback_search){
        m_callback_search(query);
    }
}

/////
///  Server callback for sequential data transfer request
void DatabaseServer::
    callback_sequential_transfer_set(DBSequentialTransferHandler callback)
{
    /// Transmission functors to pass to the database manager
    auto meta_transmitter = [this](const IDataSource::DataSourceSettings &metadata){
        return this->metadata_send(metadata);
    };
    auto csd_transmitter = [this](const CSDCarrierFloat &csd){
        return this->csd_float_send(csd);
    };    
    auto terminator = [this](int signal_code){
        return this->complete_send(signal_code);
    };

    /// Bind the functors into a simplified internal callback
    m_callback_sequential_transfer = 
        [callback, meta_transmitter, csd_transmitter, terminator](
            const std::string &query
        ){
            return callback(
                query, 
                meta_transmitter, 
                csd_transmitter, 
                terminator
            );
        };
}

/////
///  Forwards a sequential data transfer request to the supplied callback
void DatabaseServer::
    _sequential_transfer_request_rcv(const std::string &query)
{
    if(m_callback_sequential_transfer){
        m_callback_sequential_transfer(query);
    }
}

/////
///  Server callback for pre-accumulated data transfer request
void DatabaseServer::
    callback_preaccum_transfer_set(DBPreaccumTransferHandler callback)
{
    /// Transmission functors to pass to the database manager
    auto meta_transmitter = [this](const IDataSource::DataSourceSettings &metadata){
        return this->metadata_send(metadata);
    };    
    auto csd_transmitter = [this](const CSDCarrierDouble &csd){
        return this->csd_double_send(csd);
    };    
    auto terminator = [this](int signal_code){
        return this->complete_send(signal_code);
    };

    /// Bind the functors into a simplified internal callback
    m_callback_preaccum_transfer = 
        [callback, meta_transmitter, csd_transmitter, terminator](
            const std::string &query
        ){
            return callback(
                query, 
                meta_transmitter, 
                csd_transmitter, 
                terminator
            );
        };    
}

/////
///  Forwards a preaccumulated data transfer request to the supplied callback
void DatabaseServer::
    _preaccum_transfer_request_rcv(const std::string &query)
{
    if(m_callback_preaccum_transfer){
        m_callback_preaccum_transfer(query);
    }
}

/////
///  Receives a single-precision CSD frame from the HDF5 database & transmits to remote client
///  Returns TRUE if transfer actually occured, FALSE otherwise
bool DatabaseServer::
    csd_float_send(const CSDCarrierFloat &csd)
{
    try{
        m_inner->m_sndr_csd_float.object_send_throw(csd);
        m_logger.detail("Sending CSD frame #", csd.sequence_number_get());
        return true;
    }catch(const SDT::Disconnected &){
        this->stop();
        throw;
    }
    return false;
}

/////
///  Receives a double-precision CSD frame from the HDF5 database & transmits to remote client
///  Returns TRUE if transfer actually occured, FALSE otherwise
bool DatabaseServer::
    csd_double_send(const CSDCarrierDouble &csd)
{
    try{
        m_inner->m_sndr_csd_double.object_send_throw(csd);
        m_logger.detail("Sending CSD frame #", csd.sequence_number_get());
        return true;
    }catch(const SDT::Disconnected &){
        this->stop();
        throw;
    }
    return false;
}

/////
///  Receives hardware metadata from the HDF5 database & transmits to remote client
///  Returns TRUE if transfer actually occured, FALSE otherwise
bool DatabaseServer::
    metadata_send(const IDataSource::DataSourceSettings &settings)
{
    try{
        m_inner->m_sndr_metadata.async_call(settings);
        m_logger.detail("Sending hardware metadata");
        return true;
    }catch(const SDT::Disconnected &){
        this->stop();
        throw;
    }
    return false;
}

/////
///  Receives a search hit from the HDF5 database & transmits to remote client
///  Returns TRUE if transfer actually occured, FALSE otherwise
bool DatabaseServer::
    search_hit_send(const std::string &hit)
{
    try{
        m_inner->m_sndr_search_hit.async_call(hit);
        m_logger.detail("Sending search hit");
        return true;
    }catch(const SDT::Disconnected &){
        this->stop();
        throw;
    }
    return false;
}

/////
///  Sends the "transfer complete" signal
///  (Returns TRUE if transfer actually occured, FALSE otherwise)
bool DatabaseServer::
    complete_send(int signal_code)
{ 
    try{
        m_inner->m_sndr_transfer_complete.async_call(signal_code);
        return true;
    }catch(const SDT::Disconnected &){
        this->stop();
        throw;
    }
    return false;
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
