/////
///

#ifndef H_DATABASE_INNER_SERVER
#define H_DATABASE_INNER_SERVER

#include "jstream/config.hpp"
#if JSTREAM_HAS_HDF5

#include <sdt/streams/multiplexer/sender.hpp>
#include <sdt/streams/multiplexer/receiver.hpp>
#include <sdt/streams/objects/integers.hpp>
#include <sdt/streams/objects/strings.hpp>
#include <sdt/streams/rpc/caller.hpp>
#include <sdt/streams/rpc/handler.hpp>
#include <sdt/streams/task_transport.hpp>

#include "data_management/database_manager.hpp"

#include "jstream_bridge/shuttles/csd_shuttle.hpp"
#include "jstream_bridge/shuttles/metadata_shuttle.hpp"

namespace JStream {

namespace _detail {


//////
///  Inner server type which handles the protocol itself
struct _DatabaseServer
{
    friend class _DatabaseClient;

    /// Network stream multiplexer
    SDT::StreamMultiplexerSender         
		m_mplex_sndr;        
    SDT::StreamMultiplexerReceiver       
		m_mplex_rcvr;

    /// Data stream sender ends        
    SDT::StreamRPCHandler<void, SDT::StringShuttle<> >::TCallerPair
        m_sndr_search_hit;        
    SDT::StreamRPCHandler<void, MetadataShuttle>::TCallerPair
        m_sndr_metadata;   
    SDT::ObjectSender<CSDShuttle<CSDCarrierFloat> > 
        m_sndr_csd_float;
    SDT::ObjectSender<CSDShuttle<CSDCarrierDouble> > 
        m_sndr_csd_double;
    SDT::StreamRPCHandler<void, SDT::VarintShuttle<int> >::TCallerPair
        m_sndr_transfer_complete;           
        
    /// Data stream receiver ends         
    SDT::StreamRPCHandler<void, SDT::StringShuttle<> > 
        m_rcvr_data_search;
    SDT::StreamRPCHandler<void, SDT::StringShuttle<> > 
        m_rcvr_sequential_data_transfer;     
    SDT::StreamRPCHandler<void, SDT::StringShuttle<> > 
        m_rcvr_preaccum_data_transfer;          

    ////
    ///  Template used to conveniently add all of the heterogeneous RPC types into the multiplexer
    template<typename TRPC>
    inline void add_to_multiplexer_pair(std::string name, TRPC &comm_attribute)
	{
		m_mplex_sndr.substreamer_add(name, comm_attribute.sender_dispatch());
		m_mplex_rcvr.substreamer_add(name, comm_attribute.receiver_dispatch());
	}

    /////
    ///  Constructor
	_DatabaseServer()
	{
        /// Add the CSD stream sender end
        m_mplex_sndr.substreamer_add(
            "csd_float",  
            m_sndr_csd_float.sender_dispatch()
        );
        m_mplex_sndr.substreamer_add(
            "csd_double", 
            m_sndr_csd_double.sender_dispatch()
        );
        
        /// Add the multiplexer pairs
        this->add_to_multiplexer_pair(
            "data_search",                
            m_rcvr_data_search
        ); 
        this->add_to_multiplexer_pair(
            "sequential_data_transfer",   
            m_rcvr_sequential_data_transfer
        ); 
        this->add_to_multiplexer_pair(
            "preaccum_data_transfer",     
            m_rcvr_preaccum_data_transfer
        );         
        this->add_to_multiplexer_pair(
            "search_hit",                 
            m_sndr_search_hit
        );    
        this->add_to_multiplexer_pair(
            "csd_metadata",               
            m_sndr_metadata
        );        
        this->add_to_multiplexer_pair(
            "transfer_complete",          
            m_sndr_transfer_complete
        );      
	}
    
};

}; //~namespace _detail

}; //~namespace JStream

#endif //~JSTREAM_HAS_HDF5

#endif //H_DATABASE_INNER_SERVER
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :