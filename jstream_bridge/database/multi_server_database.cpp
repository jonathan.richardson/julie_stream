/////
///

#include "multi_server_database.hpp"

namespace JStream {


/////
///  Constructor
DatabaseMultiServer::
    DatabaseMultiServer(SDT::LoggerAccess logger) :
        m_logger(logger)
{
    /// Set the callback for a new client connection
    this->event_connection_create_set(
        /// Launches a new subserver
        [this](){
            return this->_make_server();
        }
    );
}

/////
///  Sets the callback for receiving a database search request
void DatabaseMultiServer::
    callback_search_set(TServer::DBSearchHandler callback)
{
	m_callback_search = callback;
}

/////
///  Sets the callback for receiving a data transfer request
void DatabaseMultiServer::
    callback_sequential_transfer_set(TServer::DBSequentialTransferHandler callback)
{
	m_callback_sequential_transfer = callback;
}

/////
///  Sets the callback for receiving a data transfer request
void DatabaseMultiServer::
    callback_preaccum_transfer_set(TServer::DBPreaccumTransferHandler callback)
{
	m_callback_preaccum_transfer = callback;
}

/////
///  Creates a new CSD server
DatabaseMultiServer::TServerManagedPtr DatabaseMultiServer::
    _make_server()
{
    /// Create the data server
    auto server = std::make_shared<TServerManaged>(m_logger);
    
    /// Set the server callbacks
    server->callback_search_set(m_callback_search);
    server->callback_sequential_transfer_set(m_callback_sequential_transfer);
    server->callback_preaccum_transfer_set(m_callback_preaccum_transfer);
    return server;
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
