/////
///

#ifndef H_DATABASE_CLIENT_INNER_H
#define H_DATABASE_CLIENT_INNER_H

#include "jstream/config.hpp"
#if JSTREAM_HAS_HDF5

#include <sdt/streams/multiplexer/sender.hpp>
#include <sdt/streams/multiplexer/receiver.hpp>
#include <sdt/streams/objects/integers.hpp>
#include <sdt/streams/objects/strings.hpp>
#include <sdt/streams/rpc/caller.hpp>
#include <sdt/streams/rpc/handler.hpp>
#include <sdt/streams/task_transport.hpp>

#include "inner_server_database.hpp"

namespace JStream {

namespace _detail {

//////
///  Inner client type which handles the protocol itself
struct _DatabaseClient
{
    /// Network stream multiplexer
    SDT::StreamMultiplexerSender	 
		m_mplex_sndr;
    SDT::StreamMultiplexerReceiver 
		m_mplex_rcvr;

    /// Data stream receiver ends
    SDT::StreamRPCHandler<void, SDT::StringShuttle<> >
        m_rcvr_search_hit;
    SDT::StreamRPCHandler<void, MetadataShuttle>
        m_rcvr_metadata;
    decltype(_detail::_DatabaseServer::m_sndr_csd_float)::TReceiver
        m_rcvr_csd_float;  
    decltype(_detail::_DatabaseServer::m_sndr_csd_double)::TReceiver
        m_rcvr_csd_double;           
    SDT::StreamRPCHandler<void, SDT::VarintShuttle<int> >
        m_rcvr_transfer_complete;        
        
    /// Data stream sender ends
    decltype(_detail::_DatabaseServer::m_rcvr_data_search)::TCallerPair
        m_sndr_data_search;
    decltype(_detail::_DatabaseServer::m_rcvr_sequential_data_transfer)::TCallerPair
        m_sndr_sequential_data_transfer;
    decltype(_detail::_DatabaseServer::m_rcvr_preaccum_data_transfer)::TCallerPair
        m_sndr_preaccum_data_transfer;         

    ////
    ///  Template used to conveniently add all of the heterogeneous RPC types into the multiplexer
    template<typename TRPC>
    inline void add_to_multiplexer_pair(std::string name, TRPC &comm_attribute)
	{
		m_mplex_sndr.substreamer_add(name, comm_attribute.sender_dispatch());
		m_mplex_rcvr.substreamer_add(name, comm_attribute.receiver_dispatch());
	}

    /////
    ///  Constructor
	_DatabaseClient()
	{
        /// Add the CSD stream receiver ends
        m_mplex_rcvr.substreamer_add(
            "csd_float",
            m_rcvr_csd_float.receiver_dispatch()
        );    
        m_mplex_rcvr.substreamer_add(
            "csd_double",
            m_rcvr_csd_double.receiver_dispatch()
        );
        
        /// Add the multiplexer pairs
        this->add_to_multiplexer_pair(
            "data_search",
            m_sndr_data_search
        ); 
        this->add_to_multiplexer_pair(
            "sequential_data_transfer",
            m_sndr_sequential_data_transfer
        ); 
        this->add_to_multiplexer_pair(
            "preaccum_data_transfer",
            m_sndr_preaccum_data_transfer
        ); 
        this->add_to_multiplexer_pair(
            "search_hit",
            m_rcvr_search_hit
        ); 
        this->add_to_multiplexer_pair(
            "csd_metadata",
            m_rcvr_metadata
        );  
        this->add_to_multiplexer_pair(
            "transfer_complete",
            m_rcvr_transfer_complete
        ); 
	}
};

}; //~namespace _detail

}; //~namespace JStream

#endif //~JSTREAM_HAS_HDF5

#endif //H_DATABASE_CLIENT_INNER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :