///
////

#ifndef H_DATABASE_MULTI_SERVER_H
#define H_DATABASE_MULTI_SERVER_H

#include "jstream/config.hpp"
#if JSTREAM_HAS_HDF5

#include <sdt/transport/generic_multi_server.hpp>

#include "database_server.hpp"

namespace JStream {


/////
///  Database multi-server class
class DatabaseMultiServer :
    public SDT::MultiServerGeneric< SDT::ServerManagedWrapper<DatabaseServer> >
{
    /// Convenience typedefs (for internal use)
    typedef DatabaseServer
        TServer;
    typedef SDT::ServerManagedWrapper<TServer>  
        TServerManaged;
    typedef std::shared_ptr<TServerManaged>     
        TServerManagedPtr; 

    /// Logger
    SDT::LoggerAccess                       m_logger;
        
    /// Wrappers for the server callbacks
    TServer::DBSearchHandler                m_callback_search;
    TServer::DBSequentialTransferHandler    m_callback_sequential_transfer;
    TServer::DBPreaccumTransferHandler      m_callback_preaccum_transfer;       

public:
    /// Constructor
    DatabaseMultiServer(SDT::LoggerAccess logger);
    
    /// Methods to set the receiver callbacks
    void callback_search_set(TServer::DBSearchHandler);
    void callback_sequential_transfer_set(TServer::DBSequentialTransferHandler);
    void callback_preaccum_transfer_set(TServer::DBPreaccumTransferHandler);

protected:
    /// Creates a new CSD server
    TServerManagedPtr _make_server();

};

}; //~namespace JStream

#endif //~JSTREAM_HAS_HDF5

#endif //H_DATABASE_MULTI_SERVER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
