////
///

#ifndef H_JSTREAM_REPEATER_H
#define H_JSTREAM_REPEATER_H

#include <boost/thread.hpp>

#include <thread>
#include <condition_variable>

#include <sdt/utilities/logger.hpp>
#include <sdt/transport/generic_multi_server.hpp>

#include "jstream_bridge/realtime/multi_server_realtime.hpp"
#include "jstream_bridge/realtime/realtime_client.hpp"

namespace JStream {
using SDT::LoggerAccess;


/////
///  Network relayer class
class JStreamNetworkRepeater
{
    /// Convenience typedefs
	typedef std::mutex              
        Mutex;
	typedef std::lock_guard<Mutex>  
        Guard;
    typedef std::unique_lock<Mutex> 
        Lock;
    typedef std::condition_variable 
        CondVar;

	/// Networking state
	Mutex 	                    m_protect;                  //Protects state changes 
	bool	                    m_comm_state    = false;    //True if networking enabled
    CondVar                     m_signal_shutdown;          //Signals shutdown
    
    /// Logger
    LoggerAccess                m_logger;    

    /// Connections to DAQ server
    std::string                 m_address       = "tcp://julie2.fnal.gov:8701";  
    RealtimeClient              m_client;
    
    /// Connections to DAQ GUI clients
	unsigned	                m_service_port  = 8700;
    RealtimeMultiServer         m_server;

public:
    /// Constructor
	JStreamNetworkRepeater(LoggerAccess logger);
    
    /// Toggles networking state
	void comm_state_set(bool on_off);

    /// Blocks main thread of execution, launches keyboard input handler
    void wait_for_shutdown(); 

protected:
    ///  Configure the server/client modules
    void _networking_setup();

    /// Keyboard input handler (blocking)
    void _key_handler();    

};

}; //~namespace JStream

#endif //H_JSTREAM_REPEATER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :