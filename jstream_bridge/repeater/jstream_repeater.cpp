////
///

#include "jstream_repeater.hpp"

namespace JStream {


/////
///  DAQ repeater constructor
JStreamNetworkRepeater::
	JStreamNetworkRepeater(LoggerAccess logger) :
        m_logger(logger),
        m_client(m_address),
        m_server(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);
    
    /// Configure the network modules
    this->_networking_setup();   
}

/////
///  Configure the server/client modules
void JStreamNetworkRepeater::
	_networking_setup()
{
    /// Set the ports on which the servers will listen for clients
    std::string address = "tcp://*:" + std::to_string(m_service_port);
    m_server.accept_address_add(address);
    
    /// Client(s) --> DAQ server forwarding
    m_server.callback_run_state_set(
        [this](bool state){
            return m_client.run_state_send(state);
        }
    );
    m_server.callback_run_config_set(
        [this](const std::string &config){
            return m_client.run_settings_send(config);
        }
    );

    /// DAQ server --> Client(s) forwarding
    m_client.callback_msg_set(
        [this](const std::string &msg){
            return m_server.broadcaster().broadcast(msg);
        }
    );    
    m_client.callback_csd_set(
        [this](const CSDCarrierDouble &csd){
            return m_server.broadcaster().broadcast(csd);
        }
    );
}

/////
///  Enable/disable TCP networking
void JStreamNetworkRepeater::
    comm_state_set(bool on_off)
{
    Guard guard(m_protect);
    
    /// If repeater is already in the requested state, 
    /// return immediately
    if(m_comm_state == on_off){
        return;
    }

    /// Change the state
    m_comm_state = on_off;
    if(m_comm_state){
        /// Connect to DAQ realtime server 
        /// (Block until a connection is established)
        m_logger.action("Connecting to DAQ-realtime...");
        m_client.start();
        m_client.wait_for_connection();

        /// Start accepting GUI client connections
        m_server.connections_begin();
        m_logger.action("Ready for client connections");          
    }else{
        /// Shut down all connections
        m_logger.action("Stopping all connections...");
        m_client.stop();
        m_server.connections_end();
    }
}

/////
///  Does not return until shutdown is signaled (manually keyed)
void JStreamNetworkRepeater::
    wait_for_shutdown()
{
    Mutex   local_mutex;
    Lock    lock(local_mutex);

    /// Launch a thread to accept keyboard input from the local terminal
    boost::thread key_handler(
        &JStreamNetworkRepeater::_key_handler, 
        this
    );                       
    
    /// Block until system shutdown flag is set
    m_signal_shutdown.wait(
        lock, 
        [this](){
            return not m_comm_state;
        }
    );
}

/////
///  Accepts/handles keyboard input from the local terminal
void JStreamNetworkRepeater::
    _key_handler()
{
    std::string command;
    
    /// Wait for a key command to arrive
    std::cout << "\n\nEnter key option:\n";
    std::cin >> command;
    
    /// Handle the command
    if(command == "run"){
        /// Enable networking
        this->comm_state_set(true);
    }else if(command == "suspend"){
        /// Suspend networking
        this->comm_state_set(false);
    }else if(command == "shutdown"){
        /// Signal system-wide shutdown and quit
        std::cout << "\nDAQ repeater shutting down...\n\n";
        this->comm_state_set(false);
        m_signal_shutdown.notify_all();
        return;
    }else{
        std::cout << "\nUnrecognized key option: " << command << "\n" <<
            "\tValid options:\n" <<
            "\t run\n" <<
            "\t suspend\n" <<
            "\t shutdown\n";
    }
    
    /// Continue waiting for user input
    _key_handler();
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :