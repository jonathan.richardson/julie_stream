///
////

#ifndef H_REALTIME_MULTI_SERVER_H
#define H_REALTIME_MULTI_SERVER_H

#include <sdt/transport/generic_multi_server.hpp>

#include "realtime_server.hpp"

namespace JStream {


/////
///  CSD multi-server class
class RealtimeMultiServer :
    public SDT::MultiServerGeneric< SDT::ServerManagedWrapper<RealtimeServer> >
{
    /// Convenience typedefs (for internal use)
    typedef RealtimeServer
        TServer;
    typedef SDT::ServerManagedWrapper<TServer>  
        TServerManaged;
    typedef std::shared_ptr<TServerManaged>     
        TServerManagedPtr;
       
    /// Logger
    SDT::LoggerAccess               m_logger;
        
    /// Client --> Server callbacks
    TServer::RunStateHandler        m_callback_ifo_lock_state;
    TServer::RunStateHandler        m_callback_run_state;
    TServer::RunStateQueryHandler   m_callback_run_state_query;
    TServer::RunConfigHandler       m_callback_run_config; 
    
    /// Server --> Client(s) broadcasters
    TServer::TMsgBroadcaster        m_msg_broadcaster;
    TServer::TCSDBroadcaster        m_csd_broadcaster;
    TServer::TRunStateBroadcaster   m_run_state_broadcaster;
    
public:
    /// Constructor
    RealtimeMultiServer(SDT::LoggerAccess logger);

    /// Methods to set the receiver callbacks
    void callback_ifo_lock_state_set(TServer::RunStateHandler);
    void callback_run_state_set(TServer::RunStateHandler);
    void callback_run_state_query_set(TServer::RunStateQueryHandler);
    void callback_run_config_set(TServer::RunConfigHandler); 
    
    /// Returns a reference to the live data broadcasters
    TServer::TMsgBroadcaster& msg_broadcaster();
    TServer::TCSDBroadcaster& csd_broadcaster();
    TServer::TRunStateBroadcaster& run_state_broadcaster();

protected:
    /// Creates a new CSD server
    TServerManagedPtr make_server();
    
};

}; //~namespace JStream

#endif //H_REALTIME_MULTI_SERVER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
