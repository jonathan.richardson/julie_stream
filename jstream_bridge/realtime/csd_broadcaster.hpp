////
///

#ifndef H_REALTIME_CSD_BROADCASTER_H
#define H_REALTIME_CSD_BROADCASTER_H

#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>
#include <sdt/utilities/keyed_signal.hpp>

#include "analysis/csd_carrier_double.hpp"

namespace JStream {


/////
///  Broadcasts data updates to all registered realtime servers
template<typename TServer>
class RealtimeCSDBroadcaster
{
public:
    typedef SDT::KeyedSignal<void(const CSDCarrierDouble &)> 
        TCSDBroadcaster;  
    typedef SDT::KeyedSignal<void()> 
        TResetter;

protected:
    TCSDBroadcaster         m_broadcaster;
    TResetter               m_resetter;
    
public:
    /// Add/remove server to/from broadcast list
    void subscribe(TServer *server);
    void unsubscribe(TServer *server);
    
    /// Broadcast over the specified data stream
    void broadcast(const CSDCarrierDouble &);
    
    /// Signal all the servers to reset their internal CSD queues
    void reset();
};

}; //~namespace JStream

#include "csd_broadcaster_T.hpp"
#endif //H_REALTIME_CSD_BROADCASTER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
