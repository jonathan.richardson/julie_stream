////
///

#ifndef H_REALTIME_MSG_BROADCASTER_H
#define H_REALTIME_MSG_BROADCASTER_H

#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>
#include <sdt/utilities/keyed_signal.hpp>

namespace JStream {


/////
///  Broadcasts logger messages to all registered realtime servers
template<typename TServer>
class RealtimeMsgBroadcaster
{
public:
    typedef SDT::KeyedSignal<void(const std::string &)> 
        TMsgBroadcaster;  

protected:
    TMsgBroadcaster     m_broadcaster;
    
public:
    /// Add/remove server to/from broadcast list
    void subscribe(TServer *server);
    void unsubscribe(TServer *server);
    
    /// Broadcast over the specified data stream
    void broadcast(const std::string &);
};

}; //~namespace JStream

#include "msg_broadcaster_T.hpp"
#endif //H_REALTIME_MSG_BROADCASTER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
