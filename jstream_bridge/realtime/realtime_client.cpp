////
///

#include "realtime_client.hpp"

namespace {
	uLogger::LoggerAccess logger = uLogger::logger_root().child("client");
}

namespace JStream {


/////
///  Real-time client subclass constructor
RealtimeClient::
    RealtimeClient(
        const std::string &connection_addr,
        bool subscribe
    ) :
        m_is_connecting(false),
        m_is_connected(false)
{
    /// Associate this class type with the logger context
    logger.this_typename_set(this);
    
    /// Create the SDT connections task manager (uses boost::asio)
    /// and set the callback for a newly-established connection
	auto asio_manager = std::make_shared<SDT::AsioTaskManager>();
    SDT::TaskConnectFactory connect_factory;
	asio_manager->task_factory_access(connect_factory);
    m_connections_task = connect_factory(
        connection_addr, 
        SDT::TransportActorGenerator(
            [this, subscribe](SDT::TaskManagerPtr manager){
                return this->_connection_established(manager, subscribe);
            }
        )
    );
            
    /// Create the inner SDT transceiver & set the forwarding callbacks
	m_inner = std::make_shared<_detail::_RealtimeClient>();
    m_inner->m_rcvr_logger_msg.handler_set(
        [this](const std::string &msg){
            return this->_msg_rcv(msg);
        }
    );  
    m_inner->m_rcvr_csd_double.binding_set(
        [this](const CSDCarrierDouble &csd){
            return this->_csd_rcv(csd);
        }
    );      
    m_inner->m_rcvr_run_state.handler_set(
        [this](bool state){
            return this->_run_state_rcv(state);
        }
    );              
}

/////
///  Client destructor
RealtimeClient::
    ~RealtimeClient()
{
	this->stop();
	this->join();
}

/////
///  Callback function for receiving a logger message
void RealtimeClient::
    _msg_rcv(const std::string &message)
{
    /// Print message to screen
    std::cout << message;
}

/////
///  Initiate a connection
void RealtimeClient::
    start()
{
	Guard p_guard(m_p_protect);
    if(not m_is_connecting){
        m_connections_task->start();
        m_is_connecting.set();
    }
}

/////
///  Close the connection
bool RealtimeClient::
    stop()
{
    bool retval = false;
    Guard p_guard(m_p_protect);
    if(m_is_connecting){
        m_connections_task->stop();
        m_is_connecting.clear();
        retval = true;
    }
    this->_inner_stop_p();
    return retval;
}

/////
///  Enforces a full stop on the task (blocks until it has fully stopped)  
void RealtimeClient::
    join()
{
    Guard p_guard(m_p_protect);
    m_connections_task->join();
    this->_inner_join_p();
}

/////
///  Returns TRUE if currently connected/trying to connect
bool RealtimeClient::
    is_live()
{
    return bool(m_is_connecting);
}

/////
///  Blocks until either a connection is established, or the connection task is stopped.
///  (Returns true for a good connection, false if the task is stopped)
bool RealtimeClient::
    wait_for_connection()
{
    SDT::MultiWait multi_wait;
    switch(
        multi_wait.block_many(
            m_is_connected.set_wait(), 
            m_is_connecting.clear_wait()
        )
    ){
        case 0:
            return true;
            break;

        case 1:
            return false;
            break;

        default:
            throw std::logic_error("SHOULD NEVER REACH");
            return false;
    }
}

/////
///  Returns client connection status
bool RealtimeClient::
    is_connected()
{
    return bool(m_is_connected);
}

/////
///  Callback function for a newly established connection
void RealtimeClient::
	_connection_established(
        SDT::TaskManagerPtr transport_man,
        bool subscribe
    )
{
    {
        Guard p_guard(m_p_protect);
        if(m_is_connected){
            throw SDT::fatal_error("Must stop and join previous connection");
        }    
        
        /// Make sure any previously stopped connection is fully done
        this->_inner_join_p();

        /// Configure the SDT multiplexer
        SDT::DuplexDispatchFactory duplex;
        transport_man->task_factory_access(duplex);
        auto transport_duplex = duplex(
            m_inner->m_mplex_sndr.sender_dispatch(),
            m_inner->m_mplex_rcvr.receiver_dispatch()
        );
        m_transport_sending = transport_duplex.sender_task;
        m_transport_receiving = transport_duplex.receiver_task;

        /// Start the send/receive machinery
        this->_inner_start_p();
        if(not subscribe){
            /// Unsubscribe from the real-time data broadcasts
            logger.detail("Unsubscribed from real-time data feed");        
            this->_unsubscribe();
        }        
    }
    //separate from the p_guard in case this callback is interested in modifying the client state
    if(m_connection_alert){
        m_connection_alert(true);
    }
}

/////
///  Forwards disconnect event to the set handler
void RealtimeClient::
    _disconnect_event()
{
    if(m_connection_alert){
        m_connection_alert(false);
    }
}

/////
///  Sets the connection established handler
void RealtimeClient::
    connection_handler_set(StatusAlertHandler handler)
{
    m_connection_alert = handler;
}

/////
///  Sets the handler for a run state change
void RealtimeClient::
    run_status_handler_set(StatusAlertHandler handler)
{
    m_run_status_alert = handler;
}

////
///  Starts receiving data from the server
void RealtimeClient::
	_inner_start_p()
{
	if(m_transport_sending){
        m_transport_receiving->join_ready_event_handler_set(
                [this](){this->_inner_stop_p();}
        );
		m_transport_sending->start();
		m_transport_receiving->start();

        SDT::ReceiverDispatchWaitForReady ext;
        m_transport_receiving->extension(ext);
        m_is_connected.set();
	}else{
		throw SDT::fatal_error("Must connect first!");
	}
}

/////
///  
bool RealtimeClient::
    _inner_stop()
{
	Guard p_guard(m_p_protect);
    return this->_inner_stop_p();
}

/////
///
bool RealtimeClient::
    _inner_stop_p()
{
	if(m_is_connected){
		m_transport_sending->finalize();
		m_transport_receiving->finalize();
        m_is_connected.clear();
        // restart the connection attempts (they pause on a successful connection)
        if(m_is_connecting){
            m_connections_task->unpause();
        }
        this->_disconnect_event();
		return true;
	}
	return false;
}

/////
///
void RealtimeClient::
    _inner_join_p()
{
	if(m_transport_sending){
		m_transport_receiving->join();
		//The close here is necessary because
		//the receiver join only stops one half of the Duplex connection.
		//If the receiver is not actively sending data, it cannot tell if
		//the connection is done. This forces it closed.
		m_transport_sending->close();
		m_transport_sending->join();
        m_transport_sending = nullptr;
        m_transport_receiving = nullptr;
	}
}

/////
///  Handler for receiving a new CSD update
void RealtimeClient::
    _csd_rcv(CSDCarrierDouble csd)
{
    /// Push the new frame onto the arrival queue
    if(m_csds_queue.length() < m_max_pending_csds){
        m_csds_queue.push(std::make_shared<CSDCarrierDouble>(std::move(csd)));
    }
}


/////
///  Unsubscribe from receiving real-time data updates
void RealtimeClient::
    _unsubscribe()
{
    try{
        m_inner->m_sndr_unsubscribe.async_call(false);
    }catch(const SDT::Disconnected &){
        this->_inner_stop();
        throw;
    }
}

/////
///  Send an IFO lock-state change to the server
void RealtimeClient::
    ifo_lock_state_send(bool state)
{
    try{
        m_inner->m_sndr_ifo_lock_state.async_call(state);
    }catch(const SDT::Disconnected &){
        this->_inner_stop();
        throw;
    }
}

/////
///  Send a run state change to the server
void RealtimeClient::
    run_state_send(bool state)
{
    try{
        m_inner->m_sndr_run_state.async_call(state);
    }catch(const SDT::Disconnected &){
        this->_inner_stop();
        throw;
    }
}

/////
///  Send the run settings to the server
void RealtimeClient::
    run_settings_send(const std::string &settings)
{
    try{
        m_inner->m_sndr_run_settings.async_call(settings);
    }catch(const SDT::Disconnected &){
        this->_inner_stop();
        throw;
    }
}

/////
///  Returns the run state of the real-time server
bool RealtimeClient::
    run_state_query()
{
    m_run_state = false;

    /// Send the query to the server
    m_is_transferring.set();
    try{
        m_inner->m_sndr_run_state_query.async_call(true);
    }catch(const SDT::Disconnected &){
        this->_inner_stop();
        throw;
    }

    /// Block until the server response is received
    /// (Asynchronous handler will receive, store the transmitted response)
    SDT::MultiWait multi_wait;
    switch(
        multi_wait.block_many(
            m_is_transferring.clear_wait(),
            m_is_connected.clear_wait()
        )
    ){
        case 0:
            //logger.detail("Realtime server run state:\t", m_run_state);
            break;
            
        case 1:
            logger.error("Query failed! Connection was lost.");
            break;
            
        default:
            throw std::logic_error("SHOULD NEVER REACH");
    }
    return m_run_state;
}

/////
///  Callback function for receiving the server run state
void RealtimeClient::
    _run_state_rcv(bool state)
{
    m_run_state = state;
    m_is_transferring.clear();
    if(m_run_status_alert){
        m_run_status_alert(state);
    }
}

/////
///  Returns a reference to the next CSD update
///  (Blocks until data are received, or the connection task is ended)
std::shared_ptr<CSDCarrierDouble> RealtimeClient::
    csd_next()
{
    SDT::MultiWait multi_wait;
    std::shared_ptr<CSDCarrierDouble> csd;

    switch(
        multi_wait.block_many(
            m_csds_queue.pull_wait(csd),
            m_is_connected.clear_wait()
        )
    ){
        case 0:
            return csd;
            break;

        case 1:
            return nullptr;
            break;

        default:
            throw std::logic_error("SHOULD NEVER REACH");
            return nullptr;
    }
    return nullptr;
}

/////
///  Copies the next CSD data into the supplied frame
///  (Returns TRUE if good data received)
bool RealtimeClient::
    csd_next(std::shared_ptr<CSDCarrierDouble> csd_to)
{
    auto csd = this->csd_next();
    if(csd != nullptr){
        *csd_to = *csd;
        return true;
    }
    return false;
}

/////
///  Returns the number of CSD updates currently available
unsigned RealtimeClient::
    num_csd_ready()
{
    return m_csds_queue.length();
}

/////
///  Overrides the default handler for logger messages
void RealtimeClient::
    message_handler_set(MessageHandler callback)
{
    m_inner->m_rcvr_logger_msg.handler_set(callback);
}

}; //~namespace JStream 
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
