/////
///

#include "run_state_broadcaster.hpp"

namespace JStream {


/////
///  Add server to broadcast list
template<typename TServer>
void RealtimeRunStateBroadcaster<TServer>::
    subscribe(TServer *server)
{
    m_broadcaster.call_insert(
        server, 
        [server](bool state){
            return server->run_state_send(state);
        }
    );
}

/////
///  Remove server from broadcast list
template<typename TServer>
void RealtimeRunStateBroadcaster<TServer>::
    unsubscribe(TServer *server)
{
    m_broadcaster.call_remove(server);
}

/////
///  Broadcast a logger message
template<typename TServer>
void RealtimeRunStateBroadcaster<TServer>::
    broadcast(bool state)
{
    m_broadcaster(state);
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
