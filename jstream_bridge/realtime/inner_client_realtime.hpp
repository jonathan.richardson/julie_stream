/////
///

#ifndef H_REALTIME_CLIENT_INNER_H
#define H_REALTIME_CLIENT_INNER_H

#include <sdt/streams/multiplexer/sender.hpp>
#include <sdt/streams/multiplexer/receiver.hpp>
#include <sdt/streams/objects/integers.hpp>
#include <sdt/streams/objects/strings.hpp>
#include <sdt/streams/rpc/caller.hpp>
#include <sdt/streams/rpc/handler.hpp>
#include <sdt/streams/task_transport.hpp>

#include "inner_server_realtime.hpp"

namespace JStream {

namespace _detail {


//////
///  Inner client type which handles the protocol itself
struct _RealtimeClient
{
    /// Network stream multiplexer
    SDT::StreamMultiplexerSender	 
		m_mplex_sndr;
    SDT::StreamMultiplexerReceiver 
		m_mplex_rcvr;

    /// Data stream receiver ends
    decltype(_detail::_RealtimeServer::m_sndr_csd_double)::TReceiver
        m_rcvr_csd_double;           
    SDT::StreamRPCHandler<void, SDT::StringShuttle<> > 
        m_rcvr_logger_msg;               
    SDT::StreamRPCHandler<void, SDT::VarintShuttle<bool> >  
        m_rcvr_run_state;        
        
    /// Data stream sender ends
    decltype(_detail::_RealtimeServer::m_rcvr_unsubscribe)::TCallerPair
        m_sndr_unsubscribe;     
    decltype(_detail::_RealtimeServer::m_rcvr_ifo_lock_state)::TCallerPair
        m_sndr_ifo_lock_state;    
    decltype(_detail::_RealtimeServer::m_rcvr_run_state)::TCallerPair
        m_sndr_run_state;
    decltype(_detail::_RealtimeServer::m_rcvr_run_state_query)::TCallerPair
        m_sndr_run_state_query;        
    decltype(_detail::_RealtimeServer::m_rcvr_run_settings)::TCallerPair
        m_sndr_run_settings;

    ////
    ///  Template used to conveniently add all of the heterogeneous RPC types into the multiplexer
    template<typename TRPC>
    inline void add_to_multiplexer_pair(std::string name, TRPC &comm_attribute)
	{
		m_mplex_sndr.substreamer_add(name, comm_attribute.sender_dispatch());
		m_mplex_rcvr.substreamer_add(name, comm_attribute.receiver_dispatch());
	}

    /////
    ///  Constructor
	_RealtimeClient()
	{
        /// Add the CSD stream receiver ends
		m_mplex_rcvr.substreamer_add(
            "csd_double",         
            m_rcvr_csd_double.receiver_dispatch()
        );

        /// Add the multiplexer pairs
        this->add_to_multiplexer_pair(
            "logger_msg",         
            m_rcvr_logger_msg
        );           
		this->add_to_multiplexer_pair(
            "run_state_to_client",          
            m_rcvr_run_state
        );   
		this->add_to_multiplexer_pair(
            "ifo_lock_state",          
            m_sndr_ifo_lock_state
        );              
		this->add_to_multiplexer_pair(
            "run_state_from_client",          
            m_sndr_run_state
        );
        this->add_to_multiplexer_pair(
            "run_state_query",    
            m_sndr_run_state_query
        );
        this->add_to_multiplexer_pair(
            "run_settings",       
            m_sndr_run_settings
        );
        this->add_to_multiplexer_pair(
            "unsubscribe",       
            m_sndr_unsubscribe
        );        
	}
};

}; //~namespace _detail

}; //~namespace JStream

#endif //H_REALTIME_CLIENT_INNER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :