/////
///

#include "realtime_server.hpp"

namespace JStream {
using namespace ConfigTree;


/////
///  Data server constructor
RealtimeServer::
	RealtimeServer(
        TMsgBroadcaster &msg_broadcaster,
        TCSDBroadcaster &csd_broadcaster,
        TRunStateBroadcaster &run_state_broadcaster,
        SDT::LoggerAccess logger
    ) :
        m_msg_broadcaster(msg_broadcaster),
        m_csd_broadcaster(csd_broadcaster),
        m_run_state_broadcaster(run_state_broadcaster),
        m_logger(logger)
{
    /// Associate this class type with the logger context
    m_logger.this_typename_set(this);

    /// Set up the inner receiver forwarding
    m_inner = std::make_shared<_detail::_RealtimeServer>();
    m_inner->m_rcvr_ifo_lock_state.handler_set( 
        [this](bool state){
            return this->_ifo_lock_state_rcv(state);
        }
    );     
    m_inner->m_rcvr_run_state.handler_set(
        [this](bool state){
            return this->_run_state_rcv(state);
        }
    ); 
    m_inner->m_rcvr_run_state_query.handler_set( 
        [this](bool){
            return this->_run_state_query_rcv();
        }
    );     
    m_inner->m_rcvr_run_settings.handler_set(  
        [this](const std::string &config){
            return this->_run_config_rcv(config);
        }
    );
    m_inner->m_rcvr_unsubscribe.handler_set(
        [this](bool){
            return m_csd_broadcaster.unsubscribe(this);
        }
    );    

    /// Set the CSD notifier to a cancelled state
    /// (CSDs notify is always in a cancelled state when not running)
    m_csds_notify.cancel_inc();
    
    /// Register this server with the live data broadcasters
    m_msg_broadcaster.subscribe(this);
    m_csd_broadcaster.subscribe(this);
    m_run_state_broadcaster.subscribe(this);    
}

/////
///  Add CSD accumulation containers to the internal notifier queue
void RealtimeServer::
    prefill_queues(const CSDCarrierDouble &prototype)
{
	auto csd_locality = localization_manager()->memory_locality_get(m_logger.context());
    for(auto idx : icount(2)){
        auto csd = std::shared_ptr<CSDCarrierDouble>(
            new CSDCarrierDouble(
                prototype,
                csd_locality
            )
        );
        m_csds_fresh.push(csd);
    }
}

/////
///  Destroy the internal CSD queues
void RealtimeServer::
    release_queues()
{
    shared_ptr<CSDCarrierDouble > csd;
    while(m_csds_fresh.pull(csd)){
        csd.reset();
    }
    while(m_csds_stale.pull(csd)){
        csd.reset();
    }    
    m_queues_initialized = false;
}

/////
///  Server destructor
RealtimeServer::
    ~RealtimeServer()
{
    /// Deallocate any remaining internal CSD queue elements
    this->release_queues();
    
    /// Unregister this server from the live data broadcasters
    m_msg_broadcaster.unsubscribe(this);     
    m_csd_broadcaster.unsubscribe(this);     
    m_run_state_broadcaster.unsubscribe(this);
}

/////
///  Connect the data server to the client
void RealtimeServer::
	connect(SDT::TaskManagerPtr transport_man)
{
	Guard p_guard(m_p_protect);

	if(not m_transport_sending){
        /// Set up the send and receive task lists for the server
        /// (Does not start the tasks yet)
        SDT::DuplexDispatchFactory duplex;
		transport_man->task_factory_access(duplex);
		auto transport_duplex = duplex(
			m_inner->m_mplex_sndr.sender_dispatch(),
			m_inner->m_mplex_rcvr.receiver_dispatch()
		);
		m_transport_sending = transport_duplex.sender_task;
		m_transport_receiving = transport_duplex.receiver_task;

        /// Create the transmission loop thread
        /// (Does not launch the thread yet)
        SDT::TaskThreadFactory thread_factory;
        transport_man->task_factory_access(thread_factory);
        m_thread_csd = thread_factory( 
            /// Starts the CSD broadcast loop
            [this](){
                return this->csd_send_loop();
            }
        );
	}else{
		throw SDT::fatal_error("Must stop and join the previous connection!");
	}
}

/////
///  Starts the data transmission to a client
void RealtimeServer::
	start()
{
	Guard p_guard(m_p_protect);

	if(m_csds_notify.cancelled_is()){
        /// Signal that the server is running
        m_csds_notify.cancel_dec();

        /// Start the send and receive tasks
		m_transport_sending->start();
		m_transport_receiving->start();
        
        /// Force a wait to make sure that it is fully connected before continuing
        SDT::ReceiverDispatchWaitForReady ext;
        m_transport_receiving->extension(ext);
        
        /// Launch the transmission loop thread
        m_thread_csd->start();
	}else{
		throw SDT::fatal_error("Must connect first!");
	}
}

/////
///  Returns TRUE if the server is currently running (connected to a client)
bool RealtimeServer::
    is_live()
{
	Guard p_guard(m_p_protect);
    
	if(m_transport_receiving){
		return m_transport_receiving->is_live();
	}
	return false;
}

/////
///  Shut down the server operations
///  (Returns TRUE if successful, FALSE if server was already shut down)
bool RealtimeServer::
    stop()
{
	Guard p_guard(m_p_protect);
    
	if(not m_csds_notify.cancelled_is()){
        /// Stop sending and receiving data
		m_transport_sending->finalize();
		m_transport_receiving->finalize();

        /// Signal that the server was shut down
        m_csds_notify.cancel_inc();
		return true;
	}
	return false;
}

/////
///  Terminates the connection (ends the transmission thread)
void RealtimeServer::
    join()
{
    SDT::TaskTransportPtr sender;
    SDT::TaskTransportPtr receiver;
    SDT::TaskGeneralPtr   thread_csd;
	{
		Guard p_guard(m_p_protect);
		sender = m_transport_sending;
		receiver = m_transport_receiving;
        thread_csd = m_thread_csd;
	}

	if(sender){
		receiver->join();
		/// The close here is necessary because the receiver join only stops one half 
        /// of the Duplex connection. If the receiver is not actively sending data, 
        /// it cannot tell if the connection is done. This forces it closed.
		sender->close();
		sender->join();
        m_thread_csd->join();
		{
			Guard p_guard(m_p_protect);
			m_transport_sending   = nullptr;
			m_transport_receiving = nullptr;
			m_thread_csd          = nullptr;
		}
	}
}

/////
///  Sets the function the server will call when an IFO lock-state change is received
void RealtimeServer::
    callback_ifo_lock_state_set(RunStateHandler callback)
{
	m_callback_ifo_lock_state = callback;
}

/////
///  Handler for a change in IFO lock state
///  (Wrapper for the IFO lock-state change callback)
void RealtimeServer::
    _ifo_lock_state_rcv(bool on_off)
{
    if(m_callback_ifo_lock_state){
        m_callback_ifo_lock_state(on_off);
    }
}

/////
///  Sets the function the server will call when a run-state change is received
void RealtimeServer::
    callback_run_state_set(RunStateHandler callback)
{
	m_callback_run_state = callback;
}

/////
///  Handler for a change in run state
///  (Wrapper for the run-state change callback)
void RealtimeServer::
    _run_state_rcv(bool on_off)
{
    if(m_callback_run_state){
        m_callback_run_state(on_off);
    }
}

/////
///  Sets the function the server will call when a run-state change is received
void RealtimeServer::
    callback_run_state_query_set(RunStateQueryHandler callback)
{
    /// Transmission functor to pass to the topology manager
    auto transmitter = 
        [this](bool state){
            return this->run_state_send(state);
        };

    /// Bind the functor into a simplified internal callback
    m_callback_run_state_query = 
        [callback, transmitter](){
            return callback(transmitter);
        };
}

/////
///  Handler for a run state query
void RealtimeServer::
    _run_state_query_rcv()
{
    if(m_callback_run_state_query){
        m_callback_run_state_query();
    }
}

/////
///  Sets the function the server will call when the JSON configuration is received
void RealtimeServer::
    callback_run_config_set(RunConfigHandler callback)
{
	m_callback_run_config = callback;
}

/////
///  Handler for the JSON run configuration
void RealtimeServer::
    _run_config_rcv(const std::string &config)
{
    m_logger.detail("Network config:\n", config, "\n");
    if(m_callback_run_config){
        m_callback_run_config(config);
    }
}

/////
///  Receives the updated CSD accumulation
///  Returns TRUE if transfer actually occured, FALSE otherwise
///  (Called by the CSD exporter every time a new superframe is accumulated)
void RealtimeServer::
    csd_double_send(const CSDCarrierDouble &csd_from)
{
    if(not m_queues_initialized){
        /// Populate the internal server CSD queues with frames
        this->prefill_queues(csd_from);
        m_queues_initialized = true;
    }

    /// If an accumulation container is available, accept the new data
    std::shared_ptr<CSDCarrierDouble> csd_to;
    if(m_csds_stale.pull(csd_to)){
        /// Memcopy the update into internal container & push onto queue
        *csd_to = csd_from;
        m_csds_fresh.push(std::move(csd_to));     
    }
}

/////
///  Receives an intercepted stream message & transmits to remote client
///  Returns TRUE if transfer actually occured, FALSE otherwise
void RealtimeServer::
    message_send(const std::string &msg)
{
    if(this->is_live()){
        try{
            m_inner->m_sndr_msg.async_call(msg);
        }catch(const SDT::Disconnected &){
            this->stop();
            throw;
        }
    }
}

/////
///  Send the current run state to the client
void RealtimeServer::
    run_state_send(bool state)
{
    try{
        m_inner->m_sndr_run_state.async_call(state);
    }catch(const SDT::Disconnected &){
        this->stop();
        throw;
    }
}

/////
///  Data transmission loop
void RealtimeServer::
    csd_send_loop()
{
    std::shared_ptr<CSDCarrierDouble> csd;

    /// First move all old CSD accumulation containers to the "stale" pile to be filled up
    while(m_csds_fresh.pull(csd)){
        m_csds_stale.push(std::move(csd));
    }

    try{
        while(true){
            /// Pull in the next CSD accumulation update (blocking but cancellable)
            csd = nullptr;
            bool pull_success = m_csds_notify.block(m_csds_fresh.pull_wait(csd));
            if(not pull_success){
                assert(not csd);
                break;
            }

            /// Send the accumulation update over the network
            m_inner->m_sndr_csd_double.object_send_throw(*csd);
            m_logger.detail("Sending CSD update #", csd->sequence_number_get());

            /// Push the container onto the "stale" pile to be refilled
            m_csds_stale.push(std::move(csd));
        }
    }catch(const SDT::Disconnected &exc){//this exception is normal control flow
        /// Return the CSD container to the flow & close the connection
        if(csd){
            m_csds_fresh.push(std::move(csd));
        }
        this->stop();
    }
    
    /// Move the remaining CSD containers back so we don't needlessly copy
    while(m_csds_stale.pull(csd)){
        m_csds_fresh.push(std::move(csd));
    }
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
