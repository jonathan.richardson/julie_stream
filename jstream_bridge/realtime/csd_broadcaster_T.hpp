/////
///

#include "csd_broadcaster.hpp"

namespace JStream {


/////
///  Add server to broadcast list
template<typename TServer>
void RealtimeCSDBroadcaster<TServer>::
    subscribe(TServer *server)
{
    m_broadcaster.call_insert(
        server, 
        [server](const CSDCarrierDouble &csd){
            return server->csd_double_send(csd);
        }
    );
    m_resetter.call_insert(
        server, 
        [server](){
            return server->release_queues();
        }
    );    
}

/////
///  Remove server from broadcast list
template<typename TServer>
void RealtimeCSDBroadcaster<TServer>::
    unsubscribe(TServer *server)
{
    m_broadcaster.call_remove(server);  
    m_resetter.call_remove(server);  
}

/////
///  Broadcast a CSD update
template<typename TServer>
void RealtimeCSDBroadcaster<TServer>::
    broadcast(const CSDCarrierDouble &csd)
{
    m_broadcaster(csd);
}

/////
///  Signal servers to reset their internal queues
template<typename TServer>
void RealtimeCSDBroadcaster<TServer>::
    reset()
{
    m_resetter();
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
