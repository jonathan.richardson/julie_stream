////
///

#ifndef H_JSTREAM_REALTIME_SERVER_H
#define H_JSTREAM_REALTIME_SERVER_H

#include <thread>
#include <cstdint>
#include <memory>

#include <boost/asio.hpp>

#include <sdt/transport/asio/task_asio.hpp>
#include <sdt/transport/task_connections.hpp>
#include <sdt/transport/multi_acceptor.hpp>
#include <sdt/queues/notify_queue.hpp>
#include <sdt/config.hpp>
#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>

#include "hardware_locality/localization_manager.hpp"

#include "inner_server_realtime.hpp"
#include "msg_broadcaster.hpp"
#include "csd_broadcaster.hpp"
#include "run_state_broadcaster.hpp"

namespace JStream {
using SDT::LoggerAccess;

//////
///  Compiler firewall as this guy has heavy machinery that shouldn't slow down compiles 
///  (internally holds all of the SDT stream classes)
namespace _detail {
    class _RealtimeServer;
}; //~namespace _detail


/////
///  The realtime server class
class RealtimeServer
{
    friend class RealtimeClient;

public:
    /// Receiver callback typedefs
    typedef std::function<void(bool)>                          
        RunStateHandler;
    typedef std::function<void(RunStateHandler)>
        RunStateQueryHandler;        
    typedef std::function<void(const std::string &)>           
        RunConfigHandler;
        
    /// Broadcaster typedefs
    typedef RealtimeMsgBroadcaster<RealtimeServer>
        TMsgBroadcaster;      
    typedef RealtimeCSDBroadcaster<RealtimeServer>
        TCSDBroadcaster;        
    typedef RealtimeRunStateBroadcaster<RealtimeServer>
        TRunStateBroadcaster;                
        
protected:
    /// Convenience typedefs
    typedef std::mutex                      
        Mutex;
    typedef std::unique_lock<Mutex>         
        Guard;
    typedef SDT::NotifyQueue<std::shared_ptr<CSDCarrierDouble > > 
        TCSDQueue;
    typedef std::function<void()>
        RunStateQueryHandler2;       
                                            
    /// Protects the server state
    Mutex                                   m_p_protect;

    /// Logger
    SDT::LoggerAccess                       m_logger;

    /// Used by SDT internal transceiver
    std::shared_ptr<_detail::_RealtimeServer>    
                                            m_inner;               //internal data transmitter
    SDT::TaskTransportPtr                   m_transport_sending;   //internal send task list
    SDT::TaskTransportPtr                   m_transport_receiving; //internal receive task list
    SDT::TaskGeneralPtr                     m_thread_csd;          //TCP transmission loop thread

    /// Receiver callback function wrappers
    RunStateHandler                         m_callback_ifo_lock_state;
    RunStateHandler                         m_callback_run_state;
    RunStateQueryHandler2                   m_callback_run_state_query;
    RunConfigHandler                        m_callback_run_config;
    
    /// Data broadcasters (server registers and unregisters itself)
    TMsgBroadcaster&                        m_msg_broadcaster;
    TCSDBroadcaster&                        m_csd_broadcaster;
    TRunStateBroadcaster&                   m_run_state_broadcaster;    
 
    /// Internal notifier queues for the CSD accumulation
    TCSDQueue                               m_csds_fresh;
    TCSDQueue                               m_csds_stale;
    bool                                    m_queues_initialized = false;
    SDT::SingleWait                         m_csds_notify;

public:
    RealtimeServer(
        TMsgBroadcaster &msg_broadcaster,
        TCSDBroadcaster &csd_broadcaster,
        TRunStateBroadcaster &run_state_broadcaster,
        SDT::LoggerAccess logger
    );
    
    ~RealtimeServer();

    /// Server controls
    void connect(SDT::TaskManagerPtr manager);
	void start();
    bool stop();
    void join();
    bool is_live();

    /// Set the receive callback functions
    void callback_ifo_lock_state_set(RunStateHandler);
    void callback_run_state_set(RunStateHandler);
    void callback_run_state_query_set(RunStateQueryHandler);
    void callback_run_config_set(RunConfigHandler);
    
    /// Send data to remote client
    void csd_double_send(const CSDCarrierDouble &);
    void run_state_send(bool);
    void message_send(const std::string &);

    /// Destroy the internal queues
    void release_queues();

protected:
    /// Event handlers
    void _ifo_lock_state_rcv(bool);
    void _run_state_rcv(bool);
    void _run_state_query_rcv();
    void _run_config_rcv(const std::string &);

    /// Transmission loop thread
    void csd_send_loop();
    
    /// Allocate the internal queue elements
    void prefill_queues(const CSDCarrierDouble &);    
};

}; //~namespace JStream

#endif //H_JSTREAM_REALTIME_SERVER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
