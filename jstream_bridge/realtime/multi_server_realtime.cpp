/////
///

#include "multi_server_realtime.hpp"

namespace JStream {


/////
///  Constructor
RealtimeMultiServer::
    RealtimeMultiServer(SDT::LoggerAccess logger) :
        m_logger(logger)
{
    /// Set the callback for a new client connection
    /// (Launches a new subserver)
    this->event_connection_create_set(
        [this](){
            return this->make_server();
        }
    );
}

/////
///  Returns a reference to the logger message broadcaster
RealtimeMultiServer::TServer::TMsgBroadcaster& RealtimeMultiServer::
    msg_broadcaster()
{
    return m_msg_broadcaster;
}

/////
///  Returns a reference to the CSD broadcaster
RealtimeMultiServer::TServer::TCSDBroadcaster& RealtimeMultiServer::
    csd_broadcaster()
{
    return m_csd_broadcaster;
}

/////
///  Returns a reference to the run state broadcaster
RealtimeMultiServer::TServer::TRunStateBroadcaster& RealtimeMultiServer::
    run_state_broadcaster()
{
    return m_run_state_broadcaster;
}

/////
///  Sets the callback for receiving an IFO lock state change
void RealtimeMultiServer::
    callback_ifo_lock_state_set(TServer::RunStateHandler callback)
{
	m_callback_ifo_lock_state = callback;
}

/////
///  Sets the callback for receiving a run state change
void RealtimeMultiServer::
    callback_run_state_set(TServer::RunStateHandler callback)
{
	m_callback_run_state = callback;
}

/////
///  Sets the callback for receiving a run state query
void RealtimeMultiServer::
    callback_run_state_query_set(TServer::RunStateQueryHandler callback)
{
	m_callback_run_state_query = callback;
}

/////
///  Sets the callback for receiving a run configuration
void RealtimeMultiServer::
    callback_run_config_set(TServer::RunConfigHandler callback)
{
	m_callback_run_config = callback;
}

/////
///  Creates a new CSD server
RealtimeMultiServer::TServerManagedPtr RealtimeMultiServer::
    make_server()
{
    /// Create the data server
    auto server = std::make_shared<TServerManaged>(
        m_msg_broadcaster,
        m_csd_broadcaster,
        m_run_state_broadcaster,
        m_logger
    );
    
    /// Set the server callbacks 
    /// (the connecting client can send any/all of these things)
    server->callback_ifo_lock_state_set(m_callback_ifo_lock_state);
    server->callback_run_state_set(m_callback_run_state);
    server->callback_run_state_query_set(m_callback_run_state_query);
    server->callback_run_config_set(m_callback_run_config);
    return server;
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
