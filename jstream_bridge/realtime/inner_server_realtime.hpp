/////
///

#ifndef H_REALTIME_INNER_SERVER_H
#define H_REALTIME_INNER_SERVER_H

#include <sdt/streams/multiplexer/sender.hpp>
#include <sdt/streams/multiplexer/receiver.hpp>
#include <sdt/streams/objects/integers.hpp>
#include <sdt/streams/objects/strings.hpp>
#include <sdt/streams/rpc/caller.hpp>
#include <sdt/streams/rpc/handler.hpp>
#include <sdt/streams/task_transport.hpp>

#include "analysis/csd_carrier_double.hpp"

#include "jstream_bridge/shuttles/csd_shuttle.hpp"

namespace JStream {

namespace _detail {

//////
///  Inner server type which handles the protocol itself
struct _RealtimeServer
{
    friend class _RealtimeClient;

    /// Network stream multiplexer
    SDT::StreamMultiplexerSender         
		m_mplex_sndr;        
    SDT::StreamMultiplexerReceiver       
		m_mplex_rcvr;

    /// Data stream sender ends        
    SDT::ObjectSender<CSDShuttle<CSDCarrierDouble> > 
        m_sndr_csd_double;          
    SDT::StreamRPCHandler<void, SDT::StringShuttle<> >::TCallerPair
        m_sndr_msg;          
    SDT::StreamRPCHandler<void, SDT::VarintShuttle<bool> >::TCallerPair
        m_sndr_run_state;        

    /// Data stream receiver ends  
    SDT::StreamRPCHandler<void, SDT::VarintShuttle<bool> > 
        m_rcvr_unsubscribe;    
    SDT::StreamRPCHandler<void, SDT::VarintShuttle<bool> > 
        m_rcvr_ifo_lock_state;    
    SDT::StreamRPCHandler<void, SDT::VarintShuttle<bool> > 
        m_rcvr_run_state;
    SDT::StreamRPCHandler<void, SDT::VarintShuttle<bool> > 
        m_rcvr_run_state_query;        
    SDT::StreamRPCHandler<void, SDT::StringShuttle<> > 
        m_rcvr_run_settings;

    ////
    ///  Template used to conveniently add all of the heterogeneous RPC types into the multiplexer
    template<typename TRPC>
    inline void add_to_multiplexer_pair(std::string name, TRPC &comm_attribute)
	{
		m_mplex_sndr.substreamer_add(name, comm_attribute.sender_dispatch());
		m_mplex_rcvr.substreamer_add(name, comm_attribute.receiver_dispatch());
	}

    /////
    ///  Constructor
	_RealtimeServer()
	{
        /// Add the CSD stream sender end
		m_mplex_sndr.substreamer_add(
            "csd_double",         
            m_sndr_csd_double.sender_dispatch()
        );
        
        /// Add the multiplexer pairs        
        this->add_to_multiplexer_pair(
            "logger_msg",         
            m_sndr_msg
        );   
		this->add_to_multiplexer_pair(
            "run_state_to_client",          
            m_sndr_run_state
        );  
		this->add_to_multiplexer_pair(
            "ifo_lock_state",          
            m_rcvr_ifo_lock_state
        );                
		this->add_to_multiplexer_pair(
            "run_state_from_client",          
            m_rcvr_run_state
        );
        this->add_to_multiplexer_pair(
            "run_state_query",    
            m_rcvr_run_state_query
        );
        this->add_to_multiplexer_pair(
            "run_settings",       
            m_rcvr_run_settings
        );
        this->add_to_multiplexer_pair(
            "unsubscribe",       
            m_rcvr_unsubscribe
        );        
	}

};

}; //~namespace _detail

}; //~namespace JStream

#endif //H_REALTIME_INNER_SERVER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :