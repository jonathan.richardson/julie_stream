////
///

#ifndef H_JSTREAM_REALTIME_CLIENT_H
#define H_JSTREAM_REALTIME_CLIENT_H

#include <boost/asio.hpp>

#include <thread>
#include <cstdint>
#include <memory>
#include <functional>

#include <sdt/transport/asio/task_asio.hpp>
#include <sdt/transport/task_connections.hpp>
#include <sdt/streams/exceptions.hpp>
#include <sdt/queues/notify_queue.hpp>
#include <sdt/threading/notify/multi_wait.hpp>
#include <sdt/config.hpp>
#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>

#include "inner_client_realtime.hpp"

namespace JStream {


/////
///  Real-time client class
class RealtimeClient
{
public:
    /// Receiver callback typedefs
    typedef std::function<void(const std::string &)>
        MessageHandler;
    typedef std::function<void(bool)> 
        StatusAlertHandler;
        
protected:
    /// Convenience typedefs
    typedef std::mutex                              
        Mutex;
    typedef std::unique_lock<Mutex>                 
        Guard;    
    typedef SDT::NotifyQueue<std::shared_ptr<CSDCarrierDouble> >
        TCSDQueue;
        
    /// Protects the connection state
    Mutex                                   m_p_protect;
    SDT::SwitchNotify                       m_is_connecting;
    SDT::SwitchNotify                       m_is_connected;
    SDT::SwitchNotify                       m_is_transferring;    

    /// Used by the SDT internal transceiver
    std::shared_ptr<_detail::_RealtimeClient> 
                                            m_inner; 
    SDT::TaskTransportPtr                   m_transport_sending;
    SDT::TaskTransportPtr                   m_transport_receiving;
    SDT::TaskConnectPtr                     m_connections_task;

    /// Internal notifier queue for the CSD accumulation updates
    TCSDQueue                               m_csds_queue;
    unsigned                                m_max_pending_csds = 3;
    
    /// Real-time server run state
    bool                                    m_run_state = false;

    /// For notifications
    StatusAlertHandler                      m_connection_alert;
    StatusAlertHandler                      m_run_status_alert;
    
public:
    RealtimeClient(
        const std::string &connection_addr,
        bool subscribe = true
    ); 
    
    ~RealtimeClient();
    
#ifndef SWIG
    /// Makes this class noncopyable
    RealtimeClient(const RealtimeClient&) = delete;
#endif

    /// Set event handlers
    void message_handler_set(MessageHandler);
    void connection_handler_set(StatusAlertHandler);
    void run_status_handler_set(StatusAlertHandler);    
      
    /// Client controls
	void start();    
    bool wait_for_connection();
    bool is_connected();
    bool stop();
    void join();
    bool is_live();    
    
    /// Send run state, run configuration to the real-time server
    void ifo_lock_state_send(bool);
    void run_state_send(bool);
    void run_settings_send(const std::string &);    
    
    /// Returns the run state of the real-time server
    bool run_state_query();
    
    /// Returns a reference to the next CSD update
    /// (Blocks until an update is received, or the connection task is ended)
    std::shared_ptr<CSDCarrierDouble> csd_next();
    
    /// Copies the next CSD data into the supplied frame (returns TRUE on success)
    /// (Blocks until an update is received, or the connection task is ended)
    bool csd_next(std::shared_ptr<CSDCarrierDouble> csd_to);

    /// Returns the number of CSD updates currently available
    unsigned num_csd_ready();

protected:    
    /// Lifetime management for the connected side of this object
	void _inner_start_p();
    bool _inner_stop();
    bool _inner_stop_p();
    void _inner_join_p();
    
	/// Event handlers
    void _connection_established(
        SDT::TaskManagerPtr transport_man,
        bool subscribe
    );
    void _msg_rcv(const std::string &); 
    void _csd_rcv(CSDCarrierDouble);
    void _run_state_rcv(bool);
    
    /// Unsubscribe from receiving real-time data updates
    void _unsubscribe(); 
    void _disconnect_event();
};

}; //~namespace JStream

#endif //H_JSTREAM_REALTIME_CLIENT_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
