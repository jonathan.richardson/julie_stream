/////
///

#include "msg_broadcaster.hpp"

namespace JStream {


/////
///  Add server to broadcast list
template<typename TServer>
void RealtimeMsgBroadcaster<TServer>::
    subscribe(TServer *server)
{
    m_broadcaster.call_insert(
        server, 
        [server](const std::string &msg){
            return server->message_send(msg);
        }
    );
}

/////
///  Remove server from broadcast list
template<typename TServer>
void RealtimeMsgBroadcaster<TServer>::
    unsubscribe(TServer *server)
{
    m_broadcaster.call_remove(server);
}

/////
///  Broadcast a logger message
template<typename TServer>
void RealtimeMsgBroadcaster<TServer>::
    broadcast(const std::string &msg)
{
    m_broadcaster(msg);
}

}; //~namespace JStream

//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
