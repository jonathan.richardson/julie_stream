////
///

#ifndef H_REALTIME_RUN_STATE_BROADCASTER_H
#define H_REALTIME_RUN_STATE_BROADCASTER_H

#include <sdt/utilities/logger.hpp>
#include <sdt/utilities/logger_impl.hpp>
#include <sdt/utilities/keyed_signal.hpp>

namespace JStream {


/////
///  Broadcasts run-state changes to all registered realtime servers
template<typename TServer>
class RealtimeRunStateBroadcaster
{
public:
    typedef SDT::KeyedSignal<void(bool)> 
        TStatusAlertBroadcaster;  

protected:
    TStatusAlertBroadcaster m_broadcaster;
    
public:
    /// Add/remove server to/from broadcast list
    void subscribe(TServer *server);
    void unsubscribe(TServer *server);
    
    /// Broadcast over the specified data stream
    void broadcast(bool);
};

}; //~namespace JStream

#include "run_state_broadcaster_T.hpp"
#endif //H_REALTIME_RUN_STATE_BROADCASTER_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
