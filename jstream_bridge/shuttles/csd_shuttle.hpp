/////
///

#ifndef H_JSTREAM_CSD_SHUTTLE_H
#define H_JSTREAM_CSD_SHUTTLE_H

#include <sdt/serialization/json_schemas.hpp>
#include <sdt/serialization/json_helpers.hpp>

#include "analysis/csd_carrier_base.hpp"

namespace JStream {
using SDT::var_int;
using SDT::json_object_field;

/////
///  Contains the serialization methods for the CSDCarrier type
///  (Used internally by SDT)
template <typename TCSDContainer> 
struct CSDShuttle
{
    typedef TCSDContainer TCarrier;

    /////
    ///  Constructor
	CSDShuttle(){}

    /////
    ///  Verify JSON headers are consistent between client and server
    CSDShuttle(Json::Value &sink_grammar)
    {
        Json::Value source_grammar = json_schema();
        if(source_grammar != sink_grammar){
            std::cout << "Client/server version incompatibility!\n";
        }
        assert(source_grammar == sink_grammar);
    }

    /////
    ///  Returns the JSON header for the supported settings container type
    inline Json::Value json_schema()
    {
        //TODO: MAKE THIS DESCRIPTIVE
        Json::Value grammar;
        grammar["transmission_type"] = "CSDAccumulation";
		Json::Value &header = grammar["header"];
        //json_object_field(grammar, "num_channels"    , var_int());
        //json_object_field(grammar, "num_length"      , var_int(csd_collection.job_type));
        json_object_field(header, "version", std::atof(JSTREAM_VERSION));
        json_object_field<double>(header, "array_numeric_type");

        return grammar;
    }

    /////
    ///  Serially writes the TCarrier object to the tranmission stream
    template<typename TSerialWriter>
    inline void stream_write(TSerialWriter &stream, const TCarrier& csd_collection)
    {
        /// Serially write the metadata to the stream
        stream.serial_writeLE(unsigned(csd_collection.channels_num()));           //Number of channels
        stream.serial_writeLE(unsigned(csd_collection.length()));                 //CSD length
        stream.serial_writeLE(double(csd_collection.sample_freq_get()));          //Sample frequency (Hz)
        stream.serial_writeLE(double(csd_collection.overlap_frac_get()));         //FFT frame overlap fraction
        stream.serial_writeLE(uint64_t(csd_collection.sequence_number_get()));    //Frame ID
        stream.serial_writeLE(uint64_t(csd_collection.accumulation_get()));       //Accumulation 
        stream.serial_writeLE(double(csd_collection.ENBW_get()));                 //ENBW (Hz)
        stream.serial_writeLE(double(csd_collection.normalization_to_ps_get()));  //Normalization for a single FFT batch
        stream.serial_writeLE(uint64_t(csd_collection.timestamp_begin()));        //Begin time (UNIX ns)
        stream.serial_writeLE(uint64_t(csd_collection.timestamp_end()));          //End time (UNIX ns)
        stream.serial_writeLE(unsigned(csd_collection.error_flag_get()));         //Error state flag

        /// Serially write the PSDs to the stream
		for(unsigned chn_idx = 0; chn_idx < csd_collection.channels_num(); chn_idx += 1){
			auto psd = csd_collection.psd_get(chn_idx);
			stream.serial_write_arrayLE(psd.begin(), psd.end());
		}

        /// Serially write the CSDs to the stream
		for(unsigned row_idx = 1; row_idx < csd_collection.channels_num(); row_idx += 1){
			for(unsigned col_idx = 0; col_idx < row_idx; col_idx += 1){
				auto csd = csd_collection.csd_get(row_idx, col_idx);
				stream.serial_write_arrayLE((typename TCarrier::TNum *)csd.begin(), (typename TCarrier::TNum *)csd.end());
			}
		}
    }

    /////
    ///  Reassembles the CSDCarrierDouble  container from the binary network stream
    template<typename TSerialReader>
	inline TCarrier stream_read(TSerialReader &stream)
    {
        /// NOTE: Data must be read from the binary stream in the same order that they were added

        uint64_t value_uL;
        unsigned value_u;
        double   value_d;

        /// Number of channels
		stream.serial_readLE(value_u);
        auto channels_num = value_u;

        /// CSD length
		stream.serial_readLE(value_u);
        auto length = value_u;
        
        /// Sample frequency (Hz)
		stream.serial_readLE(value_d);
        auto sample_freq = value_d;   

        /// FFT frame overlap fraction
		stream.serial_readLE(value_d);
        auto overlap_frac = value_d;       
        
        /// Create a CSDCarrier container of the size specified by
        /// the above parameters, and read the remaining data into it.
		TCarrier csd_collection(
            channels_num, 
            length,
            sample_freq,
            overlap_frac
        );

        /// Frame ID
		stream.serial_readLE(value_uL);
        csd_collection.sequence_number_set(value_uL);
        
        /// Accumulation
		stream.serial_readLE(value_uL);
        csd_collection.accumulation_set(value_uL);

        /// ENBW (Hz)
		stream.serial_readLE(value_d);
        csd_collection.ENBW_set(value_d);
       
        /// Normalization factor for a single FFT batch
		stream.serial_readLE(value_d);
        csd_collection.normalization_to_ps_set(value_d);

        /// Begin timestamp (UNIX ns)
		stream.serial_readLE(value_uL);
        auto time_begin = value_uL;
        
        /// End timestamp (UNIX ns)
		stream.serial_readLE(value_uL);
        auto time_end = value_uL;
        csd_collection.timestamps_set(time_begin, time_end);

        /// Frame-dropping flag
		stream.serial_readLE(value_u);
        csd_collection.error_flag_set(value_u);

        /// PSD data
		for(unsigned chn_idx = 0; chn_idx < csd_collection.channels_num(); chn_idx += 1){
			auto psd = csd_collection.psd_get(chn_idx);
			stream.serial_read_arrayLE(psd.begin(), psd.end());
		}

        /// CSD data
		for(unsigned row_idx = 1; row_idx < csd_collection.channels_num(); row_idx += 1){
			for(unsigned col_idx = 0; col_idx < row_idx; col_idx += 1){
				auto csd = csd_collection.csd_get(row_idx, col_idx);
				stream.serial_read_arrayLE((typename TCarrier::TNum *)csd.begin(), (typename TCarrier::TNum *)csd.end());
			}
		}

		return csd_collection;
	}
};

}; //~namespace JStream

#endif //H_JSTREAM_CSD_SHUTTLE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
