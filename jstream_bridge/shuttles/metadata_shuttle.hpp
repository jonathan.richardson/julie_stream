/////
///

#ifndef H_JSTREAM_METADATA_SHUTTLE_H
#define H_JSTREAM_METADATA_SHUTTLE_H

#include<cstring>

#include <sdt/serialization/json_schemas.hpp>
#include <sdt/serialization/json_helpers.hpp>

#include "data_sources/data_source_base.hpp"

namespace JStream {


/////
///  Contains the serialization methods for the DataSourceSettings type
///  (Used internally by SDT)
struct MetadataShuttle
{
    typedef IDataSource::DataSourceSettings TCarrier;

    SDT::StringShuttle<>    string_shuttle;
    
    /////
    ///  Constructors
	MetadataShuttle(){}
    
    /// Verifies that JSON headers are consistent between client and server
    MetadataShuttle(Json::Value &sink_grammar)
    {
        Json::Value source_grammar = json_schema();
        if(source_grammar != sink_grammar){
            std::cout << "Client/server version incompatibility!\n";
        }
        assert(source_grammar == sink_grammar);
    }

    /////
    ///  Returns the JSON header for the supported settings container type
    inline Json::Value json_schema()
    {
        Json::Value grammar;
        grammar["transmission_type"] = "CSDMetadata";
		Json::Value &header = grammar["header"];
        json_object_field(header, "version", std::atof(JSTREAM_VERSION));
        return grammar;
    }

    /////
    ///  Serially writes the TCarrier object to the tranmission stream
    template<typename TSerialWriter>
    inline void stream_write(TSerialWriter &stream, const TCarrier &settings)
    {
        /// Serially write the metadata to the stream    
        string_shuttle.stream_write<TSerialWriter>(                 //ADC MAX name
            stream, 
            std::string(
                settings.adc_name, 
                settings.adc_name + STR_LEN
            )
        );                                                      
        string_shuttle.stream_write<TSerialWriter>(                 //ADC type
            stream, 
            std::string(
                settings.adc_type, 
                settings.adc_type + STR_LEN
            )
        );    
        string_shuttle.stream_write<TSerialWriter>(                 //GPS phase-lock loop state
            stream, 
            std::string(
                settings.pll_status, 
                settings.pll_status + STR_LEN
            )
        );
        stream.serial_writeLE(double(settings.sample_freq));	    //Sampling frequency (Hz)
        stream.serial_writeLE(double(settings.input_range));	    //Input signal range (V)
        stream.serial_writeLE(double(settings.dc_offset));	        //DC offset (V)
        stream.serial_writeLE(double(settings.scaling));	        //Scaling factor for input signal
        stream.serial_writeLE(double(settings.bandwidth)); 	        //Maximum frequency (bandwidth, Hz)
        stream.serial_writeLE(double(settings.impedance)); 	        //Channel impedance	
        stream.serial_writeLE(int(settings.coupling));	            //Channel coupling mode        
    }

    /////
    ///  Reassembles the CSDCarrierDouble  container from the binary network stream
    template<typename TSerialReader>
	inline TCarrier stream_read(TSerialReader &stream)
    {
        TCarrier    settings;
        std::string str;
            
        /// Serially read the metadata from the stream
        /// (NOTE: Data must be read from the binary stream in the same order that they were added)
        str = string_shuttle.stream_read<TSerialReader>(stream);    //ADC MAX name
        std::strncpy(
            settings.adc_name,
            str.c_str(),
            str.size() < STR_LEN ? str.size() : STR_LEN
        );                                                  
        str = string_shuttle.stream_read<TSerialReader>(stream);    //ADC type   
        std::strncpy(
            settings.adc_type,
            str.c_str(),
            str.size() < STR_LEN ? str.size() : STR_LEN
        );                                                     
        str = string_shuttle.stream_read<TSerialReader>(stream);    //GPS phase-lock loop state
        std::strncpy(
            settings.pll_status,
            str.c_str(),
            str.size() < STR_LEN ? str.size() : STR_LEN
        );                                                        
        stream.serial_readLE(settings.sample_freq);	                //Sampling frequency (Hz)
        stream.serial_readLE(settings.input_range);	                //Input signal range (V)
        stream.serial_readLE(settings.dc_offset);	                //DC offset (V)
        stream.serial_readLE(settings.scaling);	                    //Scaling factor for input signal
        stream.serial_readLE(settings.bandwidth); 	                //Maximum frequency (bandwidth, Hz)
        stream.serial_readLE(settings.impedance); 	                //Channel impedance	
        stream.serial_readLE(settings.coupling);	                //Channel coupling mode          

		return settings;
	}
};

}; //~namespace JStream

#endif //H_JSTREAM_METADATA_SHUTTLE_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
