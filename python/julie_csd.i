// Refer to this site for reference:
// http://docs.scipy.org/doc/numpy/reference/swig.interface-file.html#other-common-types-complex
// http://www.scipy.org/Cookbook/SWIG_NumPy_examples

%include "jstream/config.hpp";
#if JSTREAM_USES_POSIX
    #define SWIGWORDSIZE64
#endif

%define DOCSTRING
"""
    The CSDCarrier classes contain data about the auto-correlation and cross-
    correlation of signals.
"""
%enddef
%module(docstring=DOCSTRING) julie_csd

%feature("autodoc", "1");

%include "exception.i";
%include "std_shared_ptr.i";
%include "stdint.i"
%include "std_string.i"
%include "std_complex.i"
%include "std_vector.i"

%{
    #include <vector>
    #include <complex>

    #define SWIG_FILE_WITH_INIT
    
    #include "jstream_bridge/realtime/realtime_client.hpp"
    #include "jstream_bridge/database/database_client.hpp"
    #include "utilities/range.hpp"
    #include "analysis/csd_carrier_float.hpp"
    #include "analysis/csd_carrier_double.hpp"
    #include "queues/csd_accumulation_queue.hpp"
    #include "sdt/utilities/logger_impl.hpp"
    #include "python/python_help.hpp"

    using namespace JStream;    
    
    void set_verbosity(int level){
        uLogger::logrouter_root()->settings(level);
    }
    
    // Wrapper class for Python-callable C++ string handler
    class PyBoolFunc : public PyCallback
    {
    public:
        PyBoolFunc(PyObject *callback) :
            PyCallback(callback)
        {
        }

        void operator()(bool b)
        {
            PyGILGuard py_guard;
            
            PyObject *result = PyObject_CallFunction(
                this->py_func(),
                const_cast<char *>("b"),
                uint8_t(b)
            );
            return;
        }
    };    

    // Wrapper class for Python-callable C++ string handler
    class PyStrFunc : public PyCallback
    {
    public:
        PyStrFunc(PyObject *callback) :
            PyCallback(callback)
        {
        }

        void operator()(const std::string& s)
        {
            PyGILGuard py_guard;
            
            PyObject *result = PyObject_CallFunction(
                this->py_func(),
                const_cast<char *>("s"),
                const_cast<char *>(s.c_str())
            );
            return;
        }
    };    
%}

void set_verbosity(int level);

%include "numpy.i"
%init %{
    import_array();
%}

%template() std::vector<std::string>;
%template() std::vector<uint64_t>;
%template() std::vector<std::vector<uint64_t> >;

%exception{
    try{
        $action
    } catch(SDT::Disconnected &err){
        SWIG_exception(SWIG_IOError, "Disconnected");
        return NULL;
    } catch(std::runtime_error &err){
        SWIG_exception(SWIG_ValueError, err.what());
        return NULL;
    }
}

/////
///  Wrap the single-precision CSD container subclass

// PSD C++ to numpy mapping
%apply (float* INPLACE_ARRAY1, int DIM1) {(float* inData, int length)};
%apply (float** ARGOUTVIEW_ARRAY1, int* DIM1) {(float** outData, int* length)};

// CSD C++ to numpy mapping
%numpy_typemaps(std::complex<float>, NPY_CFLOAT, int)
%apply (std::complex<float>* INPLACE_ARRAY1, int DIM1) {(std::complex<float>* inData, int length)};
%apply (std::complex<float>** ARGOUTVIEW_ARRAY1, int* DIM1) {(std::complex<float>** outData, int* length)};

// First wrap the single-precision instantiation of the base class
%shared_ptr(JStream::CSDCarrierBase<float>);
%include "analysis/csd_carrier_base.hpp"
%template(CSDFloat) JStream::CSDCarrierBase<float>;

// Wrap the derived class
%shared_ptr(JStream::CSDCarrierFloat);
%include "analysis/csd_carrier_float.hpp"


/////
///  Wrap the double-precision CSD container subclass

// PSD C++ to numpy mapping
%apply (double* INPLACE_ARRAY1, int DIM1) {(double* inData, int length)};
%apply (double** ARGOUTVIEW_ARRAY1, int* DIM1) {(double** outData, int* length)};

// CSD C++ to numpy mapping
%numpy_typemaps(std::complex<double>, NPY_CDOUBLE, int)
%apply (std::complex<double>* INPLACE_ARRAY1, int DIM1) {(std::complex<double>* inData, int length)};
%apply (std::complex<double>** ARGOUTVIEW_ARRAY1, int* DIM1) {(std::complex<double>** outData, int* length)};

// First wrap the double-precision instantiation of the base class
%shared_ptr(JStream::CSDCarrierBase<double>);
%include "analysis/csd_carrier_base.hpp"
%template(CSDDouble) JStream::CSDCarrierBase<double>;

// Wrap the derived class
%shared_ptr(JStream::CSDCarrierDouble);
%include "analysis/csd_carrier_double.hpp"


/////
///  Wrap the TCP client classes
///  (Note: This must come AFTER the container class wrappings)
%include "jstream_bridge/database/database_client.hpp"
%include "jstream_bridge/realtime/realtime_client.hpp"

// Add set-Python-callback methods
%extend JStream::RealtimeClient {
    void message_handler_set(PyObject *callback){
        $self->message_handler_set(PyStrFunc(callback));
    }

    void connection_handler_set(PyObject *callback){
        $self->connection_handler_set(PyBoolFunc(callback));
    }
    
    void run_status_handler_set(PyObject *callback){
        $self->run_status_handler_set(PyBoolFunc(callback));
    }    
}
