
MACRO(find_python_module module)
    STRING(TOUPPER ${module} module_upper)
    IF(NOT PY_${module_upper})
        IF(ARGC GREATER 1 AND ARGV1 STREQUAL "REQUIRED")
            SET(${module}_FIND_REQUIRED TRUE)
        ENDIF()
        # A module's location is usually a directory, but for binary modules
        # it's a .so file.
        EXECUTE_PROCESS(COMMAND 
            "${PYTHON_EXECUTABLE}" "-c" "import re, ${module}; print(re.compile('/${module}/__init__.py.*').sub('',${module}.__file__))"
            RESULT_VARIABLE _${module}_status 
            OUTPUT_VARIABLE _${module}_location
            ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
        IF(NOT _${module}_status)
            SET(PY_${module_upper} ${_${module}_location} CACHE STRING 
                "Location of Python module ${module}")
        ENDIF(NOT _${module}_status)
    ENDIF(NOT PY_${module_upper})
    FIND_PACKAGE_HANDLE_STANDARD_ARGS(PY_${module} DEFAULT_MSG PY_${module_upper})
ENDMACRO(find_python_module)

