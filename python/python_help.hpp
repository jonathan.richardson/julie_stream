/////
///
///

#ifndef H_PYTHON_HELP_H
#define H_PYTHON_HELP_H

namespace JStream {

class PyGILGuard
{
    PyGILState_STATE m_gstate;
public:
    inline PyGILGuard()
    {
        m_gstate = PyGILState_Ensure();
    }

    PyGILGuard(const PyGILGuard &) = delete;
    
    inline ~PyGILGuard()
    {
        PyGILState_Release(m_gstate);
    }

};

class PyCallback
{
    PyObject *m_func;
public:
    inline PyCallback(const PyCallback& o) : m_func(o.m_func) 
    {
        Py_INCREF(m_func);
    }
    inline PyCallback(PyObject *func) : m_func(func) 
    {
        if (!func || Py_None == m_func || !PyCallable_Check(m_func))
            throw std::runtime_error("bad python function");
        Py_INCREF(this->m_func);
    }
    inline PyCallback& operator=(const PyCallback&) = delete; // Not allowed
    ~PyCallback() 
    {
        Py_DECREF(m_func);
    }
protected:
    inline PyObject *py_func()
    {
        return m_func;
    }
};


}; //~namespace JStream

#endif //H_PYTHON_HELP_H
//doxygen vim:set comments=sl\://///,m0\:///,ex\://! :
