#!${PYTHON_EXECUTABLE}
import sys
import os
sys.path.append(os.path.join("${PROJECT_BINARY_DIR}","python"))
import julie_csd
import unittest
import numpy as np

nChannels = 4
length = 65536
class TestCSDAccumulation(unittest.TestCase):
    """
    Test interfaces to CSDCarrierDoublePrec defined in
    analysis/csd_carrier.hpp 
    """

    def test_csd(self):
        """
        set and get all members of CSDCarrierDoublePrec and test that all is well
        """
        csd = julie_csd.CSDCarrierDoublePrec(nChannels, length)
        self.assertEquals(nChannels, csd.channels_num())

        frameId = 12345
        csd.sequence_number_set(frameId)
        self.assertEquals(frameId, csd.sequence_number_get());

        accumulation = 54321
        csd.accumulation_set(accumulation)
        self.assertEquals(accumulation, csd.accumulation_get())

        norm = 1.23456
        csd.normalization_to_ps_set(norm)
        self.assertEquals(norm, csd.normalization_to_ps_get())

        enbw = 9.876
        csd.ENBW_set(enbw)
        self.assertEquals(enbw, csd.ENBW_get())

        timestampBegin = 987654321
        timestampEnd   = 987656789
        csd.timestamps_set(timestampBegin,timestampEnd)
        self.assertEquals(timestampBegin, csd.timestamp_begin())
        self.assertEquals(timestampEnd,   csd.timestamp_end())
        
        for iChannel in range(nChannels):
            inValues = np.zeros(length, dtype=np.float64)
            for i in range(length):
                inValues[i] += iChannel + 0.00001*i
            csd.psd_setValues(inValues, iChannel)

            outValues = csd.psd_getValues(iChannel)

            for i in range(length):
                self.assertAlmostEqual(
                    outValues[i],iChannel + 0.00001*i,5)

        for iRow in range(nChannels):
            for iCol in range(iRow):
                inReal = np.zeros(length, dtype=np.float64)
                inImag = np.zeros(length, dtype=np.float64)
                for i in range(length):
                    inReal[i] += 10*iRow + iCol
                    inImag[i] += 0.00001*i
                inComplex = inReal + 1j*inImag
                csd.csd_setValues(inComplex, iRow, iCol)

        for iRow in range(nChannels):
            for iCol in range(iRow):
                outComplex = csd.csd_getValues(iRow, iCol)
                for i in range(length):
                    self.assertAlmostEqual(
                        outComplex[i],10*iRow + iCol + 1j*(0.00001*i),5)

if __name__ == '__main__':
    unittest.main()
